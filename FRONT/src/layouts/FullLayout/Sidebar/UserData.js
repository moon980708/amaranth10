import DashboardOutlinedIcon from "@mui/icons-material/DashboardOutlined";
import AddToPhotosOutlinedIcon from "@mui/icons-material/AddToPhotosOutlined";
import AspectRatioOutlinedIcon from "@mui/icons-material/AspectRatioOutlined";
import AssignmentTurnedInOutlinedIcon from "@mui/icons-material/AssignmentTurnedInOutlined";
import AlbumOutlinedIcon from "@mui/icons-material/AlbumOutlined";
import SwitchCameraOutlinedIcon from "@mui/icons-material/SwitchCameraOutlined";
import SwitchLeftOutlinedIcon from "@mui/icons-material/SwitchLeftOutlined";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import AutoAwesomeMosaicOutlinedIcon from "@mui/icons-material/AutoAwesomeMosaicOutlined";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import ReceiptLongIcon from "@mui/icons-material/ReceiptLong";
import InventoryOutlinedIcon from "@mui/icons-material/InventoryOutlined";

const UserData = [
  {
    title: "사용자마이페이지",
    icon: ManageAccountsIcon,
    href: "/user/userinfo",
  },
  {
    title: "사용자 지출정보등록",
    icon: ReceiptLongIcon,
    href: "/user/userchargetable",
  },
  {
    title: "사용자 청구내역조회",
    icon: InventoryOutlinedIcon,
    href: "/user/chargelog",
  },
];

export default UserData;
