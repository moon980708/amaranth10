import React from "react";
import { useLocation } from "react-router";
import { Link, NavLink } from "react-router-dom";
import {
  Box,
  Drawer,
  useMediaQuery,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@mui/material";
import ButtonGroup from "@mui/material/ButtonGroup";
import Button from "@mui/material/Button";
import { SidebarWidth } from "../../../assets/global/Theme-variable";
import AdminData from "./AdminData";
import DouzoneImg from "../../../assets/images/DouzoneImg.png";
import YouTubeIcon from "@mui/icons-material/YouTube";
import WebIcon from "@mui/icons-material/Web";
import Swal from "sweetalert2";
import { useDispatch, useSelector } from "react-redux";

const AdminSidebar = (props) => {
  const [open, setOpen] = React.useState(true);
  const { pathname } = useLocation();
  const pathDirect = pathname;
  const lgUp = useMediaQuery((theme) => theme.breakpoints.up("lg"));
  const isSidebarOpen = lgUp ? props.isSidebarOpen : props.isMobileSidebarOpen;
  const userDept = useSelector((state) => state.loginReducer.loginUserDept);

  const handleClick = (index) => {
    if (open === index) {
      setOpen((prevOpen) => !prevOpen);
    } else {
      setOpen(index);
    }
  };

  const SidebarContent = (
    <Box
      sx={{
        p: 3,
        height: "calc(100vh - 40px)",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Link to="/admin/main">
        <Box sx={{ display: "flex", alignItems: "Center" }}>
          <img src={DouzoneImg} width={"170px"} />
        </Box>
      </Link>

      <Box>
        <List
          sx={{
            mt: 4,
          }}
        >
          {AdminData.map((item, index) => {
            //{/********SubHeader**********/}

            if (item.subheader) {
              //여기 작성
              return (
                <ListItem
                  key={`subheader-${index}`}
                  sx={{
                    padding: 0,
                  }}
                >
                  <ListItemText
                    primary={
                      <span style={{ fontSize: "13px" }}>{item.subheader}</span>
                    }
                  />
                </ListItem>
              );
            } else {
              return (
                <List component="li" disablePadding key={item.title}>
                  <ListItem
                    onClick={() => handleClick(index)}
                    // onClick={Swal.close()}
                    disabled={userDept !== "관리부" && item.option === true ? true : false}
                    button
                    component={NavLink}
                    to={item.href}
                    selected={pathDirect === item.href}
                    sx={{
                      mb: 1,
                      ...(pathDirect === item.href && {
                        color: "white",
                        backgroundColor: (theme) =>
                          `${theme.palette.primary.main}!important`,
                      }),
                    }}
                  >
                    <ListItemIcon
                      sx={{
                        ...(pathDirect === item.href && { color: "white" }),
                      }}
                    >
                      <item.icon width="20" height="20" />
                    </ListItemIcon>
                    <ListItemText>{item.title}</ListItemText>
                  </ListItem>
                </List>
              );
            }
          })}
        </List>
      </Box>

      <Box
        sx={{
          background: "aliceblue",
          borderRadius: "10px",
          padding: "10px 10px 25px 10px",
          
        }}
      >
        <Box>
          <Typography
            sx={{
              color: (theme) => `${theme.palette.primary.main}`,
              fontSize: "20px",
              padding: 2,
            }}
          >
            <strong>DOUZONE</strong>
          </Typography>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "space-evenly",
            }}
          >
            <Button
              component="a"
              href="https://www.youtube.com/@douzoneict"
              target="_blank"
              rel="noopener noreferrer"
              sx={{ background: "white" }}
              variant="outlined"
              startIcon={<YouTubeIcon />}
            >
              Youtube
            </Button>
            <Button
              component="a"
              href="http://www.douzone.com/main/index.jsp"
              target="_blank"
              rel="noopener noreferrer"
              sx={{ background: "white" }}
              variant="outlined"
              startIcon={<WebIcon />}
            >
              Official
            </Button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
  if (lgUp) {
    return (
      <Drawer
        anchor="left"
        open={props.isSidebarOpen}
        variant="persistent"
        PaperProps={{
          sx: {
            width: SidebarWidth,
            zIndex: 1000,
          },
        }}
      >
        {SidebarContent}
      </Drawer>
    );
  }
  return (
    //얘가 없으면 창이 작을때 햄버거 버튼 동작 안함
    <Drawer
      anchor="left"
      open={props.isMobileSidebarOpen}
      onClose={props.onSidebarClose}
      PaperProps={{
        sx: {
          width: SidebarWidth,
          zIndex: 1000,
        },
      }}
      variant="temporary"
    >
      {SidebarContent}
    </Drawer>
  );
};

export default AdminSidebar;
