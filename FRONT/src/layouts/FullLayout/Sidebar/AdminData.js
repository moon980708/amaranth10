import DashboardOutlinedIcon from "@mui/icons-material/DashboardOutlined";
import AddToPhotosOutlinedIcon from "@mui/icons-material/AddToPhotosOutlined";
import AspectRatioOutlinedIcon from "@mui/icons-material/AspectRatioOutlined";
import AssignmentTurnedInOutlinedIcon from "@mui/icons-material/AssignmentTurnedInOutlined";
import AlbumOutlinedIcon from "@mui/icons-material/AlbumOutlined";
import SwitchCameraOutlinedIcon from "@mui/icons-material/SwitchCameraOutlined";
import SwitchLeftOutlinedIcon from "@mui/icons-material/SwitchLeftOutlined";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import AutoAwesomeMosaicOutlinedIcon from "@mui/icons-material/AutoAwesomeMosaicOutlined";
import PaymentIcon from "@mui/icons-material/Payment";
import EventNoteIcon from "@mui/icons-material/EventNote";
import AddCommentOutlinedIcon from "@mui/icons-material/AddCommentOutlined";
import AccountTreeOutlinedIcon from "@mui/icons-material/AccountTreeOutlined";
import GroupsIcon from "@mui/icons-material/Groups";
import BadgeOutlinedIcon from "@mui/icons-material/BadgeOutlined";
import InsertChartOutlinedOutlinedIcon from "@mui/icons-material/InsertChartOutlinedOutlined";

const AdminData = [
  {
    title: "대시보드",
    icon: InsertChartOutlinedOutlinedIcon,
    href: "/admin/main",
    option: false,
  },
  {
    title: "경비청구내역 관리",
    icon: PaymentIcon,
    href: "/admin/chargemanage",
    option: false,
  },
  {
    navlabel: true,
    subheader: "스케줄 관리",
  },
  {
    title: "차수 관리",
    icon: EventNoteIcon,
    href: "/admin/schedulesetting",
    option: true,
  },
  {
    navlabel: true,
    subheader: "용도 관리",
  },
  {
    title: "용도-항목 생성",
    icon: AddCommentOutlinedIcon,
    href: "/admin/createcategoryanditem",
    option: true,
  },
  {
    title: "용도-항목 매칭",
    icon: AccountTreeOutlinedIcon,
    href: "/admin/matching",
    option: true,
  },
  {
    navlabel: true,
    subheader: "사원 관리",
  },
  {
    title: "경비취합그룹 관리",
    icon: GroupsIcon,
    href: "/admin/group",
    option: true,
  },

  {
    title: "사원관리",
    icon: BadgeOutlinedIcon,
    href: "/admin/usermanage",
    option: true,
  },
];

export default AdminData;
