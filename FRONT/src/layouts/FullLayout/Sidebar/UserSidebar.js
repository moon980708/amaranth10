import React from "react";
import { useLocation } from "react-router";
import { Link, NavLink } from "react-router-dom";
import {
  Box,
  Drawer,
  useMediaQuery,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Avatar,
  Button,
} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import SkipPreviousIcon from "@mui/icons-material/SkipPrevious";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import SkipNextIcon from "@mui/icons-material/SkipNext";
import { SidebarWidth } from "../../../assets/global/Theme-variable";
import UserData from "./UserData";
import DouzoneImg from "../../../assets/images/DouzoneImg.png";
import img1 from "../../../assets/images/avatar/1.png"
import img2 from "../../../assets/images/avatar/2.png";
import img3 from "../../../assets/images/avatar/3.png";
import img4 from "../../../assets/images/avatar/4.png";
import img5 from "../../../assets/images/avatar/5.png";
import img6 from "../../../assets/images/avatar/6.png";
import img7 from "../../../assets/images/avatar/7.png";
import img8 from "../../../assets/images/avatar/8.png";
import YouTubeIcon from "@mui/icons-material/YouTube";
import WebIcon from "@mui/icons-material/Web";
import { useDispatch, useSelector } from "react-redux";

const AdminSidebar = (props) => {
  const [open, setOpen] = React.useState(true);
  const { pathname } = useLocation();
  const pathDirect = pathname;
  const lgUp = useMediaQuery((theme) => theme.breakpoints.up("lg"));
  const userName = useSelector((state) => state.loginReducer.loginUserName);
  const userDept = useSelector((state) => state.loginReducer.loginUserDept);
  const userRank = useSelector((state) => state.loginReducer.loginUserRank);


    // 직급별로 아바타 설정
    const getAvatarImage = (userRank) => {
      switch (userRank) {
        case "사원":
          return img1;
        case "대리":
          return img2;
        case "과장":
          return img3;
        case "차장":
          return img5;
        case "부장":
          return img6;
        case "이사":
          return img7;
        case "상무":
          return img8;
        case "전무":
          return img4;
        default:
          return null;
      }
    };

  const handleClick = (index) => {
    if (open === index) {
      setOpen((prevopen) => !prevopen);
    } else {
      setOpen(index);
    }
  };

  const SidebarContent = (
    <Box
      sx={{
        p: 3,
        height: "calc(100vh - 40px)",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Link to="/user/userinfo">
        <Box sx={{ display: "flex", alignItems: "Center" }}>
          <img src={DouzoneImg} width={"170px"} />
        </Box>
      </Link>

      <Box
        sx={{
          padding: "15px",
          borderRadius: "15px",
          display: "flex",
          marginTop: "35px",
          marginBottom: "15px",
          background: "#eee",
        }}
      > 
        <Avatar
          src={getAvatarImage(userRank)}
          alt={getAvatarImage(userRank)}
          sx={{
            width: "130px",
            height: "130px",
          }}
        />
        <Box
          sx={{ display: "flex", flexDirection: "column", marginTop: "20px" }}
        >
          <Typography component="div" variant="h5" sx={{ textAlign: "center" }}>
            {userName}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
            fontSize={"13px"}
            sx={{ marginTop: "15px", textAlign: "center" }}
          >
            {userDept}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
            fontSize={"13px"}
            sx={{ textAlign: "center" }}
          >
            {userRank}
          </Typography>
        </Box>
      </Box>

      <Box>
        <List
          sx={{
            mt: 1,
            mb: 7,
          }}
        >
          {UserData.map((item, index) => {
            //{/********SubHeader**********/}

            return (
              <List component="li" disablePadding key={item.title}>
                <ListItem
                  onClick={() => handleClick(index)}
                  button
                  component={NavLink}
                  to={item.href}
                  selected={pathDirect === item.href}
                  sx={{
                    mb: 1,
                    ...(pathDirect === item.href && {
                      color: "white",
                      backgroundColor: (theme) =>
                        `${theme.palette.primary.main}!important`,
                    }),
                  }}
                >
                  <ListItemIcon
                    sx={{
                      ...(pathDirect === item.href && { color: "white" }),
                    }}
                  >
                    <item.icon width="20" height="20" />
                  </ListItemIcon>
                  <ListItemText>{item.title}</ListItemText>
                </ListItem>
              </List>
            );
          })}
        </List>
      </Box>

      <Box
        sx={{
          background: "aliceblue",
          borderRadius: "10px",
          padding: "10px 10px 25px 10px",
        }}
      >
        <Box>
          <Typography
            sx={{
              color: (theme) => `${theme.palette.primary.main}`,
              fontSize: "20px",
              padding: 2,
            }}
          >
            <strong>DOUZONE</strong>
          </Typography>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "space-evenly",
            }}
          >
            <Button
              component="a"
              href="https://www.youtube.com/@douzoneict"
              target="_blank"
              rel="noopener noreferrer"
              sx={{ background: "white" }}
              variant="outlined"
              startIcon={<YouTubeIcon />}
            >
              Youtube
            </Button>
            <Button
              component="a"
              href="http://www.douzone.com/main/index.jsp"
              target="_blank"
              rel="noopener noreferrer"
              sx={{ background: "white" }}
              variant="outlined"
              startIcon={<WebIcon />}
            >
              Official
            </Button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
  if (lgUp) {
    return (
      <Drawer
        anchor="left"
        open={props.isSidebarOpen}
        variant="persistent"
        PaperProps={{
          sx: {
            width: SidebarWidth,
            zIndex: 1000,
          },
        }}
      >
        {SidebarContent}
      </Drawer>
    );
  }
  return (
    <Drawer
      anchor="left"
      open={props.isMobileSidebarOpen}
      onClose={props.onSidebarClose}
      PaperProps={{
        sx: {
          width: SidebarWidth,
          zIndex: 1000,
        },
      }}
      variant="temporary"
    >
      {SidebarContent}
    </Drawer>
  );
};

export default AdminSidebar;
