import DashboardOutlinedIcon from "@mui/icons-material/DashboardOutlined";
import AddToPhotosOutlinedIcon from "@mui/icons-material/AddToPhotosOutlined";
import AspectRatioOutlinedIcon from "@mui/icons-material/AspectRatioOutlined";
import AssignmentTurnedInOutlinedIcon from "@mui/icons-material/AssignmentTurnedInOutlined";
import AlbumOutlinedIcon from "@mui/icons-material/AlbumOutlined";
import SwitchCameraOutlinedIcon from "@mui/icons-material/SwitchCameraOutlined";
import SwitchLeftOutlinedIcon from "@mui/icons-material/SwitchLeftOutlined";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import AutoAwesomeMosaicOutlinedIcon from "@mui/icons-material/AutoAwesomeMosaicOutlined";

const ManagerData = [
  {
    title: "관리자페이지",
    icon: DashboardOutlinedIcon,
    href: "/admin/main",
  },
  {
    title: "경비청구내역 관리",
    icon: AssignmentTurnedInOutlinedIcon,
    href: "/admin/chargemanage",
  }
];

export default ManagerData;
