import React from "react";
//import { Link } from 'react-router-dom';

import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import NotificationsNoneOutlinedIcon from "@mui/icons-material/NotificationsNoneOutlined";
import AddToPhotosOutlinedIcon from "@mui/icons-material/AddToPhotosOutlined";
import PersonAddOutlinedIcon from "@mui/icons-material/PersonAddOutlined";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import LogoutOutlinedIcon from "@mui/icons-material/LogoutOutlined";
import DriveFileRenameOutlineOutlinedIcon from '@mui/icons-material/DriveFileRenameOutlineOutlined';
import QueryStatsOutlinedIcon from '@mui/icons-material/QueryStatsOutlined';
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";

import {
  AppBar,
  Box,
  IconButton,
  Toolbar,
  Menu,
  MenuItem,
  Button,
  Avatar,
  Divider,
  ListItemIcon,
  Typography,
} from "@mui/material";
import img1 from "../../../assets/images/avatar/1.png"
import img2 from "../../../assets/images/avatar/2.png";
import img3 from "../../../assets/images/avatar/3.png";
import img4 from "../../../assets/images/avatar/4.png";
import img5 from "../../../assets/images/avatar/5.png";
import img6 from "../../../assets/images/avatar/6.png";
import img7 from "../../../assets/images/avatar/7.png";
import img8 from "../../../assets/images/avatar/8.png";
import { reset_CheckGroupManager, reset_LoginUserDept, reset_LoginUserId, reset_LoginUserName, reset_LoginUserRank } from '../../../reducer/module/loginReducer';

const Header = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  // 4
  const [anchorEl4, setAnchorEl4] = React.useState(null);

  const handleClick4 = (event) => {
    setAnchorEl4(event.currentTarget);
  };

  const handleClose4 = () => {
    setAnchorEl4(null);
  };

  const handleClickMain = () => {
    setAnchorEl4(null);
    navigate("/admin/main");
  };
  

  const handleClickCharge = () => {
    setAnchorEl4(null);
    navigate("/user/userinfo");
  };

  const navigate = useNavigate();

  const dispatch = useDispatch();

  // 직급별로 아바타 설정
  const getAvatarImage = (userRank) => {
    switch (userRank) {
      case "사원":
        return img1;
      case "대리":
        return img2;
      case "과장":
        return img3;
      case "차장":
        return img5;
      case "부장":
        return img6;
      case "이사":
        return img7;
      case "상무":
        return img8;
      case "전무":
        return img4;
      default:
        return null;
    }
  };

  const handelClickLogoutBtn = ()=>{
    axios
      .post("/logout")
      .then((result) => {
        console.log("로그아웃 성공");
        dispatch(reset_LoginUserId());
        dispatch(reset_LoginUserName());
        dispatch(reset_LoginUserDept());
        dispatch(reset_LoginUserRank());
        dispatch(reset_CheckGroupManager());
        
        if(result.data === 'logout'){
          navigate("/");
        }
      })
      .catch(() => {
        console.log("로그아웃 실패");
      });
      setAnchorEl4(null);
      
  }

  // 5
  const [anchorEl5, setAnchorEl5] = React.useState(null);

  const handleClick5 = (event) => {
    setAnchorEl5(event.currentTarget);
  };

  const handleClose5 = () => {
    setAnchorEl5(null);
  };

  const userName = useSelector((state) => state.loginReducer.loginUserName);
  const userRank = useSelector((state) => state.loginReducer.loginUserRank);
  // console.log("userName : " + userName);

  return (
    <AppBar sx={props.sx} elevation={0} className={props.customClass}>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="menu"
          onClick={props.toggleMobileSidebar}
          sx={{
            display: {
              lg: "none",
              xs: "inline",
            },
          }}
        >
          <MenuOutlinedIcon width="20" height="20" />
        </IconButton>

        <Box flexGrow={1} />
        <Typography>{userName}님 환영합니다</Typography>
        <Box
          sx={{
            width: "1px",
            backgroundColor: "rgba(0,0,0,0.1)",
            height: "25px",
            ml: 1,
          }}
        ></Box>
        <Button
          aria-label="menu"
          color="inherit"
          aria-controls="profile-menu"
          aria-haspopup="true"
          onClick={handleClick4}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            {/* 로그인한 사용자의 이미지 */}
            <Avatar
              src={getAvatarImage(userRank)}
              alt={getAvatarImage(userRank)}
              sx={{
                width: "30px",
                height: "30px",
              }}
            />
          </Box>
        </Button>

        <Menu
          id="profile-menu"
          anchorEl={anchorEl4}
          keepMounted
          open={Boolean(anchorEl4)}
          onClose={handleClose4}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
          transformOrigin={{ horizontal: "right", vertical: "top" }}
          sx={{
            "& .MuiMenu-paper": {
              width: "250px",
              right: 0,
              top: "70px !important",
            },

          }}
        >
          <MenuItem onClick={handleClickMain}>
          <ListItemIcon>
              <QueryStatsOutlinedIcon fontSize="small" />
            </ListItemIcon>
              메인페이지로
          </MenuItem>
          <Divider />
          <MenuItem onClick={handleClickCharge}>
          <ListItemIcon>
              <DriveFileRenameOutlineOutlinedIcon fontSize="small" />
            </ListItemIcon>
            사용자화면으로
          </MenuItem>
          <Divider />
          <MenuItem onClick={handelClickLogoutBtn}>
            <ListItemIcon>
              <LogoutOutlinedIcon fontSize="small" />
            </ListItemIcon>
            로그아웃
          </MenuItem>
        </Menu>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
