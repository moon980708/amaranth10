import { lazy, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import ManagerLayout from '../layouts/FullLayout/ManagerLayout.js';
/****Layouts*****/
const FullLayout = lazy(() => import("../layouts/FullLayout/FullLayout.js"));
const UserLayout = lazy(()=>import("../layouts/FullLayout/UserLayout.js"));

/*****Pages******/
const AdminMain = lazy(() => import("../pages/admin/AdminMain.js"));
const MyInfo = lazy(() => import("../pages/user/MyInfo.js"));
const BasicChargeTable = lazy(() =>
  import("../pages/user/BasicChargeTable.js")
);
const BasicChargeLogTable = lazy(() =>
  import("../pages/user/BasicChargeLogTable.js")
);

const ScheduleSetting = lazy(() =>
  import("../pages/admin/ScheduleSetting.js")
);
const Matching = lazy(() => import("../pages/admin/Matching.js"));
const AdminUserManage = lazy(() =>
  import("../pages/admin/AdminUserManage.js")
);
const Login = lazy(() => import("../pages/Login.js"));

const CreateCategoryAndItem = lazy(() =>
  import("../pages/admin/CreateCategoryAndItem.js")
);
const GroupDashboard = lazy(() =>
  import("../pages/admin/GroupDashboard.js")
);
const ChargeManage = lazy(() => import("../pages/admin/ChargeManage.js"));


/*****Routes******/
// const RedirectToLogin = () =>{
//   const navigate = useNavigate();
//   useEffect(()=>{
//     navigate("/login");
//   },[navigate])

//   return null;
// }

const ThemeRoutes = [
  {
    path: "/",
    element: <Login/>
  },
  {
    path: "admin",
    element: <FullLayout />,
    children: [
      { path: "main", exact: true, element: <AdminMain /> },   //관리자 메인페이지
      { path: "schedulesetting", element: <ScheduleSetting /> }, //차수설정
      { path: "matching", element: <Matching /> }, //매칭
      { path: "usermanage", element: <AdminUserManage /> }, //관리자사원관리

      { path: "createcategoryanditem", element: <CreateCategoryAndItem /> }, //용도항목생성
      { path: "chargemanage", element: <ChargeManage /> },      //청구내역 승인/반려
      { path: "group", element: <GroupDashboard /> }       //취합그룹 생성/관리
    ],
  },
  {
    path: "user",
    element: <UserLayout />,
    children: [
      { path: "chargelog", element: <BasicChargeLogTable /> }, //청구내역조회
      { path: "userchargetable", element: <BasicChargeTable /> }, //사용자지출정보등록
      { path: "userinfo", element: <MyInfo /> }, //사용자 마이페이지
    ],
  },
  {
    path: "manager",
    element: <ManagerLayout/>,
    children : [
      {path : "managermain", element : <AdminMain/>},
      {path: "chargemanage", element : <ChargeManage/>}
    ]
  }
];

export default ThemeRoutes;

