import React, { useState, useEffect } from "react";
import "./Loading.css";
import LoadingImg from "../assets/images/LoadingImg.png";

const Loading = () => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const interval = setInterval(() => {
      setIsLoading((prevIsLoading) => !prevIsLoading);
    }, 1000); // 1초마다 로딩 상태 변경

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <div className={`moving-image-container ${isLoading ? "loading" : ""}`}>
      <img src={LoadingImg} alt="로딩 이미지" className="moving-image" />
    </div>
  );
};

export default Loading;