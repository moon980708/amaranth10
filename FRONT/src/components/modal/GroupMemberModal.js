import React, { useEffect, useState } from "react";
import {
  Button,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Modal,
  Paper,
  Typography,
  Box,
  Card,
  Checkbox,
  IconButton,
  CircularProgress
} from "@mui/material";
import { TreeView, TreeItem } from "@mui/lab";

import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import axios from 'axios';
import UseApi from '../UseApi';
import { useNavigate } from 'react-router-dom';
axios.defaults.withCredentials = true;


const data = {
  'A부서': [
    { id: "user1", userName: "aaa", rankName: "사원" },
    { id: "user2", userName: "bbb", rankName: "과장" },
    { id: "user3", userName: "ccc", rankName: "대리" },
    { id: "user4", userName: "ddd", rankName: "이사" },
    { id: "user5", userName: "eee", rankName: "사원" }
  ],
  'B부서': [
    { id: "b1", userName: "가영이", rankName: "사원" },
    { id: "b2", userName: "가롱이", rankName: "과장" },
    { id: "b3", userName: "김가영", rankName: "대리" },
    { id: "b4", userName: "가융이", rankName: "이사" },
    { id: "b5", userName: "가옹이", rankName: "사원" },
    { id: "b6", userName: "가잉이", rankName: "사원" },
    { id: "b7", userName: "가엉이", rankName: "과장" },
    { id: "b8", userName: "가용이", rankName: "대리" },
    { id: "b9", userName: "가양이", rankName: "이사" },
    { id: "b10", userName: "귀여미", rankName: "사원" }
  ],
  'C부서': [
    { id: "c1", userName: "연어", rankName: "사원" },
    { id: "c2", userName: "오징어", rankName: "과장" },
    { id: "c3", userName: "돌멍게", rankName: "대리" },
    { id: "c4", userName: "대게", rankName: "이사" },
    { id: "c5", userName: "해삼", rankName: "사원" },
    { id: "c6", userName: "개불", rankName: "사원" },
    { id: "c7", userName: "말미잘", rankName: "과장" },
    { id: "c8", userName: "전복", rankName: "대리" },
    { id: "c9", userName: "광어", rankName: "이사" },
    { id: "c10", userName: "미역", rankName: "사원" }
  ]
};




const GroupMemberModal = ({
  selectedGroupNo,
  mode,
  isOpen,
  handleClose,
  groupMemberModal,
  setGroupMemberModal,
  handleButton
}) => {
  
  const api = UseApi();
  const [includeResign, setIncludeResign] = useState(false);  //퇴사포함체크박스
  const [deptUserList, setDeptUserList] = useState({});      //모달 왼쪽에 띄울 조직도 리스트
  const [selectedEmployees, setSelectedEmployees] = useState([]);      //모달 오른쪽에 띄울 그룹사원리스트
  const [groupMemberList, setGroupMemberList] = useState([]);     //db에서 가져온 그룹사원리스트

  
  const [loadingDept, setLoadingDept] = useState(true);
  const [loadingMember, setLoadingMember] = useState(true);


  const handleEmployeeSelect = (department, employee) => {
    //모달안에서 쓰는 사원리스트
    const employeeIndex = selectedEmployees.findIndex(emp => emp.id === employee.id);

    if (employeeIndex === -1) {  //선택한 리스트에 사원추가
      const uniqueKey = `${department}-${employee.id}`; // 고유한 key 생성
      setSelectedEmployees([...selectedEmployees, {deptName: department, ...employee, key: uniqueKey}]);  
    } else {
      const updatedEmployees = [...selectedEmployees];
      updatedEmployees.splice(employeeIndex, 1);     //선택한 리스트에서 사원삭제
      setSelectedEmployees(updatedEmployees);
    }


    //쿼리를 위한 insert와 delete 리스트
    const memberIndex = groupMemberList.findIndex(emp => emp.id === employee.id);
    
    if (memberIndex === -1) {
      const insertIndex = groupMemberModal.insert.findIndex(id => id === employee.id);

      if (insertIndex === -1) {  //insert배열에 없으면, insert에 추가.
        let newInsert = [...groupMemberModal.insert, employee.id];
        setGroupMemberModal({ ...groupMemberModal, insert: newInsert });
        
      } else {  //insert배열에 없으면, insert에서 빼기
        let updatedInsert = [...groupMemberModal.insert];
        updatedInsert.splice(insertIndex, 1);   
        setGroupMemberModal({ ...groupMemberModal, insert: updatedInsert });
      }
      
    } else {
      const deleteIndex = groupMemberModal.delete.findIndex(id => id === employee.id);

      if (deleteIndex === -1) {  //delete배열에 없으면, delete에 추가.
        let newDelete = [...groupMemberModal.delete, employee.id];
        setGroupMemberModal({ ...groupMemberModal, delete: newDelete });
        
      } else {  //delete배열에 있으면, delete에서 빼기
        let updatedDelete = [...groupMemberModal.delete];
        updatedDelete.splice(deleteIndex, 1);   
        setGroupMemberModal({ ...groupMemberModal, delete: updatedDelete });
      }
    }



  };

  
  const listDeptUser = () => {
    //조직도 불러오기
    setLoadingDept(true);

    api.get("/admin/group/dept/userlist")
      .then((result) => {
        console.log("조직도 : ",result.data);
        let originalData = result.data;

        // 변형된 데이터를 담을 객체
        const data = {};

        // 원본 데이터를 순회하면서 'deptName'에 해당하는 부서를 그룹으로 묶음
        originalData.forEach(item => {
          const deptName = item.deptName;
          if (!(deptName in data)) {
            data[deptName] = [];
          }
          data[deptName].push({
            id: item.id,
            userName: item.userName,
            rankName: item.rankName,
            deptNo: item.deptNo,
            deptName: item.deptName,
            resign: item.resign
          });
        });
        
        setDeptUserList(data);
        setLoadingDept(false);

      }).catch((error) => {
        if(error.response){
          console.log("조직도 불러오기 실패", error);
        }
      });
  }

  const listGroupMember = () => {
    //선택한 그룹의 사원 불러오기
    setLoadingMember(true);

    const variables = {
      groupNo: selectedGroupNo
    };

    api.post("/admin/group/member/list", variables)
      .then((result) => {
        console.log("그룹사원불러오기: ", result.data);
        setGroupMemberList(result.data);
        setSelectedEmployees(result.data);
        setLoadingMember(false);

      }).catch((error) => {
        if(error.response){
          console.log("선택한 그룹의 사원리스트 모두 불러오기 실패", error);
        }
      });
  }



  useEffect(() => { 
    if (isOpen) {
      listDeptUser();
      console.log("mode는? ", mode);
      if (mode === 'edit') {
        listGroupMember();    //수정일땐 그룹사원리스트 불러오기
      } else {
        setLoadingMember(false);
      }
   
      return () => {
        setGroupMemberList([]);
        setSelectedEmployees([]);
        setIncludeResign(false);     //default: 퇴사포함X
        //로딩창 초기화
        setLoadingDept(false);
        setLoadingMember(false);
        console.log('그룹사원모달 언마운트');
      }
    }
  }, [isOpen]);




  return (
    <div>
      <Modal open={isOpen} onClose={handleClose}>
        <Paper sx={{ position: "absolute", width: "1000px", top: "50%", left: "50%", transform: "translate(-50%, -50%)", p: 2 }}>
          <Box>
            <Box display={"flex"} flexGrow={1}>
              <Card variant='outlined' sx={{ width: "40%", p: 1 }}>
                <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                  <Typography variant="h4" sx={{ margin: 2, fontWeight: 'bold' }}>
                    조직도 리스트
                  </Typography>
                  <Box sx={{ display: "flex", alignItems: "center" }}>
                    <Checkbox
                      size='small'
                      color='secondary'
                      checked={includeResign}
                      onChange={() => setIncludeResign(!includeResign)}
                      disabled={loadingDept || loadingMember}
                    />
                    <Typography variant="body2" color="textSecondary" sx={{ alignSelf: "center" }}>
                      퇴사포함
                    </Typography>
                  </Box>
                </Box>
                {loadingDept || loadingMember ?
                  <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(50vh)" }}>
                    <CircularProgress size={50} />
                  </Box>
                :
                  <TreeView
                    sx={{
                      height: "calc(50vh)", overflowX: "hidden", overflowY: "auto"
                    }}
                    defaultCollapseIcon={<span><ExpandLessIcon fontSize='large' /></span>}
                    defaultExpandIcon={<span><ExpandMoreIcon /></span>}
                  >
                    {Object.entries(deptUserList).map(([deptName, employees]) => {
                      return (
                        <TreeItem
                          key={deptName}
                          nodeId={deptName}
                          label={<Typography variant="h6" sx={{ py: 1 }}>{deptName}</Typography>}
                        >
                          {employees[0].id && employees
                            .filter(employee => includeResign || !employee.resign) // 퇴사포함 체크 여부에 따라 필터링
                            .map((employee) => (
                              <TreeItem
                                key={employee.id}
                                nodeId={`${deptName}-${employee.id}`}
                                label={
                                  <div style={{ display: "flex", alignItems: "center" }}>
                                    <Checkbox
                                      size='small'
                                      color='secondary'
                                      checked={selectedEmployees.findIndex(e => e.id === employee.id) !== -1}
                                    />
                                    <Typography variant="h6">{employee.userName} ({employee.id}{employee.resign ? "/퇴사" : ""})</Typography>
                                  </div>
                                }
                                sx={{ width: "100%", cursor: "pointer" }}
                                onClick={() => handleEmployeeSelect(deptName, employee)}
                              />
                            ))}
                        </TreeItem>
                      );
                    })}
                  </TreeView>
                }

              </Card>

              <Card variant='outlined' sx={{ width: "60%", p: 1 }}>
                <Typography variant="h4" sx={{ margin: 2, fontWeight: 'bold' }}>
                  그룹 사원 리스트
                </Typography>
                {loadingDept || loadingMember ?
                  <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(50vh)" }}>
                    <CircularProgress size={50} />
                  </Box>
                :
                  <List sx={{ height: "calc(50vh)", overflow: "auto" }}>
                    {selectedEmployees.length === 0 ?
                      <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "100%" }}>
                        <Typography color="textSecondary" sx={{ fontSize: "14px" }}>
                          그룹에 추가하실 사원을 선택해 주세요.
                        </Typography>
                      </Box>
                      :
                      (selectedEmployees.map((emp) => (
                        <ListItem key={emp.id} sx={{ py: 0 }} >
                          <ListItemIcon>
                            <span>-</span>
                          </ListItemIcon>
                          <ListItemText
                            primary={<Typography variant="h6">{emp.userName + " (" + emp.deptName + "/" + emp.id}{emp.resign ? "/퇴사)" : ")"}</Typography>}
                            sx={{ flexGrow: 1 }} />
                          <IconButton onClick={() => handleEmployeeSelect(emp.deptName, emp)}>
                            <CloseOutlinedIcon fontSize='small' />
                          </IconButton>
                        </ListItem>
                      )))
                    }
                  </List>
                }
              </Card>
            </Box>
            <Box display="flex" justifyContent="flex-end" sx={{ m: 2 }}>
              <Button variant='outlined'
                onClick={handleClose} sx={{ mr: 1 }}>취소</Button>
              <Button variant='outlined'
                disabled={groupMemberModal.insert.length === 0 && groupMemberModal.delete.length === 0 ? true : false}
                onClick={handleButton}>저장</Button>
            </Box>
          </Box>   
        </Paper>
      </Modal>
    </div>
  );
};

export default GroupMemberModal;


