import React, { useEffect, useState } from "react";
import {
  Modal,
  Box,
  Typography,
  Button,
  TextField,
  Select,
  MenuItem,
  FormControl,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  IconButton,
} from "@mui/material";
import { Close, ExpandMore, ExpandLess } from "@mui/icons-material";
import { LocalizationProvider, DateTimePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import ko from "date-fns/locale/ko"; // 한국어 로케일 import
import { MuiFileInput } from "mui-file-input";
import Snackbar from "@mui/material/Snackbar";
import Fade from "@mui/material/Fade";
import MuiAlert from "@mui/material/Alert";
import axios from "axios";
import UseApi from "../UseApi";
import { format } from "date-fns";
import { addCommas } from "../functions/SimpleFuntion";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
axios.defaults.withCredentials = true;

const ChargeAddModal = ({ onCancel, onAdd, refresh, setRefresh }) => {
  const navigate = useNavigate();
  const [saveDate, setSaveDate] = useState(null);
  const [payment, setPayment] = useState("");
  const [charge, setCharge] = useState("");
  const [store, setStore] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [contents, setContents] = useState("");
  const [fileName, setFileName] = useState(null);
  const [expanded, setExpanded] = useState(true);
  const [categoryData, setCategoryData] = useState([]);
  const [categoryDetailForm, setCateogryDetailForm] = useState([]);
  const [detailTime, setDetailTime] = useState(null);
  const [itemContents, setItemContents] = useState([]);
  const [categoryNo, setCategoryNo] = useState("");
  const [scheduleData, setScheduleData] = useState([]);
  const [date, setDate] = useState({ min: new Date(), max: new Date() });
  const [saveButtonLock, setSaveButtonLock] = useState(true);
  const [totalCharge, setTotalCharge] = useState("");
  const [saveSuccess, setSaveSuccess] = useState(false);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const api = UseApi();
  const [fileToUpload, setFileToUpload] = useState(null);

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
      popup: "my-custom-class", // 모달 팝업의 CSS 클래스 이름
    },
    width: "45%",
  });

  // 용도 정보, 차수 정보 불러오기
  useEffect(() => {
    showCategories();
    scheduleInfo();
  }, []);

  // 용도에 맞는 용도 상세 정보 불러오기
  useEffect(() => {
    if (categoryNo) {
      showCategoryDetailForm(categoryNo);
    }
  }, [categoryNo]);

  // totalCharge 계산
  useEffect(() => {
    const item112 = itemContents.find((item) => item.itemNo === 112); //입력받은 인원수
    const itemOption112 = categoryDetailForm.find((item) => item.itemNo === 112); //인원수 항목
    const item100 = categoryDetailForm.find((item) => item.itemNo === 100); //금액 항목
    if (itemOption112 && item100) {
      //인원수 항목, 금액 항목이 모두 있을 때,
      if (itemOption112.option !== undefined) {
        //인원수 항목의 조건 값이 있다면
        const calculatedMaximumCharge = Number(itemOption112.option) * Number(item100.option); // 인원수 조건 * 금액 조건
        const calculatedTotalCharge = Number(item112.itemContentsValue) * Number(item100.option); // 입력받은 인원수 * 금액 조건
        if (Number(item112.itemContentsValue) > Number(itemOption112.option)) {
          //입력받은 인원수가 인원수 조건보다 크면
          setTotalCharge(calculatedMaximumCharge);
        } else {
          setTotalCharge(calculatedTotalCharge);
        } // totalCharge 업데이트
      } else {
        //인원수 항목의 조건 값이 없다면 (===undefined)
        const calculatedTotalCharge = Number(item100.option); //금액 조건
        setTotalCharge(calculatedTotalCharge); // totalCharge 업데이트
      }
    } else if (item100) {
      // 금액 항목만 있다면
      const calculatedTotalCharge = Number(item100.option); //금액 조건
      setTotalCharge(calculatedTotalCharge); // totalCharge 업데이트
    } else {
      //인원수 항목, 금액 항목 모두 없다면
      setTotalCharge("");
    }
  }, [itemContents]);

  // 값이 변경될 때마다 유효성 검사 실행
  useEffect(() => {
    checkFormValidity();
  }, [saveDate, categoryNo, charge, totalCharge, categoryDetailForm, itemContents]);

  // 유효성 검사 함수
  const checkFormValidity = () => {
    const isSaveButtonDisabled =
      saveDate === null || //날짜 입력 안받으면
      saveDate > date.max ||
      categoryNo === "" || //용도 입력 안받으면
      charge === "" || //금액 입력 안받으면
      charge === null || //금액 입력 안받으면
      charge < 1 || //금액 0 입력하면
      (totalCharge !== "" && charge > totalCharge) || //청구금액이 totalCharge보다 크면
      categoryDetailForm.some(
        (detail) =>
          detail.essential === true &&
          detail.itemNo !== 100 &&
          itemContents.find((item) => item.itemNo === detail.itemNo)?.itemContentsValue === ""
      ) || //입력 필수 용도상세 입력받지 않으면
      categoryDetailForm.some(
        (detail) =>
          detail.type === "숫자" && //용도상세 타입이 숫자이고
          detail.option !== undefined && //조건값이 있을 때
          Number(itemContents.find((item) => item.itemNo === detail.itemNo)?.itemContentsValue) >
            Number(detail.option) //입력값이 조건값보다 크면
      );
    setSaveButtonLock(isSaveButtonDisabled);
  };

  // 차수정보 요청
  const scheduleInfo = () => {
    api
      .get("/user/charge/schedule")
      .then((response) => {
        const result = response.data;
        // console.log(result);
        setScheduleData(result);
        updateDate(result);
      })
      .catch((error) => {
        console.error("차수 정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 차수에 맞게 선택 가능 날짜 범위 지정
  const updateDate = (result) => {
    const ongoingSchedule = result.find((item) => item.close === "진행중");
    // console.log("진행중 : ", ongoingSchedule);
    if (ongoingSchedule) {
      setDate({
        min: new Date(ongoingSchedule.expendStart),
        max: new Date(ongoingSchedule.expendEnd),
      });
    }
  };

  // 용도 정보 요청
  const showCategories = () => {
    api
      .get("/user/charge/categories")
      .then((response) => {
        const categories = response.data;
        setCategoryData(categories);
      })
      .catch((error) => {
        console.error("용도 정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 용도에 따른 용도 상세 양식 데이터 요청
  const showCategoryDetailForm = (categoryNo) => {
    api
      .get("/user/charge/categorydetailform", {
        params: {
          categoryNo: categoryNo,
        },
      })
      .then((response) => {
        const responseData = response.data[0];
        const viewDetailForm = responseData.map((item) => ({
          categoryNo: categoryNo,
          itemNo: item.ITEM_NO,
          itemTitle: item.ITEM_TITLE,
          option: item.ITEM_OPTION,
          type: item.ITEM_TYPE,
          essential: item.ITEM_ESSENTIAL,
        }));
        setCateogryDetailForm(viewDetailForm);
        setItemContents(
          viewDetailForm.map((detail) => ({
            itemNo: detail.itemNo,
            itemTitle: detail.itemTitle,
            itemContentsValue: "",
          }))
        );
      })
      .catch((error) => {
        console.error("용도상세 요청 실패", error);
        if (error.response) {
        }
      });
  };

  //상세 입력 토글 컨트롤
  const handleAccordionToggle = () => {
    setExpanded(!expanded);
  };

  // 입력 받은 값 저장 요청
  const handleSaveButtonClick = () => {
    const expendDate = format(saveDate, "yyyy-MM-dd HH:mm:ss");
    const scheduleNo = scheduleData[0].scheduleNo;
    const formData = {
      id: loginUserId,
      scheduleNo,
      expendDate,
      payment,
      charge,
      store,
      categoryNo,
      contents,
      fileName: fileToUpload ? fileToUpload.name : null,
      itemContents, // 상세 정보 양식 데이터도 추가
    };

    api
      .post("/user/charge/save", formData)
      .then((response) => {
        const responseData = response.data;
        if (fileToUpload !== null && responseData !== null) {
          handleFileUpload(responseData);
        }
        onCancel();
        setRefresh(!refresh);
        setSaveSuccess(true);
        swalWithBootstrapButtons.fire("등록 성공", "지출 정보가 등록되었습니다.", "success");
      })
      .catch((error) => {
        console.error("지출 정보 저장 실패", error);
        if (error.response) {
          swalWithBootstrapButtons.fire("등록 실패", "다시 시도하세요.", "error");
        }
      });
  };

  // 파일 저장 컨트롤
  const handleFileChange = (newFile) => {
    setFileToUpload(newFile); // 파일 정보를 상태 변수에 저장
  };

  // 첨부 파일 리액트단에 저장
  const handleFileUpload = (responseData) => {
    const formData = new FormData();
    formData.append("file", fileToUpload);
    // formData.append("savedFileName", responseData);
    formData.append("id", loginUserId);
    formData.append("expendDate", format(saveDate, "yyyy-MM-dd HH:mm:ss"));
    formData.append("categoryNo", categoryNo);
    formData.append("charge", charge);

    api
      .post("/user/charge/fileupload", formData, {
        headers: {
          "Content-Type": "multipart/form-data", // 요청 헤더에서 Content-Type 설정
        },
      })
      .then((response) => {})
      .catch((error) => {
        if (error.response) {
          alert("첨부파일 저장 실패, 다시 첨부해주세요.");
        }
      });
  };

  const handleCloseSnackbar = () => {
    setSaveSuccess(false);
  };

  return (
    <Modal open onCancel={onCancel}>
      <React.Fragment>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 600, // Increase the width to fit the table
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
            borderRadius: 1,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
            <Typography variant="h3" component="h2" id="modal-title" sx={{ margin: 2 }}>
              지출 정보 등록
            </Typography>
            <IconButton onClick={onCancel} aria-label="close">
              <Close />
            </IconButton>
          </Box>
          <TableContainer style={{ maxHeight: "600px" }}>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell>
                    <strong>지출일시</strong>
                    <Typography variant="caption" color="error" marginLeft="3px">
                      *
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ko}>
                      <DateTimePicker
                        views={["year", "month", "day", "hours", "minutes", "seconds"]}
                        value={saveDate}
                        onChange={(newValue) => {
                          setSaveDate(newValue);
                        }}
                        renderInput={(params) => <TextField {...params} />}
                        sx={{ width: 400, alignContent: "center" }}
                        format="yyyy-MM-dd HH:mm:ss"
                        timeSteps={{ hours: 1, minutes: 1, seconds: 1 }}
                        ampm={false}
                        minDate={date.min}
                        maxDate={date.max}
                      />
                    </LocalizationProvider>
                    <Box>
                      {(saveDate === null || saveDate === "") && (
                        <Typography variant="caption" color="error" marginLeft="5px">
                          지출일시를 입력해주세요.
                        </Typography>
                      )}
                    </Box>
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>
                    <strong>결제수단</strong>
                  </TableCell>
                  <TableCell>
                    <FormControl sx={{ width: "197px" }}>
                      <Select
                        onChange={(e) => setPayment(e.target.value)}
                        size="small"
                        value={payment}
                      >
                        <MenuItem value="개인카드">개인카드</MenuItem>
                        <MenuItem value="법인카드">법인카드</MenuItem>
                        <MenuItem value="현금">현금</MenuItem>
                      </Select>
                    </FormControl>
                    <Box>
                      {(payment === "" || payment === null) && (
                        <Typography variant="caption" color="error" marginLeft="5px">
                          결제수단을 선택해주세요.
                        </Typography>
                      )}
                    </Box>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <strong>사용처</strong>
                  </TableCell>
                  <TableCell>
                    <TextField
                      value={store}
                      size="small"
                      onChange={(e) => setStore(e.target.value.trim())}
                    />
                    <Box>
                      {(store === "" || store === null) && (
                        <Typography variant="caption" color="error" marginLeft="5px">
                          사용처를 입력해주세요.
                        </Typography>
                      )}
                    </Box>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <strong>용도</strong>
                    <Typography variant="caption" color="error" marginLeft="3px">
                      *
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Box sx={{ display: "flex", alignItems: "center" }}>
                      <FormControl sx={{ width: "197px", marginRight: "auto" }}>
                        <Select
                          onChange={(e) => {
                            const selectedCategory = categoryData.find(
                              (category) => category.categoryNo === e.target.value
                            );
                            setCategoryNo(e.target.value);
                            setCategoryName(selectedCategory.categoryName);
                            showCategoryDetailForm(selectedCategory.categoryNo);
                          }}
                          size="small"
                          MenuProps={{ style: { maxHeight: "285px" } }}
                          error={categoryNo === null || categoryNo === ""}
                          value={categoryNo}
                        >
                          {categoryData.map((category) => (
                            <MenuItem key={category.categoryNo} value={category.categoryNo}>
                              {category.categoryName}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>

                      <IconButton
                        onClick={handleAccordionToggle}
                        size="small"
                        sx={{ fontSize: "16px" }}
                      >
                        상세 입력 {expanded ? <ExpandLess /> : <ExpandMore />}
                      </IconButton>
                    </Box>
                    <Box>
                      {(categoryName === "" || categoryName === null) && (
                        <Typography variant="caption" color="error" marginLeft="5px">
                          용도를 선택해주세요.
                        </Typography>
                      )}
                    </Box>
                  </TableCell>
                </TableRow>
                {expanded && (
                  <TableRow>
                    <TableCell>
                      <strong>용도상세</strong>
                      {/* <Typography variant="caption" color="error" marginLeft="3px">
                          *
                        </Typography> */}
                    </TableCell>
                    <TableCell sx={{ paddingLeft: "5px", paddingTop: "0px", paddingBottom: "5px" }}>
                      {categoryDetailForm.length === 0 ||
                      (categoryDetailForm.length === 1 && categoryDetailForm[0].itemNo === 100) ? (
                        <Typography color="gray" marginLeft="15px" marginTop="7px">
                          입력할 상세 정보가 없습니다.
                        </Typography>
                      ) : (
                        <Box>
                          {categoryDetailForm.map((detail, index) => (
                            <Box key={index} margin={"10px"} marginTop={"15px"}>
                              {(detail.type === "숫자" || detail.type === "문자") &&
                              detail.itemNo !== 100 ? (
                                <TextField
                                  size="small"
                                  sx={{ width: "197px" }}
                                  label={detail.itemTitle}
                                  value={itemContents[index].itemContentsValue}
                                  type={detail.type === "숫자" ? "number" : "text"}
                                  inputProps={
                                    detail.type === "숫자"
                                      ? {
                                          min: 0,
                                          max: detail.itemNo === 112 ? detail.option : null,
                                        }
                                      : {}
                                  }
                                  onChange={(e) => {
                                    const updatedContents = itemContents.map((item) =>
                                      item.itemNo === detail.itemNo
                                        ? { ...item, itemContentsValue: e.target.value }
                                        : item
                                    );
                                    setItemContents(updatedContents);
                                  }}
                                  error={
                                    detail.type === "숫자" &&
                                    detail.option !== undefined &&
                                    Number(
                                      itemContents.find((item) => item.itemNo === detail.itemNo)
                                        ?.itemContentsValue
                                    ) > Number(detail.option)
                                  }
                                />
                              ) : detail.type === "시간" ? (
                                <LocalizationProvider
                                  dateAdapter={AdapterDateFns}
                                  adapterLocale={ko}
                                >
                                  <DateTimePicker
                                    value={detailTime}
                                    onChange={(newValue) => {
                                      const updatedContents = itemContents.map((item) =>
                                        item.itemNo === detail.itemNo
                                          ? {
                                              ...item,
                                              value: format(newValue, "yyyy-MM-dd HH:mm"),
                                            }
                                          : item
                                      );
                                      setItemContents(updatedContents);
                                    }}
                                    renderInput={(params) => <TextField {...params} />}
                                    sx={{ width: 400, alignContent: "center" }}
                                    format="yyyy-MM-dd HH:mm"
                                    timeSteps={{ hours: 1, minutes: 10 }}
                                    ampm={false}
                                    minDate={date.min}
                                    maxDate={date.max}
                                  />
                                </LocalizationProvider>
                              ) : null}
                              <Box>
                                {detail.type === "숫자" &&
                                  categoryDetailForm.some(
                                    (formDetail) =>
                                      formDetail.itemNo === detail.itemNo &&
                                      formDetail.option !== undefined &&
                                      Number(
                                        itemContents.find((item) => item.itemNo === detail.itemNo)
                                          ?.itemContentsValue
                                      ) > Number(formDetail.option)
                                  ) && (
                                    <Typography variant="caption" color="error" marginLeft="5px">
                                      {`${addCommas(detail.option)} 이하의 값을 입력하세요.`}
                                    </Typography>
                                  )}

                                {detail.essential === true &&
                                  detail.itemNo !== 100 &&
                                  itemContents[index] &&
                                  itemContents[index].itemContentsValue === "" && (
                                    <Typography
                                      variant="caption"
                                      color="error"
                                      marginLeft="5px"
                                      key={index}
                                    >
                                      {detail.itemTitle} 입력해주세요.
                                    </Typography>
                                  )}
                              </Box>
                            </Box>
                          ))}
                        </Box>
                      )}
                    </TableCell>
                  </TableRow>
                )}
                <TableRow>
                  <TableCell>
                    <strong>금액</strong>
                    <Typography variant="caption" color="error" marginLeft="3px">
                      *
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <TextField
                      value={charge}
                      size="small"
                      type="number"
                      inputProps={{ min: 1 }}
                      onChange={(e) => {
                        const newCharge = e.target.value;
                        setCharge(newCharge);
                      }}
                      error={
                        charge === null ||
                        charge === "" ||
                        charge < 1 ||
                        (categoryDetailForm.some((d) => d.itemNo === 100) && // 금액 조건이 있는 경우
                          // categoryDetailForm.some((d) => d.itemNo === 112) && // 인원 조건 있는 경우
                          charge > totalCharge)
                      }
                    />
                    <Box>
                      {(charge === null || charge === "") && (
                        <Typography variant="caption" color="error" marginLeft="5px">
                          금액을 입력해주세요.
                        </Typography>
                      )}
                      {itemContents.some(
                        (detail) =>
                          // detail.itemNo === 112 && // 인원수 항목을 입력받은 경우
                          totalCharge !== 0 &&
                          categoryDetailForm.some((d) => d.itemNo === 100) && // 금액 항목이 있는 경우
                          charge > totalCharge // totalCharge와 비교하여 에러 메시지를 표시
                      ) && (
                        <Typography variant="caption" color="error" marginLeft="5px">
                          {addCommas(totalCharge)}원 이하의 금액만 청구 가능합니다.
                        </Typography>
                      )}
                    </Box>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell sx={{ width: "60px" }}>
                    <strong>내용</strong>
                  </TableCell>
                  <TableCell>
                    <TextField
                      fullWidth
                      label="내용"
                      value={contents}
                      onChange={(e) => setContents(e.target.value)}
                      size="small"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <strong>첨부</strong>
                  </TableCell>
                  <TableCell>
                    <MuiFileInput
                      value={fileToUpload ? fileToUpload : null}
                      onChange={handleFileChange}
                      inputProps={{ accept: ".png, .jpg, .jpeg," }} // .pdf, .xlsx, .csv 는 나중에..
                      size="small"
                      fullWidth
                      // hideSizeText
                      error={fileToUpload === null}
                    />
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Box sx={{ display: "flex", justifyContent: "flex-end", marginTop: 2 }}>
            <Button
              variant="outlined"
              color="primary"
              onClick={handleSaveButtonClick}
              disabled={saveButtonLock}
            >
              저장
            </Button>
            {saveSuccess && (
              <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "center" }}
                open={saveSuccess}
                onClose={handleCloseSnackbar}
                TransitionComponent={Fade}
                autoHideDuration={3000}
                style={{ width: "400px" }}
              >
                <MuiAlert
                  elevation={6}
                  variant="filled"
                  severity="success"
                  style={{ color: "white" }}
                >
                  지출 정보가 등록되었습니다.
                </MuiAlert>
              </Snackbar>
            )}
            <Button variant="outlined" color="error" onClick={onCancel} sx={{ marginLeft: 1 }}>
              취소
            </Button>
          </Box>
        </Box>
      </React.Fragment>
    </Modal>
  );
};

export default ChargeAddModal;
