import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import React, { useState, useCallback } from "react";
import { Box, Fab, Typography, Paper, Grid } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import MuiAlert from "@mui/material/Alert";
import Swal from "sweetalert2";
axios.defaults.withCredentials = true;

export default function CreateCategoryModal({
  setModalOpen,
  submitCategory,
  setSubmitCategory,
  handlecreateCategorySnackOpen,
  handleCloseModal, api
}) {
  const navigate = useNavigate();
  const [open, setOpen] = useState(true);
  const handleClose = () => setOpen(false);
  const [categoryNoError, setCategoryNoError] = useState(true);
  const [categoryNameError, setCategoryNameError] = useState(true);

  // 용도 DB 컬럼
  const [categoryNo, setCategoryNo] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [usable, setUsable] = useState(true);

  // 용도 추가 Modal 작성 항목
  const handleCategoryNo = (e) => {
    const value = e.target.value.trim(); // 시작 또는 끝에 있는 공백 제거

    // 값을 입력한 경우에만 상태를 업데이트
    if (value !== "") {
      setCategoryNo(value);
      setCategoryNoError(false);
    } else {
      setCategoryNo("");
      setCategoryNoError(true);
    }
  };

  const handleCategoryName = (e) => {
    const value = e.target.value.trim();

    if (value !== "") {
      setCategoryName(value);
      setCategoryNameError(false);
    } else {
      setCategoryName("");
      setCategoryNameError(true);
    }
  };

  const handleUsable = (e) => {
    const value = e.target.value;

    if (value !== "") {
      setUsable(value);
    }
  };

  // 용도 생성
  const handleSubmit = (event) => {
    event.preventDefault();

    const data = JSON.stringify({
      categoryNo: categoryNo,
      categoryName: categoryName,
      usable: usable,
    });

    api({
      url: "/admin/category/add",
      method: "post",
      data: data,
      headers: { "Content-type": "application/json" },
    })
      .then(() => {
        handlecreateCategorySnackOpen();
        setSubmitCategory(!submitCategory);
        handleClose();
        // setModalOpen(false);
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        if (error.response && error.response.status === 400) {
          // alert("동일한 용도코드가 존재합니다.");
          Swal.fire({
            icon: "error",
            title: "용도 생성 실패",
            text: "동일한 용도코드가 존재합니다!",
          });
        }
        if (error.response) {
          console.log("통신 error");
        }
      });
  };

  return (
    <div>
      <Modal open={open} onClose={handleCloseModal}>
        <Paper
          sx={{
            position: "absolute",
            width: "400px",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            p: 2,
          }}
        >
          <Box>
            <Box
              flexGrow={1}
              sx={{
                alignItems: "center",
                mb: 5,
                justifyContent: "space-evenly",
                mt: 5,
                px: 5,
              }}
            >
              <Typography
                id="modal-modal-title"
                variant="h3"
                component="h2"
                sx={{ textAlign: "center", mb: 5 }}
              >
                용도 추가
              </Typography>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  mb: 5,
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "70px", textAlign: "center", alignItems: "center", mr: 2 }}
                >
                  용도 코드
                </Typography>
                <TextField
                  type="number"
                  placeholder="숫자로 입력"
                  sx={{ height: "30px", width: "170px" }}
                  InputProps={{
                    style: {
                      height: "100%",
                    },
                  }}
                  error={categoryNoError || categoryNo.trim() === "" || parseInt(categoryNo) === 0}
                  helperText={
                    categoryNoError
                      ? "용도코드를 입력해주세요."
                      : categoryNo.trim() === "" || parseInt(categoryNo) === 0
                      ? "0번은 사용 불가능합니다."
                      : null
                  }
                  onInput={(e) => {
                    e.target.value = e.target.value.replace(/[^0-9]/g, ""); // 숫자 외의 문자 제거
                  }}
                  onChange={handleCategoryNo}
                />
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  mb: 5,
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "70px", textAlign: "center", mr: 2 }}
                >
                  용도명
                </Typography>
                <TextField
                  placeholder="한글로 입력"
                  sx={{ height: "30px", width: "170px" }}
                  InputProps={{
                    style: {
                      height: "100%", // TextField 높이 조절
                    },
                  }}
                  error={categoryNameError}
                  helperText={categoryNameError ? "용도명을 입력해주세요." : ""}
                  onChange={handleCategoryName}
                />
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 3,
                  justifyContent: "center",
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "70px", textAlign: "center", mr: 2 }}
                >
                  사용 여부
                </Typography>
                <FormControl sx={{ ml: 2 }}>
                  <InputLabel id="demo-simple-select-label">선택</InputLabel>
                  <Select
                    sx={{ height: "40px" }}
                    value={usable}
                    label="Age"
                    onChange={handleUsable}
                  >
                    <MenuItem value={"true"}>사용</MenuItem>
                    <MenuItem value={"false"}>미사용</MenuItem>
                  </Select>
                </FormControl>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  mb: 4,
                }}
              >
                <Grid item xs={3.5}>
                  <Fab
                    color="primary"
                    variant="extended"
                    onClick={(event) => {
                      handleSubmit(event);
                      setModalOpen(false);
                    }}
                    disabled={
                      categoryNo === "" ||
                      categoryName === "" ||
                      categoryNo.trim() === "" ||
                      parseInt(categoryNo) === 0
                    }
                  >
                    <AddIcon />
                    <Typography
                      sx={{
                        ml: 0.2,
                        textTransform: "capitalize",
                      }}
                    >
                      생성
                    </Typography>
                  </Fab>
                </Grid>
                <Grid item xs={1}></Grid>
                <Grid item xs={3.5}>
                  <Fab color="error" variant="extended" onClick={handleCloseModal}>
                    <DeleteIcon />
                    <Typography
                      sx={{
                        ml: 0.2,
                        textTransform: "capitalize",
                      }}
                    >
                      취소
                    </Typography>
                  </Fab>{" "}
                </Grid>
              </Box>
            </Box>
          </Box>
        </Paper>
      </Modal>
    </div>
  );
}