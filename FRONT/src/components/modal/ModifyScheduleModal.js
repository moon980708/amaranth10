// import * as React from "react";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import { Box, Grid, Fab, Typography } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import Swal from "sweetalert2";

import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import dayjs from "dayjs";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import typography from "../../assets/global/Typography";
import { LocalizationProvider, DatePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import ko from "date-fns/locale/ko"; // 한국어 로케일 import
import axios from "axios";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { change_ModifyScheduleState, change_ScheduleState } from "../../reducer/module/scheduleReducer";
import modifySchedule from "../../reducer/axios/scheduleAbout/modifySchedule";
import { useNavigate } from "react-router-dom";
import UseApi from '../UseApi';

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "#fff",
  border: "2px solid #000",
  borderRadius: "30px",
  color: "black",
  boxShadow: 24,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  p: 5,
};

// 날짜를 "yyyy/mm/dd" 형식으로 변환하는 함수
const formatDate = (date) => {
  return dayjs(date).format("YYYY/MM/DD");
};

export default function ModifyScheduleModal({
  modifyInfo,
  handleModifyModalClose,
  listLoadingStart,
  setListLoadingStart,
  chargeDate
}) {
  
  console.log("수정모달에서 : " + modifyInfo.expendStart);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const api = UseApi();

  const recentSchedule = useSelector(
    (state) => state.scheduleReducer.recentSchedule
  );

  // const userName = useSelector((state) => state.loginReducer.loginUserName);
  // console.log("userName : " + userName)

  console.log("정보 : " + typeof modifyInfo.nextChargeStart);
  console.log("수정모달에서 리덕스recentSchedule : " + recentSchedule);

  const [open, setOpen] = useState(true);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [startDate, setStartDate] = useState(
    dayjs(modifyInfo.expendStart).toDate()
  );

  const [endDate, setEndDate] = useState(dayjs(modifyInfo.expendEnd).toDate());

  const handleStartDateChange = (date) => {
    setStartDate(date);
  };

  const handleEndDateChange = (date) => {
    setEndDate(date);
  };

  const [enableChargeStart, setEnableChargeStart] = useState(
    dayjs(modifyInfo.chargeStart).toDate()
  );
  const [enableChargeEnd, setEnableChargeEnd] = useState(
    dayjs(modifyInfo.chargeEnd).toDate()
  );

  const handelEnableChargeStart = (date) => {
    setEnableChargeStart(date);
    console.log("청구가능시작일 : " + enableChargeStart);
  };
  const handelEnableChargeEnd = (date) => {
    setEnableChargeEnd(date);
    console.log("청구가능마감일 : " + enableChargeEnd);
  };

  const [enablePayday, setEnablePayday] = useState(
    dayjs(modifyInfo.payday).toDate()
  );
  const handelPaydayChange = (date) => {
    setEnablePayday(date);
    console.log(enablePayday);
  };
  const scheduleNo = modifyInfo.scheduleNo;

  // const checkMethod = () =>{
  //   console.log(scheduleNo, startDate, endDate, enableChargeStart, enableChargeEnd, enablePayday);
  // }
  const handelClickModifyBtn = async () => {
    try {
      // console.log(scheduleNo, startDate, endDate, enableChargeStart, enableChargeEnd, enablePayday);

      const result = await modifySchedule({
        scheduleNo,
        startDate,
        endDate,
        enableChargeStart,
        enableChargeEnd,
        enablePayday,
        navigate,
        api
      });
      if (result !== null) {
        setListLoadingStart(!listLoadingStart); //리스트다시받음
        handleModifyModalClose();
        dispatch(change_ScheduleState());
        dispatch(change_ModifyScheduleState());
        Swal.fire(
          "수정 완료",
          "해당 차수 정보가 수정되었습니다.",
          "success"
        );
      }
    } catch (error) {
      console.log("수정 실패");
    }
  };

  return (
    <div>
      <Modal
        open={open}
        onClose={handleModifyModalClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h3"
            component="h2"
            sx={{ textAlign: "center", mb: 5 }}
          >
            {modifyInfo.schedule}차수 수정
          </Typography>

          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              mb: 5,
              width: "100%",
            }}
          >
            <Typography sx={{ mr: 2, width: "90px", textAlign: "center" }}>
              지출기간
            </Typography>
            <LocalizationProvider
              dateAdapter={AdapterDateFns}
              adapterLocale={ko}
            >
              <Box display="flex" justifyContent="center" alignItems="center">
                <Grid container spacing={2} alignItems="center">
                  <Grid item xs={5.5} lg={5.5}>
                    <DatePicker
                      value={startDate}
                      onChange={handleStartDateChange}
                      views={["year", "month", "day"]}
                      format="yyyy-MM-dd"
                      slotProps={{
                        textField: { variant: "outlined", size: "small" },
                      }}
                      maxDate={endDate}
                      // maxDate=""
                    />
                  </Grid>
                  <Grid item xs={1} lg={1}>
                    <Typography variant="h3" fontWeight={600}>
                      -
                    </Typography>
                  </Grid>
                  <Grid item xs={5.5} lg={5.5}>
                    <DatePicker
                      value={endDate}
                      onChange={handleEndDateChange}
                      views={["year", "month", "day"]}
                      format="yyyy-MM-dd"
                      slotProps={{
                        textField: { variant: "outlined", size: "small" },
                      }}
                      minDate={startDate}
                      maxDate={dayjs(chargeDate)
                        .subtract(1, "month")
                        .endOf("month")
                        .toDate()}
                    />
                  </Grid>
                </Grid>
              </Box>
            </LocalizationProvider>
          </Box>

          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              mb: 5,
              width: "100%",
            }}
          >
            <Typography sx={{ mr: 2, width: "90px", textAlign: "center" }}>
              청구가능기간
            </Typography>
            <LocalizationProvider
              dateAdapter={AdapterDateFns}
              adapterLocale={ko}
            >
              <Box display="flex" justifyContent="center" alignItems="center">
                <Grid container spacing={2} alignItems="center">
                  <Grid item xs={5.5} lg={5.5}>
                    <DatePicker
                      value={enableChargeStart}
                      onChange={handelEnableChargeStart}
                      views={["year", "month", "day"]}
                      format="yyyy-MM-dd"
                      slotProps={{
                        textField: { variant: "outlined", size: "small" },
                      }}
                      minDate={
                        modifyInfo.prevChargeEnd === null
                          ? dayjs(modifyInfo.yearMonth).toDate()
                          : dayjs(modifyInfo.prevChargeEnd)
                              .add(1, "day")
                              .toDate()
                      }
                      maxDate={enableChargeEnd}
                    />
                  </Grid>
                  <Grid item xs={1} lg={1}>
                    <Typography variant="h3" fontWeight={600}>
                      -
                    </Typography>
                  </Grid>
                  <Grid item xs={5.5} lg={5.5}>
                    <DatePicker
                      value={enableChargeEnd}
                      onChange={handelEnableChargeEnd}
                      views={["year", "month", "day"]}
                      format="yyyy-MM-dd"
                      slotProps={{
                        textField: { variant: "outlined", size: "small" },
                      }}
                      minDate={enableChargeStart}
                      maxDate={
                        modifyInfo.nextChargeStart === null
                          ? dayjs(modifyInfo.yearMonth).endOf("month").toDate()
                          : dayjs(modifyInfo.nextChargeStart)
                              .subtract(1, "day")
                              .toDate()
                      }
                    />
                  </Grid>
                </Grid>
              </Box>
            </LocalizationProvider>
          </Box>

          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              mb: 5,
              width: "100%",
            }}
          >
            <Typography sx={{ mr: 2, width: "105px", textAlign: "center" }}>
              지급일
            </Typography>

            <LocalizationProvider
              dateAdapter={AdapterDateFns}
              adapterLocale={ko}
            >
              <Grid container spacing={2} alignItems="center">
                <Grid item xs={5.5} lg={5.5}>
                  <DatePicker
                    value={enablePayday}
                    onChange={handelPaydayChange}
                    views={["year", "month", "day"]}
                    format="yyyy-MM-dd"
                    slotProps={{
                      textField: { variant: "outlined", size: "small" },
                    }}
                    minDate={
                      modifyInfo.prevPayday === null
                        ? dayjs(modifyInfo.yearMonth).add(1, "month").toDate()
                        : dayjs(modifyInfo.prevPayday).add(1, "day").toDate()
                    }
                    maxDate={
                      modifyInfo.nextPayday === null
                        ? null
                        : dayjs(modifyInfo.nextPayday)
                            .subtract(1, "day")
                            .toDate()
                    }
                  />
                </Grid>
              </Grid>
            </LocalizationProvider>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-around",
              width: "60%",
              mb: 2,
            }}
          >
            <Fab
              color="primary"
              variant="extended"
              onClick={() => {
                handelClickModifyBtn();
              }}
            >
              <AddIcon />
              <Typography
                sx={{
                  ml: 0.2,
                  textTransform: "capitalize",
                }}
              >
                수정
              </Typography>
            </Fab>
            <Fab
              color="secondary"
              variant="extended"
              onClick={handleModifyModalClose}
            >
              <DeleteIcon />
              <Typography
                sx={{
                  ml: 0.2,
                  textTransform: "capitalize",
                }}
              >
                취소
              </Typography>
            </Fab>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
