import React, { useState } from "react";
import {
  Dialog,
  DialogActions,
  DialogContent,
  Typography,
  Button,
  TextField,
  Box,
} from "@mui/material";
import Snackbar from "@mui/material/Snackbar";
import Fade from "@mui/material/Fade";
import MuiAlert from "@mui/material/Alert";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import UseApi from "../UseApi";
axios.defaults.withCredentials = true;

const PasswordCheckModal = ({ open, onClose, onPasswordSubmit, onCancel }) => {
  const [inputPassword, setInputPassword] = useState("");
  const [inputWrongPass, setInputWrongPass] = useState(false);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const navigate = useNavigate();
  const api = UseApi();

  const handlePasswordInput = (e) => {
    setInputPassword(e.target.value);
  };

  const passCheckSnackbarClose = () => {
    console.log("스낵바 닫힘");
    setInputWrongPass(false);
  };

  const handleSubmit = () => {
    api
      .get("/user/myinfo/passcheck", {
        params: {
          id: loginUserId,
          password: inputPassword,
        },
      })
      .then((result) => {
        console.log(result.data);
        if (result.data === "loginFail") {
          // alert("비밀번호를 다시 입력하세요.");
          setInputWrongPass(true);
        } else {
          onPasswordSubmit(inputPassword);
          setInputWrongPass(false);
          onClose();
        }
      })
      .catch((error) => {
        console.error("사용자 정보 요청 실패", error);
        if (error.response) {
        }
      });
  };
  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      // e.preventDefault(); // 기본 엔터 키 동작 막기

      if (inputPassword !== "" && e.key === "Enter") {
        handleSubmit();
      }
    }
  };

  return (
    <Dialog open={open} onClose={onCancel}>
      <DialogContent>
        <Typography variant="h4" align="center" marginTop={2} marginX={10}>
          비밀번호를 입력하세요.
        </Typography>
      </DialogContent>
      <DialogContent align="center">
        <Box marginBottom={2}>
          <TextField
            variant="outlined"
            type="password"
            onChange={handlePasswordInput}
            value={inputPassword}
            onKeyDown={handleKeyDown}

          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSubmit}
          disabled={inputPassword === ""}

        >
          확인
        </Button>
        <Button variant="contained" color="error" onClick={onCancel}>
          취소
        </Button>
        {inputWrongPass && (
          <Snackbar
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
            open={inputWrongPass}
            onClose={passCheckSnackbarClose}
            TransitionComponent={Fade}
            autoHideDuration={3000}
            style={{ width: "400px" }}
          >
            <MuiAlert elevation={6} variant="filled" severity="warning" style={{ color: "white" }}>
              비밀번호를 바르게 입력해주세요.
            </MuiAlert>
          </Snackbar>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default PasswordCheckModal;
