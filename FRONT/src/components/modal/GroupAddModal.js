import React, { useState, useEffect } from "react";
import { Button, TextField, Modal, Paper, Typography, Box } from "@mui/material";



//모달 열렸을때 TextField 포커스 되게 하기.해야함!
const GroupAddModal = ({ isOpen, handleClose, groupAddModal, setGroupAddModal, handleButton }) => {
  
  const [originalGroupName, setOriginalGroupName] = useState("");

  const handleGroupNameChange = (event) => {
    setGroupAddModal({ ...groupAddModal, groupName: event.target.value });
  };

  useEffect(() => { 
    if (isOpen) {
      setOriginalGroupName(groupAddModal.groupName);
      console.log("모달열릴당시 원래 그룹이름: ", groupAddModal.groupName);
    }
  }, [isOpen]);


  return (
    <div>
      <Modal open={isOpen} onClose={handleClose}>
        <Paper sx={{position: "absolute", width:"600px", top: "50%", left: "50%", transform: "translate(-50%, -50%)", p:2 }}>
          <Box>
            <Box flexGrow={1} sx={{ mt: 5, px: 5 }}>
              <Typography
                variant="h3"
                sx={{mb: 5}}
              >취합 그룹 {groupAddModal.state === 'add' ? "생성" :"수정"}</Typography>
              <TextField
                id="groupName"
                label="그룹명 (1~15자로 제한)"
                fullWidth
                variant="standard"
                size="small"
                margin="dense"
                value={groupAddModal.groupName}
                onChange={handleGroupNameChange}
                inputProps={{ maxLength: 15 }} 
              /> 
            </Box>
            <Box display="flex" justifyContent="flex-end" sx={{ m: 2 }}>
              <Button onClick={handleClose} sx={{ mr: 1 }} variant='outlined'
              >취소</Button>
                <Button
                  variant='outlined'
                  disabled={!groupAddModal.groupName.trim() || originalGroupName === groupAddModal.groupName || groupAddModal.groupName.length > 15} // 조건 추가  
                  onClick={handleButton}
                >{groupAddModal.state === 'add' ? "다음" :"수정"}</Button>
            </Box>
          </Box>
        </Paper>
      </Modal>
    </div>
  );
};

export default GroupAddModal;


