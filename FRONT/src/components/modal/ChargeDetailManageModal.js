import React, { useState } from "react";
import {
  Modal,
  Box,
  Typography,
  IconButton,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow
} from "@mui/material";
import { Close, ExpandMore, ExpandLess } from "@mui/icons-material";
import PrintIcon from '@mui/icons-material/Print';

import { addCommas } from '../functions/SimpleFuntion';
import ViewImageModal from './ViewImageModal';
import ChargePDF from '../pdf/ChargePDF';



const ChargeDetailManageModal = ({ isOpen, onClose, chargeDetail}) => {
  const [expanded, setExpanded] = useState(false);
  const [imageModalOpen, setImageModalOpen] = useState(false);
  const [showPDF, setShowPDF] = useState({
    open: false,
    userChargeNo: null
  });        //pdf파일로 보여줄 userChargeNo




  const handleAccordionToggle = () => {
    setExpanded(!expanded);
  };

  const handleClose = () => {
    setExpanded(false);
    onClose();
  }

  const handleShowPDF = () => {
    setShowPDF({
      open: true,
      userChargeNo: chargeDetail.userChargeNo
    });
  }


  return (
    <Modal
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="modal-title"
      aria-describedby="modal-description"
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: 600, 
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          borderRadius: 1,
        }}
      >
        <Box sx={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
          <Typography variant="h3" component="h2" id="modal-title" sx={{ margin: 2 }}>
            청구 상세 내역
          </Typography>
          <IconButton onClick={handleClose} aria-label="close">
            <Close />
          </IconButton>
        </Box>
        <TableContainer style={{ maxHeight: "600px" }}>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell>
                  <strong>청구일</strong>
                </TableCell>
                <TableCell>{chargeDetail.userChargeDate}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong>청구년월.차수</strong>
                </TableCell>
                <TableCell>{chargeDetail.scheduleName}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong>지출일시</strong>
                </TableCell>
                <TableCell>{chargeDetail.expendDate}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong>결제수단</strong>
                </TableCell>
                <TableCell>{chargeDetail.payment}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong>사용처</strong>
                </TableCell>
                <TableCell>{chargeDetail.store}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong>금액</strong>
                </TableCell>
                <TableCell>{addCommas(chargeDetail.charge)}원</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong>용도</strong>
                </TableCell>
                <TableCell>
                    <Box sx={{ display: "flex", alignItems: "center" }}>
                      <Typography sx={{ marginRight: "auto", fontSize: "14px" }}>
                        {chargeDetail.categoryName}
                      </Typography>
                    
                    {chargeDetail.itemList && chargeDetail.itemList.length !== 0 &&
                      <IconButton
                        onClick={handleAccordionToggle}
                        size="small"
                        sx={{fontSize: "14px"}}
                      >
                        상세 {expanded ? <ExpandLess /> : <ExpandMore />}
                      </IconButton>}
                    </Box>
                </TableCell>
              </TableRow>
              {expanded && (
                <TableRow>
                  <TableCell>
                    <strong>용도상세</strong>
                  </TableCell>
                  <TableCell sx={{ py: 0 }}>
                    <ul>
                      {chargeDetail.itemList.map((item, index) =>
                        item.itemContents !== "" && (
                          <li key={index}>
                            {item.itemTitle} : {item.itemContents}
                          </li>
                        )
                      )}
                    </ul>
                  </TableCell>
                </TableRow>
              )}
              <TableRow>
                <TableCell sx={{ width: "100px" }}>
                  <strong>내용</strong>
                </TableCell>
                <TableCell>
                  {chargeDetail.contents?
                    chargeDetail.contents : "-"
                  }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong>첨부</strong>
                </TableCell>
                <TableCell>
                  {chargeDetail.fileName ?
                    <Typography
                      sx={{
                        '&:hover': {
                          cursor: 'pointer',
                        },
                        textDecorationLine: 'underline'
                      }}
                      onClick={() => setImageModalOpen(true)} variant="subtitle2" color={"dodgerblue"}>
                      {chargeDetail.fileName}
                    </Typography>
                    : "-"
                  }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong>상태</strong>
                </TableCell>
                <TableCell
                  sx={{
                    color: chargeDetail.state === '승인대기' ? 'orange'
                      : chargeDetail.state === '승인' ? 'green'
                        : 'red'
                  }}
                >
                  {chargeDetail.state}
                </TableCell>
              </TableRow>
              {chargeDetail.rejectReason &&
                <TableRow>
                  <TableCell>
                      <strong>반려사유</strong>
                  </TableCell>
                  <TableCell>{chargeDetail.rejectReason}</TableCell>
                </TableRow>
              }
            </TableBody>
          </Table>
        </TableContainer>

        
        <Box sx={{ display: "flex", justifyContent: "flex-end", alignItems: "center", mt: 3 }}>

          <Button variant="contained" color='error'
            onClick={handleShowPDF}
            endIcon={<PrintIcon />}>
            PDF
          </Button>
          {showPDF.open &&
            showPDF.userChargeNo === chargeDetail.userChargeNo &&
            <ChargePDF
              showPDF={showPDF}
              setShowPDF={setShowPDF}
            />
          }
    
        </Box>

        {imageModalOpen && (
          <ViewImageModal onCancel={() => setImageModalOpen(false)} imagePath={"/upload/" + chargeDetail.realFileName} />
        )}

        </Box>
    </Modal>
  );
};

export default ChargeDetailManageModal;
