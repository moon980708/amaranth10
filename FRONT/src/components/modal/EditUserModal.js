// import * as React from "react";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import { Box, Fab, Typography, Paper, Grid } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import axios from "axios";
import { useNavigate } from "react-router-dom";
axios.defaults.withCredentials = true; 

export default function EditUserModal({
  editUser,
  setEditUser,
  editUserData,
  deptList,
  rankList,
  checkedId,
  setCheckedId,
  handleEditClickSnackbar,
  handleCloseEditModal,
  setEditModalOpen, api
}) {
  const navigate = useNavigate();

  const [open, setOpen] = useState(true);
  const handleClose = () => setOpen(false);

  // 비밀번호, 사원명, 부서, 직급, 이메일, 퇴사여부 수정
  const [editedUserName, setEditedUserName] = useState(editUserData.userName);
  const [editedDept, setEditedDept] = useState(editUserData.deptNo);
  const [editedRank, setEditedRank] = useState(editUserData.rankNo);
  const [editedEmail, setEditedEmail] = useState(editUserData.email);
  const [emailError, setEmailError] = useState(false);
  // const [editedResign, setEditedResign] = useState(editUserData.resign);

  const isValidEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailRegex.test(email);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    if (name === "email") {
      setEditedEmail(value);
      setEmailError(!isValidEmail(value));
    }
  };

  // 사원 정보 수정
  const handleSubmit = (event) => {
    event.preventDefault();

    if (!editedUserName) {
      alert("사원명을 입력해주세요.");
      return;
    }
    if (!editedDept) {
      alert("부서를 선택해주세요.");
      return;
    }
    if (!editedRank) {
      alert("직급을 선택해주세요.");
      return;
    }
    if (!editedEmail) {
      alert("이메일을 입력해주세요.");
      return;
    }
    if (emailError) {
      alert("올바른 이메일 형식이 아닙니다. 이메일을 다시 입력해주세요.");
      return;
    }

    api
      .put(`/admin/user/update/${editUserData.id}`, {
        userName: editedUserName,
        deptNo: editedDept,
        rankNo: editedRank,
        email: editedEmail,
        // resign: editedResign,
      })
      .then(() => {
        handleEditClickSnackbar();
        setCheckedId(checkedId.filter((selectedId) => selectedId !== editUserData.id));
        setEditUser(!editUser);
        handleClose();
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        if (error.response) {
          console.log("통신error");
          alert("사원 정보 수정에 실패했습니다.");
        }
      });
  };

  return (
    <div>
      <Modal open={open} onClose={handleCloseEditModal}>
        <Paper
          sx={{
            position: "absolute",
            width: "450px",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            p: 2,
          }}
        >
          <Box>
            <Box
              flexGrow={1}
              sx={{
                alignItems: "center",
                mb: 5,
                justifyContent: "space-evenly",
                mt: 5,
                px: 5,
              }}
            >
              <Typography
                id="modal-modal-title"
                variant="h3"
                component="h2"
                sx={{ textAlign: "center", mb: 5 }}
              >
                사원 정보 수정
              </Typography>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  justifyContent: "center",
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "100px", textAlign: "center", alignItems: "center", mr: 2 }}
                >
                  <strong>[ Id ]</strong>
                </Typography>
                <TextField
                  value={editUserData.id}
                  sx={{ height: "30px", backgroundColor: "#E5E5E5" }}
                  InputProps={{
                    style: {
                      height: "100%", // TextField 높이 조절
                    },
                    readOnly: true,
                  }}
                  disabled
                />
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  justifyContent: "center",
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "100px", textAlign: "center", mr: 2 }}
                >
                  <strong>[ 이름 ]</strong>
                </Typography>
                <TextField
                  value={editedUserName}
                  onChange={(e) => {
                    setEditedUserName(e.target.value.trim());
                  }}
                  sx={{ height: "30px" }}
                  InputProps={{
                    style: {
                      height: "100%", // TextField 높이 조절
                    },
                  }}
                />
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  justifyContent: "center",
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "100px", textAlign: "center", mr: 2 }}
                >
                  <strong>[ 부서 ]</strong>
                </Typography>
                <FormControl sx={{ width: "200px" }} variant="outlined">
                  <InputLabel id="demo-simple-select-helper-label">부서</InputLabel>
                  <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    label="부서"
                    MenuProps={{
                      PaperProps: {
                        style: {
                          height: "200px",
                          width: "auto",
                        },
                      },
                    }}
                    sx={{ height: "30px" }}
                    value={editedDept}
                    onChange={(e) => {
                      setEditedDept(e.target.value);
                    }}
                  >
                    {deptList
                      .filter((dept) => dept !== "관리부")
                      .map((dept, index) => (
                        <MenuItem key={index} value={index + 20}>
                          {dept}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  justifyContent: "center",
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "100px", textAlign: "center", mr: 2 }}
                >
                  <strong>[ 직급 ]</strong>
                </Typography>
                <FormControl sx={{ width: "200px" }} variant="outlined">
                  <InputLabel id="demo-simple-select-helper-label">직급</InputLabel>
                  <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    label="직급"
                    MenuProps={{
                      PaperProps: {
                        style: {
                          maxHeight: 195,
                          width: "auto",
                        },
                      },
                    }}
                    sx={{ height: "30px" }}
                    value={editedRank}
                    onChange={(e) => {
                      setEditedRank(e.target.value);
                    }}
                  >
                    {rankList.map((rank, index) => (
                      <MenuItem key={index} value={index + 1}>
                        {rank}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  justifyContent: "center",
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "100px", textAlign: "center", mr: 2 }}
                >
                  <strong>[ E-mail ]</strong>
                </Typography>
                <TextField
                  sx={{ height: "30px" }}
                  InputProps={{
                    style: {
                      height: "100%", // TextField 높이 조절
                    },
                  }}
                  value={editedEmail}
                  // onChange={(e) => {
                  //   setEditedEmail(e.target.value.trim());
                  // }}
                  onChange={(e) => {
                    setEditedEmail(e.target.value);
                    setEmailError(!isValidEmail(e.target.value));
                  }}
                  error={emailError}
                  helperText={emailError ? "올바른 이메일 형식이 아닙니다." : ""}
                />
              </Box>
              {/* <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  justifyContent: "center",
                }}
              >
                <Typography
                  id="modal-modal-description"
                  sx={{ width: "100px", textAlign: "center", mr: 2 }}
                >
                  <strong>[ 퇴사 여부 ]</strong>
                </Typography>
                <FormControl sx={{ ml: 2 }}>
                  <InputLabel id="demo-simple-select-label">선택</InputLabel>
                  <Select
                    sx={{ height: "30px" }}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={editedResign}
                    onChange={(e) => {
                      setEditedResign(e.target.value);
                    }}
                    // onChange={handleUsableChange}
                  >
                    <MenuItem value={false}>재직</MenuItem>
                    <MenuItem value={true}>퇴사</MenuItem>
                  </Select>
                </FormControl>
              </Box> */}
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  mb: 4,
                }}
              >
                <Grid container spacing={2}>
                  <Grid item xs={2.75}></Grid>
                  <Grid item xs={3.5}>
                    <Fab
                      color="primary"
                      variant="extended"
                      onClick={(event) => {
                        handleSubmit(event);
                        setEditModalOpen(false);
                      }}
                      disabled={
                        editedUserName === "" ||
                        editedRank === "" ||
                        editedDept === "" ||
                        editedEmail === "" ||
                        emailError
                      }
                    >
                      <AddIcon />
                      <Typography
                        sx={{
                          ml: 0.2,
                          textTransform: "capitalize",
                        }}
                      >
                        수정
                      </Typography>
                    </Fab>
                  </Grid>
                  <Grid item xs={3.5}>
                    <Fab color="error" variant="extended" onClick={handleCloseEditModal}>
                      <DeleteIcon />
                      <Typography
                        sx={{
                          ml: 0.2,
                          textTransform: "capitalize",
                        }}
                      >
                        취소
                      </Typography>
                    </Fab>{" "}
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Box>
        </Paper>
      </Modal>
    </div>
  );
}