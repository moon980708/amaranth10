import React from "react";
import Swal from "sweetalert2";
const ChargeFailModal = ({ onCancel, onClose }) => {
  Swal.mixin({
    customClass: {
      cancelButton: "btn btn-danger",
    },
    width: "45%",
    allowOutsideClick: false,
  })
    .fire({
      title: "청구 실패",
      text: "검증결과가 부적합인 항목이 포함되어 청구할 수 없습니다.",
      icon: "warning",
      confirmButtonText: "확인",
    })
    .then((result) => {
      onCancel();
    });
  // return (
  //   <Dialog open={true} onClose={onClose}>
  //     <DialogContent>
  //       <Typography variant="h4">
  //         검증결과가 부적합인 항목이 포함되어 청구할 수 없습니다.
  //       </Typography>
  //     </DialogContent>
  //     <DialogActions>
  //       <Button variant="contained" onClick={onCancel}>
  //         확인
  //       </Button>
  //     </DialogActions>
  //   </Dialog>
  // );
};

export default ChargeFailModal;
