import React, { useEffect, useState } from "react";
import {
  Button,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Modal,
  Paper,
  Typography,
  Box,
  Card,
  Checkbox,
  IconButton,
  CircularProgress
} from "@mui/material";
import { TreeView, TreeItem } from "@mui/lab";

import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import axios from 'axios';
import UseApi from '../UseApi';
axios.defaults.withCredentials = true;



const GroupManagerModal = ({
  selectedGroupNo,
  isOpen,
  handleClose,
  groupManagerModal,
  setGroupManagerModal,
  handleButton
}) => {

  const api = UseApi();
  const [deptUserList, setDeptUserList] = useState({});      //모달 왼쪽에 띄울 조직도 리스트
  const [selectedEmployees, setSelectedEmployees] = useState([]);      //모달 오른쪽에 띄울 그룹담당자리스트
  const [groupManagerList, setGroupManagerList] = useState([]);     //db에서 가져온 그룹담당자리스트

  
  const [loadingDept, setLoadingDept] = useState(true);
  const [loadingManager, setLoadingManager] = useState(true);


  const handleEmployeeSelect = (department, employee) => {
    //모달안에서 쓰는 담당자리스트
    const employeeIndex = selectedEmployees.findIndex(emp => emp.id === employee.id);

    if (employeeIndex === -1) {  //선택한 리스트에 사원추가
      const uniqueKey = `${department}-${employee.id}`; // 고유한 key 생성
      setSelectedEmployees([...selectedEmployees, {deptName: department, ...employee, key: uniqueKey}]);  
    } else {
      const updatedEmployees = [...selectedEmployees];
      updatedEmployees.splice(employeeIndex, 1);     //선택한 리스트에서 사원삭제
      setSelectedEmployees(updatedEmployees);
    }


    //쿼리를 위한 insert와 delete 리스트
    const managerIndex = groupManagerList.findIndex(emp => emp.id === employee.id);
    
    if (managerIndex === -1) {
      const insertIndex = groupManagerModal.insert.findIndex(id => id === employee.id);

      if (insertIndex === -1) {  //insert배열에 없으면, insert에 추가.
        let newInsert = [...groupManagerModal.insert, employee.id];
        setGroupManagerModal({ ...groupManagerModal, insert: newInsert });
        
      } else {  //insert배열에 없으면, insert에서 빼기
        let updatedInsert = [...groupManagerModal.insert];
        updatedInsert.splice(insertIndex, 1);   
        setGroupManagerModal({ ...groupManagerModal, insert: updatedInsert });
      }
      
    } else {
      const deleteIndex = groupManagerModal.delete.findIndex(id => id === employee.id);

      if (deleteIndex === -1) {  //delete배열에 없으면, delete에 추가.
        let newDelete = [...groupManagerModal.delete, employee.id];
        setGroupManagerModal({ ...groupManagerModal, delete: newDelete });
        
      } else {  //delete배열에 있으면, delete에서 빼기
        let updatedDelete = [...groupManagerModal.delete];
        updatedDelete.splice(deleteIndex, 1);   
        setGroupManagerModal({ ...groupManagerModal, delete: updatedDelete });
      }
    }

  };

  
  const listDeptUser = () => {
    //조직도 불러오기
    setLoadingDept(true);

    api.get("/admin/group/dept/userlist")
      .then((result) => {
        //console.log(result.data);
        let originalData = result.data;

        // 변형된 데이터를 담을 객체
        const data = {};

        // 원본 데이터를 순회하면서 'deptName'에 해당하는 부서를 그룹으로 묶음
        originalData.forEach(item => {
          const deptName = item.deptName;
          if (!(deptName in data)) {
            data[deptName] = [];
          }
          data[deptName].push({
            id: item.id,
            userName: item.userName,
            rankName: item.rankName,
            deptNo: item.deptNo,
            deptName: item.deptName,
            resign: item.resign
          });
        });
        
        setDeptUserList(data);
        setLoadingDept(false);

      }).catch((error) => {
        if(error.response){
          console.log("조직도 불러오기 실패", error);
        }
      });
  }

  const listGroupManager = () => {
    //선택한 그룹의 담당자 불러오기
    setLoadingManager(true);

    const variables = {
      groupNo: selectedGroupNo
    };

    api.post("/admin/group/manager/list", variables)
      .then((result) => {
        setGroupManagerList(result.data);
        setSelectedEmployees(result.data);
        setLoadingManager(false);

      }).catch((error) => {
        if(error.response){
          console.log("선택한 그룹의 담당자리스트 모두 불러오기 실패", error);
        }
      });
  }



  useEffect(() => { 
    if (isOpen) {
      listDeptUser();
      listGroupManager();  

   
      return () => {
        setGroupManagerList([]);
        setSelectedEmployees([]);
        //로딩창 초기화
        setLoadingDept(false);
        setLoadingManager(false);
        console.log('그룹담당자모달 언마운트');
      }
    }
  }, [isOpen]);




  return (
    <div>
      <Modal open={isOpen} onClose={handleClose}>
        <Paper sx={{ position: "absolute", width: "1000px", top: "50%", left: "50%", transform: "translate(-50%, -50%)", p: 2 }}>
          <Box>
            <Box display={"flex"} flexGrow={1}>
              <Card variant='outlined' sx={{ width: "40%", p: 1 }}>
                <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                  <Typography variant="h4" sx={{ margin: 2, fontWeight: 'bold' }}>
                    조직도 리스트
                  </Typography>
                  <Box sx={{ display: "flex", alignItems: "center" }}>
                    <Checkbox
                      size='small'
                      checked={false}
                      disabled={true}
                    />
                    <Typography variant="body2" color="textSecondary" sx={{ alignSelf: "center" }}>
                      퇴사포함
                    </Typography>
                  </Box>
                </Box>
                {loadingDept || loadingManager ?
                  <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(50vh)" }}>
                    <CircularProgress size={50} />
                  </Box>
                :
                  <TreeView
                    sx={{
                      height: "calc(50vh)", overflowX: "hidden", overflowY: "auto"
                    }}
                    defaultCollapseIcon={<span><ExpandLessIcon fontSize='large' /></span>}
                    defaultExpandIcon={<span><ExpandMoreIcon /></span>}
                  >
                    {Object.entries(deptUserList).map(([deptName, employees]) => {
                      return (
                        <TreeItem
                          key={deptName}
                          nodeId={deptName}
                          label={<Typography variant="h6" sx={{ py: 1 }}>{deptName}</Typography>}
                        >
                          {employees[0].id && employees
                            .filter(employee => !employee.resign) // 퇴사포함 체크 여부에 따라 필터링
                            .map((employee) => (
                              <TreeItem
                                key={employee.id}
                                nodeId={`${deptName}-${employee.id}`}
                                label={
                                  <div style={{ display: "flex", alignItems: "center" }}>
                                    <Checkbox
                                      size='small'
                                      color='secondary'
                                      disabled={employee.id === 'admin'}
                                      checked={selectedEmployees.findIndex(e => e.id === employee.id) !== -1}
                                    />
                                    <Typography variant="h6">{employee.userName} ({employee.id}{employee.resign ? "/퇴사" : ""})</Typography>
                                  </div>
                                }
                                sx={{ width: "100%", cursor: "pointer" }}
                                onClick={employee.id === 'admin' ? () => {} : () => handleEmployeeSelect(deptName, employee)} // disabled일 때 빈 함수로 처리
                                disabled={employee.id === 'admin'}
                              />
                            ))}
                        </TreeItem>
                      );
                    })}
                  </TreeView>
                }

              </Card>

              <Card variant='outlined' sx={{ width: "60%", p: 1 }}>
                <Typography variant="h4" sx={{ margin: 2, fontWeight: 'bold' }}>
                  그룹 담당자 리스트
                </Typography>
                {loadingDept || loadingManager ?
                  <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(50vh)" }}>
                    <CircularProgress size={50} />
                  </Box>
                :
                  <List sx={{ height: "calc(50vh)", overflow: "auto" }}>
                    {selectedEmployees.length === 0 ?
                      <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "100%" }}>
                        <Typography color="textSecondary" sx={{ fontSize: "14px" }}>
                          담당자로 설정하실 사원을 선택해 주세요.
                        </Typography>
                      </Box>
                      :
                      (selectedEmployees.map((emp) => (
                        <ListItem key={emp.id} sx={{ py: 0 }} >
                          <ListItemIcon>
                            <span>-</span>
                          </ListItemIcon>
                          <ListItemText
                            primary={<Typography variant="h6">{emp.userName + " (" + emp.deptName + "/" + emp.id}{emp.resign ? "/퇴사)" : ")"}</Typography>}
                            sx={{ flexGrow: 1 }} />
                          <IconButton
                            disabled={emp.id === 'admin'}
                            onClick={() => handleEmployeeSelect(emp.deptName, emp)}
                          >
                            <CloseOutlinedIcon fontSize='small' />
                          </IconButton>
                        </ListItem>
                      )))
                    }
                  </List>
                }
              </Card>
            </Box>
            <Box display="flex" justifyContent="flex-end" sx={{ m: 2 }}>
              <Button variant='outlined'
                onClick={handleClose} sx={{ mr: 1 }}>취소</Button>
              <Button variant='outlined'
                disabled={groupManagerModal.insert.length === 0 && groupManagerModal.delete.length === 0 ? true : false}
                onClick={handleButton}>저장</Button>
            </Box>
          </Box>   
        </Paper>
      </Modal>
    </div>
  );
};

export default GroupManagerModal;


