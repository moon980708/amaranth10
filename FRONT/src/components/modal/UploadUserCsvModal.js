import React, { useState } from "react";
import { useDropzone } from "react-dropzone";
import Papa from "papaparse";
import { useNavigate } from "react-router-dom";
import { Typography, Paper, Modal, Box, TextField, Fab, Grid, Button } from "@mui/material";
import { styled } from "@mui/material/styles";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import Swal from "sweetalert2";
import axios from "axios";
axios.defaults.withCredentials = true; 

const VisuallyHiddenInput = styled("input")`
  clip: rect(0 0 0 0);
  clip-path: inset(50%);
  height: 1px;
  overflow: hidden;
  position: absolute;
  bottom: 0;
  left: 0;
  white-space: nowrap;
  width: 1px;
`;

export default function UploadUserCsvModal({
  handleUploadUserClose, handleNotCsvOpenSnackbar,
  setUserList,
  handleClickSnackbar, submitUser,
  setSubmitUser, api
}) {
  const navigate = useNavigate();

  const [open, setOpen] = useState(true);

  const [tableData, setTableData] = useState([]);
  const [fileName, setFileName] = useState(""); // 업로드된 파일 이름을 저장하는 state

  // 파일 형식 확인 함수
  const isCSVFile = (fileName) => {
    const acceptedFormats = [".csv"];
    const fileExtension = fileName.slice(((fileName.lastIndexOf(".") - 1) >>> 0) + 2);

    return acceptedFormats.includes("." + fileExtension.toLowerCase());
  };

  // CSV 파일 업로드 및 처리
  const onDrop = (acceptedFiles) => {
    const file = acceptedFiles[0];
    setFileName(file.name); // 파일 이름을 업데이트

    // 파일 형식이 .csv인지 확인
    if (isCSVFile(file.name)) {
      Papa.parse(file, {
        complete: (result) => {
          // CSV 파일 파싱 완료 후 데이터를 상태에 저장
          setTableData(result.data);

          // userList 업데이트
          const dataToAdd = result.data.slice(1); // 헤더를 제외한 데이터 행
          setUserList((prevList) => [...prevList, ...dataToAdd]);
        },
        encoding: "UTF-8",
      });
    } else {
      // 파일 형식이 .csv가 아닌 경우
      handleNotCsvOpenSnackbar();
    }
  };

  const handleUploadUserSubmit = () => {
    // CSV 파일에 데이터가 있는지 확인
    if (tableData.length > 0) {
      const headerRow = tableData[0]; // CSV 파일의 헤더 행을 가져옴

      const deptNameList = [
        "총무부",
        "경리부",
        "구매자재부",
        "품질관리부",
        "개발 1팀",
        "개발 2팀",
        "기술부",
        "해외영업부",
        "국내영업부",
        "상품관리부",
      ];
      const rankNameList = ["사원", "대리", "과장", "차장", "부장", "이사", "상무", "전무"];

      // 각 열의 인덱스를 헤더 행에서 찾음
      const idIndex = headerRow.indexOf("Id");
      const passwordIndex = headerRow.indexOf("비밀번호");
      const userNameIndex = headerRow.indexOf("이름");
      const deptNameIndex = headerRow.indexOf("부서");
      const rankNameIndex = headerRow.indexOf("직급");
      const emailIndex = headerRow.indexOf("E-mail");

      // 데이터 행을 반복 (헤더 행은 건너뜁니다)
      for (let i = 1; i < tableData.length; i++) {
        const rowData = tableData[i]; // 현재 데이터 행을 가져옴
        const deptName = rowData[deptNameIndex];
        const rankName = rowData[rankNameIndex];

        // 엑셀에 작성한 부서와 직급이 숫자 형태가 아닌지 확인
        if (!isNaN(deptName) || !isNaN(rankName)) {
          continue;
        }

        const deptIndex = deptNameList.indexOf(deptName);
        const rankIndex = rankNameList.indexOf(rankName);
        const deptNoSubmit = 20 + deptIndex;
        const rankNoSubmit = 1 + rankIndex;

        // csv 파일에서 열을 기반으로 사용자 객체 생성
        const data = JSON.stringify({
          id: rowData[idIndex],
          password: rowData[passwordIndex],
          userName: rowData[userNameIndex],
          deptNo: deptNoSubmit,
          rankNo: rankNoSubmit,
          email: rowData[emailIndex],
        });

        api({
          url: "/admin/user/add",
          method: "post",
          data: data,
          headers: { "Content-type": "application/json" },
        })
          .then(() => {
            // successData++;
            handleClickSnackbar();
            setSubmitUser(!submitUser);
            handleUploadUserClose();
          })
          .catch((error) => {
            // if (error.response && error.response.status === 401) {
            //   alert("로그인세션만료");
            //   navigate("/");
            // }
            if (error.response && error.response.status === 400) {
              setSubmitUser(!submitUser);
              handleUploadUserClose();
              Swal.fire({
                icon: "warning",
                title: "사원 추가 완료",
                text: "일부는 누락된 항목으로 인해 추가되지 않았습니다.",
              });
            }
            if (error.response) {
              console.log("통신 error");
            }
          });
      }
    } else {
      // CSV 파일이 비어있는 경우
      console.log("CSV 파일이 비어 있습니다");
    }
  };

  // Dropzone 컴포넌트 설정
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  // 파일 업로드 버튼을 클릭했을 때 파일 선택 창을 열도록 핸들러 추가
  const handleFileUploadClick = () => {
    const fileInput = document.getElementById("fileInput");
    if (fileInput) {
      fileInput.click(); // input 요소를 클릭하여 파일 선택 창 열기
    }
  };

  return (
    <div>
      <Modal open={open} onClose={handleUploadUserClose}>
        <Paper
          sx={{
            position: "absolute",
            width: "450px",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            p: 2,
          }}
        >
          <Box>
            <Box
              flexGrow={1}
              sx={{
                alignItems: "center",
                mb: 2,
                mt: 5,
                px: 5,
              }}
            >
              <Typography variant="h3" component="h2" sx={{ textAlign: "center", mb: 5 }}>
                사원 CSV 파일 업로드
              </Typography>
            </Box>
            <Box
              sx={{
                display: "center",
                justifyContent: "center",
                alignItems: "center",
                mb: 3,
              }}
            >
              <Button
                component="label"
                variant="contained"
                startIcon={<CloudUploadIcon />}
                sx={{ zIndex: 500, width: "250px" }}
                onClick={handleFileUploadClick}
                color="success"
              >
                파일 업로드
                <VisuallyHiddenInput
                  type="file"
                  accept=".csv"
                  style={{ display: "none" }}
                  {...getInputProps()}
                />
              </Button>
            </Box>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                mt: 5,
                mb: 5,
                justifyContent: "center",
              }}
            >
              <Typography sx={{ width: "100px", textAlign: "center", alignItems: "center", mr: 2 }}>
                파일 이름 :
              </Typography>
              <TextField
                value={fileName}
                sx={{ height: "30px", backgroundColor: "#E5E5E5" }}
                InputProps={{
                  style: {
                    height: "100%", // TextField 높이 조절
                  },
                  readOnly: true,
                }}
              />
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                mb: 5,
              }}
            >
              <Grid container spacing={2}>
                <Grid item xs={2.75}></Grid>
                <Grid item xs={3.5}>
                  <Fab
                    color="primary"
                    variant="extended"
                    // onClick={(event) => {
                    //   handleSubmit(event);
                    //   setEditModalOpen(false);
                    // }}
                    onClick={(e) => {
                      handleUploadUserSubmit(e);
                    }}
                    disabled={tableData.length === 0 || !isCSVFile(fileName)}
                  >
                    <AddIcon />
                    <Typography
                      sx={{
                        ml: 0.2,
                        textTransform: "capitalize",
                      }}
                    >
                      제출
                    </Typography>
                  </Fab>
                </Grid>
                <Grid item xs={3.5}>
                  <Fab color="error" variant="extended" onClick={handleUploadUserClose}>
                    <DeleteIcon />
                    <Typography
                      sx={{
                        ml: 0.2,
                        textTransform: "capitalize",
                      }}
                    >
                      취소
                    </Typography>
                  </Fab>{" "}
                </Grid>
              </Grid>
            </Box>

            {/* {tableData.length > 0 &&
              tableData[0].map((header, index) => <Typography key={index}>{header}</Typography>)}
            {tableData.slice(1).map((row, rowIndex) => (
              <Typography key={rowIndex}>
                {row.map((cell, cellIndex) => (
                  <Typography key={cellIndex}>{cell}</Typography>
                ))}
              </Typography>
            ))} */}
          </Box>
        </Paper>
      </Modal>
    </div>
  );
}
