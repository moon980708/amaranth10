import React, { useState, useEffect } from "react";
import {
  Modal,
  Box,
  Typography,
  IconButton,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TextField,
  TableRow,
  Select,
  MenuItem,
  FormControl,
  CircularProgress,
  InputAdornment,
} from "@mui/material";
import { Close, ExpandMore, ExpandLess, AttachFile } from "@mui/icons-material";
import { LocalizationProvider, DateTimePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import ko from "date-fns/locale/ko"; // 한국어 로케일 import
import { MuiFileInput } from "mui-file-input";
import { addCommas } from "../functions/SimpleFuntion";
import axios from "axios";
import UseApi from "../UseApi";
import { useNavigate } from "react-router-dom";
import { format } from "date-fns";
import { useDispatch, useSelector } from "react-redux";
import ViewImageModal from "./ViewImageModal";
import Swal from "sweetalert2";
axios.defaults.withCredentials = true;

const ChargeDetailModal = ({ isOpen, onCancel, clickDetail, onRefresh }) => {
  const navigate = useNavigate();
  const [editMode, setEditMode] = useState(false);
  const [expanded, setExpanded] = useState(true);
  const [payment, setPayment] = useState("");
  const [charge, setCharge] = useState("");
  const [store, setStore] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [contents, setContents] = useState("");
  const [fileName, setFileName] = useState(null);
  const [realFileName, setRealFileName] = useState(null);
  const [detailData, setDetailData] = useState({});
  const [userChargeNo, setUserChargeNo] = useState("");
  const [detailsData, setDetailsData] = useState([]);
  const [categoryData, setCategoryData] = useState([]);
  const [scheduleData, setScheduleData] = useState([]);
  const [date, setDate] = useState({ min: new Date(), max: new Date() });
  const [saveDate, setSaveDate] = useState(null);
  const [categoryNo, setCategoryNo] = useState("");
  const [categoryDetailForm, setCateogryDetailForm] = useState([]);
  const [detailTime, setDetailTime] = useState(null);
  const [itemContents, setItemContents] = useState([]);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const [totalCharge, setTotalCharge] = useState("");
  const [saveButtonLock, setSaveButtonLock] = useState(true);
  const api = UseApi();
  const [fileToUpload, setFileToUpload] = useState(null);
  const [listLoading, setListLoading] = useState(true);
  const [filePath, setfilePath] = useState("");
  const [imagePath, setImagePath] = useState("");
  const [imageModalOpen, setImageModalOpen] = useState(false);
  const [imageUploadFail, setImageUploadFail] = useState(false);
  const [isFirstEditMode, setIsFirstEditMode] = useState(true);
  const [isFileDeleteButtonClick, setIsFileDeleteButtonClick] = useState(false);
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
      popup: "my-custom-class", // 모달 팝업의 CSS 클래스 이름
    },
    width: "45%",
  });

  useEffect(() => {
    showDetails(clickDetail);
    showDetailsData(clickDetail);
    showCategories();
    scheduleInfo();
  }, [clickDetail]);

  // 수정시, 용도 선택하여 용도 상세 양식 불러오기
  useEffect(() => {
    //처음 한 번만 조건을 걸어주고(itemContents 초기화 방지) 그 이후로는 조건이 없어야 함
    if (editMode === true && isFirstEditMode === true) {
      setIsFirstEditMode(false);
    } else if (editMode === true && isFirstEditMode === false) {
      showCategoryDetailForm(categoryNo);
      setIsFirstEditMode(false);
    } else if (editMode === false && isFirstEditMode === false) {
      setIsFirstEditMode(true);
    }
  }, [editMode, categoryNo]);

  // totalCharge 계산
  useEffect(() => {
    const item112 = itemContents.find((item) => item.itemNo === 112); //입력받은 인원수
    const itemOption112 = categoryDetailForm.find((item) => item.itemNo === 112); //인원수 항목
    const item100 = categoryDetailForm.find((item) => item.itemNo === 100); //금액 항목
    if (itemOption112 && item100) {
      //인원수 항목, 금액 항목이 모두 있을 때,
      if (itemOption112.option !== undefined) {
        //인원수 항목의 조건 값이 있다면
        const calculatedMaximumCharge = Number(itemOption112.option) * Number(item100.option); // 인원수 조건 * 금액 조건
        const calculatedTotalCharge = Number(item112.itemContentsValue) * Number(item100.option); // 입력받은 인원수 * 금액 조건
        if (Number(item112.itemContentsValue) > Number(itemOption112.option)) {
          //입력받은 인원수가 인원수 조건보다 크면
          setTotalCharge(calculatedMaximumCharge);
        } else {
          setTotalCharge(calculatedTotalCharge);
        } // totalCharge 업데이트
      } else {
        //인원수 항목의 조건 값이 없다면 (===undefined)
        const calculatedTotalCharge = Number(item100.option); //금액 조건
        setTotalCharge(calculatedTotalCharge); // totalCharge 업데이트
      }
    } else if (item100) {
      // 금액 항목만 있다면
      const calculatedTotalCharge = Number(item100.option); //금액 조건
      setTotalCharge(calculatedTotalCharge); // totalCharge 업데이트
    } else {
      //인원수 항목, 금액 항목 모두 없다면
      setTotalCharge("");
    }
  }, [itemContents]);

  // 값이 변경될 때마다 유효성 검사 실행
  useEffect(() => {
    checkFormValidity();
  }, [saveDate, categoryNo, charge, totalCharge, categoryDetailForm, itemContents]);

  // 입력 유효성 검사
  const checkFormValidity = () => {
    const isSaveButtonDisabled =
      saveDate === null || //날짜 입력 안받으면
      saveDate > date.max ||
      categoryNo === "" || //용도 입력 안받으면
      charge === "" || //금액 입력 안받으면
      charge === null || //금액 입력 안받으면
      charge < 1 || //금액 0 입력하면
      (totalCharge !== "" && charge > totalCharge) || //청구금액이 totalCharge보다 크면
      categoryDetailForm.some(
        (detail) =>
          detail.essential === true &&
          detail.itemNo !== 100 &&
          itemContents.find((item) => item.itemNo === detail.itemNo)?.itemContentsValue === ""
      ) || //입력 필수 용도상세 입력받지 않으면
      categoryDetailForm.some(
        (detail) =>
          detail.type === "숫자" && //용도상세 타입이 숫자이고
          detail.option !== undefined && //조건값이 있을 때
          Number(itemContents.find((item) => item.itemNo === detail.itemNo)?.itemContentsValue) >
            Number(detail.option) //입력값이 조건값보다 크면
      );
    setSaveButtonLock(isSaveButtonDisabled);
  };

  // 지출 정보 상세 데이터 요청
  const showDetails = (clickDetail) => {
    api
      .get(`/user/charge/${clickDetail}`)
      .then((response) => {
        const detailData = response.data;
        setDetailData(detailData);
        setUserChargeNo(detailData.userChargeNo);
        showCategoryDetailForm(detailData.categoryNo);
        setListLoading(false);
        setfilePath(detailData.filePath);
      })
      .catch((error) => {
        console.error("상세 정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  //용도 상세 데이터 요청
  const showDetailsData = (clickDetail) => {
    api
      .get(`/user/charge/categorydetail`, {
        params: {
          userChargeNo: clickDetail,
        },
      })
      .then((response) => {
        const responseData = response.data[0];
        const viewDetails = responseData.map((item) => ({
          itemTitle: item.ITEM_TITLE,
          itemContentsValue: item.ITEM_CONTENTS,
          itemNo: item.ITEM_NO,
        }));
        setDetailsData(viewDetails);
      })

      .catch((error) => {
        console.error("용도 상세 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 용도 목록 요청
  const showCategories = () => {
    api
      .get("/user/charge/categories")
      .then((response) => {
        const categories = response.data;
        setCategoryData(categories);
      })
      .catch((error) => {
        console.error("용도 목록 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 차수 데이터 요청
  const scheduleInfo = () => {
    api
      .get("/user/charge/schedule")
      .then((response) => {
        const result = response.data;
        setScheduleData(result);
        updateDate(result);
      })
      .catch((error) => {
        console.error("차수 정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 차수에 맞게 선택 가능 날짜 범위 지정
  const updateDate = (result) => {
    const ongoingSchedule = result.find((item) => item.close === "진행중");
    if (ongoingSchedule) {
      setDate({
        min: new Date(ongoingSchedule.expendStart),
        max: new Date(ongoingSchedule.expendEnd),
      });
    }
  };

  // 용도 상세 양식 데이터 요청
  const showCategoryDetailForm = (categoryNo) => {
    api
      .get("/user/charge/categorydetailform", {
        params: {
          categoryNo: categoryNo,
        },
      })
      .then((response) => {
        const responseData = response.data[0];
        const viewDetailForm = responseData.map((item) => ({
          categoryNo: categoryNo,
          itemNo: item.ITEM_NO,
          itemTitle: item.ITEM_TITLE,
          option: item.ITEM_OPTION,
          type: item.ITEM_TYPE,
          essential: item.ITEM_ESSENTIAL,
        }));
        setCateogryDetailForm(viewDetailForm);
        setItemContents(
          viewDetailForm.map((detail) => ({
            itemNo: detail.itemNo,
            itemTitle: detail.itemTitle,
            itemContentsValue: "",
          }))
        );
      })
      .catch((error) => {
        console.error("용도상세 양식 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 정보 수정 컨트롤
  const handleEdit = () => {
    const expendDateValue = new Date(detailData.expendDate);
    if (!isNaN(expendDateValue.getTime())) {
      setSaveDate(expendDateValue);
    }
    setPayment(detailData.payment);
    setCharge(detailData.charge);
    setStore(detailData.store);
    setCategoryName(detailData.categoryName);
    setCategoryNo(detailData.categoryNo);
    setItemContents(detailsData);
    setContents(detailData.contents);
    setFileName(detailData.fileName);
    // setRealFileName(detailData.realFileName);
    setEditMode(true);
  };

  // 정보 저장 컨트롤
  const handleSave = () => {
    const expendDate = format(saveDate, "yyyy-MM-dd HH:mm:ss");
    const scheduleNo = scheduleData[0].scheduleNo;
    const formData = {
      id: loginUserId,
      scheduleNo,
      expendDate,
      payment,
      charge,
      store,
      categoryNo,
      contents,
      fileName: fileToUpload ? fileToUpload.name : detailData.fileName ? detailData.fileName : null,
      itemContents, // 상세 정보 양식 데이터도 추가
    };

    api
      .post(`/user/charge/${clickDetail}`, formData)
      .then((response) => {
        const responseData = response.data;
        if (fileToUpload !== null && responseData !== null) {
          handleFileUpload(responseData);
        }
        showDetails(clickDetail);
        showDetailsData(clickDetail);
        alert("지출 정보가 수정되었습니다.");
        onRefresh();
        setEditMode(false);
      })
      .catch((error) => {
        if (error.response) {
          alert("수정 실패, 다시 시도하세요.");
        }
      });
    // }
    // });
  };

  // 용도 상세 토글 컨트롤
  const handleAccordionToggle = () => {
    setExpanded(!expanded);
  };

  // 파일 저장 컨트롤
  const handleFileChange = (newFile) => {
    setFileToUpload(newFile); // 파일 정보를 상태 변수에 저장
  };

  // 첨부 파일 리액트단에 저장
  const handleFileUpload = (responseData) => {
    const formData = new FormData();
    formData.append("file", fileToUpload);
    formData.append("userChargeNo", userChargeNo);
    api
      .post("/user/charge/fileupdate", formData, {
        headers: {
          "Content-Type": "multipart/form-data", // 요청 헤더에서 Content-Type 설정
        },
      })
      .then((response) => {
        setImagePath("/upload/" + response.data);
        setImageUploadFail(false);
      })
      .catch((error) => {
        console.error("첨부파일 저장 실패", error);
        if (error.response) {
          alert("첨부파일 저장 실패, 다시 첨부해주세요.");
          setImageUploadFail(true);
        }
      });
  };

  const imageOpener = () => {
    // 이미지 파일의 경로 설정
    setImageModalOpen(true);
    if (imagePath === "") {
      setImagePath("/upload/" + detailData.realFileName);
    }
  };

  const handleFileDelete = () => {
    setIsFileDeleteButtonClick(true);
    setFileToUpload(null);
  };

  return (
    <Modal
      open={isOpen}
      onCancel={() => {
        setEditMode(false);
        onCancel();
      }}
      aria-labelledby="modal-title"
      aria-describedby="modal-description"
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: 600, // Increase the width to fit the table
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          borderRadius: 1,
        }}
      >
        <Box sx={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
          <Typography variant="h3" component="h2" id="modal-title" sx={{ margin: 2 }}>
            지출 정보 상세
          </Typography>
          <IconButton onClick={onCancel} aria-label="close">
            <Close />
          </IconButton>
        </Box>
        <TableContainer style={{ maxHeight: "600px" }}>
          {listLoading ? (
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                width: "600",
                height: "500px",
              }}
            >
              <CircularProgress size={50} />
            </Box>
          ) : (
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell>
                    <strong>지출일시</strong>
                    <Typography variant="caption" color="error" marginLeft="3px">
                      *
                    </Typography>
                  </TableCell>
                  <TableCell>
                    {editMode ? (
                      <Box>
                        <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ko}>
                          <DateTimePicker
                            views={["year", "month", "day", "hours", "minutes", "seconds"]}
                            value={saveDate}
                            onChange={(newValue) => {
                              setSaveDate(newValue);
                            }}
                            renderInput={(params) => <TextField {...params} />}
                            sx={{ width: 400, alignContent: "center" }}
                            format="yyyy-MM-dd HH:mm:ss"
                            timeSteps={{ hours: 1, minutes: 1, seconds: 1 }}
                            ampm={false}
                            minDate={date.min}
                            maxDate={date.max}
                          />
                        </LocalizationProvider>
                        <Box>
                          {(saveDate === null || saveDate === "") && (
                            <Typography variant="caption" color="error" marginLeft="5px">
                              지출일시를 입력해주세요.
                            </Typography>
                          )}
                        </Box>
                      </Box>
                    ) : (
                      detailData.expendDate
                    )}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <strong>결제수단</strong>
                  </TableCell>
                  <TableCell>
                    {editMode ? (
                      <Box>
                        <FormControl sx={{ width: "197px" }}>
                          <Select
                            defaultValue={detailData.payment}
                            onChange={(e) => setPayment(e.target.value)}
                            size="small"
                          >
                            <MenuItem value="개인카드">개인카드</MenuItem>
                            <MenuItem value="법인카드">법인카드</MenuItem>
                            <MenuItem value="현금">현금</MenuItem>
                          </Select>
                        </FormControl>
                        <Box>
                          {(payment === "" || payment === null) && (
                            <Typography variant="caption" color="error" marginLeft="5px">
                              결제수단을 선택해주세요.
                            </Typography>
                          )}
                        </Box>
                      </Box>
                    ) : detailData.payment ? (
                      detailData.payment
                    ) : (
                      "-"
                    )}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <strong>사용처</strong>
                  </TableCell>
                  <TableCell>
                    {editMode ? (
                      <Box>
                        <TextField
                          value={store}
                          size="small"
                          onChange={(e) => setStore(e.target.value.trim())}
                        />
                        <Box>
                          {(store === "" || store === null) && (
                            <Typography variant="caption" color="error" marginLeft="5px">
                              사용처를 입력해주세요.
                            </Typography>
                          )}
                        </Box>
                      </Box>
                    ) : detailData.store ? (
                      detailData.store
                    ) : (
                      "-"
                    )}
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>
                    <strong>용도</strong>
                    <Typography variant="caption" color="error" marginLeft="3px">
                      *
                    </Typography>
                  </TableCell>
                  <TableCell>
                    {editMode ? (
                      <Box>
                        <Box sx={{ display: "flex", alignItems: "center" }}>
                          <FormControl sx={{ width: "197px", marginRight: "auto" }}>
                            <Select
                              defaultValue={detailData.categoryNo}
                              onChange={(e) => {
                                const selectedCategory = categoryData.find(
                                  (category) => category.categoryNo === e.target.value
                                );
                                setCategoryNo(selectedCategory.categoryNo);
                                setCategoryName(selectedCategory.categoryName);
                              }}
                              MenuProps={{ style: { maxHeight: "285px" } }}
                              size="small"
                              error={categoryNo === null || categoryNo === ""}
                            >
                              {categoryData.map((category) => (
                                <MenuItem key={category.categoryNo} value={category.categoryNo}>
                                  {category.categoryName}
                                </MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                          <IconButton
                            onClick={handleAccordionToggle}
                            size="small"
                            sx={{ fontSize: "16px" }}
                          >
                            상세 입력 {expanded ? <ExpandLess /> : <ExpandMore />}
                          </IconButton>
                        </Box>
                        <Box>
                          {(categoryNo === null || categoryNo === "") && (
                            <Typography variant="caption" color="error" marginLeft="5px">
                              용도를 선택해주세요.
                            </Typography>
                          )}
                        </Box>
                      </Box>
                    ) : (
                      <Box sx={{ display: "flex", alignItems: "center" }}>
                        <Typography sx={{ marginRight: "auto", fontSize: "14px" }}>
                          {detailData.categoryName}
                        </Typography>
                        <IconButton
                          onClick={handleAccordionToggle}
                          size="small"
                          sx={{ fontSize: "16px" }}
                        >
                          상세 {expanded ? <ExpandLess /> : <ExpandMore />}
                        </IconButton>
                      </Box>
                    )}
                  </TableCell>
                </TableRow>
                {expanded && (
                  <TableRow>
                    <TableCell>
                      <strong>용도상세</strong>
                    </TableCell>

                    {editMode ? (
                      <TableCell
                        sx={{ paddingLeft: "5px", paddingTop: "0px", paddingBottom: "5px" }}
                      >
                        {categoryDetailForm.length === 0 ||
                        (categoryDetailForm.length === 1 &&
                          categoryDetailForm[0].itemNo === 100) ? (
                          <Typography color="gray" marginLeft="15px" marginTop="7px">
                            입력할 상세 정보가 없습니다.
                          </Typography>
                        ) : (
                          <Box>
                            {categoryDetailForm.map((detail, index) => (
                              <Box key={index} margin={"10px"} marginTop={"15px"}>
                                {(detail.type === "숫자" || detail.type === "문자") &&
                                detail.itemNo !== 100 ? (
                                  <TextField
                                    key={index}
                                    size="small"
                                    label={detail.itemTitle}
                                    value={
                                      itemContents.find((item) => item.itemNo === detail.itemNo)
                                        ?.itemContentsValue
                                    }
                                    sx={{ width: "197px" }}
                                    type={detail.type === "숫자" ? "number" : "text"}
                                    inputProps={
                                      detail.type === "숫자"
                                        ? {
                                            min: 0,
                                            max: detail.itemNo === 112 ? detail.option : null,
                                          }
                                        : {}
                                    }
                                    onChange={(e) => {
                                      const updatedContents = itemContents.map((item) =>
                                        item.itemTitle === detail.itemTitle
                                          ? { ...item, itemContentsValue: e.target.value }
                                          : item
                                      );
                                      setItemContents(updatedContents);
                                    }}
                                    error={
                                      detail.type === "숫자" &&
                                      detail.option !== undefined &&
                                      Number(
                                        itemContents.find((item) => item.itemNo === detail.itemNo)
                                          ?.itemContentsValue
                                      ) > Number(detail.option)
                                    }
                                  />
                                ) : detail.type === "시간" ? (
                                  <LocalizationProvider
                                    dateAdapter={AdapterDateFns}
                                    adapterLocale={ko}
                                  >
                                    <DateTimePicker
                                      value={detailTime}
                                      onChange={(newValue) => {
                                        const updatedContents = itemContents.map((item) =>
                                          item.itemNo === detail.itemNo
                                            ? {
                                                ...item,
                                                itemContentsValue: format(
                                                  newValue,
                                                  "yyyy-MM-dd HH:mm"
                                                ),
                                              }
                                            : item
                                        );
                                        setItemContents(updatedContents);
                                      }}
                                      renderInput={(params) => <TextField {...params} />}
                                      sx={{ width: 400, alignContent: "center" }}
                                      format="yyyy-MM-dd HH:mm"
                                      timeSteps={{ hours: 1, minutes: 10 }}
                                      ampm={false}
                                      minDate={date.min}
                                      maxDate={date.max}
                                    />
                                  </LocalizationProvider>
                                ) : null}
                                <Box>
                                  {detail.type === "숫자" &&
                                    categoryDetailForm.some(
                                      (formDetail) =>
                                        formDetail.itemNo === detail.itemNo &&
                                        formDetail.option !== undefined &&
                                        Number(
                                          itemContents.find((item) => item.itemNo === detail.itemNo)
                                            ?.itemContentsValue
                                        ) > Number(formDetail.option)
                                    ) && (
                                      <Typography variant="caption" color="error" marginLeft="5px">
                                        {`${addCommas(detail.option)} 이하의 값을 입력하세요.`}
                                      </Typography>
                                    )}
                                  {detail.essential === true &&
                                    detail.itemNo !== 100 &&
                                    itemContents[index] &&
                                    itemContents[index]?.itemContentsValue === "" && (
                                      <Typography
                                        variant="caption"
                                        color="error"
                                        marginLeft="5px"
                                        key={index}
                                      >
                                        {detail.itemTitle} 입력해주세요.
                                      </Typography>
                                    )}
                                </Box>
                              </Box>
                            ))}
                          </Box>
                        )}
                      </TableCell>
                    ) : (
                      <TableCell sx={{ py: 0 }}>
                        {detailsData.length > 0 ? (
                          <ul>
                            {detailsData.map(
                              (details, index) =>
                                // 입력된 details.itemContentsValue 있는 경우에만 렌더링
                                details?.itemContentsValue !== "" && (
                                  <li key={index}>
                                    <strong>{details.itemTitle} :</strong>{" "}
                                    {details.itemContentsValue}
                                  </li>
                                )
                            )}
                          </ul>
                        ) : (
                          "-"
                        )}
                      </TableCell>
                    )}
                  </TableRow>
                )}
                <TableRow>
                  <TableCell>
                    <strong>금액</strong>
                    <Typography variant="caption" color="error" marginLeft="3px">
                      *
                    </Typography>
                  </TableCell>

                  <TableCell>
                    {editMode ? (
                      <Box>
                        <TextField
                          value={charge}
                          size="small"
                          type="number"
                          inputProps={{ min: 1 }}
                          onChange={(e) => {
                            const newCharge = e.target.value;
                            setCharge(newCharge);
                          }}
                          error={
                            charge === null ||
                            charge === "" ||
                            (categoryDetailForm.some((d) => d.itemNo === 100) && // 금액 조건이 있는 경우
                              // categoryDetailForm.some((d) => d.itemNo === 112) && // 인원 조건 있는 경우
                              charge > totalCharge)
                          }
                        />
                        <Box>
                          {(charge === null || charge === "") && (
                            <Typography variant="caption" color="error" marginLeft="5px">
                              금액을 입력해주세요.
                            </Typography>
                          )}
                          {itemContents.some(
                            (detail) =>
                              // detail.itemNo === 112 && // 인원수 항목을 입력받은 경우
                              totalCharge !== 0 &&
                              categoryDetailForm.some((d) => d.itemNo === 100) && // 금액 항목이 있는 경우
                              charge > totalCharge // totalCharge와 비교하여 에러 메시지를 표시
                          ) && (
                            <Typography variant="caption" color="error" marginLeft="5px">
                              {addCommas(totalCharge)}원 이하의 금액만 청구 가능합니다.
                            </Typography>
                          )}
                        </Box>
                      </Box>
                    ) : (
                      addCommas(detailData.charge) + " 원"
                    )}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell sx={{ width: "65px" }}>
                    <strong>내용</strong>
                  </TableCell>
                  <TableCell>
                    {editMode ? (
                      <TextField
                        fullWidth
                        label="내용"
                        value={contents}
                        onChange={(e) => setContents(e.target.value)}
                        size="small"
                      />
                    ) : detailData.contents ? (
                      detailData.contents
                    ) : (
                      "-"
                    )}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <strong>첨부</strong>
                  </TableCell>
                  <TableCell>
                    {editMode ? (
                      isFileDeleteButtonClick ? (
                        <MuiFileInput
                          value={fileToUpload}
                          onChange={handleFileChange}
                          inputProps={{ accept: ".png, .jpg, .jpeg" }}
                          size="small"
                          fullWidth
                          hideSizeText
                          error={fileToUpload === null}
                        />
                      ) : detailData.fileName ? (
                        <TextField
                          fullWidth
                          defaultValue={detailData.fileName}
                          disabled
                          size="small"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AttachFile />
                              </InputAdornment>
                            ),
                            endAdornment: (
                              <InputAdornment position="end">
                                <IconButton onClick={handleFileDelete} size="small">
                                  <Close fontSize="small" />
                                </IconButton>
                              </InputAdornment>
                            ),
                          }}
                        />
                      ) : (
                        <MuiFileInput
                          value={fileToUpload}
                          onChange={handleFileChange}
                          inputProps={{ accept: ".png, .jpg, .jpeg" }}
                          size="small"
                          fullWidth
                          hideSizeText
                          error={fileToUpload === null}
                        />
                      )
                    ) : detailData.fileName && imageUploadFail !== true ? (
                      <Typography
                        onClick={imageOpener}
                        variant="subtitle2"
                        color={"dodgerblue"}
                        sx={{
                          "&:hover": {
                            cursor: "pointer",
                          },
                          textDecorationLine: "underline",
                        }}
                      >
                        {detailData.fileName}
                      </Typography>
                    ) : (
                      "-"
                    )}
                  </TableCell>
                </TableRow>
                {editMode ? null : (
                  <TableRow>
                    <TableCell>
                      <strong>검증결과</strong>
                    </TableCell>
                    <TableCell>
                      {detailData.store !== "" && detailData.payment !== ""
                        ? "적합"
                        : detailData.store === "" && detailData.payment === ""
                        ? "부적합 (사용처, 결제수단 누락)"
                        : detailData.store === ""
                        ? "부적합 (사용처 누락)"
                        : "부적합 (결제수단 누락)"}
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          )}
        </TableContainer>
        {imageModalOpen && (
          <ViewImageModal onCancel={() => setImageModalOpen(false)} imagePath={imagePath} />
        )}
        {editMode ? (
          <Box sx={{ display: "flex", justifyContent: "flex-end", alignItems: "center", mt: 3 }}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleSave}
              disabled={saveButtonLock}
            >
              저장
            </Button>
            <Button variant="outlined" color="error" onClick={onCancel} sx={{ marginLeft: 1 }}>
              취소
            </Button>
          </Box>
        ) : (
          <Box sx={{ display: "flex", justifyContent: "flex-end", alignItems: "center", mt: 3 }}>
            <Button variant="contained" color="primary" onClick={handleEdit}>
              수정
            </Button>
          </Box>
        )}
      </Box>
    </Modal>
  );
};

export default ChargeDetailModal;
