// import * as React from "react";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import {
  Box,
  Grid,
  Fab,
  Typography,
  Paper,
  CircularProgress,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";

import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import dayjs from "dayjs";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import typography from "../../assets/global/Typography";
import { LocalizationProvider, DatePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import ko from "date-fns/locale/ko"; // 한국어 로케일 import
import axios from "axios";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  change_ScheduleState,
  complete_Modal_Loading,
  reset_Modal_Loading,
} from "../../reducer/module/scheduleReducer";
import { useNavigate } from "react-router-dom";
import UseApi from "../UseApi";
axios.defaults.withCredentials = true;

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "#fff",
  border: "2px solid #000",
  borderRadius: "30px",
  color: "black",
  boxShadow: 24,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  p: 5,
};

// 날짜를 "yyyy/mm/dd" 형식으로 변환하는 함수
// const formatDate = (date) => {
//   return dayjs(date).format("YYYY/MM/DD");
// };
export default function CreateScheduleModal({
  handleCloseModal,
  chargeDateString,
  createScheduleNum,
  chargeDate,
  scheduleLastChargeEndDate,
  payday,
  listLoadingStart,
  setListLoadingStart,
}) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const api = UseApi();

  const [open, setOpen] = useState(true);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  //리듀서의 최근차수가져옴
  const recentSchedule = useSelector(
    (state) => state.scheduleReducer.recentSchedule
  );
  const modalIsLoadgin = useSelector(
    (state) => state.scheduleReducer.modalIsLoading
  );
  console.log("모달에서 리듀서에서 받은 차수값 : " + recentSchedule);
  // console.log(`모달에서 받은 년월: ${chargeDateString}`);

  //
  const [startDate, setStartDate] = useState(
    dayjs(chargeDate).subtract(1, "month").date(1).toDate()
  );
  const [endDate, setEndDate] = useState(
    dayjs(chargeDate).subtract(1, "month").endOf("month").toDate()
  );

  const handleStartDateChange = (date) => {
    setStartDate(date);
  };

  const handleEndDateChange = (date) => {
    setEndDate(date);
  };

  const handelEnableChargeStart = (date) => {
    setEnableChargeStart(date);
    console.log("청구가능시작일 : " + enableChargeStart);
  };

  const handelEnableChargeEnd = (date) => {
    setEnableChargeEnd(date);
    console.log(enableChargeEnd);
  };
  const handelPaydayChange = (date) => {
    setEnablePayday(date);
    console.log("handlePaydayChange : " + enablePayday);
  };

  //청구가능기간 - 백에서 받아온 마지막 차수의 청구 마감일(scheduleLastChargeEndDate)
  const [enableChargeStart, setEnableChargeStart] = useState(
    dayjs(scheduleLastChargeEndDate).toDate()
  );
  const [defaultChargeStart, setDefaultChargeStart] = useState(
    dayjs(scheduleLastChargeEndDate).toDate()
  );
  const [enableChargeEnd, setEnableChargeEnd] = useState(
    dayjs(chargeDate).endOf("month").toDate()
  );
  const [enablePayday, setEnablePayday] = useState(dayjs(payday).toDate());
  // dayjs(enableChargeStart).add(1, "day").toDate();

  //기존 차수의 마감일이
  useEffect(() => {
    //차수가 없어서 백에서 해당 청구년월의 01일을 리턴해주면
    console.log("recentSchedule : " + recentSchedule);
    dispatch(reset_Modal_Loading());
    if (recentSchedule === 1) {
      setEnableChargeStart(enableChargeStart); //청구가능일을 그대로 1일부터
      setDefaultChargeStart(enableChargeStart);
      setEnablePayday(dayjs(payday).toDate());
      // console.log("1일임");
    } else {
      setEnableChargeStart(dayjs(enableChargeStart).add(1, "day").toDate());
      setDefaultChargeStart(dayjs(enableChargeStart).add(1, "day").toDate());
      setEnablePayday(dayjs(payday).add(1, "day").toDate());
      // console.log("1일이 아님");
    }
    dispatch(complete_Modal_Loading());
  }, []);

  // useEffect(() => {
  //   console.log("enableChargeStart : " + enableChargeStart);
  // }, [enableChargeStart]);

  //차수생성 함수
  const createNewSchedule = () => {
    //LocalDateTime으로 바꾸고 넘겨야됨
    const data = {
      yearMonth: dayjs(chargeDateString).format("YYYY-MM-DDTHH:mm:ss"),
      schedule: recentSchedule,
      chargeStart: dayjs(enableChargeStart).format("YYYY-MM-DDTHH:mm:ss"),
      chargeEnd: dayjs(enableChargeEnd).format("YYYY-MM-DDTHH:mm:ss"),
      close: "진행예정",
      expendStart: dayjs(startDate).format("YYYY-MM-DDTHH:mm:ss"),
      expendEnd: dayjs(endDate).format("YYYY-MM-DDTHH:mm:ss"),
      payday: dayjs(enablePayday).format("YYYY-MM-DDTHH:mm:ss"),
    };

    console.log(data);
    api
      .post("/schedule/createschedule", data)
      .then((result) => {
        console.log("차수생성 성공");
        handleCloseModal();
        dispatch(change_ScheduleState());

        setListLoadingStart(!listLoadingStart); //이걸 바꿔줘서 리스트를 다시 통신해서 가져옴 : listLoadingStart를 useEffect를 통해 리스트 가져오기때문
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        console.log("차수생성 실패");
      });
  };

  useEffect(() => {
    console.log("enablePayday : " + enablePayday);
  }, [enablePayday]);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleCloseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Paper
          sx={{
            position: "absolute",
            width: "600px",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            p: 2,
          }}
        >
          {modalIsLoadgin ? (
            <Box
              display="flex"
              alignItems="center"
              justifyContent="center"
              flexGrow={1}
              sx={{ mt: 2, px: 5, height: "366px" }}
            >
              <CircularProgress size={50} />
            </Box>
          ) : (
            <Box flexGrow={1} sx={{ mt: 2, px: 5 }}>
              <Typography
                id="modal-modal-title"
                variant="h3"
                component="h2"
                sx={{ textAlign: "center", mb: 5 }}
              >
                {chargeDateString} {recentSchedule}차수 생성
              </Typography>

              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  width: "100%",
                }}
              >
                <Typography sx={{ mr: 2, width: "105px", textAlign: "center" }}>
                  지출기간
                </Typography>
                <LocalizationProvider
                  dateAdapter={AdapterDateFns}
                  adapterLocale={ko}
                >
                  <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Grid
                      container
                      spacing={2}
                      alignItems="center"
                      sx={{ padding: 0 }}
                    >
                      <Grid item xs={5} lg={5} sx={{ padding: 0 }}>
                        <DatePicker
                          value={startDate}
                          onChange={handleStartDateChange}
                          views={["year", "month", "day"]}
                          format="yyyy-MM-dd"
                          slotProps={{
                            textField: { variant: "outlined", size: "small" },
                          }}
                          maxDate={endDate}
                        />
                      </Grid>
                      <Grid item xs={1} lg={1}>
                        <Typography variant="h3" fontWeight={0}>
                          -
                        </Typography>
                      </Grid>
                      <Grid item xs={5} lg={5} sx={{ padding: 0 }}>
                        <DatePicker
                          value={endDate}
                          onChange={handleEndDateChange}
                          views={["year", "month", "day"]}
                          format="yyyy-MM-dd"
                          slotProps={{
                            textField: { variant: "outlined", size: "small" },
                          }}
                          minDate={startDate}
                          maxDate={dayjs(chargeDate)
                            .subtract(1, "month")
                            .endOf("month")
                            .toDate()}
                        />
                      </Grid>
                    </Grid>
                  </Box>
                </LocalizationProvider>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  width: "100%",
                }}
              >
                <Typography sx={{ mr: 2, width: "105px", textAlign: "center" }}>
                  청구가능기간
                </Typography>
                <LocalizationProvider
                  dateAdapter={AdapterDateFns}
                  adapterLocale={ko}
                >
                  <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Grid container spacing={2} alignItems="center">
                      <Grid item xs={5} lg={5}>
                        <DatePicker
                          //최근 차수의 청구마감일에 +1
                          // dayjs(enableChargeStart).add(1, "day").toDate()

                          value={enableChargeStart}
                          onChange={handelEnableChargeStart}
                          views={["year", "month", "day"]}
                          format="yyyy-MM-dd"
                          slotProps={{
                            textField: { variant: "outlined", size: "small" },
                          }}
                          minDate={
                            defaultChargeStart
                            // dayjs(enableChargeStart).format("YYYY-MM-DD") ===
                            // `${dayjs(enableChargeStart).format("YYYY-MM")}-01`
                            //   ? enableChargeStart
                            //   : dayjs(enableChargeStart).add(1, "day").toDate()
                          } //받은게 1일이면 그대로 보이고 아니면 +1일해서 보임
                          maxDate={enableChargeEnd}
                        />
                      </Grid>
                      <Grid item xs={1} lg={1}>
                        <Typography variant="h3" fontWeight={600}>
                          -
                        </Typography>
                      </Grid>
                      <Grid item xs={5} lg={5}>
                        <DatePicker
                          value={enableChargeEnd}
                          onChange={handelEnableChargeEnd}
                          views={["year", "month", "day"]}
                          format="yyyy-MM-dd"
                          slotProps={{
                            textField: { variant: "outlined", size: "small" },
                          }}
                          minDate={enableChargeStart}
                          maxDate={dayjs(chargeDate).endOf("month").toDate()}
                        />
                      </Grid>
                    </Grid>
                  </Box>
                </LocalizationProvider>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  mb: 5,
                  width: "100%",
                }}
              >
                <Typography sx={{ mr: 2, width: "105px", textAlign: "center" }}>
                  지급일
                </Typography>

                <LocalizationProvider
                  dateAdapter={AdapterDateFns}
                  adapterLocale={ko}
                >
                  <Grid container spacing={2} alignItems="center">
                    <Grid item xs={5} lg={5}>
                      <DatePicker
                        value={enablePayday}
                        onChange={handelPaydayChange}
                        views={["year", "month", "day"]}
                        format="yyyy-MM-dd"
                        slotProps={{
                          textField: { variant: "outlined", size: "small" },
                        }}
                        minDate={dayjs(enablePayday).toDate()}
                      />
                    </Grid>
                  </Grid>
                </LocalizationProvider>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  width: "100%",
                  mb: 2,
                }}
              >
                <Fab
                  color="primary"
                  variant="extended"
                  onClick={createNewSchedule}
                  sx={{ mr: 5 }}
                >
                  <AddIcon />
                  <Typography
                    sx={{
                      ml: 0.2,
                      textTransform: "capitalize",
                    }}
                  >
                    생성
                  </Typography>
                </Fab>
                <Fab
                  color="secondary"
                  variant="extended"
                  onClick={handleCloseModal}
                  sx={{ ml: 5 }}
                >
                  <DeleteIcon />
                  <Typography
                    sx={{
                      ml: 0.2,
                      textTransform: "capitalize",
                    }}
                  >
                    취소
                  </Typography>
                </Fab>
              </Box>
            </Box>
          )}
        </Paper>
      </Modal>
    </div>
  );
}
