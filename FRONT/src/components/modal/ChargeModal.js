import React from "react";
import Swal from "sweetalert2";

const ChargeModal = ({ onCancel, onCharge }) => {
  Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    width: "45%",
    allowOutsideClick: false,
  })
    .fire({
      title: "선택한 항목을 청구하시겠습니까?",
      text: "해당 내역이 청구 내역 조회로 이동됩니다.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "청구",
      cancelButtonText: "취소",
      reverseButtons: false,
    })
    .then((result) => {
      if (result.isConfirmed) {
        Swal.fire("청구 완료", "해당 지출 정보가 청구되었습니다.", "success");
        onCharge();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        onCancel();
        Swal.fire("청구 취소", "신중히 검토 후 청구해주세요.", "error");
      }
    });

  // return (
  //   <Dialog open={true} onCancel={onCancel}>
  //     <DialogContent>
  //       <Typography variant="h4">선택한 항목을 청구하시겠습니까?</Typography>
  //     </DialogContent>
  //     <DialogActions>
  //       <Button variant="contained" color="error" onClick={onCharge}>
  //         예
  //       </Button>
  //       <Button variant="contained" onClick={onCancel}>
  //         아니오
  //       </Button>
  //     </DialogActions>
  //   </Dialog>
  // );
};

export default ChargeModal;
