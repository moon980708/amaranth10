import React, { useEffect, useState } from "react";
import { Box, MenuItem, Fab, Button, Menu, Typography, Divider } from "@mui/material";
import { CSVLink } from "react-csv";   // 해당 라이브러리의 CSVLink 컴포넌트를 사용하여 CSV 데이터를 생성하고 내보내기
import { useDropzone } from "react-dropzone";
import Papa from "papaparse";
import PostAddIcon from "@mui/icons-material/PostAdd";
import AdfScannerIcon from "@mui/icons-material/AdfScanner";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import UploadUserCsvModal from "../modal/UploadUserCsvModal"
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
axios.defaults.withCredentials = true; 

export default function CreateUserCsvModal({
  setUserList,
  handleClickSnackbar,
  handleNotCsvOpenSnackbar,
  submitUser,
  setSubmitUser,
  deptNo,
  rankNo,
  resign,
  keyword,
  api,
  deptList, rankList
}) {
  const navigate = useNavigate();
  const [openCsv, setOpenCsv] = useState(null);
  const open = Boolean(openCsv);

  const [saveUserList, setSaveUserList] = useState([]);
  const [formatTableData, setFormatTableData] = useState([]);

  const handleClick = (event) => {
    setOpenCsv(event.currentTarget);
  };

  const handleClose = () => {
    setOpenCsv(null);
  };

  useEffect(() => {
    const fetchSaveUserList = async () => {
      try {
        const result = await api.get("/admin/user/save", {
          params: {
            deptNo: deptNo,
            rankNo: rankNo,
            resign: resign,
            keyword: keyword,
          },
        });
        setSaveUserList(result.data);
      } catch (error) {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인 세션이 만료되었습니다");
        //   navigate("/");
        // }
        if (error.response) {
          alert("다시 로딩해주세요.");
        }
      }
    };

    fetchSaveUserList();
  }, [openCsv]);

  // CSV 내보내기 함수
  const userCSVData = (data) => {
    // data가 배열이 아니거나 null인 경우 빈 배열로 초기화
    if (!Array.isArray(data) || data === null) {
      data = [];
    }

    const header = ["Id", "이름", "부서", "직급", "E-mail", "재직상태"];
    const rows = data.map((user) => [
      user.id,
      user.userName,
      user.deptName,
      user.rankName,
      user.email,
      user.resign ? "퇴사" : "재직",
    ]); // 데이터 행

    return [header, ...rows];
  };

  // const userCSVFormatData = () => {
  //   // const header = ["Id", "이름", "부서", "직급", "E-mail", "재직상태", "", "", "", "", "", "", "", ""];
  //   const header = ["", "", "", "", "", "", "", "", "", "부서번호", "부서명", "", "직급번호", "직급명"]; 
  //   // const rows = deptList.map((dept, index) => ["", "", "", "", "", "", "", "", "", 20 + index, dept]);
  //   const rows = deptList.map((dept, index) => {
  //     if (index < 8) {
  //       return ["", "", "", "", "", "", "", "", "", 20 + index, dept, "", rankList.indexOf(rankList[index]) + 1, rankList[index]];
  //     } else {
  //       return ["", "", "", "", "", "", "", "", "", 20 + index, dept];
  //     }
  //   });

  //   rows.push([], [], ["", "", "", "", "", "", "", "", "", "사원 목록 업로드 시 해당 부분은 제외 후 업로드 해주세요."]);

  //   return [header, ...rows];
  // };
  
  const userCSVFormatData = () => {
  const header = ["Id", "이름", "부서", "직급", "E-mail", "재직상태", "", "", "", "", "", "", "", ""];
  const rows = Array.from({ length: 24 }, (_, index) => {
    let row;
    if (index < 13) {
      row = ["", "", "", "", "", "", "", "", "", "", "", "", "", ""];
    } else if (index === 13) {
      row = ["", "", "", "", "", "", "", "", "", "부서번호", "부서명", "", "직급번호", "직급명"];
    } else if (index >= 14 && index < 22) {
      row = ["", "", "", "", "", "", "", "", "", index+6, deptList[index-13], "", rankList.indexOf(rankList[index - 14]) +1, rankList[index - 14]];
    } else {
      row = ["", "", "", "", "", "", "", "", "", index+6, deptList[index-13]];
    }
    return row;
  });

  rows.push([], [], ["", "", "", "", "", "", "", "", "", "사원 목록 업로드 시 해당 부분은 제외 후 업로드 해주세요."]);

  return [header, ...rows];
};

  // CSV 파일 업로드 state
  const [tableData, setTableData] = useState([]);
  const [tempData, setTempData] = useState([]); // 임시 데이터 상태 추가

  // 사원목록 CSV 업로드
  const [openUploadModal, setOpenUploadModal] = useState(false);

  const handleUploadUserClick = () => {
    setOpenUploadModal(true);
  };

  const handleUploadUserClose = () => {
    setOpenUploadModal(false);
  };

  return (
    <div>
      <Box
        sx={{
          textAlign: "center",
          height: "100%",
          color: "#000",
        }}
      >
        <Fab
          color="secondary"
          variant="extended"
          onClick={handleClick}
          sx={{
            zIndex: 500,
            height: "50px",
            borderRadius: "30px",
          }}
          aria-controls={open ? "csv-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              ml: 0.75,
            }}
          >
            <PostAddIcon />
          </Box>
          <Typography
            sx={{
              ml: 1,
              mr: 0.75,
              textTransform: "capitalize",
            }}
          >
            {" "}
            <b> Excel </b>
          </Typography>
        </Fab>
      </Box>

      <Menu
        id="csv-menu"
        anchorEl={openCsv}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "csv-button",
        }}
      >
        <MenuItem onClick={handleClose}>
          <CSVLink
            data={userCSVFormatData(deptList)}
            filename={"UserList_Format.csv"}
            style={{ textDecoration: "none", color: "black" }}
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <AssignmentIndIcon />
              <Typography sx={{ ml: 1 }}>CSV 형식 다운로드</Typography>
          </Box>
          </CSVLink>
        </MenuItem>
        <Divider />
        <MenuItem
          onClick={() => {
            handleUploadUserClick();
            handleClose();
          }}
        >
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <FileUploadIcon />
            <Typography sx={{ ml: 1 }}>사원 목록 업로드</Typography>
          </Box>
        </MenuItem>
        <Divider />
        <MenuItem onClick={handleClose}>
          <CSVLink
            data={userCSVData(saveUserList)}
            filename={"User_List.csv"}
            style={{ textDecoration: "none", color: "black" }}
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <FileDownloadIcon />
              <Typography sx={{ ml: 1 }}>사원 목록 다운로드</Typography>
            </Box>
          </CSVLink>
        </MenuItem>
      </Menu>

      {openUploadModal === true ? (
        <UploadUserCsvModal
          handleUploadUserClose={handleUploadUserClose}
          setUserList={setUserList}
          handleClickSnackbar={handleClickSnackbar}
          submitUser={submitUser}
          setSubmitUser={setSubmitUser}
          handleNotCsvOpenSnackbar={handleNotCsvOpenSnackbar}
          api={api}
        />
      ) : null}
    </div>
  );
}

