import React from "react";
import Swal from "sweetalert2";

const ChargeDeleteModal = ({ onCancel, onDelete }) => {
  Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    width: "45%",
    allowOutsideClick: false,
  })
    .fire({
      title: "선택한 항목을 삭제하시겠습니까?",
      // text: "지출 정보 리스트에서 삭제됩니다.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "삭제",
      cancelButtonText: "취소",
      reverseButtons: false,
    })
    .then((result) => {
      if (result.isConfirmed) {
        Swal.fire("삭제 완료", "해당 지출 정보가 삭제되었습니다.", "success");
        onDelete();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        onCancel();
        Swal.fire("삭제 취소", "", "error");
      }
    });

  // return (
  //   <Dialog open={true} onCancel={onCancel}>
  //     <DialogContent>
  //       <Typography variant="h4">선택한 항목을 삭제하시겠습니까?</Typography>
  //     </DialogContent>
  //     <DialogActions>
  //       <Button variant="contained" color="error" onClick={onDelete}>
  //         예
  //       </Button>
  //       <Button variant="contained" onClick={onCancel}>
  //         아니오
  //       </Button>
  //     </DialogActions>
  //   </Dialog>
  // );
};

export default ChargeDeleteModal;
