import { Dialog, DialogActions, DialogContent, Button } from "@mui/material";
import { useEffect } from "react";

const ViewImageModal = ({ onCancel, imagePath }) => {
  // useEffect(() => {
  //   console.log(imagePath);
  // }, [imagePath]);

  return (
    <Dialog open={true} onClose={onCancel}>
      <DialogContent align="center">
        <img src={imagePath} width="100%" alt="첨부 파일"></img>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" color="secondary" onClick={onCancel}>
          닫기
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ViewImageModal;
