import React from "react";
import Swal from "sweetalert2";

const ChargeCancelModal = ({ onCancel, onDelete }) => {
  Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    width: "45%",
    allowOutsideClick: false,
  })
    .fire({
      title: "선택한 항목의 청구 내역을 삭제하시겠습니까?",
      text: "삭제하면 재청구할 수 없습니다.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "삭제",
      cancelButtonText: "취소",
      reverseButtons: false,
    })
    .then((result) => {
      if (result.isConfirmed) {
        Swal.fire("청구 삭제 완료", "해당 청구 내역이 삭제되었습니다.", "success");
        onDelete();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        onCancel();
        Swal.fire("삭제 취소", "", "error");
      }
    });
};

// const ChargeCancelModal = ({ onCancel, onDelete }) => {
//   return (
//     <Dialog open={true} onCancel={onCancel}>
//       <DialogContent>
//         <Typography variant="h4">선택한 항목의 청구를 취소하시겠습니까?</Typography>
//         <Typography variant="subtitle2">취소하면 재청구할 수 없습니다.</Typography>
//       </DialogContent>
//       <DialogActions>
//         <Button variant="contained" color="error" onClick={onDelete}>
//           예
//         </Button>
//         <Button variant="contained" onClick={onCancel}>
//           아니오
//         </Button>
//       </DialogActions>
//     </Dialog>
//   );
// };

export default ChargeCancelModal;
