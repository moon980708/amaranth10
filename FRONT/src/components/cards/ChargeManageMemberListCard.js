import React, { useState } from "react";
import { Box, Typography, Card, CardContent } from "@mui/material";

import { addCommas } from "../functions/SimpleFuntion";

const ChargeManageMemberListCard = ({summaryUserCharge, selectedUserId, setSelectedUserId}) => {

  return (
    <Box
      sx={{
        overflow: "auto",
        height: "calc(67vh)",
        my: 3,
      }}
    >

      {summaryUserCharge.length === 0 ?
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width:"100%", height:"100%" }}>
          <Typography color="textSecondary" sx={{ fontSize: "14px" }}>
            사원 결과 없음.
          </Typography>
        </Box>
      :
        (summaryUserCharge.map((userCharge) => (
          <Box
            key={userCharge.id}
            sx={{
              display: "flex",
              alignItems: "center",
              width: "100%",
              justifyContent: "space-between",
            }}
          >
            <Card
              variant="outlined"
              sx={{
                width: "100%",
                p: 0,
                mb: 2,
                mt: 0,
                backgroundColor:
                  selectedUserId === userCharge.id ? "aliceblue" : "white",
              }}
              onClick={() => setSelectedUserId(userCharge.id)}
              onMouseEnter={(e) => {
                e.currentTarget.style.cursor = "pointer";
              }}
              onMouseLeave={(e) => {
                e.currentTarget.style.cursor = "default";
              }}
            >
              <CardContent sx={{ alignItems: "center", display: "flex", m: 0 }}>
                {userCharge.id === 'total' ?
                  <Box>
                    <Typography sx={{ fontWeight: "bold", pb: 0.5, fontSize: "15px", mt:1 }}>
                      전체
                    </Typography>
                    <Typography sx={{ color: "grey.700", fontSize: "13px" }}>
                      {userCharge.count}건 / {addCommas(userCharge.sum)}원
                    </Typography>
                  </Box>
                :
                  <Box>
                    <Typography sx={{ fontSize: "11px" }}>
                      {userCharge.deptName}
                    </Typography>
                    <Typography sx={{ fontWeight: "bold", pb: 0.5, fontSize: "15px" }}>
                      {userCharge.userName} ({userCharge.id}{userCharge.resign?"/퇴사":""})
                    </Typography>
                    <Typography sx={{ color: "grey.700", fontSize: "13px" }}>
                      {userCharge.count}건 / {addCommas(userCharge.sum)}원
                    </Typography>
                  </Box>
                }
              </CardContent>
            </Card>
          </Box>
        )))
      }

    </Box>
  );

};

export default ChargeManageMemberListCard;
