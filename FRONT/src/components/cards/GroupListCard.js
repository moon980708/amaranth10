import React, { useState } from 'react';
import {
  Box,
  Typography,
  Card,
  CardContent,
  Menu,
  MenuItem,
  IconButton
} from '@mui/material';
import Swal from "sweetalert2";
import axios from 'axios';
import UseApi from '../UseApi';

import MoreVertOutlinedIcon from '@mui/icons-material/MoreVertOutlined';
import GroupAddModal from '../modal/GroupAddModal';
axios.defaults.withCredentials = true;



const GroupListCard = ({groups, selectedGroupNo, setSelectedGroupNo, refresh}) => {
  const api = UseApi();
  const options1 = ["수정", "사용", "삭제"];
  const options2 = ["수정", "미사용", "삭제"];

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClickMenu = (event, groupNo) => {
    setSelectedGroupNo(groupNo);
    setAnchorEl(event.currentTarget);
    event.stopPropagation();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const groupMenu = (usable, name) => {
    let options = usable ? options2 : options1;

    const handleMenuItemClick = (option) => {
      handleClose();
      console.log(selectedGroupNo, ": ", option);

      switch (option) {
        case '수정':
          setGroupAddModal({ state: 'edit', groupName: name });
          setIsAddModalOpen(true);
          break;
        case '삭제':
          handleDeleteGroup(name);
          break;
        default: handleUsableChange(usable, name);
          break;
      }

    };

    return (
      options.map((option) => (
        <MenuItem
          key={option}
          onClick={() => handleMenuItemClick(option)}
        >
          {option}
        </MenuItem>
      ))
    );
  };

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
      popup: "my-custom-class", // 모달 팝업의 CSS 클래스 이름
    },
    width: "45%",
  });

  const handleEditGroupName = () => {
    setIsAddModalOpen(false);
    console.log(selectedGroupNo, "/그룹이름 수정:", groupAddModal.groupName);
    //그룹이름 수정 axios호츌

    const variables = {
      groupNo: selectedGroupNo,
      groupName: groupAddModal.groupName
    }

    api.put("/admin/group/update", variables)
      .then((result) => {
        swalWithBootstrapButtons.fire('그룹이름 수정 완료',
          "'" + groupAddModal.groupName + "' 로 수정되었습니다.", 'success');

        refresh();
          
      }).catch((error) => {
        if (error.response) {
          console.log("그룹이름 수정 실패");
          swalWithBootstrapButtons.fire('그룹이름 수정 실패', '다시 시도해 주세요.', 'error');
        }
      });

  }

  const handleDeleteGroup = (name) => {
    swalWithBootstrapButtons.fire({
      icon: 'warning',
      title: "'" + name + "' 그룹을 삭제하시겠습니까?",
      text: "해당 사항은 복구가 불가합니다.",
      denyButtonText: "삭제",
      cancelButtonText: "취소",
      showConfirmButton: false,
      showDenyButton: true,
      showCancelButton: true,
       
    }).then((result) => {
      if (result.isDenied) {
        console.log(selectedGroupNo, "를 삭제");
        //그룹 삭제 axios호츌

        api.delete("/admin/group/delete/"+ selectedGroupNo)
          .then((result) => {
            swalWithBootstrapButtons.fire('그룹 삭제 완료',
              "'" + name + "' 그룹이 삭제되었습니다.", 'success');

            setSelectedGroupNo(null);     //삭제했으니까

            refresh();
           
          }).catch((error) => {
            
            if(error.response){
              console.log("그룹 삭제 실패");
              swalWithBootstrapButtons.fire('그룹 삭제 실패', '다시 시도해 주세요.', 'error');
            }
          });

      }
    })
  }

  const handleUsableChange = (usable, name) => {
    swalWithBootstrapButtons.fire({
      icon: 'warning',
      title: usable?"'"+name+"' 그룹을 미사용하시겠습니까?" : "'"+name+"' 그룹을 사용하시겠습니까?",
      confirmButtonText: usable?"미사용":"사용",
      cancelButtonText: "취소",
      showCancelButton: true,
       
    }).then((result) => {
      if (result.isConfirmed) {
        console.log(selectedGroupNo, "를 ", usable ? "미사용" : "사용");
        //그룹 사용여부 변경 axios호출

        const variables = {
          groupNo: selectedGroupNo,
          usable: !usable
        }
    
        api.put("/admin/group/update", variables)
          .then((result) => {
            if (usable) {
              swalWithBootstrapButtons.fire("그룹정보 수정 완료", "'"+name+"' 그룹이 미사용 처리되었습니다.", 'success');
            } else {
              swalWithBootstrapButtons.fire("그룹정보 수정 완료", "'"+name+"' 그룹이 사용 처리되었습니다.", 'success');
            }

            refresh();
            
          }).catch((error) => {
            //alert(error.response.data.error.message);
            
            if(error.response){
              console.log("그룹사용여부 수정 실패");
              swalWithBootstrapButtons.fire('그룹정보 수정 실패', '다시 시도해 주세요.', 'error');
              
            }
          });
 
      }
    })

  }

  const [isAddModalOpen, setIsAddModalOpen] = useState(false);    //그룹이름 수정모달
  const [groupAddModal, setGroupAddModal] = useState({state:'edit', groupName:""});


  return (
    <Box
      sx={{
        overflowX: "auto",
        mb: 2,
        display: 'flex',
      }}
    >
      {groups.length === 0 ?
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width:"100%", height: 'calc(10vh)', }}>
          <Typography color="textSecondary" sx={{ fontSize: "14px" }}>
            결과 없음.
          </Typography>
        </Box>
      :
        (groups.map((group) => (
          <Box
            key={group.groupNo}
          >
            <Card
              variant='outlined'
              sx={{
                minWidth: '200px',
                minHeight: 'calc(12vh)',
                p: 1,
                mt: 0,
                backgroundColor: selectedGroupNo === group.groupNo ? "aliceblue" : "white"
              }}
              onClick={() => {
                !open && setSelectedGroupNo(group.groupNo)
              }}
              onMouseEnter={(e) => { e.currentTarget.style.cursor = "pointer" }}
              onMouseLeave={(e) => { e.currentTarget.style.cursor = "default" }}
            >
              <CardContent
                sx={{ alignItems: 'center', display: "flex", px:1 }}
              >
                <Box>
                  <Typography variant='h5' sx={{ fontWeight: 'bold', mb: 1 }}>
                    {group.groupNo}. {group.groupName}
                  </Typography>
                  <Typography variant='subtitle2' sx={{ color: 'grey.700' }}>
                    {group.count}명 / {group.usable ? "사용" : "미사용"}
                  </Typography>
                </Box>
  
                <Box
                  sx={{
                    marginLeft: "auto",
                    alignSelf: "flex-start"
                  }}
                >
                  <IconButton
                    aria-expanded={open ? "true" : undefined}
                    aria-haspopup="true"
                    onClick={(e) => handleClickMenu(e, group.groupNo)}
                    size='small'
                  >
                    <MoreVertOutlinedIcon />
                  </IconButton>
                  <Menu
                    id="long-menu"
                    MenuListProps={{
                      "aria-labelledby": "long-button",
                    }}
                    anchorEl={anchorEl}
                    open={open && selectedGroupNo === group.groupNo}
                    onClose={handleClose}
                    anchorOrigin={{
                      vertical: "bottom",
                      horizontal: "right",
                    }}
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                  >
                    {groupMenu(group.usable, group.groupName)}
                  </Menu>
                </Box>
                
              </CardContent>
            </Card>
          </Box>
        )))
      }


      <GroupAddModal
        isOpen={isAddModalOpen}
        handleClose={() => setIsAddModalOpen(false)}
        groupAddModal={groupAddModal}
        setGroupAddModal={setGroupAddModal}
        handleButton={handleEditGroupName}
      />

    </Box>
  );
}

export default GroupListCard;
