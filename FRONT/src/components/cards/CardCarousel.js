import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { BsArrowLeft, BsArrowRight } from "react-icons/bs";
import { Card, Box, CircularProgress, Typography } from "@mui/material";
import axios from "axios";
import UseApi from "../UseApi";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
axios.defaults.withCredentials = true;

const CardCarousel = ({ ongoingSchedule }) => {
  const navigate = useNavigate();
  const [scheduleData, setScheduleData] = useState([]);
  const [listLoading, setListLoading] = useState(true);
  const api = UseApi();

  useEffect(() => {
    cardData();
  }, []);

  const cardData = () => {
    api
      .get("/user/charge/schedule")
      .then((response) => {
        const result = response.data;
        setScheduleData(result);
        setListLoading(false);
      })
      .catch((error) => {
        console.error("차수 정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 이전 화살표 아이콘 컴포넌트
  const PrevArrow = (props) => (
    <div {...props} style={{ position: "absolute", left: "-10px", top: "50%", zIndex: 1 }}>
      <BsArrowLeft size={40} />
    </div>
  );

  // 다음 화살표 아이콘 컴포넌트
  const NextArrow = (props) => (
    <div {...props} style={{ position: "absolute", right: "-10px", top: "50%", zIndex: 1 }}>
      <BsArrowRight size={40} />
    </div>
  );

  // 캐러셀 설정
  const settings = {
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: <PrevArrow />, // 이전 화살표 아이콘 컴포넌트 사용
    nextArrow: <NextArrow />, // 다음 화살표 아이콘 컴포넌트 사용
  };

  const firstScheduleIsNotOngoing = scheduleData.length > 0 && scheduleData[0].close !== "진행중";

  if (firstScheduleIsNotOngoing) {
    ongoingSchedule(firstScheduleIsNotOngoing);
  }

  return (
    <Box>
      <Card
        style={{
          backgroundColor: "aliceblue",
          paddingTop: "5px",
          paddingBottom: "0",
          height: "162px",
        }}
      >
        {scheduleData.length === 0 ? (
          <Typography textAlign="center">등록된 차수 정보가 없습니다.</Typography>
        ) : (
          ""
        )}
        {listLoading ? (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "100%",
              height: "162px",
            }}
          >
            <CircularProgress size={50} />
          </Box>
        ) : (
          <Slider {...settings}>
            {scheduleData.map((result, index) => (
              <div key={index} className="Card">
                <Card
                  className="CardContent"
                  style={{ textAlign: "center" }}
                  sx={
                    result.close === "진행중"
                      ? { backgroundColor: "lightskyblue", height: "100px" }
                      : { height: "100px" }
                  }
                >
                  <h2 style={{ marginTop: 13, marginBottom: 3 }}>
                    {result.year}년 {result.month}월 {result.schedule}차
                  </h2>
                  <div style={{ marginTop: 10 }}>
                    지출기간 [{result.expendStart.substring(5)} ~ {result.expendEnd.substring(5)}]
                  </div>
                  <div>
                    청구기간 [{result.chargeStart.substring(5)} ~ {result.chargeEnd.substring(5)}]
                  </div>
                </Card>
              </div>
            ))}
          </Slider>
        )}
      </Card>
    </Box>
  );
};

export default CardCarousel;
