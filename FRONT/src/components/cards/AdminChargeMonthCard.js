import React, {useState, useEffect} from 'react';
import { Card, CardContent, Typography, Button, Grid, Box, IconButton } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import axios from 'axios';
import UseApi from '../UseApi';

import NotificationsIcon from '@mui/icons-material/Notifications';
import ChargeMonthPieChart from '../charts/ChargeMonthPieChart';
import { addCommas } from '../functions/SimpleFuntion';
axios.defaults.withCredentials = true;



const AdminChargeMonthCard = () => {
  const navigate = useNavigate();
  const api = UseApi();
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const mainLoading = useSelector((state) => state.adminMainReducer.mainLoading);
  const [waitList, setWaitList] = useState({ totalCount: 0, totalSum: 0 });


  const listWait = () => {
    api.get("/admin/main/waitlist", {
      params: {
        managerId: loginUserId
      }
    })
      .then((result) => {
        setWaitList({
          totalCount: result.data.totalCount,
          totalSum: result.data.totalSum
        });
      }).catch((error) => {
        if (error.response) {
          console.log("현재 승인대기 내역불러오기 실패", error);
        }
      });
  }

  useEffect(() => { 
    if (loginUserId) {
      listWait();
    }
  }, [loginUserId, mainLoading]);



  return (
    <Grid container spacing={2} height={"100%"} sx={{alignContent:"space-between"}}>

      <Grid item xs={12} lg={12} sx={{height:"20%"}}>
        <Card
          variant='outlined'
          sx={{
            height: "75%", 
            boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
          }}
        >
          <CardContent >
            <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", textAlign: "center", width: "100%" }}>
              <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", textAlign: "center"}}>
                <IconButton>
                  <NotificationsIcon color="warning" />
                </IconButton>
                <Typography sx={{fontSize:'15px', fontWeight: 'bold'}}>
                  승인 대기중인 청구 내역 :
                </Typography>
                <Typography sx={{ fontWeight: 'bold', fontSize:'20px'}}>
                  &nbsp;&nbsp;{ waitList.totalCount }건
                </Typography>
                <Typography sx={{color: 'grey.700', fontSize:'13px', }}>
                  &nbsp;/ { addCommas(waitList.totalSum) }원
                </Typography>
              </Box>

              <Box sx={{ marginLeft: "auto" }}>
                <Button onClick={()=>navigate("/admin/chargemanage")} variant='outlined' color='primary'>바로가기</Button>
              </Box>
            </Box>

          </CardContent>
        </Card>
      </Grid>


      <Grid item xs={12} lg={12} sx={{height:"75%"}}>
        
        <ChargeMonthPieChart />

      </Grid>

    </Grid>
  )
}

export default AdminChargeMonthCard;



