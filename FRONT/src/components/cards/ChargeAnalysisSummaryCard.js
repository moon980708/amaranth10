import React from 'react';
import { Card, CardContent, Typography, Grid, Box } from "@mui/material";
import { addCommas } from '../functions/SimpleFuntion';


const ChargeAnalysisSummaryCard = ({summaryCharge}) => {
  return (
    <Grid container spacing={2} height={"100%"}>
      <Grid item xs={12} lg={12}>
        <Card
          variant='outlined'
          sx={{
            height: "75%"
          }}
        >
          <CardContent
            sx={{height:"100%", pt:0}}
          >
            <Box sx={{ display:"flex", flexDirection:"column", justifyContent:"center", alignItems:"center", textAlign:"center", height:"100%"}}>
              <Typography sx={{ fontWeight: 'bold', fontSize:'17px', mb:2 }}>
                총 경비청구
              </Typography>
              <Typography sx={{fontWeight: 'bold', fontSize:'22px', mb:0.5}}>
                {addCommas(summaryCharge.total.sum)}원
              </Typography>
              <Typography sx={{color: 'grey.700', fontSize:'15px'}}>
                / {summaryCharge.total.count}건
              </Typography>
            </Box>
          </CardContent>
        </Card>
      </Grid>

      <Grid item xs={12} lg={12}>
        <Card
          variant='outlined'
          sx={{
            height: "85%",
            mt: 0
          }}
        >
          <CardContent
            sx={{display:"flex", justifyContent:"space-between", height:"100%", pt:0}}
          >
            <Box sx={{p:0, display:"flex", flexDirection:"column", justifyContent:"center", alignItems:"center", textAlign:"center", width:"100%"}}>
              <Typography sx={{ fontSize:'15px', fontWeight: 'bold', mb:2 }}>
                최다 청구부서
              </Typography>
              {summaryCharge.most ?
                <>
                  <Typography sx={{ fontWeight: 'bold', fontSize: '20px', mb: 0.5 }}>
                    {summaryCharge.most.name}
                  </Typography>
                  <Typography sx={{ color: 'grey.700', fontSize: '13px' }}>
                    {summaryCharge.most.count}건 / {addCommas(summaryCharge.most.sum)}원
                  </Typography>
                </>
              :
                <>
                  <Typography sx={{ fontWeight: 'bold', fontSize: '20px', mb: 0.5 }}>
                    -
                  </Typography>
                  <Typography sx={{ color: 'grey.700', fontSize: '13px' }}>
                    -건 / -원
                  </Typography>
                </>
              }
            </Box>

            <hr style={{marginLeft:"16px", marginRight:"16px", color:'grey.700'}} />

            <Box sx={{ p:0, display:"flex", flexDirection:"column", justifyContent:"center", alignItems:"center", textAlign:"center", width:"100%"}}>
              <Typography sx={{fontSize:'15px', fontWeight: 'bold', mb:2 }}>
                최고 청구용도
              </Typography>
              {summaryCharge.best ?
                <>
                  <Typography sx={{ fontWeight: 'bold', fontSize:'20px', mb:0.5 }}>
                    {summaryCharge.best.name}
                  </Typography>
                  <Typography sx={{color: 'grey.700', fontSize:'13px'}}>
                    {summaryCharge.best.count}건 / {addCommas(summaryCharge.best.sum)}원
                  </Typography>
                </>
              :
                <>
                  <Typography sx={{ fontWeight: 'bold', fontSize:'20px', mb:0.5 }}>
                    -
                  </Typography>
                  <Typography sx={{color: 'grey.700', fontSize:'13px'}}>
                    -건 / -원
                  </Typography>
                </>
              } 
            </Box>
          </CardContent>
        </Card>
      </Grid>

    </Grid>
  )
}

export default ChargeAnalysisSummaryCard;


