import React, { useState, useEffect } from "react";
import {
  Box,
  Typography,
  Fab,
  TextField,
  Grid,
  InputAdornment,
  CircularProgress,
} from "@mui/material";
import {
  Clear,
  Edit,
  Save,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import Snackbar from "@mui/material/Snackbar";
import Fade from "@mui/material/Fade";
import MuiAlert from "@mui/material/Alert";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import UseApi from "../UseApi";
axios.defaults.withCredentials = true;

const MyInfoForm = ({ pass, passwordModalOpen }) => {
  const [formData, setFormData] = useState({});
  const [originalData, setOriginalData] = useState({});
  const [isEditing, setIsEditing] = useState(false);
  const [isDataModified, setIsDataModified] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [password, setPassword] = useState("");
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const navigate = useNavigate();
  const api = UseApi();

  useEffect(() => {
    // Fetch data using axios
    if (loginUserId) {
      myData();
    }
  }, [loginUserId]);


  const myData = () => {
    api
      .get(`/user/myinfo?id=${loginUserId}`)
      .then((response) => {
        const result = response.data;
        const { userName, email, id, deptName, rankName } = result;
        setFormData({ userName, email, id, deptName, rankName, password });
        setOriginalData({ userName, email, id, deptName, rankName, password });
      })
      .catch((error) => {
        console.error("사용자 정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  useEffect(() => {
    const isModified =
      formData.email !== originalData.email ||
      originalData.password !== formData.password;
    setIsDataModified(isModified);
  }, [formData, originalData]);

  const handleSaveClick = async (e) => {
    e.preventDefault();
    if (isDataModified) {
      try {
        await updateData();
        setIsEditing(false); // 수정 취소 상태로 변경
        setIsModalOpen(true); // 모달 열기
        setOriginalData(formData); // 원본 데이터 업데이트
        console.log("formData", formData);
      } catch (error) {
        console.error("Error updating data:", error);
      }
    }
  };

  const handleShowPassword = () => {
    if (showPassword) {
      setShowPassword(false);
    } else {
      setShowPassword(true);
    }
  };

  const handleCloseModal = () => {
    setIsModalOpen(false); // 모달 닫기
  };

  const isValidEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailRegex.test(email);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    if (name === "email" && !isValidEmail(value)) {
      setEmailError(true);
    } else {
      setEmailError(false);
    }

    setFormData({ ...formData, [name]: value });
  };

  const handlePasswordInputChange = (e) => {
    const { name, value } = e.target;

    if (name === "password" && value === formData.password) {
      setEmailError(true);
    } else {
      setEmailError(false);
    }

    setFormData({ ...formData, [name]: value });
  };
  const handleEditClick = () => {
    if (isEditing) {
      setFormData(originalData);
      setEmailError(false);
    }
    setIsEditing(!isEditing);
  };

  const updateData = async () => {
    try {
      const { id, email, password } = formData;
      const response = await axios.post("/user/myinfo", {
        id: loginUserId,
        email,
        password,
      });
      console.log("Update successful:", response.data);
      formData.password = "";
    } catch (error) {
      console.error("사용자 정보 수정 실패 ", error);
      if (error.response && error.response.status === 401) {
        alert("로그인세션만료");
        navigate("/");
      }
    }
  };

  return (
    <Box p={3}>
      {passwordModalOpen ? (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "400px",
          }}
        >
          <CircularProgress size={50} />
        </Box>
      ) : (
        <form onSubmit={handleSaveClick}>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <Typography variant="h6">이름</Typography>
              <TextField
                sx={{ background: "lightgrey", borderRadius: "5px" }}
                variant="outlined"
                name="userName"
                value={formData.userName}
                InputProps={{
                  readOnly: true,
                }}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6">이메일</Typography>
              <TextField
                variant="outlined"
                name="email"
                value={formData.email}
                onChange={handleInputChange}
                fullWidth
                disabled={!isEditing}
                error={emailError} // Show the email error state
                helperText={
                  emailError
                    ? "잘못된 이메일 형식입니다. 다시 입력해주세요."
                    : ""
                }
              />
            </Grid>

            <Grid item xs={6}>
              <Typography variant="h6">ID</Typography>
              <TextField
                name="id"
                variant="outlined"
                sx={{ background: "lightgrey", borderRadius: "5px" }}
                value={formData.id}
                InputProps={{
                  readOnly: true,
                }}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6">비밀번호</Typography>
              <TextField
                variant="outlined"
                type={showPassword ? "text" : "password"}
                name="password"
                value={formData.password} // 수정된 부분
                fullWidth
                onChange={handlePasswordInputChange}
                disabled={!isEditing}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      {showPassword ? (
                        <Visibility onClick={handleShowPassword} />
                      ) : (
                        <VisibilityOff onClick={handleShowPassword} />
                      )}
                    </InputAdornment>
                  ),
                }}
              />
            </Grid>

            <Grid item xs={6}>
              <Typography variant="h6">부서</Typography>
              <TextField
                variant="outlined"
                sx={{ background: "lightgrey", borderRadius: "5px" }}
                name="deptName"
                value={formData.deptName}
                InputProps={{
                  readOnly: true,
                }}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6">직급</Typography>
              <TextField
                sx={{ background: "lightgrey", borderRadius: "5px" }}
                variant="outlined"
                name="rankName"
                value={formData.rankName}
                InputProps={{
                  readOnly: true,
                }}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} container justifyContent="flex-end">
              <Grid item xs={12} container justifyContent="flex-end">
                {!isEditing ? (
                  <Box marginRight="15px">
                    <Fab
                      color="primary"
                      variant="extended"
                      onClick={handleEditClick}
                    >
                      <Edit />
                      <Typography
                        sx={{
                          ml: 0.2,
                          textTransform: "capitalize",
                        }}
                      >
                        수정하기
                      </Typography>
                    </Fab>
                  </Box>
                ) : (
                  <Box marginRight="15px">
                    <Fab
                      color="secondary"
                      variant="extended"
                      onClick={handleEditClick}
                    >
                      <Clear />
                      <Typography
                        sx={{
                          ml: 0.2,
                          textTransform: "capitalize",
                        }}
                      >
                        수정 취소
                      </Typography>
                    </Fab>
                  </Box>
                )}
                <Fab
                  color="primary"
                  variant="extended"
                  type="submit"
                  disabled={!isDataModified || emailError}
                >
                  <Save />
                  <Typography
                    sx={{
                      ml: 0.2,
                      textTransform: "capitalize",
                    }}
                  >
                    저장하기
                  </Typography>
                </Fab>
              </Grid>
            </Grid>
          </Grid>
        </form>
      )}
      {/* <Modal
        open={isModalOpen}
        onClose={handleCloseModal}
        sx={{
          backdropFilter: "blur(3px)", // 배경 흐림 효과 적용 (최신 브라우저에서만 지원됨)
          backgroundColor: "rgba(255, 255, 255, 0.8)", // 배경 색상과 알파 값 조절
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Box sx={{ p: 2, backgroundColor: "#fff", borderRadius: "8px" }}>
          <Typography>수정사항이 저장되었습니다.</Typography>
          <Button onClick={handleCloseModal} variant="contained">
            확인
          </Button>
        </Box>
      </Modal> */}
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={isModalOpen}
        onClose={handleCloseModal}
        TransitionComponent={Fade}
        autoHideDuration={3000}
        style={{ width: "400px" }}
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          severity="success"
          style={{ color: "white" }}
        >
          수정사항이 저장되었습니다.
        </MuiAlert>
      </Snackbar>
    </Box>
  );
};

export default MyInfoForm;
