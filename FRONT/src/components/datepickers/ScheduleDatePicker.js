import React, { useEffect, useState } from "react";
import { LocalizationProvider, DatePicker } from '@mui/x-date-pickers';
import { Popover, Box, FormControl, RadioGroup, FormControlLabel, Radio, TextField, InputAdornment, Divider, Checkbox, Typography } from '@mui/material';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import ko from 'date-fns/locale/ko';
import moment from 'moment';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';



// const monthList = [
//   {month: '2023-07', rounds : [1, 2, 3]},
//   {month: '2023-08', rounds: [1, 2]},
//   {month: '2023-09', rounds: [1, 2]},
// ];


const ScheduleDatePicker = ({ scheduleList, search, tempSearch, setTempSearch}) => {
  const [monthList, setMonthList] = useState([]);     //가공한 데이터 형태
  const [selectedMonth, setSelectedMonth] = useState(null);    //선택한 청구년월
  const [selectedRound, setSelectedRound] = useState(null);     //선택한 차수
  const [anchorEl, setAnchorEl] = useState(null);     //Popover
  const [selectedMonthRounds, setSelectedMonthRounds] = useState([]);   //선택한 청구년월에 따른 차수리스트
  const [date, setDate] = useState({ min: new Date(), max: new Date() });
  const [isChecked, setIsChecked] = useState(true);    //전체체크박스
  

  const handleMonthSelect = (value) => {
    //console.log(value);
    setSelectedMonth(value);
    setSelectedRound(1); // 선택한 달이 바뀌면 선택한 라운드 초기화
  }


  const handleClose = () => {
    console.log(selectedMonth, selectedRound);
    
    if (isChecked) {
      //전체 체크했을때
      console.log("청구년월.차수 '전체'선택");
      setTempSearch({
        ...tempSearch,
        schedule: 0
      });

    } else {
      const yearMonth = moment(selectedMonth).format('YYYY-MM');
      const selectedSchedule = scheduleList.find(schedule => moment(schedule.yearMonth).format('YYYY-MM') === yearMonth && schedule.schedule === parseInt(selectedRound));
      if (selectedSchedule) {
        const selectedScheduleNo = selectedSchedule.scheduleNo;
        setTempSearch({
          ...tempSearch,
          schedule: selectedScheduleNo
        });
        console.log(selectedScheduleNo);
      }
    }
    setAnchorEl(null);
  }

  function getMonthList(temp) {
    const monthSet = new Set(temp.map(obj => moment(obj.yearMonth).format('YYYY-MM')));
    const monthArray = Array.from(monthSet);
    monthArray.sort();    //정렬
    const monthList = monthArray.map(month => {
      const rounds = temp.filter(obj => moment(obj.yearMonth).format('YYYY-MM') === month).map(obj => obj.schedule);
      return {month, rounds};
    })
    return monthList;
  }


  

  useEffect(() => {
    if (scheduleList.length != 0) {
      let monthListTemp = getMonthList(scheduleList);
      setMonthList(monthListTemp);
      let min = new Date(monthListTemp[0].month + '-01');
      let max = new Date(monthListTemp[monthListTemp.length - 1].month + '-01');
      setDate({
        min: min,
        max: max
      });
      setSelectedMonth(max);
      setSelectedRound(monthListTemp[monthListTemp.length - 1].rounds[monthListTemp[monthListTemp.length - 1].rounds.length - 1]);
    }
  }, [scheduleList]);


  useEffect(() => {
    if (monthList.length !== 0) {
      console.log(monthList);
      let temp = monthList.find(item => item.month === moment(selectedMonth).format('YYYY-MM')).rounds;
      setSelectedMonthRounds(temp); 
      //console.log('차수선택리스트: ', temp);
    }
  }, [selectedMonth]);

  useEffect(() => { 
    if (monthList.length !== 0) {
      if (search.schedule == 0) {
        //청구년월.차수 를 '전체'로 검색
        setIsChecked(true);
        const recentSchedule = scheduleList[scheduleList.length - 1];    //진행예정을 제외하고,제일 최근의 차수
        recentSchedule && setSelectedMonth(new Date(moment(recentSchedule.yearMonth).format('YYYY-MM') + '-01'));
        recentSchedule && setSelectedRound(recentSchedule.schedule);
      } else {
        setIsChecked(false);
        const selectedSchedule = scheduleList.find(schedule => schedule.scheduleNo === search.schedule);
        selectedSchedule && setSelectedMonth(new Date(moment(selectedSchedule.yearMonth).format('YYYY-MM') + '-01'));
        selectedSchedule && setSelectedRound(selectedSchedule.schedule);
      }
    }
  }, [search.startRefresh]);
 


  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ko}>
      <Box>
        <TextField
          label="청구년월.차수"
          value={isChecked?'전체': moment(selectedMonth).format('YYYY년 MM월') + (selectedRound && ` ${selectedRound}차`)}
          onClick={(event)=>setAnchorEl(event.currentTarget)}
          size='small'
          InputProps={{
            readOnly: true,
            endAdornment: ( 
              <InputAdornment position='end'>
                <ArrowDropDownIcon size='small' />
              </InputAdornment>
            ),
            sx: {
              '&:hover': {
                cursor: "pointer", 
              }
            }
          }} 
        />
        <Popover
          open={Boolean(anchorEl)}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <Box p={2} sx={{ width: '200px' }}>
            <Box>
              <Box
                sx={{ display: 'flex', alignItems: 'center', backgroundColor: isChecked?'aliceblue':'white' }}
              >
                <Checkbox
                  checked={isChecked}
                  onChange={()=>setIsChecked(!isChecked)}
                />
                <Typography variant="body1" color={'black'}>전체</Typography>
              </Box>
              <Divider />
              <Box sx={{pt:1.5}}>
                <DatePicker
                  value={selectedMonth}
                  onChange={handleMonthSelect}
                  disabled={isChecked}
                  views={['year', 'month']}
                  openTo="month"
                  format='yyyy년 MM월'
                  minDate={date.min}
                  maxDate={date.max} 
                  slotProps={{ textField: { variant:'outlined', size:'small' } }}
                />
                <FormControl component="fieldset">
                  <RadioGroup
                    aria-label="gender"
                    name="gender1"
                    value={selectedRound}
                    onChange={(e)=>setSelectedRound(parseInt(e.target.value))}
                    sx={{ display: 'flex', flexDirection: 'row' }}
                  >
                    {selectedMonthRounds && selectedMonthRounds.map(round => (
                      <FormControlLabel
                        key={round}
                        value={round}
                        disabled={isChecked}
                        control={<Radio size="small" />}
                        label={round + "차"}
                      />
                    ))}
                  </RadioGroup>
                </FormControl>
              </Box>
            </Box>
          </Box>
        </Popover>
      </Box>
    </LocalizationProvider>
  );
}

export default ScheduleDatePicker;
