import React, { useState } from 'react';
import { LocalizationProvider, DatePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { Box, Grid, Typography } from '@mui/material';
import ko from 'date-fns/locale/ko';    // 한국어 로케일 import


const NewPicker = () => {
  // const [startDate, setStartDate] = useState(null);
  const [startDate, setStartDate] = useState(new Date('2023-02-01'));
  const [endDate, setEndDate] = useState(new Date());

  const handleStartDateChange = (date) => {
    setStartDate(date);
  };

  const handleEndDateChange = (date) => {
    setEndDate(date);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ko}>
      <Box display="flex" justifyContent="center" alignItems="center">
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={5.5} lg={5.5}>
            <DatePicker
              label="조회 기간"
              value={startDate}
              onChange={handleStartDateChange}
              views={['year', 'month']}
              format='yyyy-MM'
              slotProps={{ textField: { variant: 'outlined', size: "small" } }}
              minDate={new Date('2022-07-01')}
              maxDate={endDate} // Optional: Restrict selection up to the end date
            />
          </Grid>
          <Grid item xs={1} lg={1}>
            <Typography variant="h3" fontWeight={600}>
              ~
            </Typography>

          </Grid>
          <Grid item xs={5.5} lg={5.5}>
            <DatePicker
              value={endDate}
              onChange={handleEndDateChange}
              views={['year', 'month']}
              format='yyyy-MM'
              slotProps={{ textField: { variant: 'outlined', size: "small" } }}
              minDate={startDate} // Optional: Restrict selection after the start date
              maxDate={new Date()} // Optional: Restrict selection up to the current date
            />
          </Grid>
        </Grid>
      </Box>
    </LocalizationProvider>
  );
};

export default NewPicker;
