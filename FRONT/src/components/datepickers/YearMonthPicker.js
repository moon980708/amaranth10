import * as React from "react";
import { useState, useEffect } from "react";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { YearCalendar } from "@mui/x-date-pickers/YearCalendar";
import { MonthCalendar } from "@mui/x-date-pickers/MonthCalendar";
import dayjs from "dayjs";
import { Button, TextField, IconButton, Popover, Box } from "@mui/material";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";

import { LocalizationProvider, DatePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import ko from "date-fns/locale/ko"; // 한국어 로케일 import
import { format } from "date-fns";
import { useDispatch, useSelector } from "react-redux";
import getRecentSchedule from "../../reducer/axios/scheduleAbout/getRecentSchedule";
import { update_Schedule } from "../../reducer/module/scheduleReducer";
import { useNavigate } from 'react-router-dom';


export default function YearMonthCalendar({
  setChargeDateString,
  setChargeDate,
  chargeDate,
  chargeDateString,
}) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleChargeDateChange = async (date) => {
    const correctedDate = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate()
    );
    const saveDate = format(correctedDate, "yyyy-MM");
    // console.log(format(correctedDate, 'yyyy-MM'));
    setChargeDate(correctedDate); //date객체로 저장
    setChargeDateString(saveDate);
    try {
      const result= await getRecentSchedule(saveDate, navigate);
      dispatch(update_Schedule(result));
    } catch (error) {
      console.log("yearmonthpicker에서 디스패치 실패");
    }
    // console.log(`선택한 청구년월: ${chargeDateString}`);
    // dispatch(get_Recent_Schedule(saveDate));
  };

  //페이지 렌더링 시 현재 날짜를 기준으로 청구년월을 설정하고 최근차수를 받아옴
  useEffect(() => {
    const fetchData = async () => {
      const correctedDate = new Date(
        chargeDate.getFullYear(),
        chargeDate.getMonth(),
        chargeDate.getDate()
      );
      const saveDate = format(correctedDate, "yyyy-MM");
      // console.log(format(correctedDate, 'yyyy-MM'));
      setChargeDate(correctedDate); //date객체로 저장
      setChargeDateString(saveDate);

      try {
        const result= await getRecentSchedule(saveDate, navigate);
        dispatch(update_Schedule(result));
      } catch (error) {
        console.log("yearmonthpicker에서 디스패치 실패");
      }
    };

    fetchData();
  }, []);

  return (
    <div style={{ margin: "15px", padding: "10px" }}>
      <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ko}>
        <DatePicker
          label="청구년월"
          value={chargeDate}
          onChange={handleChargeDateChange}
          views={["year", "month"]}
          format="yyyy-MM"
          slotProps={{ textField: { variant: "outlined", size: "small" } }}
          minDate={new Date("2022-01")}
          maxDate={new Date("2024-12")}
        />
      </LocalizationProvider>
    </div>
  );
}
