import React, { useState, useEffect } from 'react';
import { LocalizationProvider, DatePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { Box, Grid, Typography } from '@mui/material';
import ko from 'date-fns/locale/ko';    // 한국어 로케일 import


const DateRangePicker = ({startDate, endDate, handleStartDate, handleEndDate}) => {

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ko}>
      <Box display="flex" justifyContent="center" alignItems="center">
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={5.5} lg={5.5}>
            <DatePicker
              label="조회 기간"
              value={startDate}
              onChange={handleStartDate}
              views={['year', 'month']}
              format='yyyy-MM'
              slotProps={{ textField: { variant: 'outlined', size: "small" } }}
              minDate={new Date('2022-08-01')}     //더미데이터 넣고 수정
              maxDate={endDate} //시작기간이기때문
            />
          </Grid>
          <Grid item xs={1} lg={1}>
            <Typography variant="h2" fontWeight={600}>~</Typography>
          </Grid>
          <Grid item xs={5.5} lg={5.5}>
            <DatePicker
              value={endDate}
              onChange={handleEndDate}
              views={['year', 'month']}
              format='yyyy-MM'
              slotProps={{ textField: { variant: 'outlined', size: "small" } }}
              minDate={startDate} 
              maxDate={new Date()} 
            />
          </Grid>
        </Grid>
      </Box>
    </LocalizationProvider>
  );
};

export default DateRangePicker;
