import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { Box, IconButton, Typography } from "@mui/material";
import axios from 'axios';
import UseApi from '../UseApi';

import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
axios.defaults.withCredentials = true;



// 달력의 이벤트를 표시하기 위해 사용되는 스타일 설정입니다.
const styles = (event) => {
  let color = '#3174ad';       //진행중
  switch (event.close) {
    case "진행예정":
      color = 'skyblue'; break;
    case "마감":
      color = 'grey'; break;
    default: break;
  }
  return {
    backgroundColor: color,
    color: 'white',
    fontSize: '11px'
  }
};

// moment를 사용하여 시간을 처리합니다.
moment.locale('ko-KR');
const localizer = momentLocalizer(moment);


const eventsData = [
  {
    title: '2023년 6월 3차',
    chargeStart: new Date('2023-06-11'),
    chargeEnd: new Date('2023-06-25'),
    close: '마감'
  },
  {
    title: '2023년 7월 1차',
    chargeStart: new Date('2023-07-02'),
    chargeEnd: new Date('2023-07-05'),
    close: '마감'
  },
  {
    title: '2023년 7월 2차',
    chargeStart: new Date('2023-07-10'),
    chargeEnd: new Date('2023-07-15'),
    close: '진행중'
  },
  {
    title: '2023년 7월 3차',
    chargeStart: new Date('2023-07-18'),
    chargeEnd: new Date('2023-07-20'),
    close: '진행중'
  },
  {
    title: '2023년 7월 4차',
    chargeStart: new Date('2023-07-26'),
    chargeEnd: new Date('2023-07-31'),
    close: '진행중'
  },
  {
    title: '2023년 8월 1차',
    chargeStart: new Date('2023-08-01'),
    chargeEnd: new Date('2023-08-03'),
    close: '진행예정'
  },
  {
    title: '2023년 8월 2차',
    chargeStart: new Date('2023-08-05'),
    chargeEnd: new Date('2023-08-11'),
    close: '진행예정'
  },
  
];



const AdminCalendar = () => {
  const navigate = useNavigate();
  const api = UseApi();
  const [events, setEvents] = useState([]);
  const [date, setDate] = useState(new Date());



  const CustomToolbar = (toolbar) => {
    const { onNavigate } = toolbar;

    return (
      <div style={{marginBottom:"15px"}}>
        <div style={{ display: 'flex', justifyContent:"space-between", alignItems:"center" }}>

          <IconButton size='small' onClick={() => onNavigate('PREV')}>
            <KeyboardArrowLeftIcon />
          </IconButton>
          
          <div style={{display:"flex", alignItems:"center"}}>
            <Typography variant='h5'>
              {moment(toolbar.date).format("YYYY년 MM월")}
            </Typography>
          </div>
          
          <IconButton size='small' onClick={() => onNavigate('NEXT')}>
            <KeyboardArrowRightIcon />
          </IconButton>

        </div>
      </div>
    );
  };


  const listSchedule = () => {
    const chargeMonth = moment(date).format("YYYY-MM");
    const variables = {
      chargeMonth: chargeMonth
    };

    api.post("/admin/main/schedulelist", variables)
      .then((result) => {
        console.log(result.data);    
        let scheduleData = result.data;
        let temp = [];

        if (scheduleData.length !== 0) {
          for (let schedule of scheduleData) {
            let newSchedule = {
              title: moment(schedule.yearMonth).format('YYYY년 MM월 ')+schedule.schedule+"차",  //ex.'2023년 08월 1차'
              chargeStart: schedule.chargeStart,
              chargeEnd: schedule.chargeEnd,
              close: schedule.close
            }

            temp.push(newSchedule);
          }

        }

        setEvents(temp);
    
      }).catch((error) => {
        if (error.response) {
          console.log("청구년월에 따른 차수리스트 가져오기 실패", error);
        }
      });

  }

  useEffect(() => {
    //console.log("달력: ",date);
    listSchedule();
    
   }, [date]);



  
  return (
    <Box>
      <Calendar
        date={date}
        localizer={localizer}
        culture='ko-KR'
        events={events}
        views={['month']}
        defaultView="month"
        style={{ height: '380px' }}
        eventPropGetter={(event) => ({
          style: styles(event)
        })}
        tooltipAccessor={(event) => (event.close)}
        startAccessor={"chargeStart"}
        endAccessor={"chargeEnd"}
        components={{ toolbar: CustomToolbar }}
        onSelectEvent={(event) => { console.log(event.title); navigate("/admin/schedulesetting"); }}
        onNavigate={(newDate)=>setDate(newDate)}
            
      />

      <div style={{display:"flex", justifyContent:"flex-end", padding:"5px"}}>
        <span style={{ color: "skyblue", fontSize: "10px" }}>&#9679;</span>
        <span style={{ fontSize: "10px" }}>진행예정&nbsp;</span>
        <span style={{ color: "#3174ad", fontSize: "10px" }}>&#9679;</span>
        <span style={{ fontSize: "10px" }}>진행중&nbsp;</span>
        <span style={{ color: "grey", fontSize: "10px" }}>&#9679;</span>
        <span style={{fontSize:"10px"}}>마감</span>
      </div>
    </Box>
  );

};

export default AdminCalendar;
