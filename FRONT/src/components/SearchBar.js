import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import SearchIcon from "@mui/icons-material/Search";
import { IconButton, NativeSelect } from "@mui/material";

const SearchBar = ({ onSearch, searchButtonClicked, setSearchButtonClicked }) => {
  const [searchValue, setSearchValue] = useState("");
  const [selectedOption, setSelectedOption] = useState(0);
  const [searchField, setSearchField] = useState("");

  useEffect(() => {
    if (searchButtonClicked) {
      setSearchValue("");
      setSearchField("");
      setSelectedOption(0);
      setSearchButtonClicked(!searchButtonClicked);
    }
  }, [searchButtonClicked, setSearchButtonClicked]);

  const handleSearch = () => {
    onSearch(searchValue, searchField); // onSearch 함수를 호출하여 상태를 부모 컴포넌트로 전달
  };

  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);

    let searchOption = "";

    switch (event.target.value) {
      case "1":
        searchOption = "category";
        break;
      case "2":
        searchOption = "store";
        break;
      case "3":
        searchOption = "contents";
        break;
      case "4":
        searchOption = "payment";
        break;
      default:
        break;
    }
    setSearchField(searchOption);
  };

  return (
    <TextField
      variant="outlined"
      size="small"
      placeholder="검색"
      // label="전체검색"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <NativeSelect
              value={selectedOption}
              onChange={handleOptionChange}
              displayempty="true"
              inputProps={{ "aria-label": "검색 옵션" }}
              sx={{
                width: "80px",
              }}
            >
              <option value={0}>선택</option>
              <option value={1}>용도</option>
              <option value={2}>사용처</option>
              <option value={3}>내용</option>
              <option value={4}>결제수단</option>
            </NativeSelect>
          </InputAdornment>
        ),
        endAdornment: (
          <InputAdornment position="end">
            <IconButton color="info" size="small" onClick={handleSearch} disabled={!searchField}>
              <SearchIcon />
            </IconButton>
          </InputAdornment>
        ),
      }}
      value={searchValue}
      onChange={(e) => setSearchValue(e.target.value)}
    />
  );
};

export default SearchBar;
