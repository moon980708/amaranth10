export function addCommas(num) {
  if (num === undefined) {
    return ""; // num이 undefined이면 빈 문자열을 반환
  }

  let numStr = num.toString();
  let result = "";
  while (numStr.length > 3) {
    result = `,${numStr.slice(-3)}${result}`;
    numStr = numStr.slice(0, -3);
  }
  if (numStr) {
    return `${numStr}${result}`;
  }
  return result.slice(1);
}
