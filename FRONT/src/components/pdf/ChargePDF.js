import React, { useEffect } from 'react';
import { jsPDF } from 'jspdf';
// import 'jspdf-autotable';
import autoTable from "jspdf-autotable";
import imageCompression from 'browser-image-compression';


import UseApi from '../UseApi';
import malgunFont from './FontBase64';
import { addCommas } from '../functions/SimpleFuntion';
import DouzoneImg from "../../assets/images/DouzoneImg.png";


 // Define 스타일
 const styles = {
  pageMargin: 15,
};



const ChargePDF = ({ showPDF, setShowPDF }) => {
  const api = UseApi();


  useEffect(() => {
    // userChargeNo에 따른 정보를 가져오는 비동기 함수 호출
    // 이 함수 내에서 PDF를 생성하고 엽니다.
    if (showPDF.open) {
      
      api.get("/admin/charge/detail/" + showPDF.userChargeNo)
        .then((result) => {
          console.log(result.data);
          generatePDF(result.data);
        })
        .catch((error) => {
          if (error.response) {
            console.log("하나의 청구건에 대한 상세내역 불러오기 실패", error);
            alert("PDF 생성 실패");
          }
        });
    }
  }, [showPDF.open]);



  const generatePDF = async (chargeDetail) => {

    let chargeState = [
      { content: "처리상태", styles: { fillColor: [240, 248, 255] }, },
      { content: chargeDetail.state, styles: { halign: "center" } },
    ];

    if (chargeDetail.state !== "반려") {
      let temp = { content: "", colSpan: 2, styles: { halign: "center" } };
      chargeState = [...chargeState, temp];
    } else {
      let temp1 = { content: "반려사유", styles: { fillColor: [240, 248, 255] } };
      let temp2 = { content: chargeDetail.rejectReason, styles: { halign: "center" } };

      chargeState = [...chargeState, temp1, temp2];
    }
  

    const tableBody =
      [
        [
          { content: "청구년월.차수", styles: { fillColor: [240, 248, 255] }, },
          { content: chargeDetail.scheduleName, styles: { halign: "center" } },
          { content: "청구일", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.userChargeDate, styles: { halign: "center" } },
        ],

        chargeState,

        [
          { content: "청구자", colSpan: 4, styles: { fillColor: [240, 248, 255] } },
        ],
        [
          { content: "부서", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.deptName, styles: { halign: "center" } },
          { content: "사원번호", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.id, styles: { halign: "center" } },
        ],
        [
          { content: "성명", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.userName, styles: { halign: "center" } },
          { content: "이메일", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.email, styles: { halign: "center" } },
        ],
        [
          { content: "지출정보", colSpan: 4, styles: { fillColor: [240, 248, 255] } },
        ],
        [
          { content: "지출일시", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.expendDate, styles: { halign: "center" } },
          { content: "결제수단", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.payment, styles: { halign: "center" } },
        ],
        [
          { content: "사용처", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.store, colSpan: 3, styles: { halign: "center" } },
        ],
        [
          { content: "내용", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.contents ? chargeDetail.contents : "-", colSpan: 3, styles: { halign: "center" } },
        ],
        [
          { content: "용도", styles: { fillColor: [240, 248, 255] } },
          { content: chargeDetail.categoryName, styles: { halign: "center" } },
          { content: "금액", styles: { fillColor: [240, 248, 255] } },
          { content: addCommas(chargeDetail.charge) + "원", styles: { halign: "center" } },
        ],
        [
          { content: "용도상세", colSpan: 4, styles: { fillColor: [240, 248, 255] } },
        ]
      ];
  

    //용도상세
    const itemList = chargeDetail.itemList;    
    let dynamicContent = [];
    if (chargeDetail.itemList.length === 0) {
      let row = [
        { content: "-", colSpan: 4 }
      ];

      dynamicContent.push(row);
    } else {
      for (let i = 0; i < itemList.length; i += 2) {
        const item1 = itemList[i];
        const item2 = i + 1 < itemList.length ? itemList[i + 1] : null;
  
        let row = [
          { content: item1.itemTitle, styles: { fillColor: [240, 248, 255] } },
          { content: item1.itemContents !== '' ? item1.itemContents : "-", styles: { halign: "center" } },
        ];
  
        if (item2) {
          row.push(
            { content: item2.itemTitle, styles: { fillColor: [240, 248, 255] } },
            { content: item2.itemContents !== '' ? item2.itemContents : "-", styles: { halign: "center" } }
          );
        } else {
          // 아이템이 홀수개인 경우 빈 셀 추가
          // row.push(
          //   { content: "", styles: { fillColor: [240, 248, 255] } },
          //   { content: "", styles: { halign: "center" } }
          // );
          row.push(
            { content: "", colSpan: 2, styles: { halign: "center" } }
          );
        }
  
        dynamicContent.push(row);
      }
    }

    let fileContent = [];
    if (chargeDetail.realFileName) {
      let row = [
        [
          { content: "첨부파일", colSpan: 4, styles: { fillColor: [240, 248, 255] } },
        ],
        [
          { content: "", colSpan: 4, styles: { halign: "center", minCellHeight: 100 } },
        ]
      ];

      fileContent = row;
    } else {
      let row = [
        [
          { content: "첨부파일", colSpan: 4, styles: { fillColor: [240, 248, 255] } },
        ],
        [
          { content: "-", colSpan: 4, styles: { halign: "center" } },
        ]
      ];

      fileContent = row;
    }
    

    
   

    const reader = new FileReader();

    // // Create a new jsPDF instance
    const doc = new jsPDF('p', 'mm', 'a4'); 

    doc.addFileToVFS('./malgun.ttf', malgunFont);  //malgunFont 변수는 Base64 형태로 변환된 내용입니다.
    doc.addFont('./malgun.ttf','malgun', 'normal');
    doc.setFont('malgun');


    // Add content to the PDF using chargeDetail
    doc.setProperties({
      title: `청구 내역서(${chargeDetail.state}) - ${chargeDetail.userName}`,
    });

    try {
    
      let currentY = styles.pageMargin;

      const responseLogo = await fetch(DouzoneImg);
      const blobLogo = await responseLogo.blob();
      reader.readAsDataURL(blobLogo);

      reader.onload = () => {
        const base64ImageLogo = reader.result.split(',')[1];

        // 이미지 크기 계산
        const imageWidthLogo = 25;
        const imageHeightLogo = 6;


        // PDF에 이미지 추가
        doc.addImage(
          base64ImageLogo, // 이미지 데이터
          'PNG', // 이미지 형식
          styles.pageMargin, // X 좌표
          currentY, // Y 좌표
          imageWidthLogo, // 이미지 가로 크기
          imageHeightLogo
        );


        // 페이지 상단에 헤더 추가
        doc.text(`청구 내역서(${chargeDetail.state})`, doc.internal.pageSize.getWidth() / 2, styles.pageMargin + 10, { align: 'center' });


        // 페이지 본문 시작        
        currentY = styles.pageMargin + 20;

        const finalTableBody = [...tableBody, ...dynamicContent];

        console.log(finalTableBody);
        
        doc.autoTable({
          theme: 'grid',
          headStyles: { halign: "center", valign: "middle" }, //헤더 부분 옵션
          bodyStyles: { halign: "center", valign: "middle" }, //body 부분 옵션
          //startX: styles.pageMargin,
          startY: currentY,
          tableWidth: 'auto',
          columnStyles: {
            0: { cellWidth: 35 }, // 첫 번째 열의 너비를 35으로 설정
            1: { cellWidth: 55 }, 
            2: { cellWidth: 35 },
            3: { cellWidth: 55 },      
          },
          margin: { left: styles.pageMargin, right: styles.pageMargin }, //여백
          //tableWidth : 210,
          styles: {
            font: "malgun",
            fontStyle: "normal",
            cellPadding: { top: 3, bottom: 3 }
          }, //폰트적용
          body: finalTableBody,
          
        });



        const remainingSpace = doc.internal.pageSize.height - doc.autoTable.previous.finalY;


        if (chargeDetail.realFileName && remainingSpace < 130) {
          //파일이 있으면서 첨부파일 넣을 공간이 부족할때는 다음페이지로 넘어감.
          doc.addPage();
          currentY = styles.pageMargin;
        } else {
          currentY = doc.autoTable.previous.finalY;
        }

        doc.autoTable({
          theme: 'grid',
          headStyles: { halign: "center", valign: "middle" }, //헤더 부분 옵션
          bodyStyles: { halign: "center", valign: "middle" }, //body 부분 옵션
          startY: currentY,
          tableWidth: 'auto',
          columnStyles: {
            0: { cellWidth: 35 }, // 첫 번째 열의 너비를 35으로 설정
            1: { cellWidth: 55 }, 
            2: { cellWidth: 35 },
            3: { cellWidth: 55 },      
          },
          margin: { left: styles.pageMargin, right: styles.pageMargin }, //여백
          //tableWidth : 210,
          styles: {
            font: "malgun",
            fontStyle: "normal",
            cellPadding: { top: 3, bottom: 3 }
          }, //폰트적용
          body: fileContent,
          didDrawCell: async (data) => {
            if (data.row.index === 1) {
              if (chargeDetail.realFileName) {
                try {
                  // 이미지를 Base64로 변환
                  const response = await fetch("/upload/" + chargeDetail.realFileName);
                  const blob = await response.blob();


                  const options = {
                    maxSizeMB: 0.5,          // 최대 파일 크기 (메가바이트)
                    maxWidthOrHeight: 800, // 이미지 최대 너비 또는 높이
                    useWebWorker: true,    // 웹 워커 사용 여부 (브라우저에서 비동기로 처리)
                  };
                  
                  const compressedFile = await imageCompression(blob, options);
                  

                  reader.readAsDataURL(compressedFile);
        
                  reader.onload = () => {
                    const base64Image = reader.result.split(',')[1];
                    //console.log("이미지변환: ",base64Image);

                    // 이미지 크기 계산
                    const imageWidth = 80;
                    const imageHeight = 80;

                    // 이미지를 셀의 중앙에 위치시키기 위한 계산
                    const x = data.cell.x + (data.cell.width / 2) - (imageWidth / 2);
                    const y = data.cell.y + (data.cell.height / 2) - (imageHeight / 2);

                    // PDF에 이미지 추가
                    doc.addImage(
                      base64Image, // 이미지 데이터
                      'PNG', // 이미지 형식
                      x, // X 좌표
                      y, // Y 좌표
                      imageWidth, // 이미지 가로 크기
                      imageHeight
                    );

                    doc.output('dataurlnewwindow');
                  };
                } catch (error) {
                  console.log('이미지를 PDF에 추가하는 중 오류 발생:', error);
                }

              } else {
                doc.output('dataurlnewwindow');
              }

            }
          },
          
        });


      }

    } catch (error) {
      console.log('이미지를 PDF에 추가하는 중 오류 발생:', error);
    }





    // PDF 생성을 완료하고 showPDF 상태를 닫힌 상태로 변경
    setShowPDF({ open: false, userChargeNo: null });






  };



  return <></>;
};

export default ChargePDF;

