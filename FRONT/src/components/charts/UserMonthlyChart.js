import React, { useEffect, useState } from "react";
import * as echarts from "echarts";
import { Card, CardContent, Typography, Box } from "@mui/material";
import axios from "axios";
import UseApi from "../UseApi";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addCommas } from "../functions/SimpleFuntion";
axios.defaults.withCredentials = true;

const UserMonthlyChart = () => {
  const [monthlyData, setMonthlyData] = useState([]);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const navigate = useNavigate();
  const api = UseApi();

  useEffect(() => {
    if (loginUserId) {
      getMonthlyChart();
    }
  }, [loginUserId]);

  const getMonthlyChart = () => {
    api
      .get(`/user/home/monthlychart?id=${loginUserId}`)
      .then((response) => {
        const monthlyData = response.data;
        setMonthlyData(monthlyData);
      })
      .catch((error) => {
        console.error("월별 차트 요청 실패", error);
        if (error.response) {
        }
      });
  };

  useEffect(() => {
    var chartDom = document.getElementById("main");
    var myChart = echarts.init(chartDom);
    var option;

    const categoryNames = [...new Set(monthlyData.map((data) => data.categoryName))];
    const months = [...new Set(monthlyData.map((data) => data.month))];
    const yearAndMonth = [
      ...new Set(monthlyData.map((data) => data.year + "년 " + data.month + "월")),
    ];
    const seriesData = categoryNames.map((category) => {
      return {
        name: category,
        type: "line",
        stack: "Total",
        data: yearAndMonth.map((data) => {
          const dataPoint = monthlyData.find(
            (dataToFind) =>
              dataToFind.year + "년 " + dataToFind.month + "월" === data &&
              dataToFind.categoryName === category
          );
          return dataPoint ? dataPoint.totalCharge : 0;
        }),
      };
    });

    option = {
      tooltip: {
        trigger: "axis",
        formatter(params) {
          let tooltipContent = ""; // 툴팁 내용을 저장할 변수

          // params 배열을 순회하여 툴팁 내용을 동적으로 생성
          params.forEach((param) => {
            const value = param.value;
            if (value !== 0) {
              // 값이 0이 아닌 경우에만 포함
              tooltipContent += `<span style="color:${param.color};">&#9679; </span>${
                param.seriesName
              } : ${addCommas(value)}원<br>`;
            }
          });

          return tooltipContent; // 툴팁 내용 반환
        },
      }, //이게 마우스 호버 시 정보 보여줌
      grid: {
        left: "2%",
        right: "3%",
        bottom: "3%",
        containLabel: true,
      }, //여기가 차트자체의 크기
      legend: {
        data: categoryNames,
      }, //색상별 뭔지
      xAxis: {
        type: "category",
        data: yearAndMonth.map((data) => {
          return data;
        }),
      },

      yAxis: {
        type: "value",
      },
      series: seriesData,
    };

    option && myChart.setOption(option);
  }, [monthlyData]);

  return (
    <Card
      variant="outlined"
      sx={{
        paddingBottom: "0",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
      }}
    >
      <CardContent
        sx={{
          paddingBottom: "16px !important",
          position: "relative", // 부모 요소에 relative 추가
        }}
      >
        <Box
          sx={{
            alignItems: "center",
          }}
        >
          <Box>
            <Typography
              variant="h3"
              sx={{
                marginBottom: "0",
              }}
              gutterBottom
            >
              월별 차트
            </Typography>

            {monthlyData.length === 0 ? (
              <>
                <Typography
                  variant="h6"
                  sx={{
                    textAlign: "center",
                    position: "absolute",
                    top: "50%",
                    left: "0",
                    right: "0",
                    transform: "translateY(-50%)",
                  }}
                >
                  데이터 없음
                </Typography>
                <Box id="main" sx={{ width: "100%", height: "300px" }}></Box>
              </>
            ) : (
              <Box id="main" sx={{ width: "100%", height: "300px" }}></Box>
            )}
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
};

export default UserMonthlyChart;
