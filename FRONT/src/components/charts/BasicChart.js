// BasicChart.js
import React, { useEffect } from "react";
import * as echarts from "echarts";
import { Card, CardContent, Typography, Box } from "@mui/material";

const BasicChart = () => {
  useEffect(() => {
    var chartDom = document.getElementById("main");
    var myChart = echarts.init(chartDom);
    var option;

    option = {
      tooltip: {
        trigger: 'axis'
      }, //이게 마우스 호버 시 정보 보여줌
      grid: {
        left: '2%',
        right: '3%',
        bottom: '3%',
        containLabel: true
      }, //여기가 차트자체의 크기
      legend: {
        data: ['A부서', 'B부서', 'C부서', 'D부서', 'E부서']
      }, //색상별 뭔지
      xAxis: {
        type: "category",
        data: [
          "1월",
          "2월",
          "3월",
          "4월",
          "5월",
          "6월",
          "7월",
          "8월",
          "9월", 
          "10월",
          "11월",
          "12월"
        ],
      },
      yAxis: {
        type: "value",
      },
      series: [
        {
          name: 'A부서',
          type: 'line',
          stack: 'Total',
          data: [120, 132, 101, 134, 90, 230, 210,132, 101, 134, 90, 230]
        },
        {
          name: 'B부서',
          type: 'line',
          stack: 'Total',
          data: [220, 182, 191, 234, 290, 330, 310,182, 191, 234, 290, 330]
        },
        {
          name: 'C부서',
          type: 'line',
          stack: 'Total',
          data: [150, 232, 201, 154, 190, 330, 410,232, 201, 154, 190, 330]
        },
        {
          name: 'D부서',
          type: 'line',
          stack: 'Total',
          data: [320, 332, 301, 334, 390, 330, 320, 332, 301, 334, 390, 330]
        },
        {
          name: 'E부서',
          type: 'line',
          stack: 'Total',
          data: [820, 932, 901, 934, 1290, 1330, 1320,932, 901, 934, 1290, 1330]
        }
      ]
    };

    option && myChart.setOption(option);

    // 창 크기 조절 이벤트 핸들러 추가
    const handleResize = () => {
      myChart.resize();
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Card
      variant="outlined"
      sx={{
        paddingBottom: "0",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
        height:"85%"
      }}
    >
      <CardContent
        sx={{
          paddingBottom: "16px !important",
          //height:"100%"
        }}
      >
        <Box
          sx={{
            alignItems: "center",
            //height:"100%"
          }}
        >
          <Typography
            variant="h4"
            sx={{
              marginBottom: "0",
            }}
            gutterBottom
          >
            최근 1년간 부서별 차트
          </Typography>
          <Box id="main" sx={{ width: "100%", height: "295px" }}></Box>
        </Box>
      </CardContent>
    </Card>
  );
};

export default BasicChart;
