import React, { useEffect, useState, useRef } from "react";
import * as echarts from "echarts";
import { Card, CardContent, Typography, Box } from "@mui/material";
import { addCommas } from '../functions/SimpleFuntion';
import { LocalizationProvider, DatePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import ko from 'date-fns/locale/ko';    // 한국어 로케일 import
import moment from 'moment';
import axios from 'axios';
import UseApi from '../UseApi';
import { useSelector } from "react-redux";
axios.defaults.withCredentials = true;


const ChargeMonthPieChart = () => {
  const api = UseApi();
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const mainLoading = useSelector((state) => state.adminMainReducer.mainLoading);

  const chartRef = useRef(null);
  const [chargeMonth, setChargeMonth] = useState(new Date());    //청구년월 날짜피커
  const [total, setTotal] = useState({ count: 0, sum: 0 });
  const [deptChartResult, setDeptChartResult] = useState([]);    //파이차트  부서별 best5 + 그 외    //금액기준
  

  const handleChargeMonthChange = (date) => {
    setChargeMonth(date);
  };


  useEffect(() => {
    if (loginUserId) {
    
      //console.log(moment(chargeMonth).format("YYYY-MM"));
      const variables = {
        managerId: loginUserId,
        chargeMonth: moment(chargeMonth).format("YYYY-MM")
      };

      api.post("/admin/main/monthdept", variables)
        .then((result) => {
          console.log(result.data);

          let totalData = result.data.total;
          let best5Data = result.data.best5;
          let etcData = result.data.etc;

          setTotal({
            count: totalData.countChargeDept,
            sum: totalData.sumChargeDept ? totalData.sumChargeDept : 0
          });


          let temp = [];

          if (best5Data.length !== 0) {
            for (let dept of best5Data) {
              let newDept = {
                value: dept.sumChargeDept,
                name: dept.deptName,
                Number: dept.countChargeDept
              }

              temp.push(newDept);
            }
          }

          if (etcData !== null) {
            let etc = {
              value: etcData.sumChargeDept,
              name: etcData.deptName,
              Number: etcData.countChargeDept
            }
            temp.push(etc);
          }

          setDeptChartResult(temp);    //파이차트에 갱신
        

        }).catch((error) => {
          if (error.response) {
            console.log("청구년월에 따른 전체 + best5부서 + 그외 가져오기 실패", error);
          }
        });
    }
    
  }, [loginUserId, chargeMonth, mainLoading]);


  useEffect(() => {
    const chart = chartRef.current;
    if (!chart) return;
    
    // 기존에 초기화된 차트 인스턴스를 삭제
    echarts.dispose(chart);

    var myChart = echarts.init(chart);
    var option;

    option = {
      tooltip: {
        trigger: 'item',
        formatter: function (params) {
          return `<span style="color:${params.color}">&#9679;</span> <b>${
            params.data.name
          }</b> (${params.percent}%)<br/>총 건수 : ${
            params.data.Number
          }건<br/>총 금액 : ${addCommas(params.value)}원`;
        }
      },
      legend: {
        orient: 'vertical',
        right: 3,
        top: 'middle'
      },
      series: [
        {
          name: '승인내역 없음',
          type: 'pie',
          radius: '80%',
          data: deptChartResult,
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };
    

    option && myChart.setOption(option);

    // 창 크기 조절 이벤트 핸들러 추가
    const handleResize = () => {
      myChart.resize();
    };
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
      // 차트 인스턴스 삭제
      echarts.dispose(chart);
    };

  }, [deptChartResult]);



  return (
    <Card
      variant="outlined"
      sx={{
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
        height: "90%",
        p:0
      }}
    >
      <CardContent
        sx={{
          height: "100%",
          justifyContent: "space-between"
        }}
      > 
        <Box sx={{ display:"flex", alignItems:"center", height:"15%", justifyContent:"space-between"}}>
          <Box sx={{ display:'flex', alignItems:'center', height:'100%' }}>
            <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ko}>
              <DatePicker
                label="청구년월"
                value={chargeMonth}
                onChange={handleChargeMonthChange}
                views={['year', 'month']}
                format='yyyy-MM'
                slotProps={{ textField: { variant:'outlined', size:'small' } }}
                minDate={new Date('2022-07-01')}
                maxDate={new Date()} 
              />
            </LocalizationProvider>
          </Box>
          <Box sx={{display:'flex', alignItems:'flex-end', flexDirection:'column', height:'100%', justifyContent:'flex-end' }}>
            <div>
              <Typography variant="h5" style={{ marginBottom: 4 }}>총 {addCommas(total.sum)}원</Typography>
              <Typography variant="h6" color="textSecondary" textAlign={'right'}>/ {total.count}건</Typography>
            </div>
          </Box>
        </Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "85%"
          }}
        >
          <Box ref={chartRef} sx={{ width: "90%", height: "90%" }}></Box>
        </Box>

      </CardContent>
    </Card>
  );
};

export default ChargeMonthPieChart;

