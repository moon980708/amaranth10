import React, { useEffect, useState } from "react";
import * as echarts from "echarts";
import { Card, CardContent, Typography, Box } from "@mui/material";
import axios from "axios";
import UseApi from "../UseApi";
import { addCommas } from "../functions/SimpleFuntion";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
axios.defaults.withCredentials = true;

const UserCategoryChart = ({ year, month }) => {
  const [categoryData, setCategoryData] = useState([]);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const navigate = useNavigate();
  const api = UseApi();

  useEffect(() => {
    if (loginUserId && year && month) {
      getCategoryChart(year, month);
    }
  }, [loginUserId, year, month]);

  const getCategoryChart = (year, month) => {
    api
      .get(`/user/home/categorychart?id=${loginUserId}`, {
        params: {
          year: year,
          month: month,
        },
      })
      .then((response) => {
        const categoryData = response.data;
        setCategoryData(categoryData);
      })
      .catch((error) => {
        console.error("용도 차트 요청 실패", error);
        if (error.response) {
        }
      });
  };

  useEffect(() => {
    var chartDom = document.getElementById("UserCategoryChart");
    var myChart = echarts.init(chartDom);
    var option;

    option = {
      tooltip: {
        trigger: "item",
        formatter: function (params) {
          return (
            params.name +
            " : " +
            addCommas(params.value) +
            "원 (" +
            params.percent +
            "%)"
          );
        },
      },
      legend: {
        type: "scroll",
        orient: "vertical",
        right: 10,
        top: "middle",
      },
      series: [
        {
          type: "pie",
          radius: ["50%", "90%"],
          itemStyle: {
            borderRadius: 10,
            borderColor: "#fff",
            borderWidth: 2,
          },
          label: {
            show: false,
            position: "center",
          },
          emphasis: {
            label: {
              show: true,
              fontSize: 20,
              fontWeight: "bold",
            },
          },
          data: categoryData.map((dataItem) => ({
            name: dataItem.categoryName,
            value: dataItem.totalCharge,
          })),
        },
      ],
    };

    option && myChart.setOption(option);

    return () => {
      myChart.dispose();
    };
  }, [categoryData]);

  return (
    <Card
      variant="outlined"
      sx={{
        paddingBottom: "0",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
      }}
    >
      <CardContent
        sx={{
          paddingBottom: "16px !important",
          position: "relative", // 부모 요소에 relative 추가

        }}
      >
        <Box
          sx={{
            alignItems: "center",
          }}
        >
          <Box>
            <Typography
              variant="h3"
              sx={{
                marginBottom: "0",
              }}
              gutterBottom
            >
              {month}월 용도 차트
            </Typography>
            {categoryData.length === 0 ? (
              <>
                <Typography
                  variant="h6" // 원하는 스타일로 변경하세요
                  sx={{
                    textAlign: "center",
                    marginTop: "16px",
                    position: "absolute", // 메시지 오버랩을 위한 absolute
                    top: "50%", // 상단 여백을 조절하여 중앙 정렬
                    left: "0",
                    right: "0",
                    transform: "translateY(-50%)", // 수직으로 중앙 정렬
                  }}
                >
                  데이터 없음
                </Typography>
                <Box
                  id="UserCategoryChart"
                  sx={{ width: "100%", height: "300px" }}
                ></Box>
              </>
            ) : (
              <Box
                id="UserCategoryChart"
                sx={{ width: "100%", height: "300px" }}
              ></Box>
            )}
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
};

export default UserCategoryChart;
