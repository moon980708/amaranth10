import React, { useEffect, useState, useRef } from "react";
import * as echarts from "echarts";
import { Card, CardContent, Typography, Box, ToggleButton, ToggleButtonGroup } from "@mui/material";
import { addCommas } from '../functions/SimpleFuntion';


const totalData = [
  { value: 11000, name: '중식대', Number: 30 },
  { value: 100000, name: '출장비', Number: 20 },
  { value: 12345, name: '소모품비', Number: 11 },
  { value: 1111188, name: '도서인쇄비', Number: 9 },
  { value: 1199999, name: '접대비', Number: 3 }
];

const countData = [
  { value: 30, name: '중식대', Number: 11000 },
  { value: 20, name: '출장비', Number: 100000 },
  { value: 11, name: '소모품비', Number: 12345 },
  { value: 9, name: '도서인쇄비', Number: 1111188 },
  { value: 3, name: '접대비', Number: 1199999 }
];




const ChargeCategoryAnalysisChart = ({ categoryRank }) => {
  const chartRef = useRef(null);
  const [alignment, setAlignment] = useState("sum");     //정렬
  const [showCategoryRank, setShowCategoryRank] = useState({});

  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };



  useEffect(() => { 
    //categoryRank -> sum이랑 count가 담겨있음. 각각 정렬되어서.

    let sumTemp = [];

    if (categoryRank.sum.length != 0) {
      for (let category of categoryRank.sum) {
        let newCategory = {
          value: category.sum,
          name: category.name,
          Number: category.count
        }
        sumTemp.push(newCategory);
      }
    }

    let countTemp = [];

    if (categoryRank.count.length != 0) {
      for (let category of categoryRank.count) {
        let newCategory = {
          value: category.count,
          name: category.name,
          Number: category.sum
        }

        countTemp.push(newCategory);
      }
    }

    setShowCategoryRank({
      sum: sumTemp,
      count: countTemp
    });

  }, [categoryRank]);


  useEffect(() => {
    const chart = chartRef.current;
    if (!chart) return;
    
    // 기존에 초기화된 차트 인스턴스를 삭제
    echarts.dispose(chart);

    var myChart = echarts.init(chart);
    var option;

    option = {
      tooltip: {
        trigger: 'item',
        formatter: function (params) {
          return (
            alignment === "sum" ?
              `<span style="color:${params.color}">&#9679;</span> <b>${params.data.name}</b> (${params.percent}%)<br/>총 건수 : ${params.data.Number}건<br/>총 금액 : ${addCommas(params.value)}원`
            :
              `<span style="color:${params.color}">&#9679;</span> <b>${params.data.name}</b> (${params.percent}%)<br/>총 건수 : ${params.value}건<br/>총 금액 : ${addCommas(params.data.Number)}원`
          );
        }
      },
      legend: {
        orient: "vertical",
        right: 10,
        top: "middle",
        type: "scroll",  // 스크롤링 기능 추가
      },
      series: [
        {
          name: '승인내역 없음',
          type: 'pie',
          radius: ['50%', '85%'],
          avoidLabelOverlap: false,
          itemStyle: {
            borderRadius: 10,
            borderColor: '#fff',
            borderWidth: 2
          },
          label: {
            show: false,
            position: 'center'
          },
          emphasis: {
            label: {
              show: true,
              fontSize: 20,
              fontWeight: 'bold'
            }
          },
          labelLine: {
            show: false
          },
          data: alignment === "sum"? showCategoryRank.sum : showCategoryRank.count
        }
      ]
    };

    option && myChart.setOption(option);

    // 창 크기 조절 이벤트 핸들러 추가
    const handleResize = () => {
      myChart.resize();
    };
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
      // 차트 인스턴스 삭제
      echarts.dispose(chart);
    };

  }, [alignment, showCategoryRank]);



  return (
    <Card
      variant="outlined"
      sx={{
        paddingBottom: "0"
      }}
    >
      <CardContent
        sx={{
          paddingBottom: "16px !important",
        }}
      > 
        <Box sx={{ display: "flex", alignItems:"center", mb:3 }}>
          <Typography variant="h3">용도별 차트</Typography>

          <ToggleButtonGroup
            color="primary"
            value={alignment}
            exclusive
            onChange={handleChange}
            aria-label="Platform"
            style={{ marginLeft: "auto" }}
          >
            <ToggleButton value="sum">금액순</ToggleButton>
            <ToggleButton value="count">건수순</ToggleButton>
          </ToggleButtonGroup>
        </Box>

        <Box ref={chartRef} sx={{ width: "100%", minHeight:"300px", pb:2 }}></Box>

      </CardContent>
    </Card>
  );
};

export default ChargeCategoryAnalysisChart;

