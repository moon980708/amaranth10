// BasicChart.js
import React, { useEffect } from "react";
import * as echarts from "echarts";
import { Card, CardContent, Typography, Box } from "@mui/material";

const CategoryChart = () => {
  useEffect(() => {
    var chartDom = document.getElementById("CategoryChart");
    var myChart = echarts.init(chartDom);
    var option;

    option = {
      xAxis: {
        type: 'category',
        data: ['식대', '출장비', '회의비', '도서구입비', '유지비']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          data: [1500, 2300, 2240, 2180, 1350],
          type: 'line'
        }
      ]
    };

    option && myChart.setOption(option);

    // 창 크기 조절 이벤트 핸들러 추가
    const handleResize = () => {
      myChart.resize();
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Card
      variant="outlined"
      sx={{
        paddingBottom: "0",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
      }}
    >
      <CardContent
        sx={{
          paddingBottom: "16px !important",
        }}
      >
        <Box
          sx={{
            alignItems: "center",
          }}
        >
          <Box>
            <Typography
              variant="h3"
              sx={{
                marginBottom: "0",
              }}
              gutterBottom
            >
              용도별 차트
            </Typography>
            <Box id="CategoryChart" sx={{ width: "100%", height: "300px" }}></Box>
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
};

export default CategoryChart;
