import React, {useState} from "react";
import { Card, CardContent, Typography, Box } from "@mui/material";
import Chart from 'react-apexcharts';
import { addCommas } from '../functions/SimpleFuntion';



const ChargeMonthChart = ({monthlyCharge}) => {
  //period : 통계차트에 x축에 띄울 조회기간배열
  //applyCharge   : 신청금액 배열
  //confirmCharge : 승인금액 배열
  //maxCharge : y축에서 max값으로 띄울 신청금액, 승인금액 중 max금액


  const optionsChargeMonthChart = {
    grid: {
      show: true,
      borderColor: "transparent",
      strokeDashArray: 2,
      padding: {
        left: 0,
        right: 0,
        bottom: 0,
      },
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: "42%",
        endingShape: "rounded",
        borderRadius: 3,
    
      },
    },
    colors: ["#1e4db7", "#a7e3f4"],
    fill: {
      type: "solid",
      opacity: 1,
    },
    chart: {
      offsetX: -15,
      toolbar: {
        show: true,
      },
      foreColor: "#adb0bb",
      fontFamily: "'DM Sans',sans-serif",
      sparkline: {
        enabled: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    markers: {
      size: 0,
    },
    legend: {
      show: true,
    },
    xaxis: {
      type: "category",
      categories: monthlyCharge.period,
      labels: {
        style: {
          cssClass: "grey--text lighten-2--text fill-color",
        },
      },
    },
    yaxis: {
      show: true,
      min: 0,
      max: monthlyCharge.maxCharge !== 0? monthlyCharge.maxCharge : 100000,
      tickAmount: 5,
      labels: {
        style: {
          cssClass: "grey--text lighten-2--text fill-color",
        },
        formatter: (value)=>{return addCommas(value)}
      },
    },
    stroke: {
      show: true,
      width: 5,
      lineCap: "butt",
      colors: ["transparent"],
    },
    tooltip: {
      theme: "dark",
    },
  };

  const seriesChargeMonthChart = [
    {
      name: "신청 금액",
      data: monthlyCharge.applyCharge,
    },
    {
      name: "승인 금액",
      data: monthlyCharge.confirmCharge,
    },
  ];



  return (
    <Card
      variant="outlined"
      sx={{
        paddingBottom: "0",
      }}
    >
      <CardContent
        sx={{
          paddingBottom: "16px !important",
        }}
      >
        <Box
          sx={{
            display: {
              sm: "flex",
              xs: "block",
            },
            alignItems: "center",
          }}
        >
          <Box>
            <Typography
              variant="h3"
              sx={{
                marginBottom: "0",
              }}
              gutterBottom
            >
              기간별 통계
            </Typography>
          </Box>
        </Box>
        <Box
          sx={{
            marginTop: "25px",
          }}
        >
          <Chart
            options={optionsChargeMonthChart}
            series={seriesChargeMonthChart}
            type="bar"
            height="300px"
          />
        </Box>
      </CardContent>
    </Card>
  );
};

export default ChargeMonthChart;
