// AlertContext.js
import React, { createContext, useState } from "react";
import Swal from "sweetalert2";

export const UserDeleteAlert = createContext();

export const AlertProvider = ({ children }) => {
  const [isAlertOpen, setIsAlertOpen] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  const showAlert = (message) => {
    setAlertMessage(message);
    setIsAlertOpen(true);
  };

  const hideAlert = () => {
    setIsAlertOpen(false);
  };

  const showDeleteAlert = (handleUserDelete) => {
    Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      width: "45%",
      allowOutsideClick: false,
    })
      .fire({
        title: "해당 사원을 정말 삭제하시겠습니까?",
        text: "'확인'을 누르면 사원 정보가 삭제됩니다",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "삭제",
        cancelButtonText: "취소",
        reverseButtons: false,
      })
      .then((result) => {
        if (result.isConfirmed) {
          Swal.fire("삭제 완료", "해당 사원의 정보가 삭제되었습니다", "success");
          handleUserDelete();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire("삭제 취소", "해당 사원의 정보가 삭제되지 않았습니다 :)", "error");
        }
      });
  };

  return (
    <UserDeleteAlert.Provider value={{ showAlert, hideAlert, showDeleteAlert }}>
      {children}
      {isAlertOpen && (
        <div>
          <div>{alertMessage}</div>
          <button onClick={hideAlert}>닫기</button>
        </div>
      )}
    </UserDeleteAlert.Provider>
  );
};
