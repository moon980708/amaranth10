import React, { useState, useEffect } from "react";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Checkbox,
  Card,
  CardContent,
  Tooltip,
  ToggleButton,
  ToggleButtonGroup
} from "@mui/material";

import { addCommas } from "../functions/SimpleFuntion";

const membersdata = [
  {
    id: "a01",
    userName: "김가영",
    deptName: "솔루션",
    rankName: "사원",
    count: 22,
    total: 10000,
  },
  {
    id: "a02",
    userName: "김가옹",
    deptName: "플랫폼",
    rankName: "대리",
    count: 20,
    total: 102345,
  },
  {
    id: "b01",
    userName: "김가용",
    deptName: "솔루션",
    rankName: "팀장",
    count: 18,
    total: 10078,
  },
  {
    id: "a15",
    userName: "뿡뿡이",
    deptName: "솔루션",
    rankName: "사원",
    count: 15,
    total: 10000,
  },
  {
    id: "f55",
    userName: "성소진",
    deptName: "erp",
    rankName: "사원",
    count: 10,
    total: 10000,
  },
  {
    id: "g78",
    userName: "이감자",
    deptName: "플랫폼",
    rankName: "사원",
    count: 9,
    total: 1000078,
  },
  {
    id: "g98",
    userName: "애기",
    deptName: "erp",
    rankName: "과장",
    count: 6,
    total: 10000788,
  },
];

const ChargeUserRankTable = ({userRank}) => {
  const [alignment, setAlignment] = useState("sum");     //정렬
  const [showUserRank, setShowUserRank] = useState([]);


  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };


  useEffect(() => { 
    if (alignment === "sum") {
      setShowUserRank(userRank.sum);
    } else {
      setShowUserRank(userRank.count);
    }

  }, [alignment]);

  return (
    <Card
      variant="outlined"
      sx={{
        paddingBottom: "0"
      }}
    >
      <CardContent
        sx={{
          paddingBottom: "16px !important",
        }}
      >
        <Box sx={{ display: "flex", alignItems:"center" }}>
          <Typography variant="h3">청구자 순위</Typography>

          <ToggleButtonGroup
            color="primary"
            value={alignment}
            exclusive
            onChange={handleChange}
            aria-label="Platform"
            style={{ marginLeft: "auto" }}
          >
            <ToggleButton value="sum">금액순</ToggleButton>
            <ToggleButton value="count">건수순</ToggleButton>
          </ToggleButtonGroup>
        </Box>

          <Box
            sx={{
              mt: 3,
              width: "100%",
              overflowX: "auto",
              margin: "auto", // 수평 중앙 정렬
            }}
          >
            <Table
              aria-label="simple table"
              sx={{
                mt: 3,
                whiteSpace: "nowrap",
              }}
            >
              <TableHead sx={{backgroundColor:"aliceblue"}}>
                <TableRow sx={{ width: "100%" }}>
                  <TableCell sx={{ width: "5%", textAlign: "center" }}>
                    <Typography sx={{ fontSize: "12px" }}>
                      순위
                    </Typography>
                  </TableCell>
                  <TableCell sx={{ width: "10%", textAlign: "center" }}>
                    <Typography sx={{ fontSize: "12px" }}>
                      Id
                    </Typography>
                  </TableCell>
                  <TableCell sx={{ width: "15%", textAlign: "center" }}>
                    <Typography sx={{ fontSize: "12px" }}>
                      이름
                    </Typography>
                  </TableCell>
                  <TableCell sx={{ width: "20%", textAlign: "center" }}>
                    <Typography sx={{ fontSize: "12px" }}>
                      부서
                    </Typography>
                  </TableCell>
                  <TableCell sx={{ width: "20%", textAlign: "center" }}>
                    <Typography sx={{ fontSize: "12px" }}>
                      건수
                    </Typography>
                  </TableCell>
                  <TableCell sx={{ width: "30%", textAlign: "center" }}>
                    <Typography sx={{ fontSize: "12px" }}>
                      금액
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {showUserRank.length === 0 ?
                  <TableRow>
                    <TableCell colSpan={6} >
                        <Typography color="textSecondary" sx={{ fontSize: "12px", textAlign:"center" }}>
                          해당 내역이 존재하지 않습니다.
                        </Typography>
                    </TableCell>
                  </TableRow>
                :
                  (showUserRank.map((user, index) => (
                    <TableRow key={user.id} sx={{ alignItems: "center" }}>
                      <TableCell sx={{ width: "5%", textAlign: "center" }}>
                        <Typography color="textSecondary" sx={{ fontSize: "12px" }}>
                          {user.rank}
                        </Typography>
                      </TableCell>
                      <TableCell sx={{ width: "10%", textAlign: "center" }}>
                        <Typography color="textSecondary" sx={{ fontSize: "12px" }}>
                          {user.id}
                        </Typography>
                      </TableCell>
                      <TableCell
                        sx={{
                          width: "15%",
                          textAlign: "center",
                          cursor: "pointer",
                        }}
                      >
                        <Tooltip title={user.resign ? "퇴사" : "재직"} placement="top">
                          <Typography color="textSecondary"
                            sx={{ fontSize: "12px", fontWeight: "600" }}
                          >
                            {user.userName}
                          </Typography>
                        </Tooltip>
                      </TableCell>
                      <TableCell sx={{ width: "20%", textAlign: "center" }}>
                        <Typography color="textSecondary" sx={{ fontSize: "12px" }}>
                          {user.deptName}
                        </Typography>
                      </TableCell>
                      <TableCell sx={{ width: "20%", textAlign: "center" }}>
                        <Typography color="textSecondary" sx={{ fontSize: "12px" }}>
                          {user.count}건
                        </Typography>
                      </TableCell>
                      <TableCell sx={{ width: "30%", textAlign: "center" }}>
                        <Typography color="textSecondary" sx={{ fontSize: "12px" }}>
                          {addCommas(user.sum)}원
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )))
                }
              </TableBody>
            </Table>
          </Box>
      </CardContent>
    </Card>
  );
};

export default ChargeUserRankTable;
