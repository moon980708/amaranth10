import React, { useState, useEffect } from "react";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Checkbox,
  IconButton,
  TablePagination,
  TableContainer,
  CircularProgress,
} from "@mui/material";
import ChargeLogDetailModal from "../modal/ChargeLogDetailModal";
import { Description, UnfoldMore } from "@mui/icons-material";
import { addCommas } from "../functions/SimpleFuntion";
import { format } from "date-fns";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import UseApi from "../UseApi";
axios.defaults.withCredentials = true;

const ChargeLogTable = ({
  onCheckboxChange,
  searchValue,
  searchField,
  searchYear,
  searchMonth,
  searchState,
  refresh,
  searchButtonClicked,
}) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [sortOrder, setSortOrder] = useState("asc");
  const [sortedField, setSortedField] = useState("expendDate");
  const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);
  const [chargeLogData, setChargeLogData] = useState([]);
  const [checkItems, setCheckItems] = useState([]);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const navigate = useNavigate();
  const [itemNum, setItemNum] = useState();
  const [refreshList, setRefreshList] = useState(false);
  const api = UseApi();
  const [listLoading, setListLoading] = useState(true);
  const handleRefreshList = () => {
    setRefreshList(!refreshList); // 상태를 변경하여 리렌더링을 유발
  };

  useEffect(() => {
    //지출정보리스트 뿌리기
    chargeLogs();
    // 체크박스 모두 해제
    setCheckItems([]);
    //첫번째 페이지 표시
    setPage(0);
  }, [searchValue, searchField, refresh, searchButtonClicked, refreshList]);

  // useEffect(() => {
  //   console.log(checkItems);
  // }, [checkItems]);

  // 청구내역 요청
  const chargeLogs = () => {
    const searchData = {
      id: loginUserId,
      searchValue: searchValue,
      searchField: searchField,
      year: searchYear,
      month: searchMonth,
      state: searchState === "전체" ? "" : searchState,
    };
    api
      .post("/user/chargelog/list", searchData)
      .then((response) => {
        const chargeData = response.data;
        setChargeLogData(chargeData);
        setListLoading(false);
      })
      .catch((error) => {
        console.error("지출정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 체크박스 단일 선택
  const handleSingleCheck = (checked, userChargeNo) => {
    if (checked) {
      // 단일 선택 시 체크된 아이템을 배열에 추가
      setCheckItems((prev) => [...prev, userChargeNo]);
    } else {
      // 단일 선택 해제 시 체크된 아이템을 제외한 배열 (필터)
      setCheckItems((prev) => prev.filter((el) => el !== userChargeNo));
    }
  };

  // 칩 컬러
  const getBackgroundColor = (state) => {
    switch (state) {
      case "승인":
        return "skyblue";
      case "반려":
        return "gray";
      case "승인대기":
        return "orange";
      default:
        return "white";
    }
  };

  // 페이징 - 다음 페이지
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  // 페이징 - 페이지당 줄 수
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // 정렬버튼 컨트롤
  const handleSort = (field) => {
    if (field === sortedField) {
      setSortOrder((prevSortOrder) => (prevSortOrder === "asc" ? "desc" : "asc"));
    } else {
      setSortedField(field);
      setSortOrder("asc");
    }
  };

  // 정렬 - 지출 날짜 비교
  const compareDates = (a, b) => {
    const dateA = new Date(a.expendDate);
    const dateB = new Date(b.expendDate);
    if (sortOrder === "asc") {
      return dateA - dateB;
    } else {
      return dateB - dateA;
    }
  };

  // 정렬 - 청구 날짜 비교
  const compareChargeDates = (a, b) => {
    const dateA = new Date(a.userChargeDate);
    const dateB = new Date(b.userChargeDate);
    if (sortOrder === "asc") {
      return dateA - dateB;
    } else {
      return dateB - dateA;
    }
  };

  // 정렬
  const sortedProducts = chargeLogData.sort((a, b) => {
    if (sortedField === "expendDate") {
      return compareDates(a, b);
    } else if (sortedField === "userChargeDate") {
      return compareChargeDates(a, b);
    } else {
      return sortOrder === "asc"
        ? a[sortedField].localeCompare(b[sortedField])
        : b[sortedField].localeCompare(a[sortedField]);
    }
  });

  // 상세 버튼 컨트롤
  const handleDetailButtonClick = (product) => {
    setIsDetailModalOpen(true);
    setItemNum(product.userChargeNo);
  };

  // 닫기 버튼 컨트롤
  const handleCloseModal = () => {
    setIsDetailModalOpen(false);
  };

  return (
    <div>
      <TableContainer style={{ maxHeight: "600px" }}>
        {listLoading ? (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "100%",
              height: "450px",
            }}
          >
            <CircularProgress size={50} />
          </Box>
        ) : (
          <Table>
            <TableHead style={{ position: "sticky", top: 0, zIndex: 1, background: "aliceblue" }}>
              <TableRow>
                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    선택
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    지출일시
                    <IconButton size="small" onClick={() => handleSort("expendDate")}>
                      <UnfoldMore
                        fontSize="small"
                        style={{
                          transform:
                            sortedField === "expendDate"
                              ? `scaleY(${sortOrder === "asc" ? -1 : 1})`
                              : "scaleY(1)",
                        }}
                      />
                    </IconButton>
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    청구일
                    <IconButton size="small" onClick={() => handleSort("userChargeDate")}>
                      <UnfoldMore
                        fontSize="small"
                        style={{
                          transform:
                            sortedField === "userChargeDate"
                              ? `scaleY(${sortOrder === "asc" ? -1 : 1})`
                              : "scaleY(1)",
                        }}
                      />
                    </IconButton>
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    차수
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    용도
                  </Typography>
                </TableCell>

                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    사용처
                  </Typography>
                </TableCell>

                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    청구금액
                  </Typography>
                </TableCell>

                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    결제수단
                  </Typography>
                </TableCell>

                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    청구현황
                  </Typography>
                </TableCell>

                <TableCell>
                  <Typography color="textSecondary" variant="h6" align="center">
                    상세
                  </Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            {sortedProducts.length === 0 ? ( // sortedProducts 배열의 길이가 0인 경우 메시지를 표시
              <TableRow>
                <TableCell colSpan={12} align="center">
                  <Typography variant="body1" align="center" style={{ padding: "16px" }}>
                    조회된 청구 내역이 없습니다.
                  </Typography>
                </TableCell>
              </TableRow>
            ) : (
              <TableBody>
                {sortedProducts
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((product) => (
                    <TableRow key={product.userChargeNo}>
                      <TableCell align="center">
                        {product.state === "반려" ? (
                          <Checkbox
                            color="secondary"
                            key={product.userChargeNo}
                            onChange={(e) => {
                              handleSingleCheck(e.target.checked, product.userChargeNo, product);
                              onCheckboxChange(
                                e.target.checked
                                  ? [...checkItems, product.userChargeNo]
                                  : checkItems.filter((el) => el !== product.userChargeNo)
                              );
                            }}
                            checked={checkItems.includes(product.userChargeNo)}
                          />
                        ) : (
                          <Checkbox disabled />
                        )}
                      </TableCell>

                      <TableCell align="center">
                        <Box>
                          <Typography variant="h6">{product.expendDate}</Typography>
                        </Box>
                      </TableCell>

                      <TableCell align="center">
                        <Typography variant="h6">
                          {format(new Date(product.userChargeDate), "yyyy-MM-dd")}
                        </Typography>
                      </TableCell>

                      <TableCell align="center">
                        <Typography variant="h6">{product.schedule}차</Typography>
                      </TableCell>

                      <TableCell align="center">
                        <Typography variant="h6">{product.categoryName}</Typography>
                      </TableCell>

                      <TableCell align="center">
                        <Typography variant="h6">{product.store}</Typography>
                      </TableCell>

                      <TableCell align="center">{addCommas(product.charge)} 원</TableCell>

                      <TableCell align="center">
                        <Typography variant="h6">{product.payment}</Typography>
                      </TableCell>

                      <TableCell align="center">
                        <Chip
                          sx={{
                            pl: "4px",
                            pr: "4px",
                            backgroundColor: getBackgroundColor(product.state),
                            color: "#fff",
                          }}
                          size="small"
                          label={product.state}
                        ></Chip>
                      </TableCell>

                      <TableCell align="center">
                        <IconButton
                          color="info"
                          size="small"
                          onClick={() => handleDetailButtonClick(product)}
                        >
                          <Description fontSize="small" />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            )}
          </Table>
        )}
        {isDetailModalOpen && (
          <ChargeLogDetailModal
            isOpen={isDetailModalOpen}
            onClose={handleCloseModal}
            clickDetail={itemNum}
            chargeLogData={chargeLogData}
            onRefresh={handleRefreshList}
          />
        )}
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]} // 페이지당 행 개수 선택 옵션
        component="div"
        count={chargeLogData.length} // 전체 행 개수
        page={page} // 현재 페이지
        rowsPerPage={rowsPerPage} // 현재 페이지당 행 개수
        onPageChange={handleChangePage} // 페이지 변경 핸들러
        onRowsPerPageChange={handleChangeRowsPerPage} // 페이지당 행 개수 변경 핸들러
      />
    </div>
  );
};

export default ChargeLogTable;
