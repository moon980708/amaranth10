import React from "react";

import {
  Button,
  Card,
  CardContent,
  Box,
  Typography,
  TextField,
  IconButton,
  Fab,
} from "@mui/material";
import CategoryTableData from "../tabledata/CategoryTableData";
import { Search } from "@mui/icons-material";
import DoDisturbIcon from "@mui/icons-material/DoDisturb";
import ScheduleTableData from "../tabledata/ScheduleTableData";
import Swal from "sweetalert2";

import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import {
  change_ScheduleState,
  reset_Select_Schedule,
} from "../../reducer/module/scheduleReducer";
import { useNavigate } from "react-router-dom";
import UseApi from "../UseApi";
axios.defaults.withCredentials = true;

const ScheduleTable = ({
  scheduleList,
  setScheduleList,
  listUpdate,
  setListUpdate,
  listLoadingStart,
  setListLoadingStart,
  chargeDate,
}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const api = UseApi();
  //체크박스 선택된 차수no
  const selectScheduleNo = useSelector(
    (state) => state.scheduleReducer.selectScheduleNo
  );

  // console.log("selectScheduleNo : " + typeof JSON.stringify(selectScheduleNo));
  //차수 체크박스로 삭제 버튼
  const handleDeleteSelectedSchedules = () => {
    if (selectScheduleNo.length === 0) {
      return;
    }

    Swal.fire({
      title: "선택한 스케줄 삭제",
      text: "선택한 스케줄을 삭제하시겠습니까?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "네",
      cancelButtonText: "아니요",
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .delete(`/schedule/deleteAll`, {
            params: {
              selectScheduleNo: selectScheduleNo.join(","),
            },
          })
          .then((result) => {
            console.log("삭제성공");
            dispatch(reset_Select_Schedule());
            dispatch(change_ScheduleState());

            setListLoadingStart(!listLoadingStart); //삭제했으니 리스트 다시 랜더링 시키기위한 set변경함수
          })
          .catch((error) => {
            // if(error.response && error.response.status === 401){
            //   alert("로그인세션만료");
            //   navigate("/");
            // }
            console.log("삭제 요청 실패");
          });
        // 선택한 스케줄 삭제 후, 선택된 아이템과 스케줄번호 초기화
        // setSelectedItems([]);
        // dispatch(add_Select_Schedule([])); // 선택한 스케줄번호를 빈 배열로 초기화
      }
    });
  };

  return (
    <Box>
      <Card
        variant="outlined"
        sx={{
          borderRadius: "10px",
          boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)", // 그림자 스타일을 직접 지정합니다
        }}
      >
        <CardContent>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              ml: 2,
              mr: 2,
              mb: 2,
            }}
          >
            <Typography variant="h3">경비스케줄 설정</Typography>
            <Fab
              color="primary"
              variant="extended"
              sx={{
                ml: "auto",
              }}
              onClick={handleDeleteSelectedSchedules}
            >
              <DoDisturbIcon />
              <Typography
                sx={{
                  ml: 1,
                  textTransform: "capitalize",
                }}
              >
                차수삭제
              </Typography>
            </Fab>
          </Box>
          <Box
            sx={{
              overflow: {
                xs: "auto",
                sm: "unset",
              },
            }}
          >
            <ScheduleTableData
              scheduleList={scheduleList}
              setScheduleList={setScheduleList}
              listLoadingStart={listLoadingStart}
              setListLoadingStart={setListLoadingStart}
              chargeDate={chargeDate}
            />
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
};

export default ScheduleTable;
