import React, { useState, useEffect } from "react";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  IconButton,
  Checkbox,
  Avatar,
  CircularProgress,
} from "@mui/material";
import { TableSortLabel } from "@mui/material";
import { DeleteSweep, EditOutlined } from "@mui/icons-material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import EditUserModal from '../modal/EditUserModal';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import FirstPageIcon from '@mui/icons-material/FirstPage';
import LastPageIcon from "@mui/icons-material/LastPage";
import TableContainer from "@mui/material/TableContainer";
import img1 from "../../assets/images/avatar/1.png"
import img2 from "../../assets/images/avatar/2.png";
import img3 from "../../assets/images/avatar/3.png";
import img4 from "../../assets/images/avatar/4.png";
import img5 from "../../assets/images/avatar/5.png";
import img6 from "../../assets/images/avatar/6.png";
import img7 from "../../assets/images/avatar/7.png";
import img8 from "../../assets/images/avatar/8.png";
axios.defaults.withCredentials = true; 

const CustomTableSortLabel = ({ active, direction, onClick, children }) => {
  return (
    <TableSortLabel
      active={active}
      direction={direction}
      onClick={onClick}
      sx={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        cursor: "pointer",
        ".MuiTableSortLabel-icon": {
          display: "none", // 기본으로 뜨는 화살표 제거
        },
      }}
    >
        {children}
      {active ? direction === "asc" ? <ArrowDropUpIcon /> : <ArrowDropDownIcon /> : null}
    </TableSortLabel>
  );
};

// 부서 색상 설정
const theme = createTheme({
  palette: {
    red: { main: "#FF4400" },
    orange: { main: "#FF6F00" },
    yellow: { main: "#FBC02D" },
    lime: { main: "#CDDC39" },
    green: { main: "#2db400" },
    skyblue: { main: "#99CCFF" },
    blue: { main: "#17BDFF" },
    navy: { main: "#0000CD" },
    purple: { main: "#A97BF5" },
    pink: { main: "#FA8ee5" },
    grey: { main: "#A9A9A9" },
  },
});

const UserTable = ({
  userList,
  setUserList,
  editUser,
  setEditUser,
  deptList,
  rankList,
  checkedId,
  handleCheckedAllId,
  handleCheckedId,
  setCheckedId,
  handleEditClickSnackbar,
  submitUser,
  deleteUser,
  pagingUser,
  setPagingUser,
  page,
  setPage,
  totalCount,
  api,
  loadingDept,
  loadingRank,
  loadingUserList,
  setLoadingUserList,
}) => {
  const navigate = useNavigate();

  // 정렬
  const [orderBy, setOrderBy] = useState("id"); // 기본 정렬 열
  const [order, setOrder] = useState("asc"); // 기본 정렬 순서

  const handleRequestSort = (property) => {
    const newOrderBy = property;
    const newOrder = orderBy === property && order === "asc" ? "desc" : "asc";

    setOrderBy(newOrderBy);
    setOrder(newOrder);
  };

  function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  function getComparator(order, orderBy) {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) {
        return order;
      }
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  // 사원 수정 모달
  const [editModalOpen, setEditModalOpen] = useState(false);

  const handleOpenEditModal = () => {
    setEditModalOpen(true);
  };
  const handleCloseEditModal = () => {
    setEditModalOpen(false);
  };

  const sortedData = stableSort(userList, getComparator(order, orderBy));
  const totalPages = Math.ceil(totalCount / 10); // 총 페이지 수

  // 페이징 처리
  const pageRange = 10; // 현재 페이지 주변에 표시할 페이지 아이콘의 개수

  function generatePageIcons(currentPage, totalPages, pageRange) {
    let startPage = Math.max(currentPage - Math.floor(pageRange / 2), 1);
    let endPage = Math.min(startPage + pageRange - 1, totalPages);

    if (endPage - startPage + 1 < pageRange) {
      startPage = Math.max(endPage - pageRange + 1, 1);
    }

    const pageIcons = [];
    for (let i = startPage; i <= endPage; i++) {
      pageIcons.push(i);
    }

    return pageIcons;
  }

  const pageIcons = generatePageIcons(page, totalPages, pageRange);

  // 페이지 클릭
  const handlePageClick = (pageNumber) => {
    setPage(pageNumber);
    setPagingUser(!pagingUser);
  };

  // 첫 페이지 클릭
  const handleFirstPage = () => {
    setPage(1);
    setPagingUser(!pagingUser);
  };

  // 이전 페이지 클릭
  const handlePrevPage = () => {
    if (page > 1) {
      setPage(page - 1);
      setPagingUser(!pagingUser);
    }
  };

  // 다음 페이지 클릭
  const handleNextPage = () => {
    if (page < totalPages) {
      setPage(page + 1);
      setPagingUser(!pagingUser);
    }
  };

  // 마지막 페이지 클릭
  const handleLastPage = () => {
    setPage(totalPages);
    setPagingUser(!pagingUser);
  };

  // 사원 목록 가져오는 axios 통신
  // useEffect(() => {
  //   const fetchData = async () => {
  //     try {
  //       // 첫 번째 axios 통신 : 전체 사원 개수 가져오기
  //       const totalResult = await axios.get("/admin/user/getsearchcount");
  //       setTotalCount(totalResult.data);
  //       console.log("사원 총 개수 : " + totalResult.data);

  //       // 두 번째 axios 통신 : 사원 목록 가져오기
  //       const userListResult = await axios.get("/admin/user", {
  //         params: {
  //           page: page,
  //         },
  //       });
  //       console.log("사원 목록 : " + userListResult.data);
  //       setUserList(userListResult.data);
  //     } catch (error) {
  //       if (error.response && error.response.status === 401) {
  //         alert("로그인 세션이 만료되었습니다");
  //         navigate("/");
  //       }
  //       console.log(page);
  //       console.log("통신 오류");
  //       alert("다시 로딩해주세요.");
  //     }
  //   };

  //   fetchData(); // fetchData 함수 호출
  // }, [submitUser, editUser, deleteUser, pagingUser]);

  // 사원 수정할 state
  const [editUserData, setEditUserData] = useState("");

  // 수정 버튼 클릭 시
  const handleEditBtnClick = (id, userName, email, resign, deptNo, rankNo) => {
    const userInfo = {
      id,
      userName,
      email,
      resign,
      deptNo,
      rankNo,
    };

    setEditUserData(userInfo);
  };

  // 퇴사처리 아이콘 클릭 시 (SweetAlert)
  const handleDeleteBtnClick = (id, resign, userName) => {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      width: "45%",
    });

    swalWithBootstrapButtons
      .fire({
        title: "'" + userName + "' 사원을 퇴사 처리하시겠습니까?",
        text: "'확인'을 누르면 해당 사원이 퇴사처리됩니다.",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "확인",
        cancelButtonText: "취소",
        reverseButtons: false,
      })
      .then((result) => {
        if (result.isConfirmed) {
          // 확인
          api
            .put(`/admin/user/update/resign/${id}`, {
              resign: resign,
            })
            .then((result) => {
              swalWithBootstrapButtons.fire(
                "퇴사 처리 완료",
                "'" + userName + "' 사원의 퇴사처리가 완료되었습니다",
                "success"
              );

              setCheckedId(checkedId.filter((selectedId) => selectedId !== id));
              setEditUser(!editUser);
            })
            .catch((error) => {
              // if (error.response && error.response.status === 401) {
              //   alert("로그인세션만료");
              //   navigate("/");
              // }
              if (error.response) {
                console.log("통신error");
              }
            });
        } //여기까지 확인눌렀을떄
        else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            "퇴사 처리 취소",
            "'" + userName + "' 사원이 퇴사 처리되지 않았습니다.",
            "error"
          );
        }
      });
  };

  // 부서별로 배경색 설정
  const getDeptColor = (deptName) => {
    switch (deptName) {
      case "총무부":
        return theme.palette.red.main;
      case "경리부":
        return theme.palette.yellow.main;
      case "구매자재부":
        return theme.palette.lime.main;
      case "품질관리부":
        return theme.palette.green.main;
      case "개발 1팀":
        return theme.palette.skyblue.main;
      case "개발 2팀":
        return theme.palette.blue.main;
      case "기술부":
        return theme.palette.navy.main;
      case "해외영업부":
        return theme.palette.purple.main;
      case "국내영업부":
        return theme.palette.pink.main;
      case "상품관리부":
        return theme.palette.orange.main;
      default:
        return theme.palette.grey.main;
    }
  };

  // 직급별로 아바타 설정
  const getAvatarImage = (rankName) => {
    switch (rankName) {
      case "사원":
        return img1;
      case "대리":
        return img2;
      case "과장":
        return img3;
      case "차장":
        return img5;
      case "부장":
        return img6;
      case "이사":
        return img7;
      case "상무":
        return img8;
      case "전무":
        return img4;
      default:
        return null;
    }
  };

  return (
    <div>
      <TableContainer
        sx={{
          overflowX: "auto", // 가로 스크롤 추가
        }}
      >
        <Table
          aria-label="simple table"
          sx={{
            mt: 4,
            whiteSpace: "nowrap",
          }}
        >
          <TableHead
            sx={{
              backgroundColor: "aliceblue",
              position: "sticky",
              top: 0,
              zIndex: 1,
            }}
          >
            <TableRow>
              <TableCell align="center">
                <Checkbox
                  color="secondary"
                  checked={
                    checkedId.length > 0 &&
                    checkedId.length === userList.filter((user) => !user.resign).length
                  }
                  onChange={handleCheckedAllId}
                />
              </TableCell>
              <TableCell align="center">
                <CustomTableSortLabel
                  active={orderBy === "id"}
                  direction={order}
                  onClick={() => handleRequestSort("id")}
                >
                  <Typography color="textprimary" variant="h6" sx={{ fontWeight: "bold" }}>
                    Id
                  </Typography>
                </CustomTableSortLabel>
              </TableCell>
              <TableCell align="center">
                <CustomTableSortLabel
                  active={orderBy === "userName"}
                  direction={orderBy === "userName" ? order : "asc"}
                  onClick={() => handleRequestSort("userName")}
                >
                  <Typography color="textprimary" variant="h6" sx={{ fontWeight: "bold" }}>
                    이름
                  </Typography>
                </CustomTableSortLabel>
              </TableCell>
              <TableCell align="center">
                <CustomTableSortLabel
                  active={orderBy === "deptNo"}
                  direction={orderBy === "deptNo" ? order : "asc"}
                  onClick={() => handleRequestSort("deptNo")}
                >
                  <Typography color="textprimary" variant="h6" sx={{ fontWeight: "bold" }}>
                    부서
                  </Typography>
                </CustomTableSortLabel>
              </TableCell>
              <TableCell align="center">
                <CustomTableSortLabel
                  active={orderBy === "rankNo"}
                  direction={orderBy === "rankNo" ? order : "asc"}
                  onClick={() => handleRequestSort("rankNo")}
                >
                  <Typography color="textprimary" variant="h6" sx={{ fontWeight: "bold" }}>
                    직급
                  </Typography>
                </CustomTableSortLabel>
              </TableCell>
              <TableCell align="center">
                <CustomTableSortLabel
                  active={orderBy === "email"}
                  direction={orderBy === "email" ? order : "asc"}
                  onClick={() => handleRequestSort("email")}
                >
                  <Typography color="textprimary" variant="h6" sx={{ fontWeight: "bold" }}>
                    E-mail
                  </Typography>
                </CustomTableSortLabel>
              </TableCell>
              <TableCell align="center">
                <Typography color="textprimary" variant="h6" sx={{ fontWeight: "bold" }}>
                  재직상태
                </Typography>
              </TableCell>
              <TableCell align="center">
                <Typography color="textprimary" variant="h6" sx={{ fontWeight: "bold" }}>
                  수정
                </Typography>
              </TableCell>
              <TableCell align="center">
                <Typography color="textprimary" variant="h6" sx={{ fontWeight: "bold" }}>
                  퇴사처리
                </Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loadingUserList ? (
              <TableRow>
                <TableCell sx={{ height: "700px" }} align="center" colSpan={9}>
                  <CircularProgress size={50} />
                </TableCell>
              </TableRow>
            ) : sortedData.length === 0 ? (
              <TableRow>
                <TableCell colSpan={9}>
                  <Typography color="textSecondary" sx={{ fontSize: "17px", textAlign: "center" }}>
                    검색결과가 없습니다.
                  </Typography>
                </TableCell>
              </TableRow>
            ) : (
              sortedData.map((user, index) => (
                <TableRow
                  key={index}
                  sx={{
                    "&:hover": {
                      backgroundColor: "rgba(0, 0, 0, 0.1)",
                      cursor: "pointer", // Hover 시 커서 모양 변경
                    },
                  }}
                >
                  <TableCell align="center">
                    <Checkbox
                      color="secondary"
                      checked={checkedId.indexOf(user.id) !== -1}
                      onChange={(event) => handleCheckedId(event, user.id)}
                      disabled={user.resign === true}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {user.id}
                    </Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Box display="flex" alignItems="center" justifyContent="center">
                      <Avatar
                        sx={{
                          width: 32,
                          height: 32,
                          mr: 1,
                          ml: 1,
                          border: "1px solid black",
                          borderRadius: "50%",
                        }}
                        src={getAvatarImage(user.rankName)}
                        alt={user.avatar}
                        variant="circular"
                      />
                      <Typography color="textprimary" variant="h6">
                        {user.userName}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell align="center">
                    <ThemeProvider theme={theme}>
                      <Chip
                        sx={{
                          pl: "4px",
                          pr: "4px",
                          backgroundColor: getDeptColor(user.deptName),
                          color: "#fff",
                          width: "90px", // 크기 조정을 위한 width 값 지정
                        }}
                        size="small"
                        label={user.deptName}
                      ></Chip>
                    </ThemeProvider>
                  </TableCell>
                  <TableCell align="center">
                    <Typography color="textprimary" variant="h6">
                      {user.rankName}
                    </Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography color="textprimary" variant="h6">
                      {user.email}
                    </Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography color="textprimary" variant="h6">
                      {user.resign ? "퇴사" : "재직"}
                    </Typography>
                  </TableCell>
                  {/* 수정 아이콘 */}
                  <TableCell align="center">
                    <Typography color="textprimary" variant="h6">
                      <Box>
                        <IconButton
                          color="neutral"
                          variant="contained"
                          size="small"
                          onClick={() => {
                            handleEditBtnClick(
                              user.id,
                              user.userName,
                              user.email,
                              user.resign,
                              user.deptNo,
                              user.rankNo
                            );
                            setEditModalOpen(true);
                          }}
                          disabled={user.resign === true ? true : false}
                        >
                          <EditOutlined />
                        </IconButton>
                      </Box>
                    </Typography>
                  </TableCell>
                  {/* 삭제 아이콘 */}
                  <TableCell align="center">
                    <Typography color="textprimary" variant="h6">
                      <Box>
                        <IconButton
                          color="neutral"
                          variant="contained"
                          onClick={() => {
                            handleDeleteBtnClick(user.id, user.resign, user.userName);
                          }}
                          disabled={user.resign === true ? true : false}
                        >
                          <DeleteSweep />
                        </IconButton>
                      </Box>
                    </Typography>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>

      {/* 페이징 */}
      <div style={{ display: "flex", justifyContent: "center", margin: "20px 0" }}>
        <IconButton
          onClick={handleFirstPage}
          disabled={page === 1}
          style={{
            border: "1px solid #ccc",
            borderRadius: "50%",
            marginRight: "5px",
          }}
        >
          <FirstPageIcon />
        </IconButton>
        {/* 이전 페이지 아이콘 */}
        <IconButton
          onClick={handlePrevPage}
          disabled={page === 1}
          style={{
            border: "1px solid #ccc",
            borderRadius: "50%",
            marginRight: "5px",
          }}
        >
          <KeyboardArrowLeftIcon />
        </IconButton>
        {/* 페이지 아이콘 */}
        {pageIcons.map((pageNumber, index) => (
          <IconButton
            key={index}
            onClick={() => handlePageClick(pageNumber)}
            style={{
              color: page === pageNumber ? "white" : "gray",
              border: page === pageNumber ? "1px #D3D3D3" : "1px solid #ccc",
              borderRadius: "50%",
              width: "40px",
              height: "40px",
              marginLeft: "5px",
              marginRight: "5px",
              backgroundColor: page === pageNumber ? "#D3D3D3" : "white",
            }}
          >
            {pageNumber}
          </IconButton>
        ))}
        {/* 다음 페이지 아이콘 */}
        <IconButton
          onClick={handleNextPage}
          disabled={page === totalPages}
          style={{
            border: "1px solid #ccc",
            borderRadius: "50%",
            marginLeft: "5px",
          }}
        >
          <KeyboardArrowRightIcon />
        </IconButton>
        <IconButton
          onClick={handleLastPage}
          disabled={page === totalPages}
          style={{
            border: "1px solid #ccc",
            borderRadius: "50%",
            marginRight: "5px",
          }}
        >
          <LastPageIcon />
        </IconButton>{" "}
      </div>

      {editModalOpen === true ? (
        <EditUserModal
          editUser={editUser}
          setEditUser={setEditUser}
          editUserData={editUserData}
          deptList={deptList}
          rankList={rankList}
          checkedId={checkedId}
          setCheckedId={setCheckedId}
          handleEditClickSnackbar={handleEditClickSnackbar}
          setEditModalOpen={setEditModalOpen}
          handleCloseEditModal={handleCloseEditModal}
          api={api}
        />
      ) : null}
    </div>
  );
};

export default UserTable;