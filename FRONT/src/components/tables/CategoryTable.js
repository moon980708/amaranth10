import React from "react";
import { useState, useEffect } from "react";

import {
  Card,
  CardContent,
  Box,
  Typography,
  TextField,
  IconButton,
} from "@mui/material";
import CategoryTableData from "../tabledata/CategoryTableData";
import { Search } from "@mui/icons-material";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { get_CategoryList } from "../../reducer/module/matchingReducer";
import getCategoryList from "../../reducer/axios/matchingAbout/getCategoryList";
import getSearchCategoryKeyword from "../../reducer/axios/matchingAbout/getSearchCategoryKeyword";
import { useNavigate } from "react-router-dom";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import UseApi from '../UseApi';


const CategoryTable = ({ setSelectCategory }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const api = UseApi();

  const [keyword, setKeyword] = useState("");
  const handleClearKeyword = () => {
    setKeyword("");
  }

  useEffect(() => {
    const fetchCategoryList = async () => {
      try {
        const result = await getCategoryList(navigate, api);
        dispatch(get_CategoryList(result));
      } catch (error) {
        console.log("용도리스트 불러와서 리덕스 저장 실패");
      }
    };

    fetchCategoryList();
  }, []);
  const searchCategoryList = async (keyword) => {
    try {
      const result = await getSearchCategoryKeyword(keyword, navigate, api);
      dispatch(get_CategoryList(result));
    } catch (error) {
      console.log("용도리스트 검색결과 리덕스 저장 실패");
    }
  };
  // 검색 시 엔터 입력 가능하게
  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      e.preventDefault(); // 기본 엔터 키 동작 막기

      searchCategoryList(keyword);
    }
  };
  const list = useSelector((state) => state.matchingReducer.categoryList);
  // console.log("용도리스트 : " + list);
  if (list === null) {
    return (
      <Box>
        <Card variant="outlined" sx={{ maxWidth: 600 }}>
          <CardContent>
            <div>Loading...</div>
          </CardContent>
        </Card>
      </Box>
    );
  } else {
    return (
      <Box>
        <Card variant="outlined" sx={{ borderRadius: "10px",
          boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)", maxWidth: 600 }}>
          <CardContent>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                ml: 8,
                mr: 5,
              }}
            >
              <Typography variant="h3">용도</Typography>
              <Box
                sx={{ display: "flex", alignItems: "center", width: "160px" }}
              >
                <TextField
                  id="search"
                  label="용도명 검색"
                  variant="outlined"
                  placeholder="전체검색"
                  value={keyword}
                  size="small"
                  onChange={(e) => {
                    setKeyword(e.target.value);
                  }}
                  onKeyDown={handleKeyDown} // 엔터 키 이벤트 리스너 추가

                  InputProps={{
                    endAdornment: (
                      <React.Fragment>
                        {keyword && ( // 내용이 있을 때만 x 아이콘 표시
                          <IconButton
                            sx={{ padding: 0, mr: 0.4, color: "#d3d3d3" }}
                            // color="primary"
                            aria-label="clear-search"
                            onClick={()=>{handleClearKeyword()}}
                          >
                            <HighlightOffIcon />
                          </IconButton>
                        )}
                        <IconButton
                          sx={{ padding: 0 }}
                          color="primary"
                          aria-label="search"
                          onClick={() => {
                            searchCategoryList(keyword);
                          }}
                        >
                          <Search />
                        </IconButton>
                      </React.Fragment>
                    ),
                  }}
                />
              </Box>
            </Box>
            <Box
              sx={{
                overflow: {
                  xs: "auto",
                  sm: "unset",
                },
              }}
            >
              <CategoryTableData setSelectCategory={setSelectCategory} />
            </Box>
          </CardContent>
        </Card>
      </Box>
    );
  }
};

export default CategoryTable;
