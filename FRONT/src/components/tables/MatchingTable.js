import React from "react";
import { Card, CardContent, Box, Typography, Button, Fab } from "@mui/material";

import CheckCircleOutlineOutlinedIcon from "@mui/icons-material/CheckCircleOutlineOutlined";
import Swal from "sweetalert2";
import MatchingTableData from "../tabledata/MatchingTableData";
import { useDispatch, useSelector } from "react-redux";
import postScheduleMatching from "../../reducer/axios/matchingAbout/postScheduleMatching";
import { useNavigate } from "react-router-dom";
import UseApi from '../UseApi';

const MatchingTable = ({ selectCategory, ItemList, selectedItems }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const api = UseApi();

  const selectedCategory = useSelector(
    (state) => state.matchingReducer.selectedCategory
  );
  const selectedItemList = useSelector(
    (state) => state.matchingReducer.selectedItemList
  );
  const onGoingSchedule = useSelector(
    (state) => state.matchingReducer.onGoingSchedule
  );

  const handleCompleteMatching = async () => {
    const result = await Swal.fire({
      title: "용도 항목 매칭을 완료하시겠습니까?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "완료",
      cancelButtonText: "취소",
    });
    if (result.isConfirmed) {
      const itemNoList = selectedItemList.map((item) => item.itemNo);
      const itemOptionList = selectedItemList.map((item) => item.itemOption);
      const itemEssential = selectedItemList.map((item) => item.itemEssential);
      console.log("====================");
      console.log(selectedCategory);
      console.log(itemNoList);
      console.log(itemOptionList);
      console.log(itemEssential);
      console.log(selectedItemList);
      console.log("====================");
      const response = await postScheduleMatching(
        selectedCategory,
        itemNoList,
        itemOptionList,
        itemEssential,
        navigate,
        api
      );
      if (response === "success") {
        Swal.fire("매칭 완료!", "매칭이 성공적으로 진행되었습니다.", "success");
      } else {
        alert("매칭 실패");
      }
      // Swal.fire("매칭 완료!", "매칭이 성공적으로 진행되었습니다.", "success");
    }
  };

  return (
    <Box>
      <Card
        variant="outlined"
        sx={{
          borderRadius: "10px",
          boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
        }}
      >
        <CardContent>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              ml: 2,
              mr: 2,
            }}
          >
            <Typography variant="h3"> {selectCategory} 항목 매칭</Typography>
            <Fab
              color="primary"
              variant="extended"
              sx={{
                ml: "auto",
              }}
              onClick={handleCompleteMatching}
              disabled={
                selectedCategory === null || onGoingSchedule !== null
                  ? true
                  : false
              }
            >
              <CheckCircleOutlineOutlinedIcon />
              <Typography
                sx={{
                  ml: 1,
                  textTransform: "capitalize",
                }}
              >
                매칭완료
              </Typography>
            </Fab>
          </Box>

          <MatchingTableData
            ItemList={ItemList}
            selectedItems={selectedItems}
          />
        </CardContent>
      </Card>
    </Box>
  );
};

export default MatchingTable;
