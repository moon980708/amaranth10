import React, { useState, useEffect } from "react";
import { Box, Card, CardContent, Divider, Typography, Grid } from "@mui/material";
import { CallMissedOutgoing, CheckCircleOutline, Grading } from "@mui/icons-material";
import { LocalizationProvider, DatePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import ko from "date-fns/locale/ko"; // 한국어 로케일 import
import axios from "axios";
import { addCommas } from "../functions/SimpleFuntion";
import UseApi from "../UseApi";
axios.defaults.withCredentials = true;

const UserChargeSummary = ({ onDateChange }) => {
  const currentDate = new Date();
  const [summaryData, setSummaryData] = useState([]);
  const [chargeDate, setChargeDate] = useState(new Date());
  const [earliestDate, setEarliestDate] = useState(new Date());
  const [filteredSummaryData, setFilteredSummaryData] = useState([]);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const navigate = useNavigate();
  const api = UseApi();

  useEffect(() => {
    if (loginUserId) {
      myData();
      onDateChange(chargeDate.year, chargeDate.month);
    }
  }, [loginUserId, chargeDate.year, chargeDate.month]);

  const handleChargeDateChange = (date) => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const filteredData = summaryData.filter(
      (dataItem) => dataItem.year === year && dataItem.month === month
    );
    setFilteredSummaryData(filteredData);
    onDateChange(year, month);
  };

  const myData = () => {
    api
      .get(`/user/home/summary?id=${loginUserId}`)
      .then((response) => {
        const summaryData = response.data;

        if (summaryData.length === 0) {
          setChargeDate({
            year: currentDate.getFullYear(),
            month: currentDate.getMonth() + 1,
          });
        } else {
          let earliestYear = summaryData.reduce((min, dataItem) => {
            return dataItem.year < min ? dataItem.year : min;
          }, Infinity);

          let earliestMonth = summaryData.reduce((min, dataItem) => {
            return dataItem.year === earliestYear && dataItem.month < min ? dataItem.month : min;
          }, Infinity);

          let latestYear = summaryData.reduce((max, dataItem) => {
            return dataItem.year > max ? dataItem.year : max;
          }, -Infinity);

          let latestMonth = summaryData.reduce((max, dataItem) => {
            return dataItem.year === latestYear && dataItem.month > max ? dataItem.month : max;
          }, -Infinity);

          setEarliestDate({ year: earliestYear, month: earliestMonth });
          setChargeDate({ year: latestYear, month: latestMonth });
          setSummaryData(summaryData);

          const filteredData = summaryData.filter(
            (dataItem) => dataItem.year === latestYear && dataItem.month === latestMonth
          );
          setFilteredSummaryData(filteredData);
        }
      })
      .catch((error) => {
        console.error("청구 요약 데이터 요청 실패", error);
        if (error.response) {
        }
      });
  };

  return (
    <Card
      variant="outlined"
      sx={{
        paddingBottom: "0",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
        height: "357px",
      }}
    >
      <CardContent
        sx={{
          paddingBottom: "16px !important",
        }}
      >
        <Box
          sx={{
            alignItems: "center",
          }}
        >
          <Box>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                height: "100%",
                marginTop: -1,
              }}
            >
              <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ko}>
                <DatePicker
                  value={new Date(`${chargeDate.year}-${chargeDate.month}`)}
                  onChange={handleChargeDateChange}
                  views={["year", "month"]}
                  format="yyyy-MM"
                  slotProps={{
                    textField: { variant: "outlined", size: "small" },
                  }}
                  minDate={new Date(`${earliestDate.year}-${earliestDate.month}-01`)}
                  maxDate={new Date()}
                />
              </LocalizationProvider>
            </Box>
            <Box id="UserChargeSummary" sx={{ width: "100%", height: "auto" }}>
              {filteredSummaryData.length === 0 ? (
                <Box sx={{ textAlign: "center", mt: 5 }}>
                  <Typography>표시할 청구 내역이 없습니다.</Typography>
                </Box>
              ) : (
                filteredSummaryData.map((dataItem, index) => (
                  <Grid key={index} container spacing={2}>
                    <Grid item xs={12}>
                      <Card style={{ backgroundColor: "aliceblue", marginBottom: 0 }}>
                        <Typography variant="h4">
                          총 청구 금액 : {addCommas(dataItem.approvedSum + dataItem.pendingSum)} 원
                        </Typography>
                      </Card>
                    </Grid>
                    <Grid item xs={12}>
                      <Card style={{ backgroundColor: "aliceblue", marginTop: 0 }}>
                        <Typography variant="h4">
                          총 청구 건 : {dataItem.cmp + dataItem.fail + dataItem.ing} 건
                        </Typography>
                        <Divider sx={{ marginBottom: "10px", marginTop: "10px" }} />
                        <Grid container justifyContent="space-between">
                          <Grid item xs={4}>
                            <Card
                              style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                flexDirection: "column",
                              }}
                            >
                              <Box margin="10px" color="green">
                                <CheckCircleOutline />
                              </Box>
                              <Typography>승인 : {dataItem.cmp} 건</Typography>
                            </Card>
                          </Grid>

                          <Grid item xs={4}>
                            <Card
                              style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                flexDirection: "column",
                              }}
                            >
                              <Box margin="10px">
                                <Grading />
                              </Box>
                              <Typography>대기 : {dataItem.ing} 건</Typography>
                            </Card>
                          </Grid>
                          <Grid item xs={4}>
                            <Card
                              style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                flexDirection: "column",
                              }}
                            >
                              <Box margin="10px" color="red">
                                <CallMissedOutgoing />
                              </Box>
                              <Typography>반려 : {dataItem.fail} 건</Typography>
                            </Card>
                          </Grid>
                        </Grid>
                      </Card>
                    </Grid>
                  </Grid>
                ))
              )}
            </Box>
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
};
export default UserChargeSummary;
