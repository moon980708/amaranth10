import React from "react";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Checkbox,
  IconButton,
  TableContainer
} from "@mui/material";
import { DeleteSweep } from "@mui/icons-material";


const GroupMemberTable = ({selectedGroupNo, originalMembers, members, selectedIds, handleSelectIds, handleAllSelectIds, deleteGroupMember}) => {

  return (
    <>
      <TableContainer
        sx={{ height: "calc(59vh)" }}
      >
        <Table
          aria-label="simple table"
          sx={{
            whiteSpace: "nowrap"
          }}
        >
          <TableHead sx={{backgroundColor:"aliceblue", position: "sticky", top: 0, zIndex: 1}}>
            <TableRow sx={{width:"100%", }}>
              <TableCell sx={{width:"5%", py:1}}>
                <Typography variant="h6">
                  <Checkbox
                    size='small'
                    color='secondary'
                    checked={selectedIds.length>0 && selectedIds.length === members.length}
                    onChange={handleAllSelectIds}
                  />
                </Typography>
              </TableCell>
              <TableCell sx={{width:"15%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  Id
                </Typography>
              </TableCell>
              <TableCell sx={{width:"15%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  이름
                </Typography>
              </TableCell>
              <TableCell sx={{width:"20%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  부서
                </Typography>
              </TableCell>
              <TableCell sx={{width:"15%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  직급
                </Typography>
              </TableCell>
              <TableCell sx={{width:"15%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  퇴사여부
                </Typography>
              </TableCell>
              <TableCell sx={{width:"15%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  삭제
                </Typography>
              </TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {selectedGroupNo === null ?
              <TableRow>
                <TableCell colSpan={7}>
                  <Typography color="textSecondary" sx={{ fontSize: "14px", textAlign:"center" }}>
                    그룹을 선택해 주세요.
                  </Typography>
                </TableCell>
              </TableRow>
            : members.length === 0 ?
                <TableRow>
                  <TableCell colSpan={7}>
                    <Typography color="textSecondary" sx={{ fontSize: "14px", textAlign:"center" }}>
                      그룹 사용자 정보가 존재하지 않습니다.
                    </Typography>
                  </TableCell>
                </TableRow>
              :
                (members.map((member, index) => (
                  <TableRow
                    key={member.id}
                    sx={{alignItems:'center' }}
                  >
                    <TableCell sx={{width:"5%", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        <Checkbox
                          size='small'
                          color='secondary'
                          checked={selectedIds.indexOf(member.id) !== -1}
                          onChange={(event) => handleSelectIds(event, member.id)}
                        />
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"15%", textAlign:"center", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        {member.id}
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"15%", textAlign:"center", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        {member.userName}
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"20%", textAlign:"center", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        {member.deptName}
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"15%", textAlign:"center", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        {member.rankName===null?'-':member.rankName}
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"15%", textAlign:"center", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        {member.resign?"퇴사":"재직"}
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"15%", textAlign:"center", py:1}}>
                      <IconButton
                        size='small'
                        color="neutral"
                        variant="contained"
                        onClick={()=>deleteGroupMember([member.id])}
                      >
                        <DeleteSweep />
                        </IconButton>
                    </TableCell>
                  </TableRow>
                )))
            }

          </TableBody>
        </Table>
      </TableContainer>  
    </>
  );
};

export default GroupMemberTable;
