import React, {useState, useEffect} from "react";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Checkbox,
  Tooltip,
  IconButton,
  TablePagination,
  TableContainer,
  CircularProgress
} from "@mui/material";
import Swal from "sweetalert2";
import axios from 'axios';
import UseApi from '../UseApi';
import { useSelector } from "react-redux";

import DescriptionIcon from '@mui/icons-material/Description';
import { addCommas } from '../functions/SimpleFuntion';
import ChargeDetailManageModal from '../modal/ChargeDetailManageModal';
axios.defaults.withCredentials = true;



const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: "btn btn-success",
    cancelButton: "btn btn-danger",
    popup: "my-custom-class", // 모달 팝업의 CSS 클래스 이름
  },
  width: "45%"
});




const ChargeManageTable = ({
  search,
  setSearch,
  selectedUserId,
  selectedChargeNo,
  setSelectedChargeNo,
  updateStateConfirm,
  loadingChargeList,
  setLoadingChargeList
}) => {

  const api = UseApi();
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const [chargeList, setChargeList] = useState([]); //조건에 맞는 청구내역리스트
  const [isDetailModalOpen, setIsDetailModalOpen] = useState(false); //상세모달
  const [chargeDetail, setChargeDetail] = useState({}); //상세모달에 띄울 데이터
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const [refresh, setRefresh] = useState(false);



  const listCharge = () => {
    //한 사용자의 청구내역리스트 가져오기, 조건도 맞아야함.
    setLoadingChargeList(true);

    const variables = {
      managerId: loginUserId,
      scheduleNo: search.schedule,
      state: search.state,
      id: selectedUserId,
      groupDeptNo: search.groupDept,
      page: page,
      rowsPerPage: rowsPerPage,
      searchTime: search.searchTime,
    };

    api.post("/admin/charge/list", variables)
      .then((result) => {
        console.log(result.data);
        setChargeList(result.data);

        // setTimeout(() => { 
        //   setLoadingChargeList(false);
        // }, 500);

      })
      .catch((error) => {
        if (error.response) {
          console.log("조건에 맞는 한 사용자의 청구내역리스트 불러오기 실패", error);
          alert("청구내역리스트 불러오기 실패");
          setChargeList([]);
        }
      })
      .finally(() => {
        setSelectedChargeNo([]); //선택한 청구내역 초기화
        setLoadingChargeList(false);
      });
  };

  const handleOpenDetailModal = (userChargeNo) => {
    console.log(userChargeNo);
    //상세모달
    api.get("/admin/charge/detail/" + userChargeNo)
      .then((result) => {
        console.log(result.data);
        setChargeDetail(result.data);

        setIsDetailModalOpen(true);
      })
      .catch((error) => {
        if (error.response) {
          console.log("하나의 청구건에 대한 상세내역 불러오기 실패", error);
          alert("청구 상세내역 불러오기 실패");
          setChargeDetail({});
        }
      });
  };

  const handleSelectedChargeNo = (e, userChargeNo) => {
    const checked = e.target.checked;

    if (checked) {
      setSelectedChargeNo([...selectedChargeNo, userChargeNo]);
    } else {
      setSelectedChargeNo(selectedChargeNo.filter((no) => no !== userChargeNo));
    }
  };

  const handleConfirmWaitBtnClick = (charge) => {
    //승인대기 버튼 눌렀을때
    swalWithBootstrapButtons
      .fire({
        icon: "info",
        title: "해당 청구건을 처리하시겠습니까?",
        text: "해당 사항은 변경이 불가합니다.",
        confirmButtonText: "승인",
        denyButtonText: "반려",
        cancelButtonText: "취소",
        showDenyButton: true,
        showCancelButton: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          updateStateConfirm([charge.userChargeNo]);
        }

        if (result.isDenied) {
          swalWithBootstrapButtons
            .fire({
              title: "반려 사유를 작성해 주세요.",
              text: "해당 내용은 사용자에게 노출됩니다.",
              input: "textarea",
              inputPlaceholder: "반려사유 작성..",
              confirmButtonText: "반려",
              cancelButtonText: "취소",
              showCancelButton: true,
              allowEnterKey: false,
              preConfirm: (input) => {
                if (input.trim() === "") {
                  Swal.showValidationMessage("반려 사유를 작성해 주세요. (필수)");
                }
              },
            })
            .then((result) => {
              if (result.isConfirmed) {
                console.log("청구내역번호:", charge.userChargeNo, " / 반려사유:", result.value);
                updateStateReject([charge.userChargeNo], result.value);
              }
            });
        }
      });
  };

  const updateStateReject = (userChargeNoList, rejectReason) => {
    //청구상태를 '반려'로 변경 + 반려사유

    const variables = {
      managerId: loginUserId,
      state: "반려",
      userChargeNoList: userChargeNoList,
      rejectReason: rejectReason,
    };

    api.put("/admin/charge/update", variables)
      .then((result) => {
        swalWithBootstrapButtons.fire("청구 반려처리 완료", "", "success");
        
        // setSelectedChargeNo([]);
        // setSearch({ ...search, startRefresh: !search.startRefresh }); //전체 다시 조회
      })
      .catch((error) => {
        //alert(error.response.data.error.message);

        if (error.response) {
          console.log("청구건 반려처리 실패", error);
          swalWithBootstrapButtons.fire('청구 반려처리 실패', error.response.data.error.message, 'error');
        }
      })
      .finally(() => {
        setSelectedChargeNo([]);
        setSearch({ ...search, startRefresh: !search.startRefresh }); //전체 다시 조회
      });
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    setRefresh(!refresh);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0); //mui에서 0 -> 1페이지
    setRefresh(!refresh);
  };



  useEffect(() => {
    //페이징 초기화
    setPage(0);
    setRowsPerPage(10);
    setRefresh(!refresh);

  }, [selectedUserId]);

  useEffect(() => {
    if (selectedUserId !== null) {
      // console.log("refresh:", refresh, "  /search.endRefresh:", search.endRefresh);
      listCharge();
    } else {
      //선택안되어있으니까 청구내역리스트 초기화
      setChargeList([]);
    }
  }, [refresh, search.endRefresh]);

  return (
    <>
      {loadingChargeList ?
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(63vh)" }}>
          <CircularProgress size={50} />
        </Box>
      :
        <TableContainer sx={{ height: "calc(63vh)" }}>
          <Table
            //stickyHeader
            aria-label="simple table"
            sx={{
              whiteSpace: "nowrap",
            }}
          >
            <TableHead sx={{ backgroundColor: "aliceblue", position: "sticky", top: 0, zIndex: 1 }}>
              <TableRow>
                <TableCell sx={{ textAlign: "center", width: "5%" }}>
                  <Typography sx={{ fontSize: "13px" }}>선택</Typography>
                </TableCell>
                <TableCell sx={{ textAlign: "center", width: "5%" }}>
                  <Typography sx={{ fontSize: "13px" }}>사용자</Typography>
                </TableCell>
                <TableCell sx={{ textAlign: "center", width: "10%" }}>
                  <Typography sx={{ fontSize: "13px" }}>청구일</Typography>
                </TableCell>
                <TableCell sx={{ textAlign: "center", width: "10%" }}>
                  <Typography sx={{ fontSize: "13px" }}>용도</Typography>
                </TableCell>
                <TableCell sx={{ textAlign: "center", width: "25%" }}>
                  <Typography sx={{ fontSize: "13px" }}>사용처</Typography>
                </TableCell>
                <TableCell sx={{ textAlign: "center", width: "10%" }}>
                  <Typography sx={{ fontSize: "13px" }}>지출일시</Typography>
                </TableCell>
                <TableCell sx={{ textAlign: "center", width: "20%" }}>
                  <Typography sx={{ fontSize: "13px" }}>금액</Typography>
                </TableCell>
                <TableCell sx={{ textAlign: "center", width: "10%" }}>
                  <Typography sx={{ fontSize: "13px" }}>상태</Typography>
                </TableCell>
                <TableCell sx={{ textAlign: "center", width: "5%" }}>
                  <Typography sx={{ fontSize: "13px" }}>상세</Typography>
                </TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {selectedUserId === null ? (
                <TableRow>
                  <TableCell colSpan={9}>
                    <Typography color="textSecondary" sx={{ fontSize: "14px", textAlign: "center" }}>
                      청구 내역을 확인하실 사원을 선택해 주세요.
                    </Typography>
                  </TableCell>
                </TableRow>
              ) : chargeList.length === 0 ? (
                <TableRow>
                  <TableCell colSpan={9}>
                    <Typography color="textSecondary" sx={{ fontSize: "14px", textAlign: "center" }}>
                      청구 내역이 존재하지 않습니다.
                    </Typography>
                  </TableCell>
                </TableRow>
              ) : (
                <>
                  {chargeList.map((charge) => (
                    <TableRow key={charge.userChargeNo}>
                      <TableCell sx={{ textAlign: "center", width: "5%", py: 1 }}>
                        <Checkbox
                          size="small"
                          color='secondary'
                          disabled={charge.state === "승인대기" ? false : true}
                          checked={selectedChargeNo.indexOf(charge.userChargeNo) !== -1}
                          onChange={(event) => handleSelectedChargeNo(event, charge.userChargeNo)}
                        />
                      </TableCell>
                      <TableCell sx={{ textAlign: "center", width: "5%", cursor: "pointer", py: 1 }}>
                        <Tooltip title={charge.deptName + " / " + charge.id} placement="top">
                          <Typography sx={{ fontSize: "12px" }}>{charge.userName}</Typography>
                        </Tooltip>
                      </TableCell>
                      <TableCell sx={{ textAlign: "center", width: "10%", py: 1 }}>
                        <Typography sx={{ fontSize: "12px" }}>{charge.userChargeDate}</Typography>
                      </TableCell>
                      <TableCell sx={{ textAlign: "center", width: "10%", py: 1 }}>
                        <Typography sx={{ fontSize: "12px" }}>{charge.categoryName}</Typography>
                      </TableCell>
                      <TableCell sx={{ textAlign: "center", width: "25%", py: 1 }}>
                        <Typography sx={{ fontSize: "12px" }}>{charge.store}</Typography>
                      </TableCell>
                      <TableCell sx={{ textAlign: "center", width: "10%", py: 1 }}>
                        <Typography sx={{ fontSize: "12px" }}>{charge.expendDate}</Typography>
                      </TableCell>
                      <TableCell sx={{ textAlign: "center", width: "20%", py: 1 }}>
                        <Typography sx={{ fontSize: "12px" }}>
                          {addCommas(charge.charge)}원
                        </Typography>
                      </TableCell>
                      <TableCell sx={{ textAlign: "center", width: "10%", py: 1 }}>
                        {charge.state === "승인대기" ? (
                          <Chip
                            sx={{
                              pl: "4px",
                              pr: "4px",
                            }}
                            size="small"
                            label={charge.state}
                            color="primary"
                            onClick={() => handleConfirmWaitBtnClick(charge)}
                          />
                        ) : (
                          <Typography
                            sx={{
                              fontSize: "12px",
                              color: charge.state === "승인" ? "green" : "red",
                            }}
                          >
                            {charge.state}
                          </Typography>
                        )}
                      </TableCell>
                      <TableCell sx={{ textAlign: "center", width: "5%", py: 1 }}>
                        <IconButton
                          size="small"
                          onClick={() => handleOpenDetailModal(charge.userChargeNo)}
                        >
                          <DescriptionIcon fontSize="small" />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
                </>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      }

      <Box sx={{ display: "flex", justifyContent: "flex-end", alignItems: "center" }}>
        <TablePagination
          rowsPerPageOptions={[10, 15, 30]}
          component="div"
          count={chargeList.length === 0 ? 0 : chargeList[0].totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
        

      <ChargeDetailManageModal
        chargeDetail={chargeDetail}
        isOpen={isDetailModalOpen}
        onClose={() => setIsDetailModalOpen(false)}
      />


    </>
  );
};

export default ChargeManageTable;