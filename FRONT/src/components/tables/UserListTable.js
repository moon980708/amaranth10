import React, { useState, useEffect } from "react";
import { Box, Typography, Dialog, Button } from "@mui/material";
import Fab from "@mui/material/Fab";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import PersonAddAlt1Icon from "@mui/icons-material/PersonAddAlt1";
import Swal from "sweetalert2";
import UserTable from "./UserTable";
import UserCreateTableData from './../tabledata/UserCreateTableData';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import RestartAltIcon from "@mui/icons-material/RestartAlt";
axios.defaults.withCredentials = true; 

const products = [
  // {
  //   checked: false,
  //   id: "AA1",
  //   password: "abcdef",
  //   name: "박사원",
  //   dept: "총무부",
  //   pbg: "pink.main",
  //   rank: "사원",
  //   email: "aa1@douzone.com",
  //   resign: "재직",
  //   avatar: img1,
  // },
  // {
  //   checked: false,
  //   id: "BB2",
  //   password: "bcdefg",
  //   name: "우과장",
  //   dept: "품질관리부",
  //   pbg: "purple.main",
  //   rank: "과장",
  //   email: "bb2@douzone.com",
  //   resign: "재직",
  //   avatar: img2,
  // },
  // {
  //   checked: false,
  //   id: "CC3",
  //   password: "cdefgh",
  //   name: "김이사",
  //   dept: "개발 1팀",
  //   pbg: "orange.main",
  //   rank: "이사",
  //   email: "cc3@douzone.com",
  //   resign: "재직",
  //   avatar: img3,
  // },
  // {
  //   checked: false,
  //   id: "DD4",
  //   password: "defghi",
  //   name: "천전무",
  //   dept: "개발 2팀",
  //   pbg: "green.main",
  //   rank: "전무",
  //   email: "dd4@douzone.com",
  //   resign: "재직",
  //   avatar: img4,
  // },
];

const UserListTable = ({
  deptList,
  rankList,
  submitUser,
  setSubmitUser,
  editUser,
  setEditUser,
  deleteUser,
  setDeleteUser,
  userList,
  setUserList, setDeptNo, setRankNo, setResign, setKeyword,
  handleClickSnackbar, pagingSearchUser, setPagingSearchUser,
  handleEditClickSnackbar, pagingUser, setPagingUser, page, setPage, totalCount, api, loadingDept, loadingRank, loadingUserList, setLoadingUserList, checkedId, setCheckedId
}) => {
  // useEffect(() => {   // 사원 삭제 axios 통신
  // axios
  //   .delete(`http://localhost:8080/admin/user/delete/${id}`)
  //   .then((result) => {
  //     console.log(result.data);
  //     setUserList(result.data);
  //   })
  //   .catch(() => {
  //     console.log("통신 error");
  //     alert("다시 로딩해주세요.");
  //   });
  // }, [deleteUser]);

  const navigate = useNavigate();

  //모달 창 구현
  const [openAddModal, setOpenAddModal] = useState(false);

  //추가 버튼 modal
  const handleOpenAddModal = () => {
    setOpenAddModal(true);
  };
  const handleCloseAddModal = () => {
    setOpenAddModal(false);
  };

  // 사원 추가 및 퇴사 버튼의 활성화 여부 판단
  const isAnyChecked = checkedId.length > 0;

  // 체크박스
  const handleCheckedId = (e, id) => {
    const checked = e.target.checked;

    if (checked) {
      setCheckedId([...checkedId, id]);
    } else {
      setCheckedId(checkedId.filter((selectedId) => selectedId !== id));
    }
  };

  const handleCheckedAllId = (event) => {
    if (event.target.checked) {
      const checkedAllId = userList
        .filter((user) => user.resign === false) // resign이 false인 사용자만 선택
        .map((user) => user.id);
      setCheckedId(checkedAllId);
    } else if (userList.every((user) => user.resign)) {
      setCheckedId([]);
    } else {
      setCheckedId([]);
    }
  };

  //퇴사 버튼 Fab
  const handleResignBtnClick = () => {
    // 퇴사 버튼 alert
    const handleCancelAlert = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      width: "45%",
      allowOutsideClick: false,
    });

    handleCancelAlert
      .fire({
        title: "해당 사원을 정말 퇴사 처리하시겠습니까?",
        text: "'확인'을 누르면 사원이 퇴사 처리됩니다",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "확인",
        cancelButtonText: "취소",
        reverseButtons: false,
      })
      .then((result) => {
        if (result.isConfirmed) {
          api
            .put(`/admin/user/update/resignAll`, checkedId)
            .then((result) => {
              setEditUser(!editUser);
            })
            .catch((error) => {
              // if (error.response && error.response.status === 401) {
              //   alert("로그인세션만료");
              //   navigate("/");
              // }
              if (error.response) {
                console.log("전체 퇴사 통신 error");
                alert("다시 로딩해주세요.");
              }
            });

          handleCancelAlert.fire("퇴사 처리 완료", "해당 사원이 퇴사 처리되었습니다", "success");
          setCheckedId([]);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          handleCancelAlert.fire("퇴사 처리 취소", "해당 사원이 퇴사 처리되지 않았습니다", "error");
        }
      });
  };

  // 검색 조건 초기화
  const handleSearchRefresh = () => {
    setDeptNo("");
    setRankNo("");
    setResign("");
    setKeyword("");
    setPagingSearchUser(!pagingSearchUser);
  };

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          // alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center", // 세로 중앙 정렬
            ml: 3,
            mt: 3,
          }}
        >
          <Typography
            variant="h3"
            sx={{
              fontFamily: "Roboto, sans-serif",
              fontWeight: "bold",
              verticalAlign: "top",
              textAlign: "center",
            }}
          >
            사원 목록
          </Typography>
          <Typography>
            <RestartAltIcon
              sx={{ mt: 0.9, ml: 1.75, fontSize: 24, color: "grey", fontWeight: "bold" }}
              onClick={() => handleSearchRefresh()}
            />
          </Typography>
        </Box>

        <Box sx={{ display: "flex", alignItems: "center", mr: 2 }}>
          {!isAnyChecked ? (
            // 하나 이상의 체크박스가 체크되지 않은 경우, 추가 Fab 표시
            // 사원 추가 Fab
            // <Fab
            //   color="primary"
            //   variant="extended"
            //   sx={{ mt: 3, marginRight: 2.5, zIndex: 500, verticalAlign: "middle" }}
            //   onClick={handleOpenAddModal} // 모달 Open
            //   disabled={isAnyChecked} // isAnyChecked가 true면 사원추가 Fab이 비활성화
            // >
            //   <Box
            //     sx={{
            //       display: "flex",
            //       alignItems: "center",
            //       ml: 0.75,
            //     }}
            //   >
            //     <PersonAddAlt1Icon />
            //   </Box>
            //   <Typography
            //     sx={{
            //       ml: 1,
            //       mr: 0.75,
            //       textTransform: "capitalize",
            //     }}
            //   >
            //     추가
            //   </Typography>
            // </Fab>
            <Button
              color="primary"
              variant="outlined"
              sx={{
                mt: 3,
                marginRight: 2.5,
                zIndex: 500,
                verticalAlign: "middle",
                height: "50px",
                borderRadius: "30px",
                border: "2px solid",
              }}
              onClick={handleOpenAddModal} // 모달 Open
              disabled={isAnyChecked} // isAnyChecked가 true면 사원추가 Fab이 비활성화
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  ml: 0.75,
                }}
              >
                <PersonAddAlt1Icon />
              </Box>
              <Typography
                sx={{
                  ml: 1,
                  mr: 0.75,
                  textTransform: "capitalize",
                }}
              >
                추가
              </Typography>
            </Button>
          ) : (
            // 하나 이상의 체크박스가 체크된 경우, 퇴사 Fab 표시
            // 사원 퇴사처리 Fab
            <Fab
              color="error"
              variant="extended"
              sx={{ mt: 3, marginRight: 2.5, zIndex: 500, verticalAlign: "middle" }}
              onClick={() => {
                handleResignBtnClick();
              }}
              disabled={!isAnyChecked} // isAnyChecked가 false면 사원삭제 Fab이 비활성화
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  ml: 0.75,
                }}
              >
                <HighlightOffIcon />
              </Box>
              <Typography
                sx={{
                  ml: 1,
                  mr: 0.75,
                  textTransform: "capitalize",
                }}
              >
                퇴사
              </Typography>
            </Fab>
          )}

          {/* 모달 컴포넌트 */}
          <Dialog
            open={openAddModal}
            onClose={handleCloseAddModal}
            maxWidth="md" // 최대 가로 크기를 md로 설정
            fullWidth // 최대 가로 크기까지 사용
          >
            <UserCreateTableData
              submitUser={submitUser}
              setSubmitUser={setSubmitUser}
              handleCloseAddModal={handleCloseAddModal}
              deptList={deptList}
              rankList={rankList}
              handleClickSnackbar={handleClickSnackbar}
              api={api}
            />
          </Dialog>
        </Box>
      </Box>
      <UserTable
        userList={userList}
        setUserList={setUserList}
        editUser={editUser}
        setEditUser={setEditUser}
        deptList={deptList}
        rankList={rankList}
        submitUser={submitUser}
        deleteUser={deleteUser}
        setDeleteUser={setDeleteUser}
        checkedId={checkedId}
        handleCheckedId={handleCheckedId}
        handleCheckedAllId={handleCheckedAllId}
        setCheckedId={setCheckedId}
        handleEditClickSnackbar={handleEditClickSnackbar}
        pagingUser={pagingUser}
        setPagingUser={setPagingUser}
        page={page}
        setPage={setPage}
        totalCount={totalCount}
        api={api}
        loadingDept={loadingDept}
        loadingRank={loadingRank}
        loadingUserList={loadingUserList}
        setLoadingUserList={setLoadingUserList}
      />
    </Box>
  );
};

export default UserListTable;