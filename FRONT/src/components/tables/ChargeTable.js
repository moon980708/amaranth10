import React, { useEffect, useState } from "react";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Checkbox,
  IconButton,
  Tooltip,
  TableContainer,
  CircularProgress,
} from "@mui/material";
import { addCommas } from "../functions/SimpleFuntion";
import { Description, UnfoldMore } from "@mui/icons-material";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import ChargeDetailModal from "../modal/ChargeDetailModal";
import axios from "axios";
import UseApi from "../UseApi";
axios.defaults.withCredentials = true;

const ChargeTable = ({
  onCheckboxChange,
  onInvalidCountChange,
  isAnyOngoingSchedule,
  searchValue,
  searchField,
  refresh,
}) => {
  const [sortOrder, setSortOrder] = useState("asc");
  const [sortedField, setSortedField] = useState("expendDate");
  const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);
  const [chargeData, setChargeData] = useState([]);
  const [itemNum, setItemNum] = useState();
  const [checkItems, setCheckItems] = useState([]);
  const [invalidCount, setInvalidCount] = useState(0);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const navigate = useNavigate();
  const [refreshList, setRefreshList] = useState(false);
  const [listLoading, setListLoading] = useState(true);
  const api = UseApi();

  const handleRefreshList = () => {
    setRefreshList(!refreshList); // 상태를 변경하여 리렌더링을 유발
  };

  useEffect(() => {
    //지출정보리스트 요청
    charges();
    // 체크박스 모두 해제
    setCheckItems([]);
  }, [searchValue, searchField, refresh, refreshList]);

  // 지출 정보 요청
  const charges = () => {
    api
      .get(`/user/charge/list?id=${loginUserId}`, {
        params: {
          searchValue: searchValue,
          searchField: searchField,
        },
      })
      .then((response) => {
        const chargeData = response.data;
        setChargeData(chargeData);
        setListLoading(false);
      })
      .catch((error) => {
        console.error("지출정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  // 정렬버튼 컨트롤
  const handleSort = (field) => {
    if (field === sortedField) {
      setSortOrder((prevSortOrder) => (prevSortOrder === "asc" ? "desc" : "asc"));
    } else {
      setSortedField(field);
      setSortOrder("asc");
    }
  };

  // 정렬 - 날짜 비교
  const compareDates = (a, b) => {
    const dateA = new Date(a.expendDate);
    const dateB = new Date(b.expendDate);
    if (sortOrder === "asc") {
      return dateA - dateB;
    } else {
      return dateB - dateA;
    }
  };

  // 정렬
  const sortCharges = chargeData.sort((a, b) => {
    if (sortedField === "expendDate") {
      return compareDates(a, b);
    } else {
      return sortOrder === "asc"
        ? a[sortedField].localeCompare(b[sortedField])
        : b[sortedField].localeCompare(a[sortedField]);
    }
  });

  // 칩 컨트롤
  const handleChipHover = () => {};

  // 체크박스 단일 선택
  const handleSingleCheck = (checked, userChargeNo, charges) => {
    if (checked) {
      // 단일 선택 시 체크된 아이템을 배열에 추가
      setCheckItems((prev) => [...prev, userChargeNo]);
      // "부적합"인 경우 카운트 증가
      if (getValidState(charges) === "부적합") {
        const newInvalidCount = invalidCount + 1;
        setInvalidCount(newInvalidCount);
        onInvalidCountChange(newInvalidCount);
      }
    } else {
      // 단일 선택 해제 시 체크된 아이템을 제외한 배열 (필터)
      setCheckItems((prev) => prev.filter((el) => el !== userChargeNo));
      // "부적합"인 경우 카운트 감소
      if (getValidState(charges) === "부적합") {
        const newInvalidCount = invalidCount - 1;
        setInvalidCount(newInvalidCount);
        onInvalidCountChange(newInvalidCount);
      }
    }
  };

  // 체크박스 전체 선택
  const handleAllCheck = (checked) => {
    if (checked) {
      // 전체 선택 클릭 시 데이터의 모든 아이템(userChargeNo)를 담은 배열로 checkItems 상태 업데이트
      const userChargeNoArray = chargeData.map((el) => el.userChargeNo);
      setCheckItems(userChargeNoArray);

      // Check for "부적합" items and update the invalidCount
      const newInvalidCount = chargeData.reduce((count, charges) => {
        if (getValidState(charges) === "부적합") {
          return count + 1;
        }
        return count;
      }, 0);

      setInvalidCount(newInvalidCount);
      onInvalidCountChange(newInvalidCount);
    } else {
      // 전체 선택 해제 시 checkItems 를 빈 배열로 상태 업데이트
      setCheckItems([]);

      // Reset the invalidCount
      setInvalidCount(0);
      onInvalidCountChange(0);
    }
  };

  // 상세 버튼 클릭 컨트롤
  const handleDetailButtonClick = (charges) => {
    setIsDetailModalOpen(true);
    setItemNum(charges.userChargeNo);
  };

  // 모달 닫기
  const handleCloseModal = () => {
    setIsDetailModalOpen(false);
  };

  // 데이터 모두 기입했는지 체크
  const getValidState = (charges) => {
    if (
      charges.store !== null &&
      charges.payment !== null &&
      charges.store !== "" &&
      charges.payment !== ""
    ) {
      return "적합";
    } else {
      return "부적합";
    }
  };

  // 누락된 데이터 표시
  const isDataMissing = (charges) => {
    if (
      charges.store !== null &&
      charges.payment !== null &&
      charges.store !== "" &&
      charges.payment !== ""
    ) {
      return "";
    } else {
      return "누락";
    }
  };

  return (
    <TableContainer style={{ maxHeight: "600px" }}>
      {listLoading ? (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "400px",
          }}
        >
          <CircularProgress size={50} />
        </Box>
      ) : (
        <Table aria-label="simple table" sx={{ whiteSpace: "nowrap" }}>
          <TableHead style={{ position: "sticky", top: 0, zIndex: 1, background: "aliceblue" }}>
            <TableRow>
              <TableCell align="center">
                <Checkbox
                  color="secondary"
                  onChange={(e) => {
                    handleAllCheck(e.target.checked);
                    onCheckboxChange(
                      e.target.checked ? chargeData.map((el) => el.userChargeNo) : []
                    );
                  }}
                  checked={checkItems.length === chargeData.length && chargeData.length !== 0}
                />
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  지출일시
                  <IconButton size="small" onClick={() => handleSort("expendDate")}>
                    <UnfoldMore
                      fontSize="small"
                      style={{
                        transform:
                          sortedField === "expendDate"
                            ? `scaleY(${sortOrder === "asc" ? -1 : 1})`
                            : "scaleY(1)",
                      }}
                    />
                  </IconButton>
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  용도
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  사용처
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  금액
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  결제수단
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  검증결과
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  상세
                </Typography>
              </TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {sortCharges.length === 0 ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  <Typography variant="h4" color="gray">
                    {isAnyOngoingSchedule ? "청구기간이 아닙니다." : "등록된 지출 정보가 없습니다."}
                  </Typography>
                </TableCell>
              </TableRow>
            ) : (
              sortCharges.map((charges, index) => (
                <TableRow key={charges.userChargeNo}>
                  <TableCell align="center">
                    <Checkbox
                      color="secondary"
                      key={charges.userChargeNo}
                      onChange={(e) => {
                        handleSingleCheck(e.target.checked, charges.userChargeNo, charges);
                        onCheckboxChange(
                          e.target.checked
                            ? [...checkItems, charges.userChargeNo]
                            : checkItems.filter((el) => el !== charges.userChargeNo)
                        );
                      }}
                      checked={checkItems.includes(charges.userChargeNo)}
                    />
                  </TableCell>

                  <TableCell align="center">
                    <Box>
                      <Typography variant="h6">{charges.expendDate}</Typography>
                    </Box>
                  </TableCell>

                  <TableCell align="center">
                    <Typography variant="h6">{charges.categoryName}</Typography>
                  </TableCell>

                  <TableCell align="center">
                    <Typography variant="h6">{charges.store}</Typography>
                  </TableCell>

                  <TableCell align="center">{addCommas(charges.charge)} 원</TableCell>

                  <TableCell align="center">
                    <Typography variant="h6">{charges.payment}</Typography>
                  </TableCell>

                  <TableCell align="center">
                    <Tooltip
                      key={index}
                      title={
                        (charges.store === null || charges.store === "" ? "사용처 " : "") +
                        (charges.payment === null || charges.payment === "" ? "결제수단 " : "") +
                        isDataMissing(charges)
                      }
                      placement="top"
                    >
                      <Chip
                        sx={{
                          pl: "4px",
                          pr: "4px",
                          backgroundColor: getValidState(charges) === "적합" ? "skyblue" : "red",
                          color: "#fff",
                        }}
                        size="small"
                        label={getValidState(charges)}
                        onMouseEnter={handleChipHover}
                      ></Chip>
                    </Tooltip>
                  </TableCell>

                  <TableCell align="center">
                    <IconButton
                      color="info"
                      size="small"
                      onClick={() => handleDetailButtonClick(charges)}
                    >
                      <Description fontSize="small" />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      )}
      {isDetailModalOpen && (
        <ChargeDetailModal
          isOpen={isDetailModalOpen}
          onCancel={handleCloseModal}
          clickDetail={itemNum}
          onRefresh={handleRefreshList}
        />
      )}
    </TableContainer>
  );
};

export default ChargeTable;
