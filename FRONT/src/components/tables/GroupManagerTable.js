import React from "react";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Checkbox,
  IconButton,
  TableContainer
} from "@mui/material";
import { DeleteSweep } from "@mui/icons-material";


const GroupManagerTable = ({
  selectedGroupNo,
  managers,
  selectedIds,
  handleSelectIds,
  handleAllSelectIds,
  deleteGroupManager
}) => {

  return (
    <>
      <TableContainer
        sx={{ height: "calc(63vh)" }}
      >
        <Table
          aria-label="simple table"
          sx={{
            whiteSpace: "nowrap"
          }}
        >
          <TableHead sx={{backgroundColor:"aliceblue", position: "sticky", top: 0, zIndex: 1}}>
            <TableRow sx={{width:"100%", }}>
              <TableCell sx={{width:"15%", py:1}}>
                <Typography variant="h6">
                  <Checkbox
                    size='small'
                    color='secondary'
                    checked={selectedIds.length>0 && selectedIds.length === managers.length-1}
                    onChange={handleAllSelectIds}
                    disabled={managers.length == 1}
                  />
                </Typography>
              </TableCell>
              <TableCell sx={{width:"20%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  Id
                </Typography>
              </TableCell>
              <TableCell sx={{width:"20%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  이름
                </Typography>
              </TableCell>
              <TableCell sx={{width:"30%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  부서
                </Typography>
              </TableCell>
              <TableCell sx={{width:"15%", textAlign:"center"}}>
                <Typography variant="h6" sx={{fontWeight: "600"}}>
                  삭제
                </Typography>
              </TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {selectedGroupNo === null ?
              <TableRow>
                <TableCell colSpan={7}>
                  <Typography color="textSecondary" sx={{ fontSize: "14px", textAlign:"center" }}>
                    그룹을 선택해 주세요.
                  </Typography>
                </TableCell>
              </TableRow>
            : managers.length === 0 ?
                <TableRow>
                  <TableCell colSpan={7}>
                    <Typography color="textSecondary" sx={{ fontSize: "14px", textAlign:"center" }}>
                      그룹 담당자 정보가 존재하지 않습니다.
                    </Typography>
                  </TableCell>
                </TableRow>
              :
                (managers.map((manager, index) => (
                  <TableRow
                    key={manager.id}
                    sx={{alignItems:'center' }}
                  >
                    <TableCell sx={{width:"15%", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        <Checkbox
                          size='small'
                          color='secondary'
                          disabled={manager.id === "admin"}
                          checked={selectedIds.indexOf(manager.id) !== -1}
                          onChange={(event) => handleSelectIds(event, manager.id)}
                        />
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"20%", textAlign:"center", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        {manager.id}
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"20%", textAlign:"center", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        {manager.userName}
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"30%", textAlign:"center", py:1}}>
                      <Typography sx={{ fontSize: "13px" }}>
                        {manager.deptName}
                      </Typography>
                    </TableCell>
                    <TableCell sx={{width:"15%", textAlign:"center", py:1}}>
                      <IconButton
                        size='small'
                        color="neutral"
                        variant="contained"
                        disabled={manager.id === "admin"}
                        onClick={()=>deleteGroupManager([manager.id])}
                      >
                        <DeleteSweep />
                        </IconButton>
                    </TableCell>
                  </TableRow>
                )))
            }

          </TableBody>
        </Table>
      </TableContainer>  
    </>
  );
};

export default GroupManagerTable;
