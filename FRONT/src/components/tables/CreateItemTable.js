import React from "react";
import { Card, CardContent, Box, Typography, TextField, IconButton, Table, TableCell, TableRow } from "@mui/material";
import { Search } from "@mui/icons-material";
import Fab from "@mui/material/Fab";
import { ManageSearch } from "@mui/icons-material";
import { useState } from 'react';
import CreateItemTableData from '../tabledata/CreateItemTableData';
import CreateItemModal from '../modal/CreateItemModal';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { useSelector } from "react-redux";
import HighlightOffTwoToneIcon from "@mui/icons-material/HighlightOffTwoTone";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
axios.defaults.withCredentials = true; 

const CreateItemTable = ({
  ItemList,
  setItemList,
  submitItem,
  setSubmitItem,
  deleteItem,
  setDeleteItem,
  handlecreateItemSnackOpen, api, loadingItem, setLoadingItem
}) => {
  const navigate = useNavigate();
  const onGoingSchedule = useSelector((state) => state.matchingReducer.onGoingSchedule);

  // 용도 생성 모달
  const [modalOpen, setModalOpen] = useState(false);

  const handleOpenModal = () => {
    setModalOpen(true);
  };
  const handleCloseModal = () => {
    setModalOpen(false);
  };

  // 항목명 검색 state
  const [findItem, setFindItem] = useState("");
  const [isFocused, setIsFocused] = useState(false); // 포커스 여부 상태 관리

  // 검색 시 엔터 입력 가능하게
  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      handleItemSearch();
    }
  };

  // 항목명 검색 버튼 클릭 함수
  const handleItemSearch = () => {
    setLoadingItem(true);

    // 검색어 사이 공백 제거
    const ItemTitle = findItem.replace(/\s+/g, "");

    // 서버에 검색어를 보내고 항목 리스트를 다시 받아옴
    api
      .get(`/admin/itemlist?itemTitle=${ItemTitle}`)
      .then((result) => {
        setItemList(result.data);
        setLoadingItem(false);
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        if (error.response) {
          console.log("통신 error");
          alert("다시 로딩해주세요.");
        }
      });
  };

  const handleClearSearch = () => {
    setFindItem(""); // 검색어 초기화
  };

  // 항목 삭제를 위한 체크박스
  const [checkedId, setCheckedId] = useState([]); //체크박스에 체크된 항목
  const isAnyChecked = checkedId.length > 0;

  // 체크박스
  const handleCheckedId = (e, itemNo) => {
    const checked = e.target.checked;

    if (checked) {
      setCheckedId([...checkedId, itemNo]);
    } else {
      setCheckedId(checkedId.filter((selectedId) => selectedId !== itemNo));
    }
  };

  const handleCheckedAllId = (event) => {
    if (event.target.checked) {
      const checkedAllId = ItemList.map((product) => product.itemNo);
      setCheckedId(checkedAllId);
    } else if (ItemList.every((product) => product.itemDelete)) {
      setCheckedId([]);
    } else {
      setCheckedId([]);
    }
  };

  // 삭제되지 않는 항목명(= 청구내역이 있는 항목명) 리스트 불러옴
  const handleNotDeleteSearch = async () => {
    try {
      const result = await api.get("/admin/item/notDeleteList", {
        params: {
          checkedId: checkedId.join(","),
        },
      });
      const notDeleteListSort = result.data.sort((a, b) => a - b);
      return notDeleteListSort; // 결과를 리턴
    } catch (error) {
      // if (error.response && error.response.status === 401) {
      //   alert("로그인세션만료");
      //   navigate("/");
      // }
      if (error.response) {
        console.log("통신 error");
        alert("다시 로딩해주세요.");
        return []; // 결과를 리턴
      }
    }
  };

  //삭제 버튼 Fab
  const handleDeleteBtnClick = async () => {
    try {
      // "항목 삭제" 버튼을 클릭하면, 삭제되지 않는 목록을 가져옴
      const notDeleteResult = await handleNotDeleteSearch();
      // 숫자들을 문자열로 변환하고 숫자 사이에 공백 추가
      const notDeleteListBlank = notDeleteResult.join(", ");

      // 삭제 가능한 항목들의 체크된 ID만 가져옴
      const deletableList = checkedId.filter((id) => !notDeleteListBlank.includes(id));
      // 숫자들을 문자열로 변환하고 숫자 사이에 공백 추가
      const deletableListBlank = deletableList.join(", ");

      // 삭제 버튼 alert
      const handleCancelAlert = Swal.mixin({
        customClass: {
          confirmButton: "btn btn-success",
          cancelButton: "btn btn-danger",
        },
        width: "50%",
        allowOutsideClick: false,
      });

      if (deletableList == null || deletableList == "") {
        handleCancelAlert.fire("용도 삭제 불가", "삭제 가능한 용도가 없습니다.", "error");
        setCheckedId([]);
      } else {
        if (notDeleteListBlank == null || notDeleteListBlank == "") {
          // 삭제되지 않는 용도가 없는 경우
          handleCancelAlert
            .fire({
              title: `"${deletableListBlank}"번 항목을 정말 삭제하시겠습니까?`,
              text: "'확인'을 누르면 항목이 삭제됩니다",
              icon: "question",
              showCancelButton: true,
              confirmButtonText: "확인",
              cancelButtonText: "취소",
              reverseButtons: false,
            })
            .then((result) => {
              if (result.isConfirmed) {
                api
                  .delete(`/admin/item/delete`, {
                    params: {
                      checkedId: checkedId.join(","),
                    },
                  })
                  .then((result) => {
                    setDeleteItem(!deleteItem);
                    handleCancelAlert.fire(
                      "항목 삭제 완료",
                      `"${deletableListBlank}"번 항목이 삭제되었습니다.`,
                      "success"
                    );
                    setCheckedId([]);
                  })
                  .catch((error) => {
                    // if (error.response && error.response.status === 401) {
                    //   alert("로그인세션만료");
                    //   navigate("/");
                    // }
                    if (error.response) {
                      console.log("항목 삭제 통신 error");
                      alert("다시 로딩해주세요.");
                    }
                  });
              } else if (result.dismiss === Swal.DismissReason.cancel) {
                // 삭제 ? 아니오
                handleCancelAlert.fire(
                  "항목 삭제 취소",
                  `"${deletableListBlank}"번 항목이 삭제되지 않았습니다`,
                  "error"
                );
              }
            });
        } else {
          // 삭제되지 않는 항목이 있는 경우
          handleCancelAlert
            .fire({
              title: `"${deletableListBlank}"번 항목을 정말 삭제하시겠습니까?`,
              text: `"${notDeleteListBlank}"번 항목은 삭제가 불가능합니다.`, // 삭제 안되는 항목명
              icon: "info",
              showCancelButton: true,
              confirmButtonText: "확인",
              cancelButtonText: "취소",
              reverseButtons: false,
            })
            .then((result) => {
              if (result.isConfirmed) {
                api
                  .delete(`/admin/item/delete`, {
                    //용도 삭제 요청
                    params: {
                      checkedId: checkedId.join(","), //체크된 용도들
                    },
                  })
                  .then((result) => {
                    //삭제 성공시
                    setDeleteItem(!deleteItem);
                    handleCancelAlert.fire(
                      "항목 삭제 완료",
                      `"${deletableListBlank}"번 항목이 삭제되었습니다.`,
                      "success"
                    );
                    setCheckedId([]);
                  })
                  .catch((error) => {
                    // if (error.response && error.response.status === 401) {
                    //   alert("로그인세션만료");
                    //   navigate("/");
                    // }
                    if (error.response) {
                      console.log("항목 삭제 통신 error");
                      alert("다시 로딩해주세요.");
                    }
                  });
              } else if (result.dismiss === Swal.DismissReason.cancel) {
                handleCancelAlert.fire(
                  "항목 삭제 취소",
                  `"${deletableListBlank}"번 항목이 삭제되지 않았습니다`,
                  "error"
                );
              }
            });
        }
      }
    } catch (error) {
      // if (error.response && error.response.status === 401) {
      //   alert("로그인세션만료");
      //   navigate("/");
      // }
      if (error.response) {
        console.log("항목 삭제 리스트 재랜더링 error");
        alert("다시 로딩해주세요.");
      }
    }
  };

  return (
    <Box>
      <Card
        variant="outlined"
        sx={{ maxWidth: 600, height: 800, boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)" }}
      >
        <CardContent>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              ml: 8,
              mr: 4,
            }}
          >
            <Typography variant="h3">항목</Typography>
            <Box sx={{ display: "flex", alignItems: "center", width: "185px" }}>
              <TextField
                id="search"
                label={isFocused ? "항목명 검색" : ""} // 포커스가 있을 때만 레이블 표시
                variant="outlined"
                size="small"
                placeholder="전체검색"
                value={findItem}
                onChange={(e) => setFindItem(e.target.value)}
                onKeyDown={handleKeyDown}
                onFocus={() => setIsFocused(true)}
                onBlur={() => setIsFocused(false)}
                InputProps={{
                  endAdornment: (
                    <React.Fragment>
                      {findItem && ( // 내용이 있을 때만 x 아이콘 표시
                        <IconButton
                          sx={{ padding: 0, mr: 0.4, color: "#d3d3d3" }}
                          // color="primary"
                          aria-label="clear-search"
                          onClick={handleClearSearch}
                        >
                          <HighlightOffIcon />
                        </IconButton>
                      )}
                      <IconButton
                        sx={{ padding: 0 }}
                        color="primary"
                        aria-label="search"
                        onClick={handleItemSearch}
                      >
                        <Search />
                      </IconButton>
                    </React.Fragment>
                  ),
                }}
              />
            </Box>
          </Box>
          <Box
            sx={{
              overflow: {
                xs: "auto",
                sm: "unset",
              },
            }}
            my={1}
          >
            <CreateItemTableData
              ItemList={ItemList}
              checkedId={checkedId}
              handleCheckedId={handleCheckedId}
              handleCheckedAllId={handleCheckedAllId}
              loadingItem={loadingItem}
            />
          </Box>
        </CardContent>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            mt: 1,
          }}
        >
          {!isAnyChecked ? (
            // 하나 이상의 체크박스가 체크되지 않은 경우, 추가 Fab 표시
            // 항목 추가 Fab
            <Fab
              color="primary"
              variant="extended"
              sx={{
                width: "80%",
                mt: 1,
                zIndex: 500,
              }}
              onClick={() => {
                setModalOpen(true);
              }} // Fab 클릭 시 모달 열기
              disabled={onGoingSchedule !== null ? true : false} // isAnyChecked가 false면 용도삭제 Fab이 비활성화
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <ManageSearch />
              </Box>
              <Typography
                sx={{
                  ml: 1,
                  textTransform: "capitalize",
                }}
              >
                항목 추가
              </Typography>
            </Fab>
          ) : (
            // 하나 이상의 체크박스가 체크된 경우, 삭제 Fab 표시
            // 항목 삭제 Fab
            <Fab
              color="error"
              variant="extended"
              sx={{
                width: "80%",
                mt: 1,
                zIndex: 500,
              }}
              onClick={() => {
                handleDeleteBtnClick();
              }}
              disabled={!isAnyChecked || onGoingSchedule !== null ? true : false} // isAnyChecked가 false면 항목삭제 Fab이 비활성화
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <HighlightOffIcon />
              </Box>
              <Typography
                sx={{
                  ml: 1,
                  textTransform: "capitalize",
                }}
              >
                항목 삭제
              </Typography>
            </Fab>
          )}
        </Box>
      </Card>

      {/* 항목 추가 모달 */}
      {modalOpen === true ? (
        <CreateItemModal
          setModalOpen={setModalOpen}
          submitItem={submitItem}
          setSubmitItem={setSubmitItem}
          handlecreateItemSnackOpen={handlecreateItemSnackOpen}
          handleCloseModal={handleCloseModal}
          api={api}
        />
      ) : null}
    </Box>
  );
};

export default CreateItemTable;