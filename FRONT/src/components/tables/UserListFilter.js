import React, { useState, useEffect } from "react";
import {
  Grid,
  Card,
  FormControl,
  Select,
  MenuItem,
  TextField,
  IconButton,
  Box,
  Typography,
  Fab, Button
} from "@mui/material";
import { ManageSearch } from "@mui/icons-material";
import InputLabel from "@mui/material/InputLabel";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import CreateUserCsvModal from "../modal/CreateUserCsvModal"; // UserCSVMenu 컴포넌트를 가져옴
axios.defaults.withCredentials = true;

const UserListFilter = ({
  deptList,
  rankList,
  userList,
  setUserList,
  handleSearchClickSnackbar,
  pagingSearchUser,
  setPagingSearchUser,
  page,
  setTotalCount,
  submitUser,
  editUser,
  deleteUser,
  pagingUser,
  setPage,
  handleClickSnackbar,
  setSubmitUser,
  handleNotCsvOpenSnackbar,
  api,
  searchUser,
  setSearchUser,
  setLoadingUserList,
  loadingUserList, deptNo, setDeptNo, rankNo, setRankNo, resign, setResign, keyword, setKeyword, setCheckedId
}) => {
  const navigate = useNavigate();

  // const [deptNo, setDeptNo] = useState(""); // 검색 시 부서
  // const [rankNo, setRankNo] = useState(""); // 검색 시 직급
  // const [resign, setResign] = useState(""); // 검색 시 퇴사여부
  // const [keyword, setKeyword] = useState(""); //검색 시 사원명

  const handleChangeDept = (event) => {
    const selectedDept = event.target.value === "전체 선택" ? null : event.target.value;
    setDeptNo(selectedDept);
  };

  const handleChangeRank = (event) => {
    const selectedRank = event.target.value === "전체 선택" ? null : event.target.value;
    setRankNo(selectedRank);
  };

  const handleChangeResign = (event) => {
    const selectedResign = event.target.value === "전체 선택" ? null : event.target.value;
    setResign(selectedResign);
  };

  const handleChangeKeyword = (event) => {
    setKeyword(event.target.value);
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      setPagingSearchUser(!pagingSearchUser);
    }
  };

  const handleClearSearch = () => {
    setKeyword("");
  };

  useEffect(() => {
    const searchUserList = async () => {
      setLoadingUserList(true);

      try {
        const totalResult = await api.get("/admin/user/getsearchcount", {
          params: {
            deptNo,
            rankNo,
            resign,
            keyword,
          },
        });
        setTotalCount(totalResult.data);

        const searchedUserList = await api.get("/admin/user", {
          params: {
            page,
            deptNo,
            rankNo,
            resign,
            keyword,
          },
        });
        setUserList(searchedUserList.data);
        setCheckedId([]);
        // setSearchUser(!searchUser);
        setLoadingUserList(false);
      } catch (error) {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인 세션이 만료되었습니다");
        //   navigate("/");
        // }
        if (error.response) {
          console.log("통신 error");
          alert("다시 로딩해주세요.");
        }
      }
    };

    searchUserList();
  }, [submitUser, editUser, deleteUser, pagingUser]);

  useEffect(() => {
    const searchUserList = async () => {
      setLoadingUserList(true);

      try {
        const totalResult = await api.get("/admin/user/getsearchcount", {
          params: {
            deptNo,
            rankNo,
            resign,
            keyword,
          },
        });
        setTotalCount(totalResult.data);

        const searchedUserList = await api.get("/admin/user", {
          params: {
            page: 1,
            deptNo,
            rankNo,
            resign,
            keyword,
          },
        });
        setPage(1);
        setCheckedId([]);
        setUserList(searchedUserList.data);
        setLoadingUserList(false);
      } catch (error) {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인 세션이 만료되었습니다");
        //   navigate("/");
        // }
        if (error.response) {
          console.log("통신 error");
          alert("다시 로딩해주세요.");
        }
      }
    };

    searchUserList();
  }, [pagingSearchUser]);

  return (
    <Card variant="outlined" sx={{ boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)" }}>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={0.5}></Grid>
        <Grid item xs={2}>
          <FormControl sx={{ m: 1, minWidth: 60, width: "100%" }} variant="standard" fullWidth>
            <InputLabel id="dept-label">부서</InputLabel>
            <Select
              labelId="dept-label"
              value={deptNo === null ? "전체 선택" : deptNo}
              onChange={handleChangeDept}
              MenuProps={{
                PaperProps: {
                  style: {
                    maxHeight: 195,
                    width: "auto",
                  },
                },
              }}
            >
              <MenuItem value="전체 선택">
                <em>전체 선택</em>
              </MenuItem>
              {deptList
                .filter((dept) => dept !== "관리부")
                .map((dept, index) => (
                  <MenuItem key={index} value={index + 20}>
                    {dept}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <FormControl sx={{ m: 1, minWidth: 60, width: "100%" }} variant="standard" fullWidth>
            <InputLabel id="rank-label">직급</InputLabel>
            <Select
              labelId="rank-label"
              value={rankNo === null ? "전체 선택" : rankNo}
              onChange={handleChangeRank}
              MenuProps={{
                PaperProps: {
                  style: {
                    maxHeight: 195,
                    width: "auto",
                  },
                },
              }}
            >
              <MenuItem value="전체 선택">
                <em>전체 선택</em>
              </MenuItem>
              {rankList.map((rank, index) => (
                <MenuItem key={index} value={index + 1}>
                  {rank}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <FormControl sx={{ m: 1, minWidth: 60, width: "100%" }} variant="standard" fullWidth>
            <InputLabel id="resign-label">퇴사여부</InputLabel>
            <Select
              labelId="resign-label"
              value={resign === null ? "전체 선택" : resign}
              onChange={handleChangeResign}
              MenuProps={{
                PaperProps: {
                  style: {
                    maxHeight: 195,
                    width: "auto",
                  },
                },
              }}
            >
              <MenuItem value="전체 선택">
                <em>전체 선택</em>
              </MenuItem>
              <MenuItem value={0}>재직</MenuItem>
              <MenuItem value={1}>퇴사</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <TextField
            id="user-text"
            label="사원명"
            sx={{ m: 1, minWidth: 60 }}
            variant="standard"
            value={keyword}
            fullWidth
            onChange={handleChangeKeyword}
            onKeyDown={handleKeyDown}
            InputProps={{
              endAdornment: keyword && (
                <IconButton
                  sx={{ padding: 0, mr: 0.4, color: "#d3d3d3" }}
                  onClick={handleClearSearch}
                  aria-label="clear-search"
                >
                  <HighlightOffIcon />
                </IconButton>
              ),
            }}
          />
        </Grid>
        <Grid item xs={0.15}></Grid>
        <Grid item xs={1.5}>
          <Box
            sx={{
              textAlign: "center",
              height: "100%",
              color: "#000",
            }}
          >
            {/* <Fab
              variant="extended"
              color="inherit"
              sx={{
                // backgroundColor: "#616974",
                zIndex: 500,
                backgroundColor: "#e8ebed",
              }}
              onClick={() => {
                setPagingSearchUser(!pagingSearchUser);
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  ml: 0.75,
                  // color: "#fff",
                }}
              >
                <ManageSearch />
              </Box>
              <Typography
                sx={{
                  ml: 1,
                  mr: 0.75,
                  // color: "#fff",
                }}
              >
                검색
              </Typography>
            </Fab> */}
            <Fab
              color="primary"
              variant="extended"
              onClick={() => {
                setPagingSearchUser(!pagingSearchUser);
                handleSearchClickSnackbar();
              }}
              sx={{
                zIndex: 500,
                height: "50px",
                // borderRadius: "30px",
                // border: "2px solid",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  ml: 0.75,
                }}
              >
                <ManageSearch />
              </Box>
              <Typography
                sx={{
                  ml: 1,
                  mr: 0.75,
                  textTransform: "capitalize",
                }}
              >
                {" "}
                <b> 검색</b>
              </Typography>
            </Fab>
          </Box>
        </Grid>
        <Grid item xs={1.35}>
          <CreateUserCsvModal
            setUserList={setUserList}
            handleClickSnackbar={handleClickSnackbar}
            handleNotCsvOpenSnackbar={handleNotCsvOpenSnackbar}
            submitUser={submitUser}
            setSubmitUser={setSubmitUser}
            deptNo={deptNo}
            rankNo={rankNo}
            resign={resign}
            keyword={keyword}
            api={api}
            deptList={deptList}
            rankList={rankList}
          />
        </Grid>
      </Grid>
    </Card>
  );
};

export default UserListFilter;
