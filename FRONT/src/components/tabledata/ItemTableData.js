import React, { useState, useEffect } from "react";
import TableContainer from "@mui/material/TableContainer";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Checkbox,
} from "@mui/material";

import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import {
  reset_Select_Category,
  reset_Selectect_ItemList,
  select_ItemList,
} from "../../reducer/module/matchingReducer";

const ItemTableData = ({
  ItemList,
  selectCategory,
  selectedItems,
  setSelectedItems,
}) => {
  useEffect(() => {
    dispatch(reset_Selectect_ItemList());
    dispatch(reset_Select_Category());
    return () => {
      dispatch(reset_Selectect_ItemList());
      dispatch(reset_Select_Category());
    };
  }, []);

  useEffect(() => {
    setSelectedItems([]);
  }, [selectCategory]);

  const onGoingYearMonth = useSelector(
    (state) => state.matchingReducer.onGoingYearMonth
  );

  const itemList = useSelector((state) => state.matchingReducer.itemList);
  const selectedItemList = useSelector(
    (state) => state.matchingReducer.selectedItemList
  );

  const selectedCategory = useSelector(
    (state) => state.matchingReducer.selectedCategory
  );
  const dispatch = useDispatch();

  const handleCheckboxChange = (event, index) => {
    const isChecked = event.target.checked;
    // setSelectedItems((prevSelectedItems) => {
    //   const itemInfo = {
    //     itemNo: itemList[index].itemNo,
    //     itemTitle: itemList[index].itemTitle,
    //     itemType: itemList[index].itemType,
    //     itemOption: "null"
    //   };

    //   if (isChecked) {

    //     console.log(itemList[index].itemNo);
    //     dispatch(select_ItemList(itemInfo));
    //     return [...prevSelectedItems, index];
    //   } else {
    //     dispatch(select_ItemList(itemInfo));
    //     return prevSelectedItems.filter((item) => item !== index);
    //   }
    // });
    const itemInfo = {
      itemNo: itemList[index].itemNo,
      itemTitle: itemList[index].itemTitle,
      itemType: itemList[index].itemType,
      itemOption: "null",
      itemEssential : false,
    };
    dispatch(select_ItemList(itemInfo));
  };

  //전체 체크박스 선택 함수
  const handleSelectAll = (event) => {
    const isChecked = event.target.checked;
    if (isChecked) {
      // 선택되지 않은 항목들의 목록을 구합니다.
      const unselectedItems = itemList.filter((item) => {
        const itemNumbers = selectedItemList.map(
          (selectedItem) => selectedItem.itemNo
        );
        return !itemNumbers.includes(item.itemNo);
      });

      // 선택되지 않은 항목들을 dispatch를 통해 추가합니다.
      unselectedItems.forEach((item) => {
        const itemInfo = {
          itemNo: item.itemNo,
          itemTitle: item.itemTitle,
          itemType: item.itemType,
          itemOption: "null",
          itemEssential : false,
        };
        dispatch(select_ItemList(itemInfo));
      });
    }
    // if (isChecked) {
    //   setSelectedItems(ItemList.map((_, index) => index));
    else {
      // console.log(selectedItems);
      dispatch(reset_Selectect_ItemList());
    }
  };

  const isItemChecked = (no) => {
    const itemNumbers = selectedItemList.map((item) => item.itemNo);
    return itemNumbers.includes(no);
  };

  return (
    <TableContainer
      sx={{
        minHeight: "400px",
        maxHeight: "400px", // 최대 높이 설정
      }}
    >
      <Table
        aria-label="simple table"
        sx={{
          mt: 3,
          whiteSpace: "nowrap",
        }}
      >
        <TableHead
          sx={{
            backgroundColor: "aliceblue",
            position: "sticky",
            top: 0,
            zIndex: 1,
          }}
        >
          <TableRow>
            <TableCell align="center">
              {/* <Checkbox
                color="primary"
                sx={{ padding: 0 }}
                disabled={selectedCategory === null || onGoingYearMonth !== null ? true : false}
                checked={selectedItemList.length === itemList.length}
                indeterminate={
                  selectedItemList.length > 0 &&
                  selectedItemList.length < itemList.length
                }
                onChange={handleSelectAll}
              /> */}
              <Typography align="center" color="textSecondary" variant="h6">
                선택
              </Typography>
            </TableCell>
            <TableCell>
              <Typography align="center" color="textSecondary" variant="h6">
                항목코드
              </Typography>
            </TableCell>
            <TableCell>
              <Typography align="center" color="textSecondary" variant="h6">
                항목명
              </Typography>
            </TableCell>
            <TableCell>
              <Typography align="center" color="textSecondary" variant="h6">
                항목속성
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {itemList.length === 0 ? (
            <TableRow>
              <TableCell colSpan = {4}>
                <Typography
                  color="textSecondary"
                  sx={{ fontSize: "14px", textAlign: "center" }}
                >
                  검색결과가 없습니다.
                </Typography>
              </TableCell>
            </TableRow>
          ) : (
            itemList.map((itemList, index) => (
              <TableRow key={index}>
                <TableCell align="center">
                  <Checkbox
                    color="primary"
                    sx={{ padding: 0 }}
                    disabled={itemList.itemNo === 100 || selectedCategory === null || onGoingYearMonth !== null ? true : false}
                    checked={isItemChecked(itemList.itemNo)}
                    onChange={(event) => handleCheckboxChange(event, index)}
                  />
                </TableCell>

                <TableCell align="center">
                  <Typography
                    sx={{
                      fontSize: "15px",
                      fontWeight: "500",
                    }}
                  >
                    {itemList.itemNo}
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography
                    variant="h6"
                    sx={{
                      fontWeight: "600",
                    }}
                  >
                    {itemList.itemTitle}
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography
                    variant="h6"
                    sx={{
                      fontWeight: "600",
                    }}
                  >
                    {itemList.itemType}
                  </Typography>
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ItemTableData;
