import React, { useState } from "react";
import TableContainer from "@mui/material/TableContainer";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Checkbox,
  CircularProgress,
} from "@mui/material";
import { useSelector } from "react-redux";

const CreateCategoryTableData = ({
  CategoryList,
  handleChipClick,
  checkedId,
  handleCheckedAllId,
  handleCheckedId, loadingCategory
}) => {
  const [selectedRow, setSelectedRow] = useState(null);
  const onGoingSchedule = useSelector((state) => state.matchingReducer.onGoingSchedule);

  const handleRowClick = (index) => {
    setSelectedRow(index);
    // console.log("선택된카테고리 : " + CategoryList[index].categoryNo);
  };

  return (
    <TableContainer
      sx={{
        maxHeight: "600px",
        overflowY: "auto", // 세로 스크롤 적용
        mt: 2,
      }}
    >
      <Table aria-label="simple table" sx={{ mt: 3, whiteSpace: "nowrap" }}>
        <TableHead
          sx={{
            backgroundColor: "aliceblue",
            position: "sticky",
            top: 0,
            zIndex: 1,
          }}
        >
          <TableRow>
            <TableCell align="center">
              <Checkbox
                color="secondary"
                checked={
                  checkedId.length > 0 &&
                  checkedId.length ===
                    CategoryList.filter((product) => !product.categoryDelete).length
                }
                onChange={handleCheckedAllId}
              />
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                용도 코드
              </Typography>
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                용도명
              </Typography>
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                사용여부
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {loadingCategory ? (
              <TableRow>
                <TableCell sx={{ height: "500px" }} align="center" colSpan={9}>
                  <CircularProgress size={50} />
                </TableCell>
              </TableRow>
            ) : CategoryList.length === 0 ? (
            <TableRow>
              <TableCell colSpan={4}>
                <Typography color="textSecondary" sx={{ fontSize: "14px", textAlign: "center" }}>
                  검색결과가 없습니다.
                </Typography>
              </TableCell>
            </TableRow>
          ) : (
            CategoryList.map((product, index) => (
              <TableRow
                key={product.categoryNo}
                onClick={() => handleRowClick(index)}
                sx={{
                  backgroundColor: selectedRow === index ? "lightblue" : "transparent",
                  cursor: "pointer",
                }}
              >
                <TableCell align="center">
                  <Checkbox
                    color="secondary"
                    checked={checkedId.indexOf(product.categoryNo) !== -1}
                    onChange={(event) => handleCheckedId(event, product.categoryNo)}
                  />
                </TableCell>
                <TableCell align="center">
                  {/* <Typography sx={{ fontSize: "15px", fontWeight: "500" }}>{product.id}</Typography> */}
                  <Typography sx={{ fontSize: "15px", fontWeight: "500" }}>
                    {product.categoryNo}
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography variant="h6" sx={{ fontWeight: "600", textAlign: "center" }}>
                    {product.categoryName}
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Chip
                    sx={{
                      pl: "4px",
                      pr: "4px",
                      backgroundColor: product.usable ? "primary.main" : "primary.red",
                      color: "#fff",
                    }}
                    size="small"
                    label={product.usable === true ? "사용" : "미사용"} //usable 값을 숫자로 받아왔는데, 그러면 에러나므로 String 형태로 받음
                    onClick={() => handleChipClick(index)}
                    disabled={onGoingSchedule !== null ? true : false}
                  ></Chip>
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default CreateCategoryTableData;