import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Grid,
  styled,
  Typography,
} from "@mui/material";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import SendIcon from "@mui/icons-material/Send";
import DeleteIcon from "@mui/icons-material/Delete";
import { Span } from "../Typography";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import Swal from "sweetalert2";
axios.defaults.withCredentials = true; 

const TextField = styled(TextValidator)(() => ({
  width: "100%",
  marginBottom: "16px",
}));

// SnackBar 스낵바
// const Alert = React.forwardRef(function Alert(props, ref) {
//   return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
// });

const UserCreateTableData = ({
  submitUser,
  setSubmitUser,
  handleCloseAddModal,
  deptList,
  rankList,
  handleClickSnackbar, api
}) => {
  const navigate = useNavigate();

  const [id, setId] = useState("");
  const [userName, setUserName] = useState("");
  const [rankNo, setRankNo] = useState("");
  const [deptNo, setDeptNo] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");

  useEffect(() => {
    ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
      if (value !== password) return false;

      return true;
    });
    return () => ValidatorForm.removeValidationRule("isPasswordMatch");
  }, [confirmPassword, password]);

  // 아이디 textField
  const handleChangeId = (e) => {
    e.persist(); // e.persist()를 먼저 호출
    const value = e.target.value.trim(); // 시작 또는 끝에 있는 공백 제거
    setId(value);
  };

  // 사원명 textField
  const handleChangeName = (e) => {
    const value = e.target.value.trim(); // 시작 또는 끝에 있는 공백 제거
    e.persist();
    setUserName(value);
  };

  // 비밀번호 textField
  const handleChangePassword = (e) => {
    const value = e.target.value.trim(); // 시작 또는 끝에 있는 공백 제거
    e.persist();
    setPassword(value);
  };

  const handleChangeConfirmPswd = (e) => {
    const value = e.target.value.trim(); // 시작 또는 끝에 있는 공백 제거
    e.persist();

    if (e.target.name === "confirmPassword") {
      setConfirmPassword(value);
    }
  };

  const handleChangeEmail = (e) => {
    const value = e.target.value.trim(); // 시작 또는 끝에 있는 공백 제거
    e.persist();
    setEmail(value);
  };

  // Select 태그 유효성 검사 (직급, 부서)
  const [errorRank, setErrorRank] = useState(false);
  const [errorDept, setErrorDept] = useState(false);

  // 부서 dropdown
  const handleChangeDept = (event) => {
    setDeptNo(event.target.value);
  };

  // 직급 dropdown
  const handleChangeRank = (event) => {
    setRankNo(event.target.value);
  };

  // 폼 제출 (부서, 직급 유효성 검사 & 사원 추가)
  const handleSubmit = () => {
    if (rankNo === null || rankNo === "" || rankNo === undefined) {
      setErrorRank(true);
    } else {
      setErrorRank(false);
    }

    if (deptNo === null || deptNo === "" || deptNo === undefined) {
      setErrorDept(true);
    } else {
      setErrorDept(false);
    }

    const data = JSON.stringify({
      id: id,
      userName: userName,
      rankNo: rankNo,
      deptNo: deptNo,
      password: password,
      email: email,
    });

    api({
      url: "/admin/user/add",
      method: "post",
      data: data,
      headers: { "Content-type": "application/json" },
    })
      .then(() => {
        handleClickSnackbar(); // 요청이 성공했을 때 스낵바 열기
        setSubmitUser(!submitUser);
        handleCloseAddModal();
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        if (error.response && error.response.status === 400) {
          handleCloseAddModal();
          Swal.fire({
            icon: "error",
            title: "사원 추가 실패",
            text: "동일한 ID를 가진 사원이 존재합니다!",
          });
        }
        if (error.response) {
          console.log("통신error");
        }
      });
  };

  const handleBackdropClick = (e) => {
    // 모달 외부 클릭 막음
    e.stopPropagation();
  };

  return (
    <div>
      <ValidatorForm
        onSubmit={handleSubmit}
        // onError={() => null}
        backdropprops={{ onClick: handleBackdropClick }}
      >
        <Grid item xs={12} sx={{ mt: 5.5, ml: "5%", mb: 2 }}>
          <Typography
            variant="h3"
            sx={{
              fontFamily: "Roboto, sans-serif",
              fontWeight: "bold",
            }}
          >
            사원 추가
          </Typography>
        </Grid>

        <Grid
          container
          spacing={6}
          justifyContent="space-between"
          item
          sx={{ paddingRight: "40px", paddingLeft: "40px" }}
        >
          <Grid item lg={6} md={6} sm={12} xs={12} sx={{ mt: 2 }}>
            <TextField
              type="text"
              name="id"
              label="아이디"
              onChange={handleChangeId}
              value={id}
              validators={["required"]} // 값이 비어있으면 에러
              errorMessages={["아이디를 입력해주세요."]} // 에러 메시지
            />
            <TextField
              name="password"
              type="password"
              label="비밀번호"
              value={password}
              onChange={handleChangePassword}
              validators={["required"]}
              errorMessages={["비밀번호를 입력해주세요."]}
            />
            <Box sx={{}}>
              <FormControl
                sx={{ minWidth: 120, width: "100%", mb: 2 }}
                variant="outlined"
                fullWidth
                error={errorRank} // 에러 상태에 따라 FormControl에 에러 스타일 적용
              >
                <InputLabel id="demo-simple-select-helper-label">직급</InputLabel>
                <Select
                  labelId="demo-simple-select-helper-label"
                  id="demo-simple-select-helper"
                  value={rankNo || ""}
                  label="직급"
                  onChange={handleChangeRank}
                  MenuProps={{
                    PaperProps: {
                      style: {
                        maxHeight: 195,
                        width: "auto",
                      },
                    },
                  }}
                  validators={["required"]}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {rankList.map((rank, index) => (
                    <MenuItem key={index} value={index + 1}>
                      {rank}
                    </MenuItem>
                  ))}
                </Select>
                {errorRank && <span style={{ color: "red" }}>직급을 선택해주세요.</span>}
              </FormControl>
            </Box>

            <TextField
              sx={{ mb: 4 }}
              type="email"
              name="email"
              label="이메일"
              value={email}
              onChange={handleChangeEmail}
              validators={["required", "isEmail"]}
              errorMessages={["이메일을 입력해주세요.", "유효한 이메일 형식이 아닙니다."]}
            />
          </Grid>

          <Grid item lg={6} md={6} sm={12} xs={12} sx={{ mt: 2 }}>
            <TextField
              type="text"
              name="userName"
              id="standard-basic"
              value={userName}
              onChange={handleChangeName}
              errorMessages={["사원명을 입력해주세요."]}
              label="사원명 (최소 1자 이상, 최대 9자 이하)"
              validators={["required", "minStringLength: 1", "maxStringLength: 9"]}
            />

            <TextField
              type="password"
              name="confirmPassword"
              onChange={handleChangeConfirmPswd}
              label="비밀번호 확인"
              value={confirmPassword}
              validators={["required", "isPasswordMatch"]}
              errorMessages={["비밀번호를 입력해주세요.", "비밀번호가 일치하지 않습니다."]}
            />
            <Box sx={{}}>
              <FormControl
                sx={{ minWidth: 120, width: "100%", mb: 2 }}
                variant="outlined"
                fullWidth
                error={errorDept} // 에러 상태에 따라 FormControl에 에러 스타일 적용
              >
                <InputLabel id="demo-simple-select-helper-label">부서</InputLabel>
                <Select
                  labelId="demo-simple-select-helper-label"
                  id="demo-simple-select-helper"
                  value={deptNo || ""} // dept가 null일 때도 빈 문자열로 설정
                  label="부서"
                  onChange={handleChangeDept}
                  MenuProps={{
                    PaperProps: {
                      style: {
                        maxHeight: 195,
                        width: "auto",
                      },
                    },
                  }}
                  validators={["required"]}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {deptList
                    .filter((dept) => dept !== "관리부")
                    .map((dept, index) => (
                      <MenuItem key={index} value={index + 20}>
                        {dept}
                      </MenuItem>
                    ))}
                </Select>
                {errorDept && <span style={{ color: "red" }}>부서를 입력해주세요.</span>}
              </FormControl>
            </Box>
          </Grid>
        </Grid>

        <Grid
          container
          justifyContent="flex-end"
          sx={{
            paddingRight: "40px",
            paddingLeft: "20px",
            paddingBottom: "40px",
          }}
          item
          xs={12}
        >
          <Button
            color="primary"
            variant="contained"
            type="submit"
            sx={{ height: "45px" }}
            // onClick={() => {
            //   handleSubmit();
            // }}
            // onClick={handleClickSnackbar}
            disabled={
              id === "" ||
              userName === "" ||
              rankNo === "" ||
              deptNo === "" ||
              password === "" ||
              confirmPassword === "" ||
              email === ""
            }
          >
            <SendIcon />
            <Span sx={{ pl: 1, textTransform: "capitalize" }}>사원 추가</Span>
          </Button>
          <Button
            color="error"
            variant="contained"
            type="button"
            sx={{ height: "45px", ml: "10px" }}
            onClick={handleCloseAddModal}
          >
            <DeleteIcon />
            <Span sx={{ pl: 1, textTransform: "capitalize" }}>취소하기</Span>
          </Button>
        </Grid>
      </ValidatorForm>
    </div>
  );
};

export default UserCreateTableData;
