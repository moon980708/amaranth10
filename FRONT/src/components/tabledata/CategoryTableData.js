import React, { useState } from "react";
import TableContainer from "@mui/material/TableContainer";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import {
  reset_Selectect_ItemList,
  select_Category,
  select_ItemList,
} from "../../reducer/module/matchingReducer";
import getSelectCategoryAboutItemList from "../../reducer/axios/matchingAbout/getSelectCategoryAboutItemList";
import { useNavigate } from "react-router-dom";
import UseApi from "../UseApi";

const CategoryTableData = ({ setSelectCategory }) => {
  const [selectedRow, setSelectedRow] = useState(null);
  const list = useSelector((state) => state.matchingReducer.categoryList);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const api = UseApi();

  const handleRowClick = async (index) => {
    //용도 선택 시
    setSelectedRow(index); //해당 인덱스 넣고
    if (selectedRow === index) {
      //같은거클릭하면 초기화
      setSelectedRow(null);
      setSelectCategory(null);
      dispatch(select_Category(null));
      dispatch(reset_Selectect_ItemList());
    } else {
      console.log("선택된카테고리 : " + list[index].categoryName);
      setSelectCategory(list[index].categoryName);
      dispatch(select_Category(list[index].categoryNo));
      dispatch(reset_Selectect_ItemList());

      // await 키워드를 사용하여 비동기 함수의 결과를 기다립니다.
      const itemListData = await getSelectCategoryAboutItemList(
        list[index].categoryNo,
        navigate,
        api
      );

      // 받아온 결과를 dispatch를 통해 사용합니다.
      if (itemListData) {
        dispatch(reset_Selectect_ItemList());
        itemListData.forEach((item) => {
          dispatch(select_ItemList(item));
        });
      } else {
        console.log("getSelectCategoryAboutItemList 함수에서 에러 발생");
      }
    }
  };

  return (
    <TableContainer
      sx={{
        minHeight: "400px",
        maxHeight: "400px",
      }}
    >
      <Table aria-label="simple table" sx={{ mt: 3, whiteSpace: "nowrap" }}>
        <TableHead
          sx={{
            backgroundColor: "aliceblue",
            position: "sticky",
            top: 0,
            zIndex: 1,
          }}
        >
          <TableRow>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                용도 코드
              </Typography>
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                용도명
              </Typography>
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                사용여부
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {list.length === 0 ? (
            <TableRow>
              <TableCell colSpan={3}>
                <Typography
                  color="textSecondary"
                  sx={{ fontSize: "14px", textAlign: "center" }}
                >
                  검색결과가 없습니다.
                </Typography>
              </TableCell>
            </TableRow>
          ) : (
            list.map((list, index) => (
              <TableRow
                key={index}
                onClick={() =>
                  list.usable === true ? handleRowClick(index) : ""
                }
                sx={{
                  backgroundColor:
                    selectedRow === index ? "lightblue" : "transparent",
                  cursor: list.usable === true ? "pointer" : "default",
                }}
              >
                <TableCell align="center">
                  <Typography sx={{ fontSize: "15px", fontWeight: "500" }}>
                    {list.categoryNo}
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography
                    variant="h6"
                    sx={{ fontWeight: "600", textAlign: "center" }}
                  >
                    {list.categoryName}
                  </Typography>
                  {/* <Typography
                  color="textSecondary"
                  sx={{ fontSize: "13px", textAlign: "center" }}
                >
                  {list.post}
                </Typography> */}
                </TableCell>
                <TableCell align="center">
                  <Typography
                    variant="h6"
                    sx={{
                      fontWeight: "600",
                      textAlign: "center",
                      color: list.usable === true ? "skyblue" : "red",
                    }}
                  >
                    {list.usable === true ? "사용" : "미사용"}
                  </Typography>
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default CategoryTableData;
