import React, { useEffect, useState } from "react";
import TableContainer from "@mui/material/TableContainer";

import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { ExitToApp } from "@mui/icons-material";
import { useDispatch, useSelector } from "react-redux";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";

import Select from "@mui/material/Select";
import {
  remove_Selected_ItemList,
  update_Selected_ItemEssential,
  update_Selected_ItemOption,
} from "../../reducer/module/matchingReducer";

const MatchingTableData = ({ ItemList, selectedItems }) => {
  const matchingList = useSelector(
    (state) => state.matchingReducer.selectedItemList
  );

  const onGoingYearMonth = useSelector(
    (state) => state.matchingReducer.onGoingYearMonth
  );

  const dispatch = useDispatch();

  const changeItemOpion = (itemNo, itemOption) => {
    const itemNoAndOption = {
      itemNo: itemNo,
      itemOption: itemOption,
    };
    console.log("========조건 입력 확인 : " + itemNo + " // " + itemOption);

    dispatch(update_Selected_ItemOption(itemNoAndOption));
  };

  const changeItemEssential = (itemNo, itemEssential) => {
    const itemNoAndEssential = {
      itemNo: itemNo,
      itemEssential: itemEssential,
    };
    console.log("=======updateEssential : " + itemNo + " ////" + itemEssential);

    dispatch(update_Selected_ItemEssential(itemNoAndEssential));
  };

  const [essential, setEssential] = useState(1);

  const handelChangeEssential = (e) => {
    setEssential(e.target.value);
  };

  return (
    <TableContainer
      sx={{
        maxHeight: "400px", // 최대 높이 설정
        overflowY: "auto", // 세로 스크롤 적용
      }}
    >
      <Table
        aria-label="simple table"
        sx={{
          mt: 3,
          whiteSpace: "nowrap",
        }}
      >
        <TableHead
          sx={{
            backgroundColor: "aliceblue",
            position: "sticky",
            top: 0,
            zIndex: 1,
          }}
        >
          <TableRow>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                코드
              </Typography>
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                항목
              </Typography>
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                항목 속성
              </Typography>
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                필수 여부
              </Typography>
            </TableCell>
            <TableCell>
              <Typography color="textSecondary" variant="h6" align="center">
                항목 조건 내용
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {matchingList.map((matchingList, index) => (
            <TableRow key={index}>
              <TableCell align="center">
                <Typography
                  sx={{
                    fontSize: "15px",
                    fontWeight: "500",
                  }}
                >
                  {matchingList.itemNo}
                </Typography>
              </TableCell>

              <TableCell align="center">
                <Typography
                  sx={{
                    fontSize: "15px",
                    fontWeight: "500",
                  }}
                >
                  {matchingList.itemTitle}
                </Typography>
              </TableCell>

              <TableCell align="center">
                <Typography
                  sx={{
                    fontSize: "15px",
                    fontWeight: "500",
                  }}
                >
                  {matchingList.itemType}
                </Typography>
              </TableCell>

              <TableCell align="center">
                {/* <Typography
                  sx={{
                    fontSize: "15px",
                    fontWeight: "500",
                  }}
                >
                  {matchingList.itemNo === 100 ? "필수" : "선택"}
                </Typography> */}
                <FormControl variant="standard" sx={{ m: 1, minWidth: 70 }}>
                  <Select
                    value={matchingList.itemEssential}
                    disabled={
                      onGoingYearMonth === null &&
                      matchingList.itemNo !== 100
                        ? false
                        : true
                    }
                    onChange={(e) => {
                      const inputValue = e.target.value;
                      changeItemEssential(matchingList.itemNo, inputValue);
                    }}
                    sx={{
                      fontSize: "15px",
                      fontWeight: "500",
                    }}
                  >
                    <MenuItem value={true}>필수</MenuItem>
                    <MenuItem value={false}>선택</MenuItem>
                  </Select>
                </FormControl>
              </TableCell>
              <TableCell align="center">
                <TextField
                  id="standard-basic"
                  label={
                    matchingList.itemType === "숫자"
                      ? "조건입력"
                      : "조건할당 불가"
                  }
                  variant="standard"
                  disabled={
                    onGoingYearMonth === null &&
                    matchingList.itemType === "숫자"
                      ? false
                      : true
                  }
                  inputProps={{
                    pattern: "[0-9]*",
                    inputMode: "numeric",
                  }}
                  value={
                    matchingList.itemOption === "null"
                      ? ""
                      : matchingList.itemOption || ""
                  }
                  onChange={(e) => {
                    const inputValue = e.target.value;
                    if (!/^\d*$/.test(inputValue)) {
                      alert("숫자만 입력해주세요.");
                      e.target.value = inputValue.replace(/[^0-9]/g, ""); // 숫자가 아닌 문자를 모두 제거
                    } else {
                      changeItemOpion(matchingList.itemNo, inputValue);
                    }
                  }}
                />
              </TableCell>
              <TableCell>
                <IconButton
                  aria-label="delete"
                  disabled={
                    onGoingYearMonth === null && matchingList.itemNo !== 100
                      ? false
                      : true
                  }
                  onClick={() => {
                    dispatch(remove_Selected_ItemList(matchingList.itemNo));
                  }}
                >
                  <CloseIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default MatchingTableData;
