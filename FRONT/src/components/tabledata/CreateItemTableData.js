import React,{useState} from "react";
import TableContainer from "@mui/material/TableContainer";
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Checkbox,
  CircularProgress,
} from "@mui/material";

const CreateItemTableData = ({
  ItemList,
  checkedId,
  handleCheckedId,
  handleCheckedAllId,
  loadingItem,
}) => {
  return (
    <TableContainer
      sx={{
        maxHeight: "600px", // 최대 높이 설정
        overflowY: "auto", // 세로 스크롤 적용
        mt: 2,
      }}
    >
      <Table
        aria-label="simple table"
        sx={{
          mt: 3,
          whiteSpace: "nowrap",
        }}
      >
        <TableHead
          sx={{
            backgroundColor: "aliceblue",
            position: "sticky",
            top: 0,
            zIndex: 1,
          }}
        >
          <TableRow>
            <TableCell align="center">
              <Checkbox
                color="secondary"
                checked={
                  checkedId.length > 0 &&
                  checkedId.length === ItemList.filter((product) => !product.itemDelete).length
                }
                onChange={handleCheckedAllId}
              />
            </TableCell>
            <TableCell>
              <Typography align="center" color="textSecondary" variant="h6">
                항목코드
              </Typography>
            </TableCell>
            <TableCell>
              <Typography align="center" color="textSecondary" variant="h6">
                항목명
              </Typography>
            </TableCell>
            <TableCell>
              <Typography align="center" color="textSecondary" variant="h6">
                항목속성
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {loadingItem ? (
              <TableRow>
                <TableCell sx={{ height: "500px" }} align="center" colSpan={9}>
                  <CircularProgress size={50} />
                </TableCell>
              </TableRow>
            ) : ItemList.length === 0 ? (
            <TableRow>
              <TableCell colSpan={4}>
                <Typography color="textSecondary" sx={{ fontSize: "14px", textAlign: "center" }}>
                  검색결과가 없습니다.
                </Typography>
              </TableCell>
            </TableRow>
          ) : (
            ItemList.map((product, index) => (
              <TableRow key={product.itemNo}>
                <TableCell align="center">
                  <Checkbox
                    color="secondary"
                    checked={checkedId.indexOf(product.itemNo) !== -1}
                    onChange={(event) => handleCheckedId(event, product.itemNo)}
                  />
                </TableCell>

                <TableCell align="center">
                  <Typography
                    sx={{
                      fontSize: "15px",
                      fontWeight: "500",
                    }}
                  >
                    {product.itemNo}
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography variant="h6" sx={{ fontWeight: "600", textAlign: "center" }}>
                    {product.itemTitle}
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography
                    variant="h6"
                    sx={{
                      fontWeight: "600",
                    }}
                  >
                    {product.itemType}
                  </Typography>
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default CreateItemTableData;