import React, { useState, useEffect } from "react";
import TableContainer from "@mui/material/TableContainer";

import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
  Checkbox,
  CircularProgress,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { SwitchCameraOutlinedIcon } from "@mui/icons-material/SwitchCameraOutlined";
import Swal from "sweetalert2";
import dayjs from "dayjs";
import { useDispatch, useSelector } from "react-redux";
import { DeleteSweep, EditOutlined } from "@mui/icons-material";
import axios from "axios";
import {
  add_Select_Schedule,
  change_ScheduleState,
} from "../../reducer/module/scheduleReducer";
import UseApi from "../UseApi";
import ModifyScheduleModal from "../modal/ModifyScheduleModal";
import getScheduleModifyAboutInfo from "../../reducer/axios/scheduleAbout/getScheduleModifyAboutInfo";
import { useNavigate } from "react-router-dom";
import getOngoingSchedule from "../../reducer/axios/matchingAbout/getOngoingSchedule";
import {
  reset_Ongoing_Schedule,
  set_Ongoing_Schedule,
} from "../../reducer/module/matchingReducer";

const ScheduleTableData = ({
  scheduleList,
  setScheduleList,
  listUpdate,
  setListUpdate,
  listLoadingStart,
  setListLoadingStart,
  chargeDateString,
  chargeDate,
}) => {
  //체크박스 선택된 차수no
  const selectScheduleNo = useSelector(
    (state) => state.scheduleReducer.selectScheduleNo
  );
  const activeScheduleList = useSelector(
    (state) => state.scheduleReducer.activeSchedules
  );
  const scheduleIsLoading = useSelector(
    (state) => state.scheduleReducer.scheduleIsLoading
  );

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const api = UseApi();

  const [selectedItems, setSelectedItems] = useState([]);

  const [selectSchedule, setSelectSchedule] = useState([]);

  //modify에 넘기는 no
  const [selectedScheduleNo, setSelectedScheduleNo] = useState(null);
  //체크박스 ischecked옵션 함수
  const handleCheckboxChange = (event, no) => {
    const isChecked = event.target.checked;

    // setSelectedItems((prevSelectedItems) => {
    //   if (isChecked) {
    //     console.log("setselectedItems : " +selectedItems);
    //     return [...prevSelectedItems, no];
    //   } else {
    //     console.log("setselectedItems : " +selectedItems);
    //     return prevSelectedItems.filter((item) => item !== no);
    //   }
    // });
    // dispatch(add_Select_Schedule(scheduleList[index].scheduleNo);
    dispatch(add_Select_Schedule(no));
  };

  //전체 체크박스 선택 함수
  const handleSelectAll = (event, index) => {
    const isChecked = event.target.checked;
    if (isChecked) {
      console.log("ischecked: " + isChecked);
      // 이미 선택된 스케줄 번호들
      const selectedScheduleNos = selectScheduleNo;

      // 진행예정이면서 선택되지 않은 스케줄들을 선택하도록 추가
      const unselectedScheduleNos = scheduleList
        .filter(
          (list) =>
            !selectedScheduleNos.includes(list.scheduleNo) &&
            list.close === "진행예정"
        )
        .map((list) => list.scheduleNo);

      const newSelectedNos = [...selectedScheduleNos, ...unselectedScheduleNos];
      dispatch(add_Select_Schedule(newSelectedNos));
    } else {
      dispatch(add_Select_Schedule([])); // 모든 선택을 해제하도록 빈 배열로 설정
    }
  };

  // const isChecked = event.target.checked;
  // if (isChecked) {
  //   const newSelectedItems = scheduleList.map((list) => list.scheduleNo);
  //   setSelectedItems((prevSelectedItems) => {
  //     // 기존에 선택된 아이템들 중에서 scheduleList에 포함되지 않은 아이템들만 추가합니다.
  //     return [...prevSelectedItems, ...newSelectedItems.filter((item) => !prevSelectedItems.includes(item))];
  //   });
  //   dispatch(add_Select_Schedule(newSelectedItems));
  // } else {
  //   setSelectedItems((prevSelectedItems) => {
  //     // 기존에 선택된 아이템들 중에서 scheduleList에 포함된 아이템들만 선택 취소합니다.
  //     return prevSelectedItems.filter((item) => scheduleList.every((list) => list.scheduleNo !== item));
  //   });
  //   dispatch(add_Select_Schedule([]));

  //수정 모달
  const [modifyModalOpen, setModifyModalOpen] = useState(false);

  const [modifyInfo, setModifyInfo] = useState([]);

  const handleModifyBtnClick = async (
    scheduleNo,
    yearMonth,
    schedule,
    expendStart,
    expendEnd,
    chargeStart,
    chargeEnd,
    payday
  ) => {
    try {
      const result = await getScheduleModifyAboutInfo({
        yearMonth,
        schedule,
        navigate,
        api,
      });
      const prevChargeEnd = result.prevChargeEnd;
      const nextChargeStart = result.nextChargeStart;
      const prevPayday = result.prevPayday;
      const nextPayday = result.nextPayday;

      console.log(
        "data에서 받은값 : " + prevChargeEnd,
        nextChargeStart,
        prevPayday,
        nextPayday
      );

      const scheduleData = {
        scheduleNo,
        yearMonth,
        schedule,
        expendStart,
        expendEnd,
        chargeStart,
        chargeEnd,
        payday,
        prevChargeEnd,
        nextChargeStart,
        prevPayday,
        nextPayday,
      };
      setModifyInfo(scheduleData);
      // console.log(modifyInfo);
      setModifyModalOpen(true);
    } catch (error) {
      console.log("함수에서못받음");
    }
  };
  const handleModifyModalClose = () => {
    setSelectedScheduleNo(null);
    setModifyModalOpen(false);
  };

  const isItemChecked = (no) => selectScheduleNo.includes(no);
  const [closeState, setCloseState] = useState("");

  const fetchData = async () => {
    try {
      const result = await getOngoingSchedule(navigate, api);
      console.log("result : " + result);
      if (result === "") {
        console.log("===현재 진행중인 차수 없음");
        dispatch(reset_Ongoing_Schedule());
      } else {
        console.log("진행중인 차수 있음");
        dispatch(set_Ongoing_Schedule(result));
      }
    } catch (error) {
      console.log("getOngoingSchedule실패");
    }
  };

  const handleChipClick = (list) => {
    // const updatedProducts = [...products];
    // const currentClose = updatedProducts[index].close;
    console.log(list.close);
    if (list.close === "진행중") {
      // alert("진짜 마감할꺼임?");
      Swal.fire({
        title: "마감하시겠습니까?",
        text: "마감하시면 해당 차수에 대한 청구가 종료됩니다",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "네",
        cancelButtonText: "아니요",
      }).then((result) => {
        if (result.isConfirmed) {
          //마감상태 수정을 위한 Put
          api
            .put(`/schedule/updateclose/${list.scheduleNo}`, {
              close: "마감",
            })
            .then(() => {
              console.log("마감상태 마감으로 수정 성공");
              Swal.fire({
                title: "차수 마감!",
                text: "차수가 성공적으로 마감되었습니다.",
                icon: "success",
                confirmButtonText: "확인",
              });
              setListLoadingStart(!listLoadingStart);
              fetchData();
            })
            .catch((error) => {
              // if (error.response && error.response.status === 401) {
              //   alert("로그인세션만료");
              //   navigate("/");
              // }
              console.log("마감상태 수정 통신 실패");
            });
        }
      });
    } else if (list.close === "마감") {
      Swal.fire({
        title: "다시 청구진행 하시겠습니까?",
        text: "해당 차수에 대한 청구가 진행됩니다.",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "네",
        cancelButtonText: "아니요",
      }).then((result) => {
        if (result.isConfirmed) {
          api
            .put(`/schedule/updateclose/${list.scheduleNo}`, {
              close: "진행중",
            })
            .then(() => {
              console.log("마감상태 진행중으로 수정 성공");
              Swal.fire({
                title: "차수 진행!",
                text: "차수가 성공적으로 진행되었습니다.",
                icon: "success",
                confirmButtonText: "확인",
              });
              setListLoadingStart(!listLoadingStart);
              fetchData();
            })
            .catch((error) => {
              // if (error.response && error.response.status === 401) {
              //   alert("로그인세션만료");
              //   navigate("/");
              // }
              console.log("마감상태 수정 통신 실패");
            });
        }
      });
    }
  };

  const scheduleDelete = (scheduleNo) => {
    api
      .delete(`/schedule/delete/${scheduleNo}`)
      .then((result) => {
        Swal.fire("차수 삭제 완료", "해당 차수가 삭제되었습니다.", "success");
        dispatch(change_ScheduleState());

        setListLoadingStart(!listLoadingStart); //삭제했으니 리스트 다시 랜더링 시키기위한 set변경함수
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        console.log("삭제 요청 실패");
      });
  };
  // currentDate와 chargeDate를 비교하는 함수 - 등록버튼 비활성화를 위함
  const compareDates = (chargeEnd) => {
    return dayjs(chargeEnd).isBefore(dayjs(Date()));
  };

  return (
    <Box>
      <TableContainer>
        <Table>
          <TableHead sx={{ backgroundColor: "aliceblue" }}>
            <TableRow>
              <TableCell align="center">
                <Checkbox
                  color="secondary"
                  checked={
                    activeScheduleList.length === 0
                      ? false
                      : selectScheduleNo.length === activeScheduleList.length
                  }
                  disabled={activeScheduleList.length === 0 ? true : false}
                  // indeterminate={
                  //   selectScheduleNo.length > 0 &&
                  //   selectScheduleNo.length < scheduleList.length
                  // }
                  onChange={handleSelectAll}
                />
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  청구년월
                </Typography>
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  차수
                </Typography>
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  청구가능 시작일
                </Typography>
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  청구가능 마감일
                </Typography>
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  지출범위 시작일
                </Typography>
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  지출범위 마감일
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  지급일
                </Typography>
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  마감
                </Typography>
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  수정
                </Typography>
              </TableCell>
              <TableCell>
                <Typography color="textSecondary" variant="h6" align="center">
                  삭제
                </Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {scheduleIsLoading ? (
              <TableRow>
                <TableCell colSpan={11} sx={{ height: "288px" }} align="center">
                  <CircularProgress size={50} />
                </TableCell>
              </TableRow>
            ) : (
              scheduleList.map((list, index) => (
                <TableRow key={list.scheduleNo}>
                  <TableCell align="center">
                    <Checkbox
                      color="secondary"
                      disabled={list.close === "진행예정" ? false : true}
                      checked={isItemChecked(list.scheduleNo)}
                      onChange={(event) => {
                        handleCheckboxChange(event, list.scheduleNo);
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {/* dayjs().toDat()하면 date객체이기때문에 바인딩불가 */}
                      {dayjs(list.yearMonth).format("YYYY-MM")}
                    </Typography>
                  </TableCell>

                  <TableCell align="center">
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {list.schedule}
                    </Typography>
                  </TableCell>

                  <TableCell align="center">
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {dayjs(list.chargeStart).format("YYYY-MM-DD")}
                    </Typography>
                  </TableCell>

                  <TableCell align="center">
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {dayjs(list.chargeEnd).format("YYYY-MM-DD")}
                    </Typography>
                  </TableCell>

                  <TableCell align="center">
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {dayjs(list.expendStart).format("YYYY-MM-DD")}
                    </Typography>
                  </TableCell>

                  <TableCell align="center">
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {dayjs(list.expendEnd).format("YYYY-MM-DD")}
                    </Typography>
                  </TableCell>

                  
                  <TableCell align="center">
                    <Typography
                      sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                      }}
                    >
                      {dayjs(list.payday).format("YYYY-MM-DD")}
                    </Typography>
                  </TableCell>

                  <TableCell align="center">
                    <Chip
                      sx={{
                        pl: "4px",
                        pr: "4px",
                        backgroundColor:
                          list.close === "진행중"
                            ? "primary.main"
                            : list.close === "마감"
                            ? "primary.red"
                            : "primary.gray",
                        color: "#fff",
                        width: "70px",
                      }}
                      size="small"
                      label={list.close}
                      disabled={
                        list.close === "진행예정" ||
                        compareDates(list.chargeEnd)
                      }
                      onClick={() => handleChipClick(list)}
                    />
                  </TableCell>

                  {/* 수정 아이콘 */}
                  <TableCell align="center">
                    <Typography color="textprimary" variant="h6">
                      <Box>
                        <IconButton
                          color="neutral"
                          variant="contained"
                          size="small"
                          disabled={list.close === "진행예정" ? false : true}
                          onClick={() =>
                            handleModifyBtnClick(
                              list.scheduleNo,
                              list.yearMonth,
                              list.schedule,
                              list.expendStart,
                              list.expendEnd,
                              list.chargeStart,
                              list.chargeEnd,
                              list.payday
                            )
                          }
                        >
                          <EditOutlined />
                        </IconButton>
                      </Box>
                    </Typography>
                  </TableCell>
                  {/* 삭제 아이콘 */}
                  <TableCell align="center">
                    <Typography color="textprimary" variant="h6">
                      <Box>
                        <IconButton
                          color="neutral"
                          variant="contained"
                          disabled={list.close === "진행예정" ? false : true}
                          onClick={() => scheduleDelete(list.scheduleNo)}
                        >
                          <DeleteSweep />
                        </IconButton>
                      </Box>
                    </Typography>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      {modifyModalOpen && (
        <ModifyScheduleModal
          listLoadingStart={listLoadingStart}
          setListLoadingStart={setListLoadingStart}
          modifyInfo={modifyInfo}
          handleModifyModalClose={handleModifyModalClose}
          chargeDate={chargeDate}
        />
      )}
    </Box>
  );
};

export default ScheduleTableData;
