import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  //useState의 state와 같은느낌
  recentSchedule: 0,
  lastChargeEndDate: null,
  defaultPayday: null,
  selectScheduleNo: [],
  scheduleStateChange: false,
  activeSchedules: [],
  scheduleIsLoading : true,
  modalIsLoading : true,
  modifyScheduleState : false,
};

const scheduleReducer = createSlice({
  //state생성
  name: "scheduleReducer",
  initialState, //state설정
  reducers: {
    //여기가 state수정함수와 같은느낌
    update_Schedule: (state, action) => {
      console.log("scheduleReducer에서 : " + action.payload);
      state.recentSchedule = action.payload; //이렇게 적으면 해당 state가 우측값으로 바뀜
    },
    get_ActiveScheduleList: (state, action) => {
      console.log("getActiveScheduleList");
      state.activeSchedules = 
        action.payload.filter((list) => list.close === "진행예정");
        console.log("activeSchedules : "+  action.payload.filter((list) => list.close === "진행예정"))
    },
    get_Modal_Info: (state, action) => {
      console.log("get_Modal_Info");
    },
    add_Select_Schedule: (state, action) => {
      console.log("add_Select_Schedule");

      if (Array.isArray(action.payload)) {
        // 배열인 경우 전체 선택일 때이므로 선택된 모든 차수 번호를 덮어씁니다.
        state.selectScheduleNo = action.payload;
        console.log("전체선택 : " + state.selectScheduleNo);
      } else {
        if (state.selectScheduleNo.includes(action.payload)) {
          // 이미 포함되어 있는 경우, 제거
          state.selectScheduleNo = state.selectScheduleNo.filter(
            (scheduleNo) => scheduleNo !== action.payload
          );
          console.log("이미있어서 제거 :" + state.selectScheduleNo);
        } else {
          state.selectScheduleNo.push(action.payload);
          console.log("없어서 추가" + state.selectScheduleNo);
        }
      }
    },
    reset_Select_Schedule: (state, action) => {
      state.selectScheduleNo = [];
      state.activeSchedules = [];
    },
    change_ScheduleState: (state, action) => {
      state.scheduleStateChange = !state.scheduleStateChange;
    },
    change_ModifyScheduleState : (state, action) => {
      state.modifyScheduleState = !state.modifyScheduleState;
    },
    reset_Schedule_Loading : ( state, action) =>{
      state.scheduleIsLoading = true;
      console.log("reset_Schedule_Loading")
    },
    complete_Schedule_Loading : (state, action)=>{
      state.scheduleIsLoading = false;
      console.log("complete_Schedule_Loading")
    },
    reset_Modal_Loading : ( state, action) =>{
      state.modalIsLoading = true;
      console.log("reset_modalIsLoading")
    },
    complete_Modal_Loading : (state, action)=>{
      state.modalIsLoading = false;
      console.log("complete_modalIsLoading")
    }
  },
});

export const {
  update_Schedule,
  add_Select_Schedule,
  reset_Select_Schedule,
  change_ScheduleState,
  get_ActiveScheduleList,
  reset_ActiveScheduleList,
  complete_Schedule_Loading,
  reset_Schedule_Loading,
  complete_Modal_Loading,
  reset_Modal_Loading,
  change_ModifyScheduleState
} = scheduleReducer.actions; //수정함수 내보내기 - 여기에 적어줘야 다른파일에서 IMPORT 가능

export default scheduleReducer.reducer; //state 내보내기

//여기서 리듀서 작성하고
