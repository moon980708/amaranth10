import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  categoryList: null,
  itemList: null,
  selectedCategory: null,
  selectedItemList: [],
  selectedItemNo: [],
  onGoingYearMonth: null,
  onGoingSchedule: null,
};

const matchingReducer = createSlice({
  name: "matchingReducer",
  initialState,
  reducers: {
    get_CategoryList: (state, action) => {
      console.log("get_CategoryList : " + action.payload);
      state.categoryList = action.payload;
    },
    get_ItemList: (state, action) => {
      console.log("get_ItemList :" + action.payload);
      state.itemList = action.payload;
    },
    select_Category: (state, action) => {
      console.log("select_Category : " + action.payload);
      state.selectedCategory = action.payload;
    },
    reset_Select_Category : (state, action)=> {
      console.log("reset_Select_Category");
      state.selectedCategory = null;
    },
    select_ItemList: (state, action) => {
      console.log("select_ItemList : " + JSON.stringify(action.payload));

      if (
        state.selectedItemList.some(
          (item) => item.itemNo === action.payload.itemNo
        )
      ) {
        // 이미 포함되어 있는 경우, 제거
        state.selectedItemList = state.selectedItemList.filter(
          (selectedItemList) =>
            selectedItemList.itemNo !== action.payload.itemNo
        );
        state.selectedItemNo = state.selectedItemNo.filter(
          (selectedItemNo) => selectedItemNo !== action.payload.itemNo
        );
        console.log("매칭 - 항목 이미있어서 제거 :" + state.selectedItemList);
        console.log("매칭 - 이미 있어서 NO제거 : " + state.selectedItemNo);
      } else {
        state.selectedItemList.push(action.payload);
        state.selectedItemNo.push(action.payload.itemNo);
        console.log("매칭 - 항목 없어서 추가" + state.selectedItemList);
        console.log("매칭 - 없어서 NO추가 : " + state.selectedItemNo);

      }
    },
    remove_Selected_ItemList: (state, action) => {
      console.log("remove_Selected_ItemList");
      state.selectedItemList = state.selectedItemList.filter(
        (selectedItemList) => selectedItemList.itemNo !== action.payload
      );
    },
    reset_Selectect_ItemList: (state, action) => {
      state.selectedItemList = [];
      state.selectedItemNo = [];
      console.log("reset_Selectect_ItemList : " + state.selectedItemList);
    },
    update_Selected_ItemOption: (state, action) => {
      const selectedItemIndex = state.selectedItemList.findIndex(
        (item) => item.itemNo === action.payload.itemNo
      );
      if (selectedItemIndex !== -1) {
        console.log(
          "update_Selected_ItemOption : " + JSON.stringify(action.payload)
        );
        state.selectedItemList[selectedItemIndex].itemOption =
          action.payload.itemOption;
      }
      // console.log(JSON.stringify(state.selectedItemList));
    },
    update_Selected_ItemEssential: (state, action) => {
      const selectedItemIndex = state.selectedItemList.findIndex(
        (item) => item.itemNo === action.payload.itemNo
      );
      if (selectedItemIndex !== -1) {
        console.log(
          "update_Selected_ItemEssential : " + JSON.stringify(action.payload)
        );
        state.selectedItemList[selectedItemIndex].itemEssential =
          action.payload.itemEssential;
      }
      // console.log(JSON.stringify(state.selectedItemList));
    },
    set_Ongoing_Schedule: (state, action) => {
      console.log("set_Ongoing_Schedule : " + JSON.stringify(action.payload));
      state.onGoingYearMonth = action.payload[0].yearMonth.substring(0, 7);
      state.onGoingSchedule = action.payload[0].schedule;
    },
    reset_Ongoing_Schedule: (state, action) => {
      console.log("reset_Ongoing_Schedule");
      state.onGoingSchedule = null;
      state.onGoingYearMonth = null;
    },
  },
});

export const {
  get_CategoryList,
  get_ItemList,
  select_Category,
  select_ItemList,
  remove_Selected_ItemList,
  reset_Selectect_ItemList,
  update_Selected_ItemOption,
  update_Selected_ItemEssential,
  set_Ongoing_Schedule,
  reset_Ongoing_Schedule,
  reset_Select_Category
} = matchingReducer.actions; //변경함수 보내기

export default matchingReducer.reducer; //state보내기
