import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loginUserName: null,
  loginUserId : null,
  loginUserDept : null,
  loginUserRank : null,
  checkGroupManager : false,
  isSessionExpired: false,

};

const loginReducer = createSlice({
  name: "logingReducer",
  initialState,
  reducers: {
    set_CheckGroupManager:(state,action)=>{
      console.log("set_CheckGroupManager");
      state.checkGroupManager = true;
    },
    reset_CheckGroupManager:(state,action)=>{
      console.log("reset_CheckGroupManager");
      state.checkGroupManager = false;
    },
    set_LoginUserName : (state, action)=>{
      console.log("SetLoginUserName : " + action.payload );
      state.loginUserName = action.payload;
    },
    set_LoginUserId : (state, action)=>{
      console.log("SetLoginUserId : " + action.payload );
      state.loginUserId = action.payload;
    },
    set_LoginUserDept : (state, action)=>{
      console.log("set_LoginUserDept : " + action.payload );
      state.loginUserDept = action.payload;
    },
    set_LoginUserRank : (state, action)=>{
      console.log("set_LoginUserRank : " + action.payload );
      state.loginUserRank = action.payload;
    },
    reset_LoginUserName:(state, action)=>{
      console.log("resetLoginUserName");
      state.loginUserName = null;
    },
    reset_LoginUserId:(state, action)=>{
      console.log("resetLoginUserId");
      state.loginUserId = null;
    },
    reset_LoginUserDept:(state, action)=>{
      console.log("resetLoginUserName");
      state.loginUserDept = null;
    },
    reset_LoginUserRank:(state, action)=>{
      console.log("resetLoginUserId");
      state.loginUserRank = null;
    },
    sessionExpired: (state) => {
      console.log("sessionExpired");
      state.isSessionExpired = true;
    },
    resetSession: (state) => {
      console.log("resetSession");
      state.isSessionExpired = false;
    },

  },
});

export const {
  set_LoginUserName,
  set_LoginUserId,
  set_LoginUserDept,
  set_LoginUserRank,
  reset_LoginUserName,
  reset_LoginUserId,
  reset_LoginUserDept,
  reset_LoginUserRank,
  set_CheckGroupManager,
  reset_CheckGroupManager,
  sessionExpired,
  resetSession,
} = loginReducer.actions; //변경함수 보내기

export default loginReducer.reducer; //state보내기
