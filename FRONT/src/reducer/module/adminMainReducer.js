import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  mainLoading: false
};

const adminReducer = createSlice({
  name: "adminMainReducer",
  initialState,
  reducers: {
    set_MainLoading: (state, action) => {
      state.mainLoading = !state.mainLoading;
    },

  }
});

export const {
  set_MainLoading
} = adminReducer.actions;   //변경함수 보내기

export default adminReducer.reducer; //state 보내기
