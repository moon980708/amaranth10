import { combineReducers } from '@reduxjs/toolkit';

import scheduleReducer from './module/scheduleReducer';
import matchingReducer from './module/matchingReducer';
import loginReducer from './module/loginReducer';

import adminMainReducer from './module/adminMainReducer';


//리듀서들을 합침
const rootReducer = combineReducers({
    scheduleReducer: scheduleReducer,
    matchingReducer : matchingReducer,
    loginReducer: loginReducer,
    adminMainReducer: adminMainReducer
});


export default rootReducer; //내보내기