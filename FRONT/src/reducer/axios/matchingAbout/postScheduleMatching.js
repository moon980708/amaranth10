import React from "react";
import axios from "axios";

axios.defaults.withCredentials = true;

export default async function postScheduleMatching(
  selectedCategory,
  itemNoList,
  itemOptionList,
  itemEssential,
  navigate,
  api
) {

  const data = {
    categoryNo: selectedCategory,
    itemNoList: itemNoList,
    itemOptionList: itemOptionList,
    itemEssentialList : itemEssential,
  };
  try {
    const response = await api.post(
      "/categoryMatching",
        data
      
    );

    console.log(response);

    return "success";
  } catch (error) {
    console.log("getItemList 실패");
    return "error";
  }
}
