import React from "react";
import axios from "axios";

axios.defaults.withCredentials = true;

export default async function getOngoingSchedule(navigate, api) {

  try{
    const response = await api.get("/isOngoingSchedule");
    
    console.log("isOngoingSchedule 함수 결과 : " + JSON.stringify(response.data));

    return response.data;
  }catch(error){
    // if(error.response && error.response.status === 401){
    //   alert("로그인세션만료");
    //   navigate("/");
    // }

    console.log("isOngoingSchedule 실패");
    return null;
  }
}