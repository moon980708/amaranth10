import React from "react";
import axios from "axios";
axios.defaults.withCredentials = true;

export default async function getSearchCategoryKeyword(keyword, navigate, api) {

  try{
    const response = await api.get("/searchCategory",{
      params:{
        keyword : keyword
      }
    });

    console.log("getSearchCategoryKeyword 함수 결과 : " + JSON.stringify(response.data));

    return response.data;
  }catch(error){
    // if(error.response && error.response.status === 401){
    //   alert("로그인세션만료");
    //   navigate("/");
    // }
    console.log("getSearchCategoryKeyword 실패");
    return null;
  }
}