import React from "react";
import axios from "axios";

axios.defaults.withCredentials = true;

export default async function getItemList(navigate, api) {


  try{
    const response = await api.get("/getItemList");

    console.log("getItemList 함수 결과 : " + JSON.stringify(response.data));

    return response.data;
  }catch(error){
    // if(error.response && error.response.status === 401){
    //   alert("로그인세션만료");
    //   navigate("/");
    // }
    console.log("getItemList 실패");
    return null;
  }
}