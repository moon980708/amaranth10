import React from "react";
import axios from "axios";
import { Card } from '@mui/material';
axios.defaults.withCredentials = true;

export default async function getSelectCategoryAboutItemList(categoryNo, navigate, api) {

  try{
    const response = await api.get("/getSelectCategoryAboutItemList",{
      params:{
        categoryNo : categoryNo
      }
    });

    console.log("getSelectCategoryAboutItemList 함수 결과 : " + JSON.stringify(response.data));

    return response.data;
  }catch(error){
    // if(error.response && error.response.status === 401){
    //   alert("로그인세션만료");
    //   navigate("/");
    // }
    console.log("getItemList 실패");
    return null;
  }
}