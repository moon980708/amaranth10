import React from "react";
import axios from "axios";
import dayjs from "dayjs";
axios.defaults.withCredentials = true;

export default async function modifySchedule({
  scheduleNo,
  startDate,
  endDate,
  enableChargeStart,
  enableChargeEnd,
  enablePayday,
  navigate,
  api
}) {
  try {
    
    // console.log("==========");
    // console.log(scheduleNo);
    const response = await api.put(
      '/schedule/modify',
      {
        scheduleNo : scheduleNo,
        expendStart : dayjs(startDate).format("YYYY-MM-DDTHH:mm:ss"),
        expendEnd : dayjs(endDate).format("YYYY-MM-DDTHH:mm:ss"),
        chargeStart : dayjs(enableChargeStart).format("YYYY-MM-DDTHH:mm:ss"),
        chargeEnd : dayjs(enableChargeEnd).format("YYYY-MM-DDTHH:mm:ss"),
        payday : dayjs(enablePayday).format("YYYY-MM-DDTHH:mm:ss")
      }
    );

  console.log("수정 업데이트 성공")
    return response.data;
  } catch (error) {
    // if(error.response && error.response.status === 401){
    //   alert("로그인세션만료");
    //   navigate("/");
    // }
    console.log("차수 수정 업데이트 실패");
    return null;
  }
}
