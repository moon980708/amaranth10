import React from "react";
import axios from "axios";

axios.defaults.withCredentials = true;

export default async function getScheduleModifyAboutInfo({
  yearMonth,
  schedule,
  navigate,
  api,
}) {
  try {
    const response = await api.get("/schedule/modify/info", {
      params: {
        schedule: schedule,
        yearMonth: yearMonth,
      },
    });
    // console.log("getschedulemodif data :" + response.data);

    console.log("datatata : " + JSON.stringify(response.data));
    const {
      prevChargeEnd,
      nextChargeStart,
      prevPayday,
      nextPayday
    } = response.data[0];
    console.log("+++++++++++");
    console.log(prevChargeEnd);
    // ISO 8601 형식을 dayjs 객체로 변환
    // const prevChargeEnd = dayjs(PREV_CHARGE_END).locale("ko").format("YYYY-MM-DD HH:mm:ss");
    // const nextChargeStart = dayjs(NEXT_CHARGE_START).locale("ko").format("YYYY-MM-DD HH:mm:ss");

    return response.data[0];
  } catch (error) {
    // if(error.response && error.response.status === 401){
    //   alert("로그인세션만료");
    //   navigate("/");
    // }
    console.log("차수 수정 앞차수 마감일 ,뒷차수 시작일 get 실패");
    return null;
  }
}
