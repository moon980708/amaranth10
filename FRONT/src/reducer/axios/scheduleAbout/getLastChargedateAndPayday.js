import React from "react";
import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import UseApi from '../../../components/UseApi';
axios.defaults.withCredentials = true;

export default async function getLastChargedateAndPayday({
  chargeDateString,
  recentSchedule,
}) {
  try {
    const api = UseApi();
    const response = await api.get(
      "/schedulechargeend",
      {
        params: {
          chargeDateString: chargeDateString,
          createScheduleNum: recentSchedule - 1,
        },
      }
    );
    console.log("getLastChargedateAndPayday : " + response.data);
    // const newScheduleNum = result.data;
    // if (newScheduleNum === 1) {
    //   //차수가 없어서 1이 넘어온 경우 기존의 차수의 청구가능마감일을 가져오지않음
    //   console.log("Redux에서 1이라는데");
    //   setCreateScheduleNum(newScheduleNum);
    // } else {
    //   setCreateScheduleNum(newScheduleNum);
    //   console.log("Redux에서 가져온 차수+1==" + newScheduleNum);
    // }

    return response.data;
  } catch (error) {
    console.log("함수axios 청구마감일과 지급일 읽어오기 실패");
    return null; // 실패 시 null 또는 에러 처리를 원하는 값을 반환할 수 있습니다.
  }
}
