import React from "react";
import axios from "axios";
import UseApi from '../../../components/UseApi';
axios.defaults.withCredentials = true;

export default async function getScheduleList(
  navigate,
  chargeDateString
) {
  const api=UseApi();
  try {
    const response = await api.get("/schedulelist", {
      params: {
        chargeDateString: chargeDateString,
      },
    });
    console.log("getschedulelist 차수결과: " + response.data);
    // const newScheduleNum = result.data;
    // if (newScheduleNum === 1) {
    //   //차수가 없어서 1이 넘어온 경우 기존의 차수의 청구가능마감일을 가져오지않음
    //   console.log("Redux에서 1이라는데");
    //   setCreateScheduleNum(newScheduleNum);
    // } else {
    //   setCreateScheduleNum(newScheduleNum);
    //   console.log("Redux에서 가져온 차수+1==" + newScheduleNum);
    // }

    return response.data;
  } catch (error) {
    // if (error.response && error.response.status === 401) {
    //   alert("로그인세션만료");
    //   navigate("/");
    // }
    console.log("함수axios 차수등록시 최신 차수 읽어오기 실패");
    return null; // 실패 시 null 또는 에러 처리를 원하는 값을 반환
  }
}
