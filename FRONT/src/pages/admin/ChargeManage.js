import React, { useEffect, useState } from "react";
import {
  Card,
  CardContent,
  Box,
  Typography,
  Grid,
  FormControl,
  TextField,
  InputLabel,
  MenuItem,
  Select,
  Button,
  IconButton,
  ListSubheader,
  Divider,
  Fab,
  Tooltip,
  CircularProgress
} from "@mui/material";
import Swal from "sweetalert2";
import axios from "axios";
import UseApi from '../../components/UseApi';
import { useSelector } from "react-redux";

import SearchIcon from "@mui/icons-material/Search";
import TaskAltOutlinedIcon from "@mui/icons-material/TaskAltOutlined";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";

import moment from "moment";
import ChargeManageTable from "../../components/tables/ChargeManageTable";
import ChargeManageMemberListCard from "../../components/cards/ChargeManageMemberListCard";
import ScheduleDatePicker from "../../components/datepickers/ScheduleDatePicker";

axios.defaults.withCredentials = true;


const stateList = [
  { value: "all", label: "전체" },
  { value: "승인", label: "승인" },
  { value: "승인대기", label: "승인대기" },
  { value: "반려", label: "반려" },
];


const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: "btn btn-success",
    cancelButton: "btn btn-danger",
  },
  width: "45%",
});



const ChargeManage = () => {
  const api = UseApi();
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const [scheduleList, setScheduleList] = useState([]); //마감, 진행중 차수리스트
  const [groupdeptlist, setGroupDeptList] = useState({ group: [], dept: [] }); //취합그룹+부서

  const [tempSearch, setTempSearch] = useState({
    schedule: 0,
    groupDept: 0, //처음엔 '전체'로 초기화, value<50:부서  /  value>=50:취합그룹
    state: "승인대기",
  });

  const [search, setSearch] = useState({
    schedule: 0,
    groupDept: 0,
    state: "승인대기",
    startRefresh: false, //조회해라!
    endRefresh: false, //조회 다했으니까 나머지들 갱신하셈
    searchTime: new Date(), //조회시간
  }); //진짜 조회조건

  const [summaryUserChargeOrigin, setSummaryChargeOrigin] = useState([]); //쿼리결과
  const [summaryUserCharge, setSummaryUserCharge] = useState([]); //프론트에 뿌리는

  const [searchKeyword, setSearchKeyword] = useState("");
  const [selectedUserId, setSelectedUserId] = useState(null); //선택한 사원id

  const [selectedChargeNo, setSelectedChargeNo] = useState([]); //일괄승인을 위해 선택한 청구내역들 번호배열

  const [loadingSearch, setLoadingSearch] = useState(true);   //왼쪽리스트(검색결과)
  const [loadingChargeList, setLoadingChargeList] = useState(true);



  const listGroupDept = () => {
    //메뉴에 띄울 사용중인 취합그룹+부서리스트 불러오는 함수

    return new Promise((resolve, reject) => {
      console.log("로그인id", loginUserId);
      api.get("/admin/main/groupdeptlist", {
        params: {
          managerId: loginUserId
        }
      })
        .then((result) => {
          console.log(result.data);
          let groupData = result.data.group;
          let deptData = result.data.dept;
          let groupTemp = [];
          let deptTemp = [];

          if (groupData !== null) {
            for (let group of groupData) {
              let newGroup = {
                value: group.groupNo,
                label: group.groupName,
              };
              groupTemp.push(newGroup);
            }
          }
          if (deptData !== null) {
            for (let dept of deptData) {
              let newDept = {
                value: dept.deptNo,
                label: dept.deptName,
              };
              deptTemp.push(newDept);
            }
          }

          const groupDeptData = {
            group: groupTemp,
            dept: deptTemp
          };

          resolve(groupDeptData); // 데이터를 반환
      
        })
        .catch((error) => {
          if (error.response) {
            console.log("사용중 취합그룹+부서리스트 불러오기 실패", error);
          }
        });
    })
    
  };


  const listSchedule = () => {
    //메뉴에 띄울 마감, 진행중인 차수리스트 불러오는 함수
    api.get("/admin/charge/schedulelist")
      .then((result) => {
        let scheduleData = result.data;
        setScheduleList(scheduleData);
      })
      .catch((error) => {
        if (error.response) {
          console.log("마감,진행중인 차수리스트 불러오기 실패", error);
        }
      });
  };

  const handleSearchChange = (e) => {
    setTempSearch({
      ...tempSearch,
      [e.target.name]: e.target.value,
    });
  };

  
  
  const listSearch = async () => {
    //진짜로 조회후, 임시로 복사해두기. -> 조건에 맞는 사용자리스트 불러오기 (0건도 포함)
    setLoadingSearch(true);
    setLoadingChargeList(true);

    try {
      let groupDeptData = await listGroupDept();
      setGroupDeptList(groupDeptData);
      console.log("가져온 그룹부서: ",groupDeptData);

      let includeGroup = null;

      if (groupDeptData.group.length !== 0) {
        includeGroup = groupDeptData.group.find(item => item.value === search.groupDept);
      }

      let option = {
        schedule: search.schedule,
        groupDept: search.groupDept,
        state: search.state
      };

      if (search.groupDept !== 0 && !includeGroup && loginUserId !== 'admin') {
        console.log("option 초기화");
        alert("관리 그룹내역에 변동사항이 있어, 그룹 조회조건이 초기화됩니다.");

        option = {
          schedule: search.schedule,
          groupDept: 0,
          state: search.state
        };
      } 

      const variables = {
        managerId: loginUserId,
        scheduleNo: option.schedule,
        groupDeptNo: option.groupDept,   //그룹정보가 변동되었으면, 전체로 검색
        state: option.state,
      };

      api.post("/admin/charge/search", variables)
        .then((result) => {
          console.log("검색결과!!!!!!!!!!!!!!!!!!!",result.data);
          let totalData = result.data.total;
          let userData = result.data.user;

          //그룹 혹은 부서에 아무도 없으면, null값을 받음.
          if (totalData !== null) {
            setSummaryChargeOrigin([totalData].concat(userData)); //쿼리결과 저장.
            setSummaryUserCharge([totalData].concat(userData)); //객체 totalData를 배열로 새로 생성하여 concat 메서드 사용
            console.log([totalData].concat(userData));
            setSearch({
              ...option,
              startRefresh: search.startRefresh,
              endRefresh: search.endRefresh, 
              searchTime: totalData.searchTime
            }); //조회시간 갱신
            handleSearchUsers([totalData].concat(userData));

            // setTimeout(() => {
            //   setLoadingSearch(false);
            // }, 500);
            
            setLoadingSearch(false);

          } else {
            setSummaryChargeOrigin([]); //쿼리결과 저장.
            setSummaryUserCharge([]); //객체 totalData를 배열로 새로 생성하여 concat 메서드 사용
            // alert("조회 결과가 없습니다.");
            setSearch({
              ...option,
              startRefresh: search.startRefresh,
              endRefresh: search.endRefresh,
              searchTime: new Date()
            }); //조회시간 갱신
            handleSearchUsers([]);

            // setTimeout(() => {
            //   setLoadingSearch(false);
            //   setLoadingChargeList(false);
            // }, 500);
            
            setLoadingSearch(false);
            setLoadingChargeList(false);
          }

          setTempSearch(option);
          

        })
        .catch((error) => {
          if (error.response) {
            console.log("조건에 맞는 사용자리스트 불러오기 실패", error);
          }
        });
      
    } catch (error) {
      console.log(error);
    }
    
  };

  const handleSearch = () => {
    //조회버튼 눌렀을때 임시에서 복사해오기
    console.log("조회버튼 눌렀음@@")
    setSelectedUserId(null);
    setSearchKeyword(""); //키워드 초기화
    setSearch({
      ...tempSearch,
      startRefresh: !search.startRefresh,
      endRefresh: search.endRefresh,
      searchTime: search.searchTime, //조회시간
    });
  };

  const handleSearchUsers = (original) => {
    //프론트에서 검색하기.

    let keyword = searchKeyword.trim();
    if (keyword.length === 0) {
      let temp = original.slice();
      setSummaryUserCharge(temp); //다시 original 쿼리결과로 변경
      // setSummaryUserCharge(summaryUserChargeOrigin); //다시 original 쿼리결과로 변경
    } else {
      let result = original.filter(
        (userCharge) =>
          (userCharge.id !== "total" && userCharge.userName.includes(keyword)) ||
          userCharge.id.includes(keyword)
      );
      setSummaryUserCharge(result);
    }
    
    setSearchKeyword(keyword); //검색후, 양쪽 공백제거

  };

  const handleSelectedConfirmBtnClick = () => {
    //일괄 승인 눌렀을때

    swalWithBootstrapButtons
      .fire({
        icon: "question",
        title: "선택한 청구들을 승인하시겠습니까?",
        text: "해당 사항은 변경이 불가합니다.",
        confirmButtonText: "승인",
        cancelButtonText: "취소",
        showCancelButton: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          //선택한 청구배열
          updateStateConfirm(selectedChargeNo);
        }
      });
  };

  const updateStateConfirm = (userChargeNoList) => {
    //청구내역 상태를 '승인'으로 변경
    //console.log(userChargeNoList);

    const variables = {
      managerId: loginUserId,
      state: "승인",
      userChargeNoList: userChargeNoList,
    };

    api.put("/admin/charge/update", variables)
      .then((result) => {
        console.log(result);
        
        swalWithBootstrapButtons.fire("청구 승인처리 완료", result.data.data, "success");

        setSelectedChargeNo([]);
      
      })
      .catch((error) => {
        if (error.response) {
          console.log("청구건 승인처리 실패", error);
        
          swalWithBootstrapButtons.fire('청구 승인처리 실패', error.response.data.error.message, 'error');
        }
      })
      .finally(() => {
        setSearch({ ...search, startRefresh: !search.startRefresh }); //전체 다시 조회
      });
  };

  useEffect(() => {
    listSchedule();
  }, []);

  useEffect(() => {
    listSearch();
    //조회버튼 아니고서도, setSearch({ ...search, startRefresh: !search.startRefresh }); 를 통해, 이전조건으로 조회가능
  }, [search.startRefresh]);

  useEffect(() => {
    //console.log("뭐였죠 선택한 id",selectedUserId)
    if (summaryUserCharge.length === 0) {
      //검색결과 없다! 사용자 선택 초기화
      setSelectedUserId(null);
    } else {
      if (
        selectedUserId === null ||
        !summaryUserCharge.find((user) => user.id === selectedUserId)
      ) {
        setSelectedUserId(summaryUserCharge[0].id);
        //console.log("뭐였죠 변경될 id",summaryUserCharge[0].id)
      }
    }

    setSearch({ ...search, endRefresh: !search.endRefresh }); //조회 후 refresh
  }, [summaryUserCharge, search.searchTime]);

  
  return (
    <Box>
      <Grid container spacing={2} alignItems="center" sx={{ mb: 2, ml: 0.5 }}>
        <Grid item>
          <ScheduleDatePicker
            scheduleList={scheduleList}
            search={search}
            tempSearch={tempSearch}
            setTempSearch={setTempSearch}
          />
        </Grid>
        <Grid item xs={12} lg={2}>
          <FormControl variant="outlined" size="small" fullWidth>
            <InputLabel id="group-label">취합그룹/부서</InputLabel>
            <Select
              labelId="group-label"
              id="group"
              name="groupDept"
              label="취합그룹/부서"
              value={tempSearch.groupDept}
              onChange={handleSearchChange}
              sx={{ width: "100%" }}
              MenuProps={{
                style: { maxHeight: "350px" },
              }}
            >
              <MenuItem key={0} value={0}>{"전체"}</MenuItem>
                <Divider />

                {groupdeptlist.group.length !== 0 && [
                  <ListSubheader key={"group"} disableSticky>취합그룹</ListSubheader>,
                  ...groupdeptlist.group.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))
                ]}

                {groupdeptlist.dept.length !== 0 && [
                  <Divider key={"divider"} />,
                  <ListSubheader key={"dept"} disableSticky>부서</ListSubheader>,
                  ...groupdeptlist.dept.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))
                ]}
            </Select>
          </FormControl>
        </Grid>
        <Grid item>
          <FormControl variant="outlined" size="small">
            <InputLabel id="state-label">진행상태</InputLabel>
            <Select
              labelId="state-label"
              id="state"
              label="진행상태"
              name="state"
              value={tempSearch.state}
              onChange={handleSearchChange}
            >
              {stateList.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item sx={{ alignItems: "center", display: "flex" }}>
          <Button onClick={handleSearch} variant="contained" color="primary">
            조회
          </Button>
          <Tooltip
            sx={{ ml: 1 }}
            title={"조회기준: " + moment(search.searchTime).format("YYYY-MM-DD HH:mm:ss")}
          >
            <HelpOutlineIcon color="disabled" />
          </Tooltip>
        </Grid>
        <Grid item sx={{ marginLeft: "auto", mr: 5 }}>
          <Button
            onClick={() => {
              setSearch({
                ...tempSearch,
                startRefresh: !search.startRefresh,
                endRefresh: search.endRefresh,
                searchTime: search.searchTime
              });
            }}
            variant="outlined"
            color="primary"
          >
            새로고침
          </Button>
        </Grid>
      </Grid>


      <Grid container spacing={0}>
        <Grid item xs={12} lg={3}>
          <Card variant="outlined" sx={{ p: 0, pt: 1.5 }}>
            <CardContent sx={{ pb: "0 !important" }}>
              <TextField
                size="small"
                placeholder="사원명/사원코드"
                value={searchKeyword}
                onChange={(e) => setSearchKeyword(e.target.value)}
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    setSelectedUserId(null);
                    handleSearchUsers(summaryUserChargeOrigin);
                  }
                }}
                variant="standard"
                fullWidth
                sx={{ mt: 2 }}
                InputProps={{
                  endAdornment: (
                    <>
                      {searchKeyword && (
                        <IconButton
                          sx={{ padding: 0, mr: 0.4, color: "#d3d3d3" }}
                          onClick={()=>{setSearchKeyword("")}}
                        >
                          <HighlightOffIcon />
                        </IconButton>
                      )}
                      <IconButton
                        color='primary'
                        onClick={() => { setSelectedUserId(null); handleSearchUsers(summaryUserChargeOrigin); }}>
                        <SearchIcon />
                      </IconButton>
                    </>
                  ),
                }}
              />

              {loadingSearch ?
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(67vh)" }}>
                  <CircularProgress size={50} />
                </Box>
              :
                <ChargeManageMemberListCard
                  summaryUserCharge={summaryUserCharge}
                  selectedUserId={selectedUserId}
                  setSelectedUserId={setSelectedUserId}
                />
              }
            </CardContent>
          </Card>
        </Grid>

        <Grid item xs={12} lg={9}>
          <Card variant="outlined" sx={{ p: 0, pt: 3 }}>
            <CardContent>
              <Box sx={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                <Typography variant="h3">경비청구 리스트</Typography>

                <Fab
                  color="secondary"
                  variant="extended"
                  size="medium"
                  onClick={handleSelectedConfirmBtnClick}
                  disabled={selectedChargeNo.length === 0 ? true : false}
                  sx={{ zIndex: 500 }}
                >
                  <TaskAltOutlinedIcon />
                  <Typography variant="body2" sx={{ ml: 0.5 }}>
                    일괄 승인
                  </Typography>
                </Fab>
              </Box>

              <Box
                sx={{
                  mt: 3,
                  height: "calc(67vh)",
                }}
              >
                <ChargeManageTable
                  search={search}
                  setSearch={setSearch}
                  selectedUserId={selectedUserId}
                  selectedChargeNo={selectedChargeNo}
                  setSelectedChargeNo={setSelectedChargeNo}
                  updateStateConfirm={updateStateConfirm}
                  loadingChargeList={loadingChargeList}
                  setLoadingChargeList={setLoadingChargeList}
                />
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Box>
  );
};

export default ChargeManage;