import React, { useState, useEffect } from "react";
import { Grid, Box, Typography, Button } from "@mui/material";
import CreateCategoryTable from "../../components/tables/CreateCategoryTable";
import CreateItemTable from "../../components/tables/CreateItemTable";
import Swal from "sweetalert2";
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import getOngoingSchedule from "../../reducer/axios/matchingAbout/getOngoingSchedule";
import { reset_Ongoing_Schedule, set_Ongoing_Schedule } from "../../reducer/module/matchingReducer";
import { useSelector, useDispatch } from "react-redux";
import Snackbar from "@mui/material/Snackbar";
import Fade from "@mui/material/Fade";
import MuiAlert from "@mui/material/Alert";
import ErrorIcon from "@mui/icons-material/Error";
import Slide from "@mui/material/Slide";
import UseApi from './../../components/UseApi';
axios.defaults.withCredentials = true; 

const ExCategory = () => {
  const api = UseApi();

  const [loadingCategory, setLoadingCategory] = useState(false);
  const [loadingItem, setLoadingItem] = useState(false);

  const [selectedItems, setSelectedItems] = useState([]);
  const [CategoryList, setCategoryList] = useState([]);
  const [ItemList, setItemList] = useState([]);

  // 용도 생성 버튼 클릭 시 리스트 재랜더링 위한 state
  const [submitCategory, setSubmitCategory] = useState(false);
  // 항목 생성 버튼 클릭 시 리스트 재랜더링 위한 state
  const [submitItem, setSubmitItem] = useState(false);
  // 용도 사용여부 변경 시 리스트 재랜더링 위한 state
  const [categoryUsable, setCategoryUsable] = useState(true);
  // 용도 삭제 시 리스트 재랜더링 위한 state
  const [deleteCategory, setDeleteCategory] = useState(true);
  // 항목 삭제 시 리스트 재랜더링 위한 state
  const [deleteItem, setDeleteItem] = useState(true);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [open, setOpen] = useState(true);
  const handleSnackbarkClose = (e, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  // 용도 생성 스낵바에 쓰는 닫힘 함수
  const [createCategorySnackOpen, setCreateCategorySnackOpen] = useState(false);
  const handlecreateCategorySnackOpen = () => {
    setCreateCategorySnackOpen(true);
  };
  const handlecreateCategorySnackClose = (e, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setCreateCategorySnackOpen(false);
  };

  // 항목 생성 스낵바에 쓰는 닫힘 함수
  const [createItemSnackOpen, setCreateItemSnackOpen] = useState(false);
  const handlecreateItemSnackOpen = () => {
    setCreateItemSnackOpen(true);
  };
  const handlecreateItemSnackClose = (e, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setCreateItemSnackOpen(false);
  };

  // 스낵바
  const onGoingYearMonth = useSelector((state) => state.matchingReducer.onGoingYearMonth);
  const onGoingSchedule = useSelector((state) => state.matchingReducer.onGoingSchedule);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await getOngoingSchedule(navigate, api);
        // console.log("result : " + result);
        if (result === "") {
          // console.log("===현재 진행중인 차수 없음");
          dispatch(reset_Ongoing_Schedule());
        } else {
          // console.log("진행중인 차수 있음");
          dispatch(set_Ongoing_Schedule(result));
        }
      } catch (error) {
        console.log("getOngoingSchedule실패");
      }
    };
    fetchData();
  }, []);

  useEffect(() => {
    setLoadingCategory(true);

    api
      .get("/admin/categorylist")
      .then((result) => {
        setCategoryList(result.data);
        setLoadingCategory(false);
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        if (error.response) {
          console.log("통신 error");
          alert("다시 로딩해주세요.");
        }
      });
  }, [submitCategory, categoryUsable, deleteCategory]);

  useEffect(() => {
    setLoadingItem(true);

    api
      .get("/admin/itemlist")
      .then((result) => {
        setItemList(result.data);
        setLoadingItem(false);
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        if (error.response) {
          console.log("통신 error");
          alert("다시 로딩해주세요.");
        }
      });
  }, [submitItem, deleteItem]);

  // alert 창
  const handleChipClick = (index) => {
    const updatedCategoryList = [...CategoryList];
    const currentUsable = updatedCategoryList[index].usable; //문자열 true, false로 출력되므로 number로 변환

    if (currentUsable === true) {
      //alert 창
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: "btn btn-success",
          cancelButton: "btn btn-danger",
        },
        width: "45%",
      });

      swalWithBootstrapButtons
        .fire({
          title: "'" + updatedCategoryList[index].categoryName + "'" + "를 미사용하시겠습니까?",
          text: "'확인'을 누르면 해당 용도를 사용할 수 없습니다",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "확인",
          cancelButtonText: "취소",
          reverseButtons: false,
        })
        .then((result) => {
          if (result.isConfirmed) {
            // 확인
            // 사용여부 변경
            api
              .put(`/admin/category/update/${updatedCategoryList[index].categoryNo}`, {
                usable: updatedCategoryList[index].usable,
              })
              .then((result) => {
                swalWithBootstrapButtons.fire(
                  "미사용 처리 완료",
                  "'" +
                    updatedCategoryList[index].categoryName +
                    "'" +
                    " 항목은 더이상 사용하실 수 없습니다",
                  "success"
                );
                updatedCategoryList[index].usable = false;
                updatedCategoryList[index].pbg = "primary.red";
                setCategoryList(updatedCategoryList);
                setCategoryUsable(!updatedCategoryList[index].usable);
              })
              .catch((error) => {
                // if (error.response && error.response.status === 401) {
                //   alert("로그인세션만료");
                //   navigate("/");
                // }
                if (error.response) {
                  console.log("통신 error");
                  alert("용도 상태 변경에 실패했습니다.");
                }
              });
          } //여기까지 확인눌렀을떄
          else if (result.dismiss === Swal.DismissReason.cancel) {
            swalWithBootstrapButtons.fire(
              "미사용 처리 취소",
              "기존과 동일하게 " +
                updatedCategoryList[index].categoryName +
                "를 사용할 수 있습니다",
              "error"
            );
          }
        });
    } else if (currentUsable === false) {
      //alert 창
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: "btn btn-success",
          cancelButton: "btn btn-danger",
        },
        width: "40%",
      });

      swalWithBootstrapButtons
        .fire({
          title: "'" + updatedCategoryList[index].categoryName + "'" + "를 사용하시겠습니까?",
          text: "'확인'을 누르면 해당 용도를 사용할 수 있습니다",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "확인",
          cancelButtonText: "취소",
          reverseButtons: false,
        })
        .then((result) => {
          if (result.isConfirmed) {
            // 확인
            // 사용여부 변경
            api
              .put(`/admin/category/update/${updatedCategoryList[index].categoryNo}`, {
                usable: updatedCategoryList[index].usable,
              })
              .then((result) => {
                swalWithBootstrapButtons.fire(
                  "사용 처리 완료!",
                  "이제 '" +
                    updatedCategoryList[index].categoryName +
                    "'" +
                    " 항목을 사용하실 수 있습니다",
                  "success"
                );
                updatedCategoryList[index].usable = true;
                updatedCategoryList[index].pbg = "primary.main";
                setCategoryList(updatedCategoryList);
                setCategoryUsable(!updatedCategoryList[index].usable);
              })
              .catch((error) => {
                // if (error.response && error.response.status === 401) {
                //   alert("로그인세션만료");
                //   navigate("/");
                // }
                if (error.response) {
                  console.log("통신 error");
                  alert("용도 상태 변경에 실패했습니다.");
                }
              });
          } //여기까지 확인눌렀을떄
          else if (result.dismiss === Swal.DismissReason.cancel) {
            swalWithBootstrapButtons.fire(
              "사용 처리 취소",
              "기존과 동일하게 " +
                updatedCategoryList[index].categoryName +
                "를 사용할 수 없습니다",
              "error"
            );
          }
        });
    }
  };

  function SlideTransition(props) {
    return <Slide {...props} direction="down" />;
  }

  return (
    <Box>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={createCategorySnackOpen}
        onClose={handlecreateCategorySnackClose}
        TransitionComponent={Fade}
        autoHideDuration={5000}
        style={{ width: "400px" }}
      >
        <MuiAlert elevation={6} variant="filled" severity="success" style={{ color: "white" }}>
          용도가 추가되었습니다.
        </MuiAlert>
      </Snackbar>

      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={createItemSnackOpen}
        onClose={handlecreateItemSnackClose}
        TransitionComponent={Fade}
        autoHideDuration={5000}
        style={{ width: "400px" }}
      >
        <MuiAlert elevation={6} variant="filled" severity="success" style={{ color: "white" }}>
          항목이 추가되었습니다.
        </MuiAlert>
      </Snackbar>

      {onGoingYearMonth === null ? (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={open}
          onClose={handleSnackbarkClose}
          TransitionComponent={Fade}
          autoHideDuration={5000}
        >
          <MuiAlert elevation={6} variant="filled" severity="success" style={{ color: "white" }}>
            진행중인 차수가 없습니다. 수정 및 삭제가 가능합니다.
          </MuiAlert>
        </Snackbar>
      ) : (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={open}
          onClose={handleSnackbarkClose}
          TransitionComponent={Fade}
          autoHideDuration={5000}
        >
          <MuiAlert
            sx={{ color: "white" }}
            elevation={6}
            variant="filled"
            severity="error"
            icon={<ErrorIcon />}
          >
            [{onGoingYearMonth} {onGoingSchedule}차수 진행중] - 수정 및 삭제가 불가합니다.
          </MuiAlert>
        </Snackbar>
      )}

      <Box sx={{ mr: 5, ml: 3, display: "flex" }}>
        <Typography variant="h3" gutterBottom>
          용도-항목 생성
        </Typography>
      </Box>
      <Grid container spacing={0}>
        {/* 왼쪽 절반 */}
        <Grid item xs={12} md={6}>
          <CreateCategoryTable
            CategoryList={CategoryList}
            setCategoryList={setCategoryList}
            handleChipClick={handleChipClick}
            submitCategory={submitCategory}
            setSubmitCategory={setSubmitCategory}
            deleteCategory={deleteCategory}
            setDeleteCategory={setDeleteCategory}
            handlecreateCategorySnackOpen={handlecreateCategorySnackOpen}
            api={api}
            loadingCategory={loadingCategory}
            setLoadingCategory={setLoadingCategory}
          />
        </Grid>
        {/* 오른쪽 절반 */}
        <Grid item xs={12} md={6}>
          <CreateItemTable
            ItemList={ItemList}
            setItemList={setItemList}
            submitItem={submitItem}
            setSubmitItem={setSubmitItem}
            deleteItem={deleteItem}
            setDeleteItem={setDeleteItem}
            handlecreateItemSnackOpen={handlecreateItemSnackOpen}
            api={api}
            loadingItem={loadingItem}
            setLoadingItem={setLoadingItem}
          />
        </Grid>
      </Grid>
    </Box>
  );
};

export default ExCategory;