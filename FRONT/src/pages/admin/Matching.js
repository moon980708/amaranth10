import React, { useState, useEffect } from "react";

import { Grid, Box, Typography } from "@mui/material";
import CategoryTable from "../../components/tables/CategoryTable";
import ItemTable from "../../components/tables/ItemTable";
import MatchingTable from "../../components/tables/MatchingTable";
import Snackbar from "@mui/material/Snackbar";
import Fade from "@mui/material/Fade";
import MuiAlert from "@mui/material/Alert"; // Alert 컴포넌트를 임포트
import { useNavigate } from "react-router-dom";
import getOngoingSchedule from "../../reducer/axios/matchingAbout/getOngoingSchedule";
import { useDispatch } from "react-redux";
import {
  reset_Ongoing_Schedule,
  set_Ongoing_Schedule,
} from "../../reducer/module/matchingReducer";
import ErrorIcon from '@mui/icons-material/Error';
import Slide from '@mui/material/Slide';

import { useSelector } from "react-redux";
import UseApi from '../../components/UseApi';
const Matching = () => {
  const CategoryList = [
    {
      id: "1",
      name: "중식대",
      post: "숫자",
      priority: "사용중",
      pbg: "primary.main",
      budget: "3.9",
    },
    {
      id: "2",
      name: "석식대",
      post: "숫자",
      priority: "사용중",
      pbg: "primary.main",
      budget: "3.9",
    },
    {
      id: "3",
      name: "출장비",
      post: "숫자",
      priority: "사용중",
      pbg: "primary.main",
      budget: "3.9",
    },
    {
      id: "4",
      name: "주유비",
      post: "숫자",
      priority: "미사용",
      pbg: "primary.red",
      budget: "3.9",
    },
    {
      id: "5",
      name: "품위유지비",
      post: "숫자",
      priority: "사용중",
      pbg: "primary.main",
      budget: "3.9",
    },
    {
      id: "6",
      name: "접대비",
      post: "숫자",
      priority: "미사용",
      pbg: "primary.red",
      budget: "3.9",
    },
    {
      id: "7",
      name: "도서인쇄비",
      post: "숫자",
      priority: "사용중",
      pbg: "primary.main",
      budget: "3.9",
    },
    {
      id: "8",
      name: "소모품비",
      post: "숫자",
      priority: "사용중",
      pbg: "primary.main",
      budget: "3.9",
    },
  ];
  const ItemList = [
    {
      id: "100",
      name: "인원수",
      type: "숫자",
    },
    {
      id: "101",
      name: "방문처",
      type: "문자",
    },
    {
      id: "102",
      name: "목적",
      type: "문자",
    },
    {
      id: "103",
      name: "교통수단",
      type: "문자",
    },
    {
      id: "104",
      name: "비고",
      type: "문자",
    },
    {
      id: "105",
      name: "운행거리",
      type: "숫자",
    },
    {
      id: "106",
      name: "출발일",
      type: "날짜",
    },
    {
      id: "107",
      name: "출발시간",
      type: "날짜",
    },
  ];
  const [selectCategory, setSelectCategory] = useState("");
  const [selectItem, setSelectItem] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const api = UseApi();
  const [open, setOpen] = useState(true);
  const handleSnackbarkClose = (e, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };
  const onGoingYearMonth = useSelector(
    (state) => state.matchingReducer.onGoingYearMonth
  );
  const onGoingSchedule = useSelector(
    (state) => state.matchingReducer.onGoingSchedule
  );
  console.log("onGoingSchedule : " + onGoingSchedule);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await getOngoingSchedule(navigate, api);
        console.log("result : " + result);
        if (result === "") {
          console.log("===현재 진행중인 차수 없음");
          dispatch(reset_Ongoing_Schedule());
        } else {
          console.log("진행중인 차수 있음");
          dispatch(set_Ongoing_Schedule(result));
        }
      } catch (error) {
        console.log("getOngoingSchedule실패");
      }
    };
    fetchData();
  }, []);

  // function SlideTransition(props) {
  //   return <Slide {...props} direction="down" />;
  // }

  return (
    <Box>
      {onGoingYearMonth === null ? (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={open}
          onClose={handleSnackbarkClose}
          TransitionComponent={Fade}
          autoHideDuration={5000}
        >
          <MuiAlert elevation={6} variant="filled" severity="success" style={{color:"white"}}>
            진행중인 차수가 없습니다. 매칭이 가능합니다.
          </MuiAlert>
        </Snackbar>
      ) : (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={open}
          onClose={handleSnackbarkClose}
          TransitionComponent={Fade}
          autoHideDuration={5000}
        >
          <MuiAlert sx={{color:'white'}} elevation={6} variant="filled" severity="error" icon={<ErrorIcon />}>
            [{onGoingYearMonth} {onGoingSchedule}차수 진행중] - 수정 및 삭제가 불가합니다.
          </MuiAlert>
        </Snackbar>
      )}

      <Box sx={{ mr: 5, ml: 3, display: "flex" }}>
        <Typography variant="h3" gutterBottom>
          용도-항목 매칭
        </Typography>
      </Box>
      <Grid container spacing={0}>
        {/* 왼쪽 절반 */}
        <Grid item xs={12} md={6}>
          <CategoryTable
            CategoryList={CategoryList}
            setSelectCategory={setSelectCategory}
          />
        </Grid>
        {/* 오른쪽 절반 */}
        <Grid item xs={12} md={6}>
          <ItemTable
            ItemList={ItemList}
            selectCategory={selectCategory}
            selectedItems={selectedItems}
            setSelectedItems={setSelectedItems}
          />
        </Grid>
      </Grid>
      <MatchingTable
        selectCategory={selectCategory}
        ItemList={ItemList}
        selectedItems={selectedItems}
      />
    </Box>
  );
};

export default Matching;
