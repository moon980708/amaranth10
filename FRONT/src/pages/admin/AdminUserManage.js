import React, { useState, useEffect } from "react";
import { Grid, Box, Card, CardContent, Typography, Button, CircularProgress } from "@mui/material";
import UserListTable from "../../components/tables/UserListTable";
import UserListFilter from "../../components/tables/UserListFilter";
import axios from "axios";
import Snackbar from "@mui/material/Snackbar";
import Fade from "@mui/material/Fade";
import MuiAlert from "@mui/material/Alert";
import { useNavigate } from "react-router-dom";
import Slide from "@mui/material/Slide";
import UseApi from './../../components/UseApi';
axios.defaults.withCredentials = true;

const ExSlider = () => {
  const api = UseApi();
  const navigate = useNavigate();

  const [loadingDept, setLoadingDept] = useState(true);
  const [loadingRank, setLoadingRank] = useState(true);
  const [loadingUserList, setLoadingUserList] = useState(false);

  const [deptList, setDeptList] = useState([]); // 부서 목록 state
  const [rankList, setRankList] = useState([]); // 직급 목록 state

  const [deptNo, setDeptNo] = useState("");   // 검색 시 부서
  const [rankNo, setRankNo] = useState("");   // 검색 시 직급
  const [resign, setResign] = useState("");   // 검색 시 퇴사여부
  const [keyword, setKeyword] = useState("");   //검색 시 사원명

  const [checkedId, setCheckedId] = useState([]); //체크박스에 체크된 사용자

  // 사원 목록 state
  const [userList, setUserList] = useState([]);
  // 사원 추가 시 리스트 재랜더링 위한 state
  const [submitUser, setSubmitUser] = useState(false);
  // 사원 정보 수정 시 리스트 재랜더링 위한 state
  const [editUser, setEditUser] = useState(true);
  // 사원 삭제 시 리스트 재랜더링 위한 state
  const [deleteUser, setDeleteUser] = useState(false);
  // 사원 검색 시 리스트 재랜더링 위한 state
  const [searchUser, setSearchUser] = useState(false);
  // 페이징 버튼 클릭 시 리스트 재랜더링 위한 state
  const [pagingUser, setPagingUser] = useState(false);
  // 검색 시 -> 페이징 버튼 클릭 시 리스트 재랜더링 위한 state
  const [pagingSearchUser, setPagingSearchUser] = useState(false);

  // 페이징 처리
  const [page, setPage] = useState(1); // 현재 페이지
  const [totalCount, setTotalCount] = useState(null);

  // 사원 추가 시 띄울 스낵바
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const handleClickSnackbar = () => {
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackbar(false);
  };

  // 사원 수정 시 띄울 스낵바
  const [openEditSnackbar, setOpenEditSnackbar] = useState(false);

  const handleEditClickSnackbar = () => {
    setOpenEditSnackbar(true);
  };

  const handleEditCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenEditSnackbar(false);
  };

  // 사원 검색 시 띄울 스낵바
  const [openSearchSnackbar, setOpenSearchSnackbar] = useState(false);

  const handleSearchClickSnackbar = () => {
    setOpenSearchSnackbar(true);
  };

  const handleSearchCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSearchSnackbar(false);
  };

  // csv 업로드 시 형식 다를 때 띄울 스낵바
  const [openNotCsvSnackbar, setOpenNotCsvSnackbar] = useState(false);

  const handleNotCsvOpenSnackbar = () => {
    setOpenNotCsvSnackbar(true);
  };

  const handleNotCsvCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotCsvSnackbar(false);
  };

  useEffect(() => {
    // 부서 목록 받아오는 axios 통신
    setLoadingDept(true);

    api
      .get("/admin/user/dept")
      .then((result) => {
        setDeptList(result.data);
        setLoadingDept(false);
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        if (error.response) {
          console.log("통신 error");
          alert("다시 로딩해주세요.");
        }
      });
  }, []);

  useEffect(() => {
    // 직급 목록 받아오는 axios 통신
    setLoadingRank(true);

    api
      .get("/admin/user/rank")
      .then((result) => {
        setRankList(result.data);
        setLoadingRank(false);
      })
      .catch((error) => {
        // if (error.response && error.response.status === 401) {
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        if (error.response) {
          console.log("통신 error");
          alert("다시 로딩해주세요.");
        }
      });
  }, []);

  // useEffect(() => {
  //   // 사원 목록 받아오는 axios 통신 (프론트 페이징)
  //   axios
  //     .get("/admin/user")
  //     .then((result) => {
  //       console.log(result.data);
  //       setUserList(result.data);
  //     })
  //     .catch((error) => {
  //       if (error.response && error.response.status === 401) {
  //         alert("로그인세션만료");
  //         navigate("/");
  //       }
  //       console.log("통신 error");
  //       alert("다시 로딩해주세요.");
  //     });
  // }, [submitUser, editUser, deleteUser]);

  // useEffect(() => {
  //   axios
  //     .get("/admin/user/gettotalcount")
  //     .then((result) => {
  //       console.log("사원 총 개수 : " + result.data);

  return (
    <Box>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
        TransitionComponent={Fade}
        autoHideDuration={5000}
        style={{ width: "400px" }}
      >
        <MuiAlert elevation={6} variant="filled" severity="success" style={{ color: "white" }}>
          사원이 정상적으로 추가되었습니다.
        </MuiAlert>
      </Snackbar>

      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={openEditSnackbar}
        onClose={handleEditCloseSnackbar}
        TransitionComponent={Fade}
        autoHideDuration={5000}
        style={{ width: "400px" }}
      >
        <MuiAlert elevation={6} variant="filled" severity="info" style={{ color: "white" }}>
          해당 사원의 정보가 수정되었습니다.
        </MuiAlert>
      </Snackbar>

      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={openSearchSnackbar}
        onClose={handleSearchCloseSnackbar}
        TransitionComponent={Fade}
        autoHideDuration={5000}
        style={{ width: "400px" }}
      >
        <MuiAlert elevation={6} variant="filled" severity="success" style={{ color: "white" }}>
          사원 정보 검색 완료!
        </MuiAlert>
      </Snackbar>

      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={openNotCsvSnackbar}
        onClose={handleNotCsvCloseSnackbar}
        TransitionComponent={Fade}
        autoHideDuration={5000}
        style={{ width: "400px" }}
      >
        <MuiAlert elevation={6} variant="filled" severity="error" style={{ color: "white" }}>
          [파일 형식 오류] 올바른 CSV 파일을 업로드해주세요
        </MuiAlert>
      </Snackbar>

      <Box sx={{ mr: 5, ml: 6.65, mb: 4 }}>
        <Typography variant="h3">사원 관리</Typography>
      </Box>
      <UserListFilter
        deptList={deptList}
        setDeptList={setDeptList}
        rankList={rankList}
        setRankList={setRankList}
        setUserList={setUserList}
        handleSearchClickSnackbar={handleSearchClickSnackbar}
        pagingSearchUser={pagingSearchUser}
        setPagingSearchUser={setPagingSearchUser}
        page={page}
        setTotalCount={setTotalCount}
        submitUser={submitUser}
        setSubmitUser={setSubmitUser}
        editUser={editUser}
        deleteUser={deleteUser}
        pagingUser={pagingUser}
        setPage={setPage}
        handleClickSnackbar={handleClickSnackbar}
        handleNotCsvOpenSnackbar={handleNotCsvOpenSnackbar}
        api={api}
        loadingDept={loadingDept}
        loadingRank={loadingRank}
        loadingUserList={loadingUserList}
        setLoadingUserList={setLoadingUserList}
        searchUser={searchUser}
        setSearchUser={setSearchUser}
        deptNo={deptNo}
        setDeptNo={setDeptNo}
        rankNo={rankNo}
        setRankNo={setRankNo}
        resign={resign}
        setResign={setResign}
        keyword={keyword}
        setKeyword={setKeyword}
        setCheckedId={setCheckedId}
      />
      <Card variant="outlined" sx={{ mt: 3, boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)" }}>
        <CardContent>
          <UserListTable
            deptList={deptList}
            setDeptList={setDeptList}
            rankList={rankList}
            setRankList={setRankList}
            submitUser={submitUser}
            setSubmitUser={setSubmitUser}
            editUser={editUser}
            setEditUser={setEditUser}
            deleteUser={deleteUser}
            setDeleteUser={setDeleteUser}
            userList={userList}
            setUserList={setUserList}
            handleClickSnackbar={handleClickSnackbar}
            handleEditClickSnackbar={handleEditClickSnackbar}
            pagingUser={pagingUser}
            setPagingUser={setPagingUser}
            page={page}
            setPage={setPage}
            totalCount={totalCount}
            api={api}
            loadingDept={loadingDept}
            loadingRank={loadingRank}
            loadingUserList={loadingUserList}
            setLoadingUserList={setLoadingUserList}
            setDeptNo={setDeptNo}
            setRankNo={setRankNo}
            setResign={setResign}
            setKeyword={setKeyword}
            pagingSearchUser={pagingSearchUser}
            setPagingSearchUser={setPagingSearchUser}
            checkedId={checkedId}
            setCheckedId={setCheckedId}
          />
        </CardContent>
      </Card>
    </Box>
  );
};

export default ExSlider;
