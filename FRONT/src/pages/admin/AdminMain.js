import React, { useState } from "react";
import {
  Grid,
  Box,
  Card,
  CardContent,
} from "@mui/material";

import AdminChargeMonthCard from '../../components/cards/AdminChargeMonthCard';
import ChargeStatusAnalysis from './ChargeStatusAnalysis';
import AdminCalendar from '../../components/datepickers/AdminCalendar';




const AdminMain = () => {

  return (
    <Box>
      <Grid container spacing={2}>
      
        <Grid item xs={12} lg={7}>
          <AdminChargeMonthCard />
        </Grid>

        <Grid item xs={12} lg={5}>
          <Card variant='outlined' sx={{boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)"}}>
            <CardContent>
              <AdminCalendar />
            </CardContent>
          </Card> 
        </Grid>

        <Grid item xs={12} lg={12} sx={{ pb:0 }}>
          <Card variant='outlined' sx={{ boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)", pt: 5, mt: 0 }}>
            <CardContent>
              <ChargeStatusAnalysis />
            </CardContent>
          </Card> 
        </Grid>
        
      </Grid>
    </Box>
  );
};

export default AdminMain;

