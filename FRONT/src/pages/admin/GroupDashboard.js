import React, { useState, useEffect } from "react";
import {
  Grid,
  Box,
  Button,
  Typography,
  Card,
  CardContent,
  MenuItem,
  TextField,
  Select,
  InputLabel,
  FormControl,
  Fab,
  TablePagination,
  Tooltip,
  IconButton,
  CircularProgress
} from "@mui/material";
import Swal from "sweetalert2";
import axios from 'axios';
import UseApi from '../../components/UseApi';

import AddIcon from "@mui/icons-material/Add";
import GroupRemoveIcon from '@mui/icons-material/GroupRemove';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";

import GroupManagerTable from '../../components/tables/GroupManagerTable';
import GroupMemberTable from "../../components/tables/GroupMemberTable";
import GroupListCard from '../../components/cards/GroupListCard';
import GroupAddModal from "../../components/modal/GroupAddModal";
import GroupMemberModal from "../../components/modal/GroupMemberModal";
import GroupManagerModal from '../../components/modal/GroupManagerModal';
axios.defaults.withCredentials = true;




const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: "btn btn-success",
    cancelButton: "btn btn-danger",
  },
  width: "45%",
});




const GroupDashboard = () => {
  const api = UseApi();
  const [groupAddModal, setGroupAddModal] = useState({ state: 'add', groupName: "" });
  const [groupMemberModal, setGroupMemberModal] = useState({insert:[], delete:[]});    //그룹사원모달에서 변경사항
  const [groupManagerModal, setGroupManagerModal] = useState({insert:[], delete:[]});    //그룹담당자모달에서 변경사항


  const [groups, setGroups] = useState([]);    //취합그룹 조회결과
  const [selectedGroupNo, setSelectedGroupNo] = useState(null);   //GroupListCard에서 배경색 선택한 그룹정보.

  const [managers, setManagers] = useState([]);
  const [selectedManagerIds, setSelectedManagerIds] = useState([]);        //GroupManagerTable에서 선택한 사용자ids 배열

  const [members, setMembers] = useState([]);
  const [selectedIds, setSelectedIds] = useState([]);        //GroupMemberTable에서 선택한 사용자ids 배열


  const [tempKeyword, setTempKeyword] = useState("");     //그룹명, 그룹코드 키워드
  const [tempUsable, setTempUsable] = useState("all");    //사용여부
  const [search, setSearch] = useState({ refresh: true, keyword: "", usable: "all" });        //진짜 조회로 넘길것.

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const [groupListRefresh, setGroupListRefresh] = useState(false);     //그룹리스트
  const [memberListRefresh, setMemberListRefresh] = useState(false);    //사원리스트

  const usableList = [
    { value: 'all', label: "전체" },
    { value: 'true', label: "사용" },
    { value: 'false', label: "미사용" },
  ];


  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const [isManagerModalOpen, setIsManagerModalOpen] = useState(false);
  const [isMemberModalOpen, setIsMemberModalOpen] = useState(false);


  const [loadingGroup, setLoadingGroup] = useState(true);
  const [loadingManager, setLoadingManager] = useState(true);
  const [loadingMember, setLoadingMember] = useState(true);


  const handleSelectManagerIds = (e, id) => {
    const checked = e.target.checked;

    if (checked) {
      setSelectedManagerIds([...selectedManagerIds, id]);
    } else {
      setSelectedManagerIds(selectedManagerIds.filter((selectedId) => selectedId !== id));
    }
  };

  const handleAllSelectManagerIds = (event) => {
    if (event.target.checked) {
      const newSelectedIds = managers.reduce((result, manager) => {
        if (manager.id !== "admin") {
          result.push(manager.id);
        }
        return result;
      }, []);
      setSelectedManagerIds(newSelectedIds);
      //console.log(newSelectedIds);
    } else {
      setSelectedManagerIds([]);
    }
  };


  const handleSelectIds = (e, id) => {
    const checked = e.target.checked;

    if (checked) {
      setSelectedIds([...selectedIds, id]);
    } else {
      setSelectedIds(selectedIds.filter((selectedId) => selectedId !== id));
    }
  };

  const handleAllSelectIds = (event) => {
    if (event.target.checked) {
      const newSelectedIds = members.map((member) => member.id);
      setSelectedIds(newSelectedIds);
    } else {
      setSelectedIds([]);
    }
  };


  const handleChangePage = (event, newPage) => {
    //페이징
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    //페이징
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);     //mui에서 0 -> 1페이지
  };

  const handleSearch = () => {
    //조회버튼 눌렀을때 임시에서 복사해오기 : 선택 그룹을 null로 무조건 변경
    setSearch({ refresh:!search.refresh, keyword: tempKeyword, usable: tempUsable });
    setSelectedGroupNo(null);     
    setGroups([]);
    setManagers([]);
    setMembers([]);
    setPage(0);
    setRowsPerPage(10);
  }



  const listGroup = () => {
    //조건에 맞는 취합그룹 가져오기
    //console.log("조회버튼클릭!키워드:", searchKeyword, " 사용여부:", searchUsable);

    setLoadingGroup(true);
    setLoadingManager(true);
    setLoadingMember(true);


    const variables = {
      searchKeyword: search.keyword,
      searchUsable: search.usable
    }

    api.post("/admin/group/list", variables)
      .then((result) => {
        //console.log(result.data);
        let groupData = result.data;
        setGroups(groupData);
        //console.log(groupData);
        

        
        if (groupData.length === 0) {
          //취합그룹 결과 없다! 그룹 선택 초기화
          setSelectedGroupNo(null);
          // setTimeout(() => {
          //   setLoadingGroup(false);
          //   setLoadingManager(false);
          //   setLoadingMember(false);
          // }, 500);
          
          setLoadingGroup(false);
            setLoadingManager(false);
            setLoadingMember(false);

        } else {
          if (selectedGroupNo === null || !groupData.find((group) => group.groupNo === selectedGroupNo)) {
            setSelectedGroupNo(groupData[0].groupNo);    //조회하면, 첫번째로 나오는 결과로 자동선택
          }

          // setTimeout(() => {
          //   setLoadingGroup(false);
          // }, 500);
          
          setLoadingGroup(false);
        }

        setTempKeyword(search.keyword);    //검색조건 동기화
        setTempUsable(search.usable);
        setGroupListRefresh(!groupListRefresh);  //그룹담당자리스트 새로 가져오기위해.


      }).catch((error) => {
        if (error.response) {
          console.log("취합그룹 리스트 불러오기 실패", error);
          alert("취합그룹 리스트 불러오기 실패");
          setGroups([]);

          // setTimeout(() => {
          //   setLoadingGroup(false);
          //   setLoadingManager(false);
          //   setLoadingMember(false);
          // }, 500);
          
          setLoadingGroup(false);
            setLoadingManager(false);
            setLoadingMember(false);
        }
      });
  }

  const insertGroup = () => {
    setIsMemberModalOpen(false);
    //그룹 생성 axios 호출
    //console.log("그룹이름 생성:", groupAddModal.groupName, "/ 넣을 그룹사원리스트 :", groupMemberModal.insert);

    
    const variables = {
      groupName: groupAddModal.groupName,
      insertList: groupMemberModal.insert
    }

    api.post("/admin/group/add", variables)
      .then((result) => {
        console.log("생성된 그룹번호: ", result.data);
        swalWithBootstrapButtons.fire('그룹 생성 완료',
          "'" + groupAddModal.groupName + "' 그룹이 생성되었습니다.", 'success');

        setSearch({ refresh:!search.refresh, keyword: "", usable: "all" });     //검색 초기화
        setSelectedGroupNo(result.data);
        
      }).catch((error) => {
        
        if(error.response){
          console.log("취합그룹 생성 실패", error);
          swalWithBootstrapButtons.fire('취합그룹 생성 실패', error.response.data.error.message, 'error');
          
        }
      });
    
      setGroupMemberModal({ insert: [], delete: [] });     //초기화
  }


  const listGroupManager = () => {
    //선택한 그룹의 담당자 불러오기
    //console.log(selectedGroupNo);
    setLoadingManager(true);

    const variables = {
      groupNo: selectedGroupNo,
    };

    api.post("/admin/group/manager/list", variables)
      .then((result) => {
        //console.log(result.data);
        setManagers(result.data);
      }).catch((error) => {
        
        if(error.response){
          console.log("선택한 그룹의 담당자리스트 불러오기 실패", error);
          alert("그룹 담당자리스트 불러오기 실패");
          setManagers([]);
          
        }
      }).finally(() => {
        // setTimeout(() => {
        //   setLoadingManager(false);
        // }, 500);
        setLoadingManager(false);
      });
  }


  const editGroupManager = () => {
    setIsManagerModalOpen(false);
    //console.log("담당자를 수정할 그룹번호:", selectedGroupNo, "/ 새로 넣을 그룹담당자리스트 :", groupManagerModal.insert, "/ 삭제할 그룹담당자리스트 :", groupManagerModal.delete);
    //그룹 담당자 수정 axios 호출
    const variables = {
      groupNo: selectedGroupNo,
      insertList: groupManagerModal.insert,
      deleteList: groupManagerModal.delete
    }

    api.put("/admin/group/manager/update", variables)
      .then((result) => {
        swalWithBootstrapButtons.fire('그룹담당자 수정 완료', '해당 그룹의 담당자리스트가 수정되었습니다.', 'success');
        setSearch({ ...search, refresh:!search.refresh });      //그대로 이지만, 새로 리스트 불러오기
        
      }).catch((error) => {
        
        if(error.response){
          console.log("그룹담당자 수정 실패", error);
          swalWithBootstrapButtons.fire('그룹담당자 수정 실패', error.response.data.error.message, 'error');
          
        }
      });

    
    setGroupManagerModal({insert:[], delete:[]});     //초기화
    setGroupMemberModal({insert:[], delete:[]});     //초기화
  }


  const deleteGroupManager = (deleteIds) => {
    //console.log("Selected Member IDs: ", deleteIds, "그룹담당자번호: ",selectedGroupNo);

    swalWithBootstrapButtons.fire({
      icon: 'question',
      title: "해당 그룹의 담당자리스트에서 삭제하시겠습니까?",
      text: "",
      denyButtonText:"삭제",
      cancelButtonText: "취소",
      showConfirmButton: false,
      showDenyButton: true,
      showCancelButton: true,
      
    }).then((result) => {
      if (result.isDenied) {
        //그룹담당자 삭제 axios호출

        const variables = {
          groupNo: selectedGroupNo,
          deleteList: deleteIds
        }
    
        api.delete("/admin/group/manager/delete", {data: variables })
          .then((result) => {
            swalWithBootstrapButtons.fire('그룹담당자 삭제 완료', '해당 그룹의 담당자리스트에서 삭제되었습니다.', 'success');

            setSearch({ ...search, refresh:!search.refresh });      //그대로 이지만, 새로 리스트 불러오기

          }).catch((error) => {
            if(error.response){
              console.log("그룹담당자 삭제 실패", error);
              swalWithBootstrapButtons.fire('그룹담당자 삭제 실패', error.response.data.error.message, 'error');
              
            }
          });
      }
    })

  };

  const listGroupMember = () => {
    //선택한 그룹의 사원 불러오기
    setLoadingMember(true);
    //console.log(selectedGroupNo);

    const variables = {
      groupNo: selectedGroupNo,
      page: page,
      rowsPerPage: rowsPerPage
    };

    api.post("/admin/group/member/list", variables)
      .then((result) => {
        setMembers(result.data);
        
      }).catch((error) => {
        if(error.response){
          console.log("선택한 그룹의 사원리스트 불러오기 실패", error);
          alert("그룹의 사원리스트 불러오기 실패");
          setMembers([]);
          
        }
      }).finally(() => {
        setSelectedIds([]);    //선택한 그룹사원 초기화

        // setTimeout(() => { 
        //   setLoadingMember(false);
        // }, 500);
        
        setLoadingMember(false);
      });
  }

  const editGroupMember = () => {
    setIsMemberModalOpen(false);
    //console.log("사원를 수정할 그룹번호:", selectedGroupNo, "/ 새로 넣을 그룹사원리스트 :", groupMemberModal.insert, "/ 삭제할 그룹사원리스트 :", groupMemberModal.delete);
    //그룹 사원 수정 axios 호출
    const variables = {
      groupNo: selectedGroupNo,
      insertList: groupMemberModal.insert,
      deleteList: groupMemberModal.delete
    }

    api.put("/admin/group/member/update", variables)
      .then((result) => {
        swalWithBootstrapButtons.fire('그룹사원 수정 완료', '해당 그룹의 사원리스트가 수정되었습니다.', 'success');
        setSearch({ ...search, refresh:!search.refresh });      //그대로 이지만, 새로 리스트 불러오기
        
      }).catch((error) => {
        if(error.response){
          console.log("그룹사원 수정 실패", error);
          swalWithBootstrapButtons.fire('그룹사원 수정 실패', error.response.data.error.message, 'error');
        
        }
      });

    setGroupMemberModal({insert:[], delete:[]});     //초기화
  }


  const deleteGroupMember = (deleteIds) => {
    // console.log("Selected Member IDs: ", deleteIds, "그룹번호: ",selectedGroupNo);

    swalWithBootstrapButtons.fire({
      icon: 'question',
      title: "해당 그룹의 사원리스트에서 삭제하시겠습니까?",
      text: "",
      denyButtonText:"삭제",
      cancelButtonText: "취소",
      showConfirmButton: false,
      showDenyButton: true,
      showCancelButton: true,
      
    }).then((result) => {
      if (result.isDenied) {
        //그룹사원 삭제 axios호출

        const variables = {
          groupNo: selectedGroupNo,
          deleteList: deleteIds
        }

        api.delete("/admin/group/member/delete", {data: variables })
          .then((result) => {
            swalWithBootstrapButtons.fire('그룹사원 삭제 완료', '해당 그룹의 사원리스트에서 삭제되었습니다.', 'success');

            setSearch({ ...search, refresh:!search.refresh });      //그대로 이지만, 새로 리스트 불러오기

          }).catch((error) => {
            if(error.response){
              console.log("그룹사원 삭제 실패", error);
              swalWithBootstrapButtons.fire('그룹사원 삭제 실패', error.response.data.error.message, 'error');
              
            }
          });
      }
    })

  };



  useEffect(() => { 
    //그룹삭제, 그룹수정, 그룹추가, 그룹담당자수정,그룹사원수정
    listGroup();
  }, [search.refresh]);


  useEffect(() => {
    setSelectedManagerIds([]);
    setSelectedIds([]);    //선택한 그룹사원 초기화
    setPage(0);
    setRowsPerPage(10);
    setMemberListRefresh(!memberListRefresh);
    if (selectedGroupNo !== null) {
      //선택한 그룹에 대해 그룹 담당자리스트 가져오기
      listGroupManager();
    } else {
      setManagers([]);
    }
  }, [selectedGroupNo, groupListRefresh]);

  useEffect(() => {
    if (selectedGroupNo !== null) {
      //선택한 그룹에 대해 그룹 사원리스트 가져오기 (페이징)
      listGroupMember(); 
    } else {
      setMembers([]);
    }
   }, [memberListRefresh, page, rowsPerPage]);

 


  return (
    <Box>
      <Grid container spacing={2} alignItems="center" sx={{ mb: 2, ml: 0.5 }}>
        <Grid item lg={2}>
          <TextField
            id="group-info"
            label="그룹명/그룹코드"
            variant="outlined"
            size="small"
            value={tempKeyword}
            fullWidth
            onChange={(e) => setTempKeyword(e.target.value)}
            InputProps={{
              endAdornment: (
                tempKeyword && (
                  <IconButton
                    sx={{ padding: 0, mr: 0.4, color: "#d3d3d3" }}
                    onClick={()=>{setTempKeyword("")}}
                  >
                    <HighlightOffIcon />
                  </IconButton>
                )
              ),
            }}
          />
        </Grid>
        <Grid item>
          <FormControl variant="outlined" size="small">
            <InputLabel id="usable-label">사용여부</InputLabel>
            <Select
              labelId="usable-label"
              id="usable"
              value={tempUsable}
              onChange={(e) => setTempUsable(e.target.value)}
              label="사용여부"
            >
              {usableList.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item>
          <Button variant="contained" color="primary" onClick={handleSearch}>
            조회
          </Button>
        </Grid>
      </Grid>

      <Grid>
        <Card
            variant="outlined"
            sx={{
              p: 0,
              pt: 3,
            }}
          >
            <CardContent
              sx={{
                pb: "0 !important",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  mb: 3,
                  alignItems: "center", // 세로 중앙 정렬
                  justifyContent: "center", // 가로 중앙 정렬
                }}
              >
                <Box>
                  <Typography
                    variant="h3" fontWeight={600}
                    sx={{
                      marginBottom: "0",
                    }}
                    gutterBottom
                  >
                    취합 그룹
                  </Typography>
                </Box>
                <Box
                  sx={{
                    marginLeft: "auto",
                  }}
                >
                  <Fab color="primary" variant="extended" size="medium"
                    disabled={loadingGroup}
                    onClick={() => {
                      setGroupAddModal({ state: 'add', groupName: "" });
                      setIsAddModalOpen(true);
                    }}
                    sx={{ zIndex: 500 }}
                  >
                    <AddIcon />
                    <Typography sx={{ml:0.5}}>추가</Typography>
                  </Fab>
                </Box>
            </Box>
            
            {loadingGroup ?
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(15vh)" }}>
                  <CircularProgress size={50} />
                </Box>
              :
                <GroupListCard
                  groups={groups}
                  selectedGroupNo={selectedGroupNo}
                  setSelectedGroupNo={setSelectedGroupNo}
                  refresh={() => {
                    setSearch({ ...search, refresh:!search.refresh });      //그대로 이지만, 새로 리스트 불러오기
                  }}
                />
            }

            </CardContent>
          </Card>
      </Grid>

      <Grid container spacing={0}>
        <Grid item xs={12} lg={5}>
          <Card
            variant="outlined"
            sx={{
              p: 0,
              pt: 3
            }}
          >
            <CardContent
              sx={{
                pb: "0 !important",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center", // 가로 중앙 정렬
                  mb: 3
                }}
              >
                <Box sx={{ alignItems: "center", display: "flex" }}>
                  <Typography
                    variant="h3" fontWeight={600}
                    sx={{
                      marginBottom: "0",
                    }}
                    gutterBottom
                  >
                    그룹 담당자 리스트
                  </Typography>
                  <Tooltip
                    sx={{ ml: 1 }}
                    title={"재직상태의 사원만 가능합니다."}
                  >
                    <HelpOutlineIcon color="disabled" />
                  </Tooltip>
                </Box>
                <Box
                  sx={{
                    marginLeft: "auto",
                  }}
                >
                  {selectedManagerIds.length > 0 ? 
                    <>
                      <Fab variant="extended" size="medium"
                        onClick={() => setSelectedManagerIds([])}
                        sx={{ zIndex: 500, backgroundColor:'white' }}
                      >
                        <Typography >취소</Typography>
                      </Fab>
                      <Fab color="error" variant="extended" size="medium"
                        onClick={() => deleteGroupManager(selectedManagerIds)}
                        sx={{ zIndex: 500, ml: 2 }}
                      >
                        <GroupRemoveIcon />
                        <Typography sx={{ml:0.5}}>삭제</Typography>
                      </Fab>
                    </>
                  : 
                    <Fab color="secondary" variant="extended" size="medium"
                      disabled={selectedGroupNo == null ? true : false}
                      onClick={() => {
                        setIsManagerModalOpen(true);
                      }}
                      sx={{ zIndex: 500 }}
                    >
                      <ManageAccountsIcon />
                      <Typography sx={{ml:0.5}}>수정</Typography>
                    </Fab>
                  }
                </Box>
              </Box>

              <Box
                sx={{
                  //height: "calc(67vh)",
                  display: {
                    sm: "flex",
                    xs: "block",
                  },
                  width: "100%",
                  mb: 3,
                }}
              >
                <Box sx={{ width: "100%" }}>
                  {loadingManager ?
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(63vh)" }}>
                      <CircularProgress size={50} />
                    </Box>
                  :
                    <GroupManagerTable
                      selectedGroupNo={selectedGroupNo}
                      managers={managers}
                      selectedIds={selectedManagerIds}
                      handleSelectIds={handleSelectManagerIds}
                      handleAllSelectIds={handleAllSelectManagerIds}
                      deleteGroupManager={deleteGroupManager}
                    />
                  }
                </Box>
              </Box>
            </CardContent>
          </Card>

        </Grid>

        <Grid item xs={12} lg={7}>
          <Card
            variant="outlined"
            sx={{
              p: 0,
              pt: 3
            }}
          >
            <CardContent
              sx={{
                pb: "0 !important",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center", // 가로 중앙 정렬
                  mb: 3
                }}
              >
                <Box>
                  <Typography
                    variant="h3" fontWeight={600}
                    sx={{
                      marginBottom: "0",
                    }}
                    gutterBottom
                  >
                    그룹 사원 리스트
                  </Typography>
                </Box>
                <Box
                  sx={{
                    marginLeft: "auto",
                  }}
                >
                  {selectedIds.length > 0 ? 
                    <>
                      <Fab variant="extended" size="medium"
                        onClick={() => setSelectedIds([])}
                        sx={{ zIndex: 500, backgroundColor:'white' }}
                      >
                        <Typography >취소</Typography>
                      </Fab>
                      <Fab color="error" variant="extended" size="medium"
                        onClick={() => deleteGroupMember(selectedIds)}
                        sx={{ zIndex: 500, ml: 2 }}
                      >
                        <GroupRemoveIcon />
                        <Typography sx={{ml:0.5}}>삭제</Typography>
                      </Fab>
                    </>
                  : 
                    <Fab color="secondary" variant="extended" size="medium"
                      disabled={selectedGroupNo == null ? true : false}
                      onClick={() => {
                        setIsMemberModalOpen(true);
                        setGroupAddModal({ state: 'edit', groupName: "" });
                      }}
                      sx={{ zIndex: 500 }}
                    >
                      <ManageAccountsIcon />
                      <Typography sx={{ml:0.5}}>수정</Typography>
                    </Fab>
                  }
                </Box>
              </Box>

              <Box
                sx={{
                  height: "calc(63vh)",
                  display: {
                    sm: "flex",
                    xs: "block",
                  },
                  width: "100%",
                  mb: 3,
                }}
              >
                <Box sx={{ width: "100%" }}>

                  {loadingMember ?
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%", height: "calc(59vh)" }}>
                      <CircularProgress size={50} />
                    </Box>
                  :
                    <>
                      <GroupMemberTable
                        selectedGroupNo={selectedGroupNo}
                        originalMembers={members}
                        //members={members.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                        members={members}
                        selectedIds={selectedIds}
                        handleSelectIds={handleSelectIds}
                        handleAllSelectIds={handleAllSelectIds}
                        deleteGroupMember={deleteGroupMember}
                      />

                      
                      </>
                  }
        

                <Box sx={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center'}}>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 30]}
                    component="div"
                    count={groups.length===0 || !groups.find((group) => group.groupNo === selectedGroupNo)? 0 : groups.find((group) => group.groupNo === selectedGroupNo).count}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                  />
                </Box>
                </Box>
              </Box>


            </CardContent>
          </Card>
        </Grid>
      </Grid>


      <GroupAddModal
        isOpen={isAddModalOpen}
        handleClose={() => setIsAddModalOpen(false)}
        groupAddModal={groupAddModal}
        setGroupAddModal={setGroupAddModal}
        handleButton={() => {
          setIsAddModalOpen(false);
          setIsMemberModalOpen(true);
        }}
      />

      <GroupManagerModal
        selectedGroupNo={selectedGroupNo}
        isOpen={isManagerModalOpen}
        handleClose={() => { setGroupManagerModal({insert:[], delete:[]}); setIsManagerModalOpen(false);}}
        groupManagerModal={groupManagerModal}
        setGroupManagerModal={setGroupManagerModal}
        handleButton={editGroupManager}
      />

      <GroupMemberModal
        selectedGroupNo={selectedGroupNo}
        mode={groupAddModal.state}    //'add' or 'edit'
        isOpen={isMemberModalOpen}
        handleClose={() => { setGroupMemberModal({insert:[], delete:[]}); setIsMemberModalOpen(false);}}
        groupMemberModal={groupMemberModal}
        setGroupMemberModal={setGroupMemberModal}
        handleButton={groupAddModal.state ==='add'? insertGroup : editGroupMember}
      />

    </Box>
  );
};

export default GroupDashboard;
