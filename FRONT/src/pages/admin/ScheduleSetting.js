import React, { useState, useEffect } from "react";
import { Box, Fab, Typography } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import dayjs from "dayjs";
import Swal from "sweetalert2";

import ScheduleTable from "../../components/tables/ScheduleTable";
import YearMonthPicker from "../../components/datepickers/YearMonthPicker";
import ModifyScheduleModal from "../../components/modal/ModifyScheduleModal";
import CreateScheduleModal from "../../components/modal/CreateScheduleModal";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import getRecentSchedule from "../../reducer/axios/scheduleAbout/getRecentSchedule";
import {
  change_ScheduleState,
  complete_Schedule_Loading,
  get_ActiveScheduleList,
  reset_Schedule_Loading,
  reset_Select_Schedule,
  update_Schedule,
} from "../../reducer/module/scheduleReducer";
import getLastChargedateAndPayday from "../../reducer/axios/scheduleAbout/getLastChargedateAndPayday";
import { useNavigate } from "react-router-dom";
import UseApi from "../../components/UseApi";
axios.defaults.withCredentials = true;

const ScheduleSetting = () => {
  //로딩상태 확인
  const [isLoading, setIsLoading] = useState(true);
  const [listLoadingStart, setListLoadingStart] = useState(false);

  //차수 등록 모달
  const [modalOpen, setModalOpen] = useState(false);

  const handleOpenModal = () => {
    setModalOpen(true);
  };
  const handleCloseModal = () => {
    setModalOpen(false);
  };

  //리스트 수정 state
  const [listUpdate, setListUpdate] = useState(false);

  //새로운 청구년월
  const [chargeDateString, setChargeDateString] = useState("");
  const [chargeDate, setChargeDate] = useState(new Date());
  const [createScheduleNum, setCreateScheduleNum] = useState(null);
  const [currentDate, setCurrentDate] = useState(new Date());

  //차수리스트
  const [scheduleList, setScheduleList] = useState([]);

  // currentDate와 chargeDate를 비교하는 함수 - 등록버튼 비활성화를 위함
  const compareDates = () => {
    return dayjs(chargeDate).isBefore(dayjs(Date()), "month");
  };

  const [scheduleLastChargeEndDate, setScheduleLastChargeEndDate] =
    useState("");

  const [payday, setPayday] = useState("");
  //===================================================================
  //리듀서의 최근차수가져옴
  const recentSchedule = useSelector(
    (state) => state.scheduleReducer.recentSchedule
  );
  //리듀서의 차수상태 업데이트 관련
  const scheduleStateChange = useSelector(
    (state) => state.scheduleReducer.scheduleStateChange
  );

  const modifyScheduleState = useSelector(
    (state) => state.scheduleReducer.modifyScheduleState
  );

  //차수 생성,수정,삭제 시  state
  // const [scheduleStateChange,setScheduleStateChange] =useState(false);

  //dispatch 선언
  const dispatch = useDispatch();

  const navigate = useNavigate();
  const api = UseApi();

  //
  useEffect(() => {
    setChargeDateString(dayjs().format("YYYY-MM"));
    setIsLoading(!isLoading);
  }, []);

  //해당 청구년월의 차수 가져옴
  //청구년월변경과 차수생성.수정.삭제 할때마다 가져옴
  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await getRecentSchedule(chargeDateString, navigate, api);
        if (result !== null) {
          dispatch(reset_Schedule_Loading());

          dispatch(update_Schedule(result)); //recentSchedule업데이트
          setListLoadingStart(!listLoadingStart); //리스트다시받음
          console.log("마지막차수 dispatch성공 및 set리스트스테이트");

          dispatch(complete_Schedule_Loading());
        }
      } catch (error) {
        console.log("yearmonthpicker에서 디스패치 실패");
      }
    };
    fetchData();
  }, [chargeDateString, scheduleStateChange]); //청구년월과 차수생성.삭제마다

  //마감일 지급일 수정해보는 코드

  useEffect(() => {
    dispatch(reset_Select_Schedule());
    if (recentSchedule !== 0 && chargeDateString !== "") {
      api
        .get("/schedulechargeend", {
          params: {
            chargeDateString: chargeDateString,
            createScheduleNum: recentSchedule - 1,
          },
        })
        .then((result) => {
          console.log(
            "청구가능마감이랑 지급일 : " + JSON.stringify(result.data)
          );

          const lastChargeEndDate = result.data.chargeEnd;
          const defaultPayday = result.data.payday;
          setScheduleLastChargeEndDate(lastChargeEndDate);
          setPayday(defaultPayday);
          dispatch(change_ScheduleState());
        })
        .catch((error) => {
          // if(error.response && error.response.status === 401){
          //   alert("로그인세션만료");
          //   navigate("/");
          // }
          // else{
          console.log("최신 차수의 청구마감일 읽어오기 실패");

          // }
        });
    }
  }, [recentSchedule, chargeDateString, modifyScheduleState]); //해당 청구년월의 최근차수가 변할때마다 , 수정될때마다

  //청구가능마감일과 지급일 가져옴
  const handleClickCreateScheduleBtn = () => {
    if (
      dayjs(scheduleLastChargeEndDate).format("YYYY-MM-DD") ===
      dayjs(chargeDate).endOf("month").format("YYYY-MM-DD")
    ) {
      console.log("마지막날이랑 차수가일치");
      Swal.fire({
        icon: "error",
        title: "차수 생성 불가",
        text: "진행예정인 차수를 수정하거나 삭제 후 다시시도해주세요",
      });
    } else {
      // console.log("모달열림");
      setModalOpen(true);
    }
  }; //등록버튼누르면

  //해당 청구년월의 차수리스트 가져옴
  useEffect(() => {
    if (recentSchedule !== 0) {
      api
        .get("/schedulelist", {
          params: {
            chargeDateString: chargeDateString,
          },
        })
        .then((result) => {
          setScheduleList(result.data);
          dispatch(get_ActiveScheduleList(result.data));
          console.log("해당 차수리스트 : " + result.data);
        })
        .catch(() => {
          console.log("차수리스트 실패");
        });
    }
  }, [listLoadingStart]);

  return (
    <Box>
      <Box sx={{ mr: 5, ml: 3, display: "flex" }}>
        <Typography variant="h3" gutterBottom>
          차수 설정
        </Typography>
      </Box>
      <Box sx={{ display: "flex", alignItems: "center", mb: 1 }}>
        <YearMonthPicker
          setChargeDateString={setChargeDateString}
          setChargeDate={setChargeDate}
          chargeDate={chargeDate}
          chargeDateString={chargeDateString}
        />
        <Fab
          color="primary"
          variant="extended"
          disabled={compareDates()}
          sx={{ zIndex: 500 }}
          onClick={() => {
            handleClickCreateScheduleBtn();
          }}
        >
          <AddIcon />
          <Typography
            sx={{
              ml: 0.2,
              textTransform: "capitalize",
            }}
          >
            차수 등록
          </Typography>
        </Fab>
      </Box>

      <ScheduleTable
        scheduleList={scheduleList}
        setScheduleList={setScheduleList}
        listUpdate={listUpdate}
        setListUpdate={setListUpdate}
        listLoadingStart={listLoadingStart}
        setListLoadingStart={setListLoadingStart}
        chargeDate={chargeDate}
      />
      {modalOpen && (
        <CreateScheduleModal
          handleCloseModal={handleCloseModal}
          chargeDateString={chargeDateString}
          createScheduleNum={createScheduleNum}
          chargeDate={chargeDate}
          scheduleLastChargeEndDate={scheduleLastChargeEndDate}
          payday={payday}
          listLoadingStart={listLoadingStart}
          setListLoadingStart={setListLoadingStart}
        />
      )}
    </Box>
  );
};

export default ScheduleSetting;
