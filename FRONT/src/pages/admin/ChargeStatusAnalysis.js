import React, { useState, useEffect } from "react";
import {
  Grid,
  Box,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Card,
  Typography,
  ListSubheader,
  Divider,
  CircularProgress
} from "@mui/material";
import moment from 'moment';
import axios from 'axios';
import { useSelector } from "react-redux";
import { useDispatch } from 'react-redux';
import UseApi from '../../components/UseApi';

import ChargeAnalysisSummaryCard from "../../components/cards/ChargeAnalysisSummaryCard";
import ChargeUserRankTable from "../../components/tables/ChargeUserRankTable";
import DateRangePicker from "../../components/datepickers/DateRangePicker";

import ChargeMonthChart from "../../components/charts/ChargeMonthChart";
import ChargeCategoryAnalysisChart from "../../components/charts/ChargeCategoryAnalysisChart";

import { set_MainLoading } from '../../reducer/module/adminMainReducer';
axios.defaults.withCredentials = true;



function getSixMonthsAgoDate() {
  const sixMonthsAgo = moment().subtract(5, 'months');
  //조회기간이 6개월이어야하므로, 5를 빼줘야함.  ex. 2023년3월~2023년8월
  //console.log("시작일:", sixMonthsAgo.toDate());
  return sixMonthsAgo.toDate();
}

function sortBySumAndCount(temp) {
  return temp.sort((a, b) => {
    if (b.sum !== a.sum) {
      return b.sum - a.sum; // sum 기준 내림차순 정렬
    }
    return b.count - a.count; // sum이 같은 경우 count 기준 내림차순 정렬
  });
}

function sortByCountAndSum(temp) {
  return temp.sort((a, b) => {
    if (b.count !== a.count) {
      return b.count - a.count; // count 기준 내림차순 정렬
    }
    return b.sum - a.sum; // count이 같은 경우 sum 기준 내림차순 정렬
  });
}


const ChargeStatusAnalysis = () => {
  const api = UseApi();
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const dispatch = useDispatch();

  const [groupdeptlist, setGroupDeptList] = useState({group:[], dept:[]});  //취합그룹+부서
  const [selectedGroupDeptNo, setSelectedGroupDeptNo] = useState(0);   //처음엔 '전체'로 초기화  
  // value<50:부서  /  value>=50:취합그룹

  const [startDate, setStartDate] = useState(getSixMonthsAgoDate());    //오늘기준으로 6개월전
  const [endDate, setEndDate] = useState(new Date());
  const [isRangeExceeded, setIsRangeExceeded] = useState(false);     //조회기간 최대 1년까지만 할거라서, 초과인지 아닌지

  const [loading, setLoading] = useState(true);
  const [summaryCharge, setSummaryCharge] = useState({});  //청구분석요약
  const [monthlyCharge, setMonthlyCharge] = useState({});   //기간별 통계
  const [userRank, setUserRank] = useState({});   //청구자 순위
  const [categoryRank, setCategoryRank] = useState({});   //용도 순위

  

  const listGroupDept = () => {
    //메뉴에 띄울 사용중인 취합그룹+부서리스트 불러오는 함수

    return new Promise((resolve, reject) => {
      api.get("/admin/main/groupdeptlist", {
        params: {
          managerId: loginUserId
        }
      })
        .then((result) => {
          console.log(result.data);
          let groupData = result.data.group;
          let deptData = result.data.dept;
          let groupTemp = [];
          let deptTemp = [];
  
          if (groupData != null) {
            for (let group of groupData) {
              let newGroup = {
                value: group.groupNo,
                label: group.groupName
              }
              groupTemp.push(newGroup);
            }
          }
          if (deptData != null) {
            for (let dept of deptData) {
              let newDept = {
                value: dept.deptNo,
                label: dept.deptName
              }
              deptTemp.push(newDept);
            }
          }

          const groupDeptData = {
            group: groupTemp,
            dept: deptTemp
          }
  
          // setGroupDeptList({
          //   group: groupTemp,
          //   dept: deptTemp
          // });

          resolve(groupDeptData);   //데이터를 반환
          
        }).catch((error) => {
          if (error.response) {
            console.log("사용중 취합그룹+부서리스트 불러오기 실패", error);
          }
        });

    })
    
  }

  const listAnalysis = async () => {
    //경비현황분석 조회
    setLoading(true);

    try {
      let period = getPeriod();
      let groupDeptData = await listGroupDept();
      setGroupDeptList(groupDeptData);

      let includeGroup = null;

      if (groupDeptData.group.length !== 0) {
        includeGroup = groupDeptData.group.find(item => item.value === selectedGroupDeptNo);
      }

      let option = {
        groupDeptNo: selectedGroupDeptNo
      };

      if (selectedGroupDeptNo !== 0 && !includeGroup && loginUserId !== 'admin') {
        alert("관리 그룹내역에 변동사항이 있어, 그룹 조회조건이 초기화됩니다.");
        setSelectedGroupDeptNo(0);      //그룹번호: 전체로 검색
        dispatch(set_MainLoading());

        option = {
          groupDeptNo: 0
        };
      }


      const variables = {
        managerId: loginUserId,
        groupDeptNo: option.groupDeptNo,            //그룹정보가 변동되었으면, 전체로 검색
        period: period
        //조회기간 ex. ["2023-06","2023-07","2023-08"]
      }


      api.post("/admin/main/analysis", variables)
        .then((result) => {
          //console.log(result.data);
          let categoryListData = result.data.categoryList;
          let monthlyApplyData = result.data.monthlyApplyCharge;
          let monthlyConfirmData = result.data.monthlyConfirmCharge;

          let categoryBySum = sortBySumAndCount(categoryListData);    //용도금액순
          let categoryByCount = sortByCountAndSum(categoryListData);  //용도건수순

          setSummaryCharge({
            total: result.data.totalConfirm,
            most: result.data.mostCountDept,
            best: categoryBySum.length !== 0 ? categoryBySum[0] : null
          });

          let maxCharge = 0;    //차트에 max값 설정해둬야함.
          let applyCharge = [];
          for (let month of monthlyApplyData) {
            applyCharge.push(month.sum);
            if (maxCharge < month.sum) {
              maxCharge = month.sum;
            }
          }

          let confirmCharge = [];
          for (let month of monthlyConfirmData) {
            confirmCharge.push(month.sum);
            if (maxCharge < month.sum) {
              maxCharge = month.sum;
            }
          }

          setMonthlyCharge({
            period: period,
            applyCharge: applyCharge,
            confirmCharge: confirmCharge,
            maxCharge: maxCharge
          });

          //console.log("확인!!!!!! 신청금액배열:", applyCharge, "     승인금액배열:", confirmCharge);

          setUserRank({
            sum: result.data.userRankBySum,
            count: result.data.userRankByCount
          });

          setCategoryRank({
            sum: categoryBySum,
            count: categoryByCount
          });

          // setTimeout(() => {
          //   setLoading(false);
          // }, 500);
        
        
        }).catch((error) => {
          if (error.response) {
            console.log("경비현황분석불러오기 실패", error);
            alert("경비현황분석 불러오기 실패");
          }
        }).finally(() => {
          setLoading(false);
        });

    } catch (error) {
      console.log(error);
    }

  };



  const getPeriod = () => {
    let start = moment(startDate);
    let end = moment(endDate);

    const period = [];
    let currentDate = start.clone();

    if (start.isSame(end, 'month')) {
      period.push(currentDate.format('YYYY-MM'));
    } else {
      while (currentDate.isSameOrBefore(end, 'month')) {
        period.push(currentDate.format('YYYY-MM'));
        currentDate.add(1, 'month');
      }
    }
    
    return period;
  }


  useEffect(() => { 
    if(loginUserId){
      //listGroupDept();
      listAnalysis();

    }
  }, [loginUserId]);

  
  useEffect(() => {
    // 계산된 조회 기간이 1년을 초과하는지 확인
    const diffInMonths = (endDate.getFullYear() - startDate.getFullYear()) * 12 +
                          (endDate.getMonth() - startDate.getMonth()) + 1;
    setIsRangeExceeded(diffInMonths > 12);
    //console.log("조회기간 차이:", diffInMonths);   
    
  }, [startDate, endDate]);



  return (
    <Box>
      <Grid container spacing={2} height="100%">
        <Box sx={{ m:4}}>
          <Typography variant="h3" fontWeight={600}>
            경비현황분석
          </Typography>
        </Box>
          
        <Grid container spacing={2} alignItems="center" sx={{ mb: 2, ml: 2 }}>
          <Grid item xs={12} md={3} lg={2}>
            <FormControl variant="outlined" size="small" fullWidth>
              <InputLabel id="group-label">취합그룹/부서</InputLabel>
              <Select
                labelId="group-label"
                id="group"
                label="취합그룹/부서"
                value={selectedGroupDeptNo}
                onChange={(e)=>setSelectedGroupDeptNo(e.target.value)}
                sx={{ width: "100%" }}
                MenuProps={{
                  style: {maxHeight: "350px"}
                }}
              >
                <MenuItem key={0} value={0}>{"전체"}</MenuItem>
                <Divider />

                {groupdeptlist.group.length !== 0 && [
                  <ListSubheader key={"group"} disableSticky>취합그룹</ListSubheader>,
                  ...groupdeptlist.group.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))
                ]}

                {groupdeptlist.dept.length !== 0 && [
                  <Divider key={"divider"} />,
                  <ListSubheader key={"dept"} disableSticky>부서</ListSubheader>,
                  ...groupdeptlist.dept.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))
                ]}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <DateRangePicker
              startDate={startDate}
              endDate={endDate}
              handleStartDate={(date)=>setStartDate(date)}
              handleEndDate={(date)=>setEndDate(date)}
            />
          </Grid>
          <Grid item>
            <Button variant="contained" color="primary"
              disabled={isRangeExceeded} onClick={listAnalysis}>
              조회
            </Button>
          </Grid>
          <Grid item>
            {isRangeExceeded && <Typography color="error">조회 기간은 최대 1년까지 가능합니다.</Typography>}
          </Grid>
        </Grid>
        {loading ?
          <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width:"100%", height:"600px" }}>
            <CircularProgress size={50} />
          </Box>
        :
          <>
            <Grid item xs={12} lg={4} height="auto">
              <ChargeAnalysisSummaryCard
                summaryCharge={summaryCharge}
              />
            </Grid>
            <Grid item xs={12} lg={8} height="auto">
              <ChargeMonthChart
                monthlyCharge={monthlyCharge}
              />
            </Grid>
            {/* ------------------------------------------------- */}
            <Grid item xs={12} lg={6}>
              <ChargeUserRankTable
                userRank={userRank}
              />
            </Grid>
            <Grid item xs={12} lg={6}>
              <ChargeCategoryAnalysisChart
                categoryRank={categoryRank}
              />
            </Grid>
          </>  
        }

      </Grid>
    </Box>
  );
};

export default ChargeStatusAnalysis;
