import React, { useEffect, useState } from "react";
import {
  Card,
  CardContent,
  Box,
  Typography,
  Button,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Grid,
  Fab,
  IconButton,
  Tooltip,
  Fade,
} from "@mui/material";
import SearchBar from "../../components/SearchBar";
import ChargeLogTable from "../../components/tables/ChargeLogTable";
import ChargeCancelModal from "../../components/modal/ChargeCancelModal";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import RefreshIcon from "@mui/icons-material/Refresh";
import axios from "axios";
import UseApi from "../../components/UseApi";
axios.defaults.withCredentials = true;

const BasicChargeLogTable = () => {
  const [isChargeCancelModalOpen, setIsChargeCancelModalOpen] = useState(false); // 상태 관리를 위한 isModalOpen 상태 변수
  const [searchValue, setSearchValue] = useState("");
  const [searchField, setSearchField] = useState("");
  const [selectedItem, setSelectedItem] = useState([]);
  const loginUserId = useSelector((state) => state.loginReducer.loginUserId);
  const navigate = useNavigate();
  const [yearMenuItemData, setYearMenuItemData] = useState([]);
  const [selectedYear, setSelectedYear] = useState("");
  const [selectedMonth, setSelectedMonth] = useState("");
  const [selectedState, setSelectedState] = useState("");
  const [refresh, setRefresh] = useState(false);
  const [isAnyCheckboxChecked, setIsAnyCheckboxChecked] = useState(false);
  const [searchButtonClicked, setSearchButtonClicked] = useState(false);
  const [selectDisabled, setSelectDisabled] = useState(true);
  const api = UseApi();
  const currentYear = new Date().getFullYear(); // 현재 연도 가져오기
  const currentMonth = new Date().getMonth() + 1; // 현재 월 가져오기 (+1을 하는 이유는 0부터 시작하므로)

  useEffect(() => {
    yearMenuItem();
  }, []);

  // 청구취소버튼 컨트롤
  const handleChargeCancelButtonClick = () => {
    setIsChargeCancelModalOpen(true);
  };

  // 청구취소함? ㅇㅇ 클릭
  const handleChargeCancelYesClick = () => {
    api
      .delete("/user/charge/delete", {
        data: selectedItem,
      })
      .then(() => {
        setIsChargeCancelModalOpen(false);
        setSelectedItem([]);
        setIsAnyCheckboxChecked(false);
        setSearchValue("");
        setSearchField("");
        setSelectedYear("");
        setSelectedMonth("");
        setSelectedState("");
        setRefresh(!refresh);
      })
      .catch((error) => {
        console.error("청구취소 중 오류 발생:", error);
        if (error.response) {
          alert("청구취소에 실패했습니다. 다시 시도해주세요.");
        }
      });
  };

  const handleCancelModal = () => {
    setIsChargeCancelModalOpen(false);
  };

  // 체크된 항목 전달받음
  const handleCheckboxChange = (checkedItems) => {
    if (checkedItems.length > 0) {
      setIsAnyCheckboxChecked(true);
      setSelectedItem(checkedItems);
    } else {
      setIsAnyCheckboxChecked(false);
      setSelectedItem([]);
    }
  };

  // 검색 조건, 값 전달
  const handleSearch = (value, field) => {
    setSearchValue(value);
    setSearchField(field);
    setSelectedItem([]);
  };

  // 연도 조건 선택 값 전달
  const handleYearOptionChange = (event) => {
    setSelectedYear(event.target.value);
    if (event.target.value !== 0) {
      setSelectDisabled(false);
    } else {
      setSelectDisabled(true);
    }
  };

  // 월 조건 선택 값 전달
  const handleMonthOptionChange = (event) => {
    setSelectedMonth(event.target.value);
  };

  // 상태 조건 선택 값 전달
  const handleStateOptionChange = (event) => {
    setSelectedState(event.target.value);
  };

  // 조회 버튼 클릭 컨트롤
  const handleSearchButtonClick = () => {
    setSearchButtonClicked(!searchButtonClicked);
    setSelectedItem([]);
    setSearchField("");
    setSearchValue("");
  };

  // select할 연도 옵션 요청
  const yearMenuItem = () => {
    api
      .get(`/user/chargelog/menuitem?id=${loginUserId}`)
      .then((response) => {
        const itemData = response.data;
        setYearMenuItemData(itemData);
      })
      .catch((error) => {
        console.error("연도 정보 요청 실패", error);
        if (error.response) {
        }
      });
  };

  const handleRefreshButtonClick = () => {
    setSearchButtonClicked(!searchButtonClicked);
  };

  return (
    <Box>
      <Box display="flex" marginLeft={"20px"}>
        <Grid container spacing={1} alignItems="center">
          <Grid item>
            <FormControl sx={{ minWidth: 120 }} size="small">
              <InputLabel id="year">청구연도</InputLabel>
              <Select
                labelId="year"
                label="청구연도"
                MenuProps={{ style: { maxHeight: "285px" } }}
                onChange={handleYearOptionChange}
                defaultValue={0}
              >
                <MenuItem value={0}>전체</MenuItem>
                {yearMenuItemData.map((year, index) => (
                  <MenuItem key={index} value={year}>
                    {year}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item>
            <FormControl sx={{ minWidth: 120 }} size="small">
              <InputLabel id="month">청구월</InputLabel>
              <Select
                labelId="month"
                label="청구월"
                MenuProps={{ style: { maxHeight: "285px" } }}
                onChange={handleMonthOptionChange}
                disabled={selectDisabled}
                defaultValue={0}
              >
                <MenuItem value={0}>전체</MenuItem>
                <MenuItem value={1} disabled={currentYear === selectedYear && currentMonth < 1}>
                  1월
                </MenuItem>
                <MenuItem value={2} disabled={currentYear === selectedYear && currentMonth < 2}>
                  2월
                </MenuItem>
                <MenuItem value={3} disabled={currentYear === selectedYear && currentMonth < 3}>
                  3월
                </MenuItem>
                <MenuItem value={4} disabled={currentYear === selectedYear && currentMonth < 4}>
                  4월
                </MenuItem>
                <MenuItem value={5} disabled={currentYear === selectedYear && currentMonth < 5}>
                  5월
                </MenuItem>
                <MenuItem value={6} disabled={currentYear === selectedYear && currentMonth < 6}>
                  6월
                </MenuItem>
                <MenuItem value={7} disabled={currentYear === selectedYear && currentMonth < 7}>
                  7월
                </MenuItem>
                <MenuItem value={8} disabled={currentYear === selectedYear && currentMonth < 8}>
                  8월
                </MenuItem>
                <MenuItem value={9} disabled={currentYear === selectedYear && currentMonth < 9}>
                  9월
                </MenuItem>
                <MenuItem value={10} disabled={currentYear === selectedYear && currentMonth < 10}>
                  10월
                </MenuItem>
                <MenuItem value={11} disabled={currentYear === selectedYear && currentMonth < 11}>
                  11월
                </MenuItem>
                <MenuItem value={12} disabled={currentYear === selectedYear && currentMonth < 12}>
                  12월
                </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item>
            <FormControl sx={{ minWidth: 120 }} size="small">
              <InputLabel id="state">상태</InputLabel>
              <Select
                labelId="state"
                label="상태"
                onChange={handleStateOptionChange}
                defaultValue="전체"
              >
                <MenuItem value="전체">전체</MenuItem>
                <MenuItem value="승인대기">승인대기</MenuItem>
                <MenuItem value="승인">승인</MenuItem>
                <MenuItem value="반려">반려</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item>
            <Box display="flex" style={{ marginLeft: "5px" }}>
              <Fab
                color="primary"
                variant="extended"
                size="medium"
                sx={{ zIndex: 500 }}
                onClick={handleSearchButtonClick}
              >
                조회
              </Fab>
            </Box>
          </Grid>
        </Grid>
        <Box display="flex" style={{ marginRight: "20px", marginTop: "5px" }}>
          <Tooltip
            title={<Typography variant="body2">내역 새로고침</Typography>}
            placement="left"
            TransitionComponent={Fade}
            TransitionProps={{ timeout: 300 }}
          >
            <IconButton color="secondary" size="large" onClick={handleRefreshButtonClick}>
              <RefreshIcon />
            </IconButton>
          </Tooltip>
        </Box>
      </Box>
      <Card variant="outlined">
        <CardContent>
          <Box display="flex" alignItems="center" marginBottom="15px">
            <Typography variant="h3" marginRight="15px">
              청구 내역 리스트
            </Typography>
          </Box>
          <Box display="flex" style={{ marginBottom: "10px" }}>
            <div>
              <SearchBar
                onSearch={handleSearch}
                searchButtonClicked={searchButtonClicked}
                setSearchButtonClicked={setSearchButtonClicked}
              />
            </div>
            <Box display="flex" style={{ marginLeft: "auto" }}>
              <Button
                variant="outlined"
                onClick={handleChargeCancelButtonClick}
                sx={{ zIndex: 500 }}
                disabled={!isAnyCheckboxChecked}
              >
                청구취소
              </Button>
            </Box>
          </Box>
          <Box
            sx={{
              overflow: {
                xs: "auto",
                sm: "unset",
              },
            }}
          >
            <ChargeLogTable
              searchValue={searchValue}
              searchField={searchField}
              searchYear={selectedYear}
              searchMonth={selectedMonth}
              searchState={selectedState}
              onCheckboxChange={handleCheckboxChange}
              refresh={refresh}
              searchButtonClicked={searchButtonClicked}
            />
          </Box>
        </CardContent>
      </Card>
      {isChargeCancelModalOpen && (
        <ChargeCancelModal onCancel={handleCancelModal} onDelete={handleChargeCancelYesClick} />
      )}
    </Box>
  );
};

export default BasicChargeLogTable;
