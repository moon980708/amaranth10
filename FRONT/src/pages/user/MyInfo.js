import React, { useState, useEffect } from "react";
import { Tabs, Tab, Box, Fade, Grid, Card, CardContent } from "@mui/material";
import UserMonthlyChart from "../../components/charts/UserMonthlyChart";
import UserCategoryChart from "../../components/charts/UserCategoryChart";
import MyInfoForm from "../../components/forms/MyInfoForm";
import UserChargeSummary from "../../components/tables/UserChargeSummary";
import PasswordCheckModal from "../../components/modal/PasswordCheckModal";

const ViewChart = () => {
  const [year, setYear] = useState("");
  const [month, setMonth] = useState("");

  const handleDateChange = (selectedYear, selectedMonth) => {
    setYear(selectedYear);
    setMonth(selectedMonth);
  };

  return (
    <Box>
      <Grid container spacing={0}>
        {/* ------------------------- row 1 ------------------------- */}
        <Grid item xs={12} lg={6}>
          <UserChargeSummary onDateChange={handleDateChange} />
        </Grid>
        <Grid item xs={12} lg={6}>
          <UserCategoryChart year={year} month={month} />
        </Grid>
        {/* ------------------------- row 2 ------------------------- */}
        <Grid item xs={12} lg={12}>
          <UserMonthlyChart />
        </Grid>
        {/* ------------------------- row 3 ------------------------- */}
      </Grid>
    </Box>
  );
};

const TabPanel = ({ children, value, index }) => {
  return (
    <Fade in={value === index}>
      <Box hidden={value !== index}>{children}</Box>
    </Fade>
  );
};

const MyInfo = () => {
  const [value, setValue] = useState(0);
  const [isModalOpen, setModalOpen] = useState(true);
  const [pass, setPass] = useState("");

  useEffect(() => {
    if (value === 1) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  }, [value]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };

  const handleOpenModal = () => {
    setModalOpen(true);
  };

  const handleCancelButtonClick = () => {
    setModalOpen(false);
    setValue(0);
  };

  const handlePasswordSubmit = (password) => {
    setPass(password);
    setModalOpen(false);
  };

  return (
    <Card variant="outlined">
      <CardContent>
        <Tabs value={value} onChange={handleChange}>
          <Tab label="차트 보기" />
          <Tab label="내 정보" />
        </Tabs>
        <TabPanel value={value} index={0}>
          <ViewChart />
        </TabPanel>
        <TabPanel value={value} index={1}>
          {value === 1 && isModalOpen && (
            <PasswordCheckModal
              open={isModalOpen}
              onClose={handleCloseModal}
              onCancel={handleCancelButtonClick}
              onPasswordSubmit={handlePasswordSubmit}
            />
          )}
          <MyInfoForm pass={pass} passwordModalOpen={isModalOpen} />
        </TabPanel>
      </CardContent>
    </Card>
  );
};
export default MyInfo;
