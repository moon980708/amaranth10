import React, { useState } from "react";
import { Card, CardContent, Box, Typography, Button, ButtonGroup, Fab } from "@mui/material";
import SearchBar from "../../components/SearchBar";
import ChargeTable from "../../components/tables/ChargeTable";
import ChargeDeleteModal from "../../components/modal/ChargeDeleteModal";
import ChargeModal from "../../components/modal/ChargeModal";
import ChargeAddModal from "../../components/modal/ChargeAddModal";
import ChargeFailModal from "../../components/modal/ChargeFailModal";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import CardCarousel from "../../components/cards/CardCarousel";
import axios from "axios";
import UseApi from "../../components/UseApi";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
axios.defaults.withCredentials = true;

const BasicChargeTable = () => {
  const navigate = useNavigate();
  const [isAnyCheckboxChecked, setIsAnyCheckboxChecked] = useState(false);
  const [isAnyOngoingSchedule, setIsAnyOngoingSchedule] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [isChargeModalOpen, setIsChargeModalOpen] = useState(false);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const [isFailModalOpen, setIsFailModalOpen] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [searchField, setSearchField] = useState("");
  const [selectedItem, setSelectedItem] = useState([]);
  const [invalidCount, setInvalidCount] = useState(0);
  const [refresh, setRefresh] = useState(false);
  const api = UseApi();

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
      popup: "my-custom-class", // 모달 팝업의 CSS 클래스 이름
    },
    width: "45%",
  });

  // 검색 조건, 값 전달
  const handleSearch = (value, field) => {
    setSearchValue(value);
    setSearchField(field);
    setSelectedItem([]);
  };

  // 삭제버튼 컨트롤
  const handleDeleteButtonClick = () => {
    setIsDeleteModalOpen(true);
    setIsChargeModalOpen(false);
    setIsAddModalOpen(false);
    setIsFailModalOpen(false);
  };

  // 삭제할거임? ㅇㅇ 클릭
  const handleDeleteYesClick = () => {
    api
      .delete("/user/charge/delete", {
        data: selectedItem,
      })
      .then(() => {
        setIsDeleteModalOpen(false);
        setSelectedItem([]);
        setIsAnyCheckboxChecked(false);
        setSearchValue("");
        setSearchField("");
        setRefresh(!refresh);
      })
      .catch((error) => {
        console.error("삭제 중 오류 발생:", error);
        if (error.response) {
        }
      });
  };

  // 추가버튼 컨트롤
  const handleAddButtonClick = () => {
    setIsAddModalOpen(true);
    setIsDeleteModalOpen(false);
    setIsChargeModalOpen(false);
    setIsFailModalOpen(false);
  };

  // 청구버튼 컨트롤
  const handleChargeButtonClick = () => {
    api
      .get("/user/charge/ongoingschedule")
      .then((response) => {
        if (response.data !== "") {
          if (invalidCount > 0) {
            setIsFailModalOpen(true);
          } else {
            setIsChargeModalOpen(true);
            setIsDeleteModalOpen(false);
            setIsAddModalOpen(false);
          }
        } else {
          swalWithBootstrapButtons.fire("청구 불가", "차수가 마감되었습니다.", "error");
          setRefresh(!refresh);
        }
      })
      .catch((error) => {
        if (error.response) {
          alert("청구에 실패했습니다. 다시 청구해주세요.");
          setRefresh(!refresh);
        }
      });
  };

  //청구할거임? ㅇㅇ 클릭
  const handleChargeYesClick = () => {
    api
      .put("/user/charge/update", selectedItem)
      .then((response) => {
        if (response.data === "success") {
          setIsChargeModalOpen(false);
          setSelectedItem([]);
          setIsAnyCheckboxChecked(false);
          setRefresh(!refresh);
          setSearchValue("");
          setSearchField("");
        } else {
          alert("차수가 마감되어 삭제된 내역입니다.");
          setRefresh(!refresh);
          setIsAnyCheckboxChecked(false);
        }
      })
      .catch((error) => {
        if (error.response) {
          alert("청구에 실패했습니다. 다시 청구해주세요.");
        }
      });
  };

  // 모달 닫기 컨트롤
  const handleCloseModals = () => {
    setIsDeleteModalOpen(false);
    setIsChargeModalOpen(false);
    setIsAddModalOpen(false);
    setIsFailModalOpen(false);
  };

  // 체크된 항목 전달받음
  const handleCheckboxChange = (checkedItems) => {
    if (checkedItems.length > 0) {
      setIsAnyCheckboxChecked(true);
      setSelectedItem(checkedItems);
    } else {
      setIsAnyCheckboxChecked(false);
      setSelectedItem([]);
    }
  };

  // ChargeTable 컴포넌트에서 invalidCount 변경 시 호출되는 콜백 함수
  const handleInvalidCountChange = (newInvalidCount) => {
    setInvalidCount(newInvalidCount);
  };

  // 진행중인 차수 체크
  const handleOngoingSchedule = (yes) => {
    setIsAnyOngoingSchedule(yes);
  };

  return (
    <Box>
      <CardCarousel ongoingSchedule={handleOngoingSchedule} />

      <Card variant="outlined">
        <CardContent>
          <Box display="flex" alignItems="center" marginBottom="15px">
            <Typography variant="h3" marginRight="15px">
              지출 정보 등록
            </Typography>
            <Fab
              color="primary"
              variant="extended"
              onClick={handleAddButtonClick}
              disabled={isAnyCheckboxChecked || isAnyOngoingSchedule}
              size="medium"
              sx={{ zIndex: 500 }}
            >
              <AddCircleIcon />
              <Typography
                sx={{
                  ml: 0.2,
                  textTransform: "capitalize",
                  marginLeft: "10px",
                }}
              >
                추가
              </Typography>
            </Fab>
          </Box>
          <Box display="flex" style={{ marginBottom: "10px" }}>
            <div>
              <SearchBar onSearch={handleSearch} />
            </div>
            <Box display="flex" style={{ marginLeft: "auto" }}>
              <ButtonGroup variant="outlined" color="primary" aria-label="text button group">
                <Button
                  classes={{ root: "customButton" }}
                  style={{ color: "red" }}
                  disabled={selectedItem.length === 0}
                  onClick={handleDeleteButtonClick}
                >
                  삭제
                </Button>
                <Button
                  classes={{ root: "customButton" }}
                  style={
                    isAnyOngoingSchedule
                      ? { backgroundColor: "lightgray", color: "white" }
                      : { backgroundColor: "skyblue", color: "white" }
                  }
                  onClick={handleChargeButtonClick}
                  disabled={isAnyOngoingSchedule || selectedItem.length === 0}
                >
                  청구
                </Button>
              </ButtonGroup>
            </Box>
          </Box>
          <Box
            sx={{
              overflow: {
                xs: "auto",
                sm: "unset",
              },
            }}
          >
            <ChargeTable
              onCheckboxChange={handleCheckboxChange}
              onInvalidCountChange={handleInvalidCountChange}
              isAnyOngoingSchedule={isAnyOngoingSchedule}
              searchValue={searchValue}
              searchField={searchField}
              refresh={refresh}
            />
          </Box>
        </CardContent>
      </Card>
      {isDeleteModalOpen && (
        <ChargeDeleteModal onCancel={handleCloseModals} onDelete={handleDeleteYesClick} />
      )}
      {isChargeModalOpen && (
        <ChargeModal onCancel={handleCloseModals} onCharge={handleChargeYesClick} />
      )}
      {isAddModalOpen && (
        <ChargeAddModal
          onCancel={handleCloseModals}
          // onAdd={handleSnackbarOpen}
          refresh={refresh}
          setRefresh={setRefresh}
        />
      )}
      {isFailModalOpen && <ChargeFailModal onCancel={handleCloseModals} />}
    </Box>
  );
};

export default BasicChargeTable;
