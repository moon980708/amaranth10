import React, { useState, useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";
import DouzoneImg from "../../src/assets/images/DouzoneImg.png";
import axios from "axios";
import Loading from "../Loading/Loading";
import Gif from "../assets/images/GIF.gif";
import { useDispatch, useSelector } from "react-redux";

import {
  set_CheckGroupManager,
  set_LoginUserDept,
  set_LoginUserId,
  set_LoginUserName,
  set_LoginUserRank,
} from "../reducer/module/loginReducer";

import UseApi, { resetAlertStatus } from "../components/UseApi";
import LoginOutlinedIcon from "@mui/icons-material/LoginOutlined";
import InputAdornment from "@mui/material/InputAdornment";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";

axios.defaults.withCredentials = true;

const defaultTheme = createTheme();

export default function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const api = UseApi();

  const [loginFail, setLoginFail] = useState(false);
  const [message, setMessage] = useState("");
  // useEffect(()=>{

  //   axios.get("/loginCheck")
  //   .then((result)=>{
  //     if(result.data !== 'notLogin'){
  //       alert("이미 로그인 되어있습니다");
  //       navigate(result.data);

  //     }

  //   }).catch((error) => {
  //     if(error.response && error.response.status === 401){
  //       alert("로그인세션만료");
  //       navigate("/");
  //     }
  //     console.log("로그인세션 확인 에러")
  //   })
  // },[])

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log({
      email: data.get("email"),
      password: data.get("password"),
    });
    navigate("admin/main");
  };

  const handleClickLogin = () => {
    
    if(inputId === ""){
      setMessage('아이디를 입력하세요.');
      setLoginFail(true);
      return;
    }else if (inputPassword === ""){
      setMessage('비밀번호를 입력하세요.');
      setLoginFail(true);
      return;
    }

    const data = {
      id: inputId,
      password: inputPassword,
    };

    api
      .post("/realLogin", data)
      .then((result) => {
        console.log(result.data);
        if (result.data === "loginFail") {
          // navigate(result.data);
          setMessage('아이디 또는 비밀번호를 확인하세요.');
          setLoginFail(true);
        } else {
          setLoginFail(false);
          if (result.data === "admin/main") {
            dispatch(set_CheckGroupManager());
          }
          navigate(result.data);
          resetAlertStatus();
          setLoginFail(false);

          api
            .get("/getLoginInfo")
            .then((result) => {
              console.log(result.data);
              dispatch(set_LoginUserId(result.data.id));
              dispatch(set_LoginUserName(result.data.userName));
              dispatch(set_LoginUserRank(result.data.rankName));
              dispatch(set_LoginUserDept(result.data.deptName));
            })
            .catch((error) => {
              // if(error.response && error.response.status === 401){
              //   alert("로그인세션만료");
              //   navigate("/");
              // }
              console.log("로그인정보 get실패");
            });
        }
      })
      .catch((error) => {
        // if(error.response && error.response.status === 401){
        //   alert("로그인세션만료");
        //   navigate("/");
        // }
        console.log("test실패");
      });
  };
  // const handleUserLogin = () => {
  //   navigate("user/userinfo");
  // };

  // //axios test
  // const testAxios = () => {
  //   axios
  //     .get("http://localhost:8080/test")
  //     .then((result) => {
  //       console.log(result.data);
  //     })
  //     .catch(() => {
  //       console.log("test실패");
  //     });
  // };

  // //login test
  // const loginAxios = (inputId) =>{
  //   axios.get("http://localhost:8080/login",{
  //     params:{
  //       id : inputId,
  //     }
  //   }).then((result)=>{
  //     console.log("암호화성공");

  //   }).catch(()=>{
  //     console.log("암호화실패");
  //   })

  // }
  // const emailAxios = (inputId) =>{
  //   axios.get("http://localhost:8080/email",{
  //     params:{
  //       email : inputId
  //     }
  //   }).then((result) =>{
  //     console.log("양방향 성공")
  //   }).catch(()=>{
  //     console.log("양방향실패")
  //   })
  // }

  useEffect(() => {
    console.log("로그인페이지 진입");
    resetAlertStatus(); //alert창 열었는지 여부 초기화
  }, []);

  const [inputId, setInputId] = useState("");
  const [inputPassword, setInputPassword] = useState("");

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      handleClickLogin();
    }
  };
  return (
    <ThemeProvider theme={defaultTheme}>
      {/* <Loading></Loading> */}
      {/* <img src={Gif} width="100%" height="600px"></img> */}
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh", // 부모 요소의 높이를 100vh로 설정하여 수직 중앙 정렬
        }}
      >
        <Container component="main" maxWidth="xs">
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center", // 수평 중앙 정렬
              alignItems: "center", // 수직 중앙 정렬
              padding: "15px",
            }}
          >
            <img
              src={DouzoneImg}
              width={"200px"}
              height={"50px"}
              style={{ marginBottom: "10px" }}
            />
          </Box>
          <CssBaseline />
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center", // 수평 중앙 정렬
              alignItems: "center", // 수직 중앙 정렬
              border: "2px solid #ccc",
              borderRadius: "8px",
              padding: "20px 30px 20px 30px",
              boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
            }}
          >
            {/* <img
              src={DouzoneImg}
              width={"170px"}
              style={{ marginBottom: "10px" }}
            /> */}
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <LoginOutlinedIcon
                sx={{ color: "primary.main", marginRight: "10px" }}
              />
              <Typography component="h1" variant="h6" sx={{ color: "#696969" }}>
                ID 로그인
              </Typography>
            </Box>
            <Box sx={{ height: "20px" }}>
              {loginFail ? (
                <Typography fontSize={"15px"} sx={{ color: "#ff003e" }}>
                  {message}
                </Typography>
              ) : null}
            </Box>
            <Box onSubmit={handleSubmit} noValidate sx={{ mt: 2 }}>
              <TextField
                margin="normal"
                // required
                fullWidth
                id="id"
                // label="아이디"
                name="id"
                autoFocus
                onChange={(e) => {
                  setInputId(e.target.value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <PersonOutlinedIcon />
                    </InputAdornment>
                  ),
                }}
                placeholder="아이디"
                onKeyDown={handleKeyDown}
              />

              <TextField
                // margin="normal"
                // required
                fullWidth
                name="password"
                // label="비밀번호"
                type="password"
                id="password"
                onChange={(e) => {
                  setInputPassword(e.target.value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LockOutlinedIcon />
                    </InputAdornment>
                  ),
                }}
                placeholder="비밀번호"
                onKeyDown={handleKeyDown}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 1, padding: "10px" }}
                onClick={handleClickLogin}
              >
                <strong>로그인</strong>
              </Button>
              {/* <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                onClick={handleUserLogin}
              >
                사용자로그인
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                onClick={()=>{loginAxios(inputId)}}
              >
                암호화테스트(일방향)
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                onClick={()=>{emailAxios(inputId)}}
              >
                암복호화테스트(양방향)
              </Button> */}
              {/* <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="#" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid> */}
            </Box>
          </Box>
        </Container>
      </Box>
    </ThemeProvider>
  );
}
